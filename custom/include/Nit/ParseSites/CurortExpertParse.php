<?php

class CurortExpertParse
{

    /**
     *
     * @var \RemoteWebDriver
     */
    protected $driver;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var string
     */
    protected $host;
    protected $chromeProcess;
    protected $defaultSeconds = 10;
    protected $smallSeconds = 5;
    protected $featuresToFields = [];
    public $multienumDelimiter = '^,^';

    public function start()
    {
        $GLOBALS['log']->fatal("Parse Sites start datetime: " . date('d.m.Y H:i:s', time()));

        $this->setConfig(SugarConfig::get('CurortExpertParse', array()));

        $this->setHost();

        $driver = $this->getDriver();

        $this->getDescriptionJs = file_get_contents("custom/include/Nit/ParseSites/getDescription.js");

        for ($i = 1; $i <= 378; $i++) {
            $sanatorium = BeanFactory::getBean("Sanatoriums");
            
            try {
                set_time_limit(60);

                $driver->get("http://localhost/html/areal/$i/s.html");

                $this->wait();

                $name = $driver->findElement(WebDriverBy::id('title'));

                $sanatorium->full_name = $name->getText();

                $nameArray = explode(", ", $sanatorium->full_name);

                $sanatorium->name = $nameArray[0];

                $sanatorium->city_name = $nameArray[1];

                $sanatorium->price_from = $driver->findElement(WebDriverBy::className('price_short-value'))->getText();

                $sanatorium = $this->setFeaturesToFields($sanatorium);

                $sanatorium = $this->getDescription($sanatorium);

                $sanatorium->save();

                $GLOBALS['log']->fatal("Sanatorium: $i" . $sanatorium->name);
            } catch (Exception $exception) {
                $GLOBALS['log']->fatal("Sanatorium: $i" . $sanatorium->name . " error: " . $exception->getMessage());
            }
        }

        $GLOBALS['log']->fatal("Parse Sites end datetime: " . date('d.m.Y H:i:s', time()));
    }

    protected function setFeaturesToFields($sanatorium)
    {
        $features = $this->driver->findElements(WebDriverBy::className('feature-wrapper'));

        if (empty($features)) {
            return $sanatorium;
        }

        foreach ($features as $feature) {
            $title = "";
            $items = [];

            try {
                $title = $feature->findElement(WebDriverBy::className('title-inner'))->getText();
                $items = $feature->findElements(WebDriverBy::className('item'));
            } catch (Exception $ex) {
                
            }

            if (empty($title)) {
                continue;
            }

            $fieldName = $this->getFeaturesToFields($title);

            if (empty($fieldName)) {
                continue;
            }

            $fields = [];

            foreach ($items as $item) {
                if (empty($item->getText())) {
                    continue;
                }

                $fields[] = strtr($item->getText(), [',' => '']);
            }

            if (count($items) == 0) {
                continue;
            }

            $sanatorium->{$fieldName} = implode($this->multienumDelimiter, $fields);
        }

        return $sanatorium;
    }

    protected function getDescription($sanatorium)
    {
        $this->driver->executeScript($this->getDescriptionJs);

        $this->waitSmall();

        $description = $this->driver->findElement(WebDriverBy::id('myDescription'));

        $sanatorium->description = $description->getText();

        return $sanatorium;
    }

    protected function prepareTitle($title)
    {
        if (empty($title)) {
            return $title;
        }

        return mb_strtolower(strtr($title, [':' => '']), 'utf-8');
    }

    protected function getFeaturesToFields($feature)
    {
        $this->featuresToFields = [
            'заезд' => 'arrival',
            'подходит' => 'suitable',
            'питание' => 'foodoption',
            'в номерах' => 'roomsinfostructure',
            'интернет' => 'internet',
            'рекомендуемый заезд' => 'arrival_days',
            'готовые программы' => 'healthprogram',
            'услуги для здоровья' => 'beauty',
            'прокат' => 'rent',
            'своя территория' => 'territory',
            'активные занятия' => 'sport'
        ];

        $title = $this->prepareTitle($feature);

        if (empty($this->featuresToFields[$title])) {
            return;
        }

        return $this->featuresToFields[$title];
    }

    protected function wait()
    {
        sleep($this->defaultSeconds);
    }

    protected function waitSmall()
    {
        sleep($this->smallSeconds);
    }

    protected function getDriver()
    {
        if ($this->driver) {
            return $this->driver;
        }

        $host = $this->config['host'];
        $capabilities = DesiredCapabilities::chrome();
        $this->driver = RemoteWebDriver::create($host, $capabilities, 5000);

        return $this->driver;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    protected function setHost()
    {
        $this->host = $this->config['siteHost'];
    }

    protected function getUrl($url)
    {
        return $this->host . '/' . $url;
    }

}
