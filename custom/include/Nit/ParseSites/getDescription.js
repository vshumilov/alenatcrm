var $description = document.getElementsByClassName('description-content')[0];

var $clientReviews = document.getElementsByClassName("client_reviews__section");

if ($clientReviews && $clientReviews[0]) {
    $clientReviews[0].remove();
}

var description = $description.innerHTML;

var div = document.createElement('div');
  div.id = "myDescription";
  div.innerText = description;

$description.appendChild(div);
