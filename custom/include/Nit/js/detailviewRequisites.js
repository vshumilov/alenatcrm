var $doc = $(document);

$doc.ready(function () {
    var $beanType = $('#bean_type');
    var $inn = $('[field="inn"]');
    var $kpp = $('[field="kpp"]');
    var $ogrn = $('[field="ogrn"]');
    var $dateOgrn = $('[field="date_ogrn"]');

    var $kppRow = $kpp.closest('.detail-view-row-item');
    var $ogrnRow = $ogrn.closest('.detail-view-row-item');
    var $dateOgrnRow = $dateOgrn.closest('.detail-view-row-item');

    var $labelOgrn = $ogrnRow.find('.label');
    var $labelDateOgrn = $dateOgrnRow.find('.label');

    if ($beanType.val() == 'ИП') {
        $kppRow.hide();
        $labelOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_OGRN_IP"));
        $labelDateOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_DATE_OGRN_IP"));
    } else {
        $kppRow.show();
        $labelOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_OGRN"));
        $labelDateOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_DATE_OGRN"));
    }
});

