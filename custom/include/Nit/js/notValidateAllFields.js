$(document).ready(function () {
    var $notValidate = $('#not_validate');
    var $panelContent = $('.panel-content');

    $notValidate.on('change', function () {
        if ($(this).is(':checked')) {
            notValidateAllFields();
        } else {
            validateAllFields();
        }
    });

    function notValidateAllFields() {
        var fields = $panelContent.find("[id][name]");

        var $field = null;

        for (var i = 0; i < fields.length; i++) {
            $field = $(fields[i]);

            removeFromValidateField($field);
        }
    }

    function validateAllFields() {
        var fields = $panelContent.find("[id][name]");

        var $field = null;

        for (var i = 0; i < fields.length; i++) {
            $field = $(fields[i]);

            addToValidateField($field);
        }
    }

    function removeFromValidateField($field) {
        var fieldName = $field.attr('name');
        
        var $errorSpan = findRequiredSpan(fieldName);
        var $validationMessage = findValidationMessage(fieldName);

        removeFromValidate('EditView', fieldName);

        $errorSpan.hide();
        $validationMessage.hide();
    }

    function addToValidateField($field) {
        var fieldName = $field.attr('name');
        
        var $errorSpan = findRequiredSpan(fieldName);
        var $validationMessage = findValidationMessage(fieldName);
        
        if (!$errorSpan.hasClass('required')) {
            return;
        }

        addToValidate('EditView', fieldName, 'text', true, SUGAR.language.get('app_strings', "ERR_MISSING_REQUIRED_FIELDS"));

        $errorSpan.show();
        $validationMessage.show();
    }
    
    function findRequiredSpan(fieldName) {
        var $row = $("[name='" + fieldName + "']").closest('.edit-view-row-item');
        var $errorSpan = $row.find('span.required');
        
        return $errorSpan;
    }
    
    function findValidationMessage(fieldName) {
        var $row = $("[name='" + fieldName + "']").closest('.edit-view-row-item');
        var $validationMessage = $row.find('.validation-message');
        
        return $validationMessage;
    }
});



