function customCheckMaxLength(element)
{
    var maxLength = element.getAttribute('maxlength');
    var currentLength = element.value.length;

    if (currentLength > maxLength) {
        return false;
    }

    return true;
}

function customCheckEqualLength(element)
{
    var maxLength = element.getAttribute('maxlength');
    var currentLength = element.value.length;

    if (currentLength != maxLength) {
        return false;
    }

    return true;
}

function validate_form(formname, startsWith) {

    requiredTxt = SUGAR.language.get('app_strings', 'ERR_MISSING_REQUIRED_FIELDS');
    invalidTxt = SUGAR.language.get('app_strings', 'ERR_INVALID_VALUE');

    if (typeof (formname) == 'undefined')
    {
        return false;
    }
    if (typeof (validate[formname]) == 'undefined')
    {
        disableOnUnloadEditView(document.forms[formname]);
        return true;
    }

    var form = document.forms[formname];
    var isError = false;
    var errorMsg = "";
    var _date = new Date();
    if (_date.getTime() < (lastSubmitTime + 2000) && startsWith == oldStartsWith) { // ignore submits for the next 2 seconds
        return false;
    }
    lastSubmitTime = _date.getTime();
    oldStartsWith = startsWith;

    clear_all_errors(); // remove previous error messages

    inputsWithErrors = new Array();
    for (var i = 0; i < validate[formname].length; i++) {
        if (validate[formname][i][nameIndex].indexOf(startsWith) == 0) {
            if (typeof form[validate[formname][i][nameIndex]] != 'undefined' && typeof form[validate[formname][i][nameIndex]].value != 'undefined') {
                var bail = false;


                //If a field is not required and it is blank or is binarydependant, skip validation.
                //Example of binary dependant fields would be the hour/min/meridian dropdowns in a date time combo widget, which require further processing than a blank check
                if (!validate[formname][i][requiredIndex] && trim(form[validate[formname][i][nameIndex]].value) == '' && (typeof (validate[formname][i][jstypeIndex]) != 'undefined' && validate[formname][i][jstypeIndex] != 'binarydep'))
                {
                    continue;
                }

                if (validate[formname][i][requiredIndex]
                        && !isFieldTypeExceptFromEmptyCheck(validate[formname][i][typeIndex])
                        ) {
                    if (typeof form[validate[formname][i][nameIndex]] == 'undefined' || trim(form[validate[formname][i][nameIndex]].value) == "") {
                        add_error_style(formname, validate[formname][i][nameIndex], requiredTxt + ' ' + validate[formname][i][msgIndex]);
                        isError = true;
                    }
                }

                var allow = true;

                if (validate[formname][i][requiredIndex] && !trim(form[validate[formname][i][nameIndex]].value)) {
                    allow = false;
                }

                if (!bail) {
                    switch (validate[formname][i][typeIndex]) {
                        case 'basemax':
                            if (allow) {
                                if (form[validate[formname][i][nameIndex]].getAttribute('data-is_equal_mode') && !customCheckEqualLength(form[validate[formname][i][nameIndex]])) {
                                    isError = true;
                                    add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + ' ' + SUGAR.language.get('app_strings', 'LBL_ERROR_EQUAL_LENGTH') + form[validate[formname][i][nameIndex]].getAttribute('maxlength'));
                                } else if (!customCheckMaxLength(form[validate[formname][i][nameIndex]])) {
                                    isError = true;
                                    add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + ' ' + SUGAR.language.get('app_strings', 'LBL_ERROR_LENGTH') + form[validate[formname][i][nameIndex]].getAttribute('maxlength'));
                                }
                            }

                            break;
                        case 'integer':
                            if (allow) {
                                if (form[validate[formname][i][nameIndex]].getAttribute('data-is_equal_mode') && !customCheckEqualLength(form[validate[formname][i][nameIndex]])) {
                                    isError = true;
                                    add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + ' ' + SUGAR.language.get('app_strings', 'LBL_ERROR_EQUAL_LENGTH') + form[validate[formname][i][nameIndex]].getAttribute('maxlength'));
                                } else if (!customCheckMaxLength(form[validate[formname][i][nameIndex]])) {
                                    isError = true;
                                    add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + ' ' + SUGAR.language.get('app_strings', 'LBL_ERROR_LENGTH') + form[validate[formname][i][nameIndex]].getAttribute('maxlength'));
                                } else if (!isInteger(trim(form[validate[formname][i][nameIndex]].value))) {
                                    isError = true;
                                    add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                                }
                            }
                            break;
                        case 'email':
                        case 'nitemail':    
                            if (!isValidEmail(trim(form[validate[formname][i][nameIndex]].value))) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'time':
                            if (!isTime(trim(form[validate[formname][i][nameIndex]].value))) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'date':
                            if (!isDate(trim(form[validate[formname][i][nameIndex]].value))) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'alpha':
                            break;
                        case 'DBName':
                            if (!isDBName(trim(form[validate[formname][i][nameIndex]].value))) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                            // Bug #49614 : Check value without trimming before
                        case 'DBNameRaw':
                            if (!isDBName(form[validate[formname][i][nameIndex]].value)) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'alphanumeric':
                            break;
                        case 'file':
                            var file_input = form[validate[formname][i][nameIndex] + '_file'];
                            if (file_input && validate[formname][i][requiredIndex] && trim(file_input.value) == "" && !file_input.disabled) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], requiredTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'int':
                            if (!isInteger(trim(form[validate[formname][i][nameIndex]].value))) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'decimal':
                            if (!isDecimal(trim(form[validate[formname][i][nameIndex]].value))) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'currency':
                        case 'float':
                            if (!isFloat(trim(form[validate[formname][i][nameIndex]].value))) {
                                isError = true;
                                add_error_style(formname, validate[formname][i][nameIndex], invalidTxt + " " + validate[formname][i][msgIndex]);
                            }
                            break;
                        case 'teamset_mass':
                            var div_element_id = formname + '_' + form[validate[formname][i][nameIndex]].name + '_operation_div';
                            var input_elements = YAHOO.util.Selector.query('input', document.getElementById(div_element_id));
                            var primary_field_id = '';
                            var validation_passed = false;
                            var replace_selected = false;

                            //Loop through the option elements (replace or add currently)
                            for (t in input_elements) {
                                if (input_elements[t].type && input_elements[t].type == 'radio' && input_elements[t].checked == true && input_elements[t].value == 'replace') {

                                    //Now find where the primary radio button is and if a value has been set
                                    var radio_elements = YAHOO.util.Selector.query('input[type=radio]', document.getElementById(formname + '_team_name_table'));

                                    for (var x = 0; x < radio_elements.length; x++) {
                                        if (radio_elements[x].name != 'team_name_type') {
                                            primary_field_id = 'team_name_collection_' + radio_elements[x].value;
                                            if (radio_elements[x].checked) {
                                                replace_selected = true;
                                                if (trim(document.forms[formname].elements[primary_field_id].value) != '') {
                                                    validation_passed = true;
                                                    break;
                                                }
                                            } else if (trim(document.forms[formname].elements[primary_field_id].value) != '') {
                                                replace_selected = true;
                                            }
                                        }
                                    }
                                }
                            }

                            if (replace_selected && !validation_passed) {
                                add_error_style(formname, primary_field_id, SUGAR.language.get('app_strings', 'ERR_NO_PRIMARY_TEAM_SPECIFIED'));
                                isError = true;
                            }
                            break;
                        case 'teamset':
                            var table_element_id = formname + '_' + form[validate[formname][i][nameIndex]].name + '_table';
                            if (document.getElementById(table_element_id)) {
                                var input_elements = YAHOO.util.Selector.query('input[type=radio]', document.getElementById(table_element_id));
                                var has_primary = false;
                                var primary_field_id = form[validate[formname][i][nameIndex]].name + '_collection_0';

                                for (t in input_elements) {
                                    primary_field_id = form[validate[formname][i][nameIndex]].name + '_collection_' + input_elements[t].value;
                                    if (input_elements[t].type && input_elements[t].type == 'radio' && input_elements[t].checked == true) {
                                        if (document.forms[formname].elements[primary_field_id].value != '') {
                                            has_primary = true;
                                        }
                                        break;
                                    }
                                }

                                if (!has_primary) {
                                    isError = true;
                                    var field_id = form[validate[formname][i][nameIndex]].name + '_collection_' + input_elements[0].value;
                                    add_error_style(formname, field_id, SUGAR.language.get('app_strings', 'ERR_NO_PRIMARY_TEAM_SPECIFIED'));
                                }
                            }
                            break;
                        case 'error':
                            isError = true;
                            add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex]);
                            break;
                    }

                    if (typeof validate[formname][i][jstypeIndex] != 'undefined'/* && !isError*/) {

                        switch (validate[formname][i][jstypeIndex]) {
                            // Bug #47961 May be validation through callback is best way.
                            case 'callback' :
                                if (typeof validate[formname][i][callbackIndex] == 'function')
                                {
                                    var result = validate[formname][i][callbackIndex](formname, validate[formname][i][nameIndex]);
                                    if (result == false)
                                    {
                                        isError = true;
                                        add_error_style(formname, validate[formname][i][nameIndex], requiredTxt + " " + validate[formname][i][msgIndex]);
                                    }
                                }
                                break;
                            case 'range':
                                if (!inRange(trim(form[validate[formname][i][nameIndex]].value), validate[formname][i][minIndex], validate[formname][i][maxIndex])) {
                                    isError = true;
                                    var lbl_validate_range = SUGAR.language.get('app_strings', 'LBL_VALIDATE_RANGE');
                                    if (typeof validate[formname][i][minIndex] == 'number' && typeof validate[formname][i][maxIndex] == 'number')
                                    {
                                        add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + " value " + form[validate[formname][i][nameIndex]].value + " " + lbl_validate_range + " (" + validate[formname][i][minIndex] + " - " + validate[formname][i][maxIndex] + ")");
                                    } else if (typeof validate[formname][i][minIndex] == 'number')
                                    {
                                        add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + " " + SUGAR.language.get('app_strings', 'MSG_SHOULD_BE') + ' ' + validate[formname][i][minIndex] + ' ' + SUGAR.language.get('app_strings', 'MSG_OR_GREATER'));
                                    } else if (typeof validate[formname][i][maxIndex] == 'number')
                                    {
                                        add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + " " + SUGAR.language.get('app_strings', 'MSG_IS_MORE_THAN') + ' ' + validate[formname][i][maxIndex]);
                                    }
                                }
                                break;
                            case 'isbefore':
                                compareTo = form[validate[formname][i][compareToIndex]];
                                if (typeof compareTo != 'undefined') {
                                    if (trim(compareTo.value) != '' || (validate[formname][i][allowblank] != 'true')) {
                                        date2 = trim(compareTo.value);
                                        date1 = trim(form[validate[formname][i][nameIndex]].value);

                                        if (trim(date1).length != 0 && !isBefore(date1, date2)) {

                                            isError = true;
                                            //jc:#12287 - adding translation for the is not before message
                                            add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + "(" + date1 + ") " + SUGAR.language.get('app_strings', 'MSG_IS_NOT_BEFORE') + ' ' + date2);
                                        }
                                    }
                                }
                                break;
                            case 'less':
                                value = unformatNumber(trim(form[validate[formname][i][nameIndex]].value), num_grp_sep, dec_sep);
                                maximum = parseFloat(validate[formname][i][maxIndex]);
                                if (typeof maximum != 'undefined') {
                                    if (value > maximum) {
                                        isError = true;
                                        add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + " " + SUGAR.language.get('app_strings', 'MSG_IS_MORE_THAN') + ' ' + validate[formname][i][altMsgIndex]);
                                    }
                                }
                                break;
                            case 'more':
                                value = unformatNumber(trim(form[validate[formname][i][nameIndex]].value), num_grp_sep, dec_sep);
                                minimum = parseFloat(validate[formname][i][minIndex]);
                                if (typeof minimum != 'undefined') {
                                    if (value < minimum) {
                                        isError = true;
                                        add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex] + " " + SUGAR.language.get('app_strings', 'MSG_SHOULD_BE') + ' ' + minimum + ' ' + SUGAR.language.get('app_strings', 'MSG_OR_GREATER'));
                                    }
                                }
                                break;
                            case 'binarydep':
                                compareTo = form[validate[formname][i][compareToIndex]];
                                if (typeof compareTo != 'undefined') {
                                    item1 = trim(form[validate[formname][i][nameIndex]].value);
                                    item2 = trim(compareTo.value);
                                    if (!bothExist(item1, item2)) {
                                        isError = true;
                                        add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex]);
                                    }
                                }
                                break;
                            case 'comparison':
                                compareTo = form[validate[formname][i][compareToIndex]];
                                if (typeof compareTo != 'undefined') {
                                    item1 = trim(form[validate[formname][i][nameIndex]].value);
                                    item2 = trim(compareTo.value);
                                    if (!bothExist(item1, item2) || item1 != item2) {
                                        isError = true;
                                        add_error_style(formname, validate[formname][i][nameIndex], validate[formname][i][msgIndex]);
                                    }
                                }
                                break;
                            case 'in_array':
                                operator = validate[formname][i][operatorIndex];
                                item1 = trim(form[validate[formname][i][nameIndex]].value);
                                if (operator.charAt(0) == 'u') {
                                    item1 = item1.toUpperCase();
                                    operator = operator.substring(1);
                                } else if (operator.charAt(0) == 'l') {
                                    item1 = item1.toLowerCase();
                                    operator = operator.substring(1);
                                }
                                // Get all fields related to the selected module (including custom fields)
                                var current_fields = '';
                                var current_module = document.getElementsByName("view_module")[0].value;
                                $.ajax({
                                    type: "GET",
                                    url: "index.php?to_pdf=1&module=ModuleBuilder&action=getModuleFields&current_module=" + current_module,
                                    async: false,
                                    success: function (result) {
                                        current_fields = JSON.parse(result);
                                    },
                                    error: function (xhr, status, error) {
                                        var err = eval("(" + xhr.responseText + ")");
                                    }
                                });

                                for (k = 0; k < current_fields.length; k++) {
                                    if (isError != true) {
                                        val = current_fields[k].toUpperCase();
                                        if ((operator == "==" && val == item1) || (operator == "!=" && val != item1)) {
                                            isError = true;
                                            add_error_style(formname, validate[formname][i][nameIndex], 'Invalid Value: Field Name already exists');
                                        }
                                    }
                                }
                                break;
                            case 'verified':
                                if (trim(form[validate[formname][i][nameIndex]].value) == 'false') {
                                    //Fake an error so form does not submit
                                    isError = true;
                                }
                                break;
                        }
                    }
                }
            }
        }
    }
    /*	nsingh: BUG#15102
     Check min max default field logic.
     Can work with float values as well, but as of 10/8/07 decimal values in MB and studio don't have min and max value constraints.*/
    if (formsWithFieldLogic) {
        var invalidLogic = false;
        if (formsWithFieldLogic.min && formsWithFieldLogic.max && formsWithFieldLogic._default) {
            var showErrorsOn = {min: {value: 'min', show: false, obj: formsWithFieldLogic.min.value},
                max: {value: 'max', show: false, obj: formsWithFieldLogic.max.value},
                _default: {value: 'default', show: false, obj: formsWithFieldLogic._default.value},
                len: {value: 'len', show: false, obj: parseInt(formsWithFieldLogic.len.value, 10)}};

            var min = (formsWithFieldLogic.min.value != '') ? parseFloat(formsWithFieldLogic.min.value) : 'undef';
            var max = (formsWithFieldLogic.max.value != '') ? parseFloat(formsWithFieldLogic.max.value) : 'undef';
            var _default = (formsWithFieldLogic._default.value != '') ? parseFloat(formsWithFieldLogic._default.value) : 'undef';

            /*Check all lengths are <= max size.*/
            for (var i in showErrorsOn) {
                if (showErrorsOn[i].value != 'len' && showErrorsOn[i].obj.length > showErrorsOn.len.obj) {
                    invalidLogic = true;
                    showErrorsOn[i].show = true;
                    showErrorsOn.len.show = true;
                }
            }

            if (min != 'undef' && max != 'undef' && _default != 'undef') {
                if (!inRange(_default, min, max)) {
                    invalidLogic = true;
                    showErrorsOn.min.show = true;
                    showErrorsOn.max.show = true;
                    showErrorsOn._default.show = true;
                }
            }
            if (min != 'undef' && max != 'undef' && min > max) {
                invalidLogic = true;
                showErrorsOn.min.show = true;
                showErrorsOn.max.show = true;
            }
            if (min != 'undef' && _default != 'undef' && _default < min) {

                invalidLogic = true;
                showErrorsOn.min.show = true;
                showErrorsOn._default.show = true;
            }
            if (max != 'undef' && _default != 'undef' && _default > max) {

                invalidLogic = true;
                showErrorsOn.max.show = true;
                showErrorsOn._default.show = true;
            }

            if (invalidLogic) {
                isError = true;
                for (var error in showErrorsOn)
                    if (showErrorsOn[error].show)
                        add_error_style(formname, showErrorsOn[error].value, formsWithFieldLogic.msg);

            } else if (!isError)
                formsWithFieldLogic = null;
        }
    }
    if (formWithPrecision) {
        if (!isValidPrecision(formWithPrecision.float.value, formWithPrecision.precision.value)) {
            isError = true;
            add_error_style(formname, 'default', SUGAR.language.get('app_strings', 'ERR_COMPATIBLE_PRECISION_VALUE'));
        } else if (!isError) {
            isError = false;
        }
    }

//END BUG# 15102

    if (isError == true) {
        var nw, ne, sw, se;
        if (self.pageYOffset) // all except Explorer
        {
            nwX = self.pageXOffset;
            seX = self.innerWidth;
            nwY = self.pageYOffset;
            seY = self.innerHeight;
        } else if (document.documentElement && document.documentElement.scrollTop) // Explorer 6 Strict
        {
            nwX = document.documentElement.scrollLeft;
            seX = document.documentElement.clientWidth;
            nwY = document.documentElement.scrollTop;
            seY = document.documentElement.clientHeight;
        } else if (document.body) // all other Explorers
        {
            nwX = document.body.scrollLeft;
            seX = document.body.clientWidth;
            nwY = document.body.scrollTop;
            seY = document.body.clientHeight;
        }

        var inView = true; // is there an error within viewport of browser
        for (var wp = 0; wp < inputsWithErrors.length; wp++) {
            var elementCoor = findElementPos(inputsWithErrors[wp]);
            if (!(elementCoor.x >= nwX && elementCoor.y >= nwY &&
                    elementCoor.x <= seX + nwX && elementCoor.y <= seY + nwY)) { // if input is not within viewport, modify for SI bug 52497
                inView = false;
                scrollToTop = elementCoor.y - 75;
                scrollToLeft = elementCoor.x - 75;
            } else { // on first input within viewport, don't scroll
                break;
            }
        }


        if (!inView)
            window.scrollTo(scrollToLeft, scrollToTop);

        return false;
    }

    disableOnUnloadEditView(form);
    return true;

}

function set_return_basic(popup_reply_data, filter)
{
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array)
    {
        if (the_key == 'toJSON')
        {
        }
        else if (the_key.match(filter))
        {
            var displayValue = replaceHTMLChars(name_to_value_array[the_key]);
            if (window.document.forms[form_name] && window.document.forms[form_name].elements[the_key]) {
                if (window.document.forms[form_name].elements[the_key].tagName == 'SELECT') {
                    var selectField = window.document.forms[form_name].elements[the_key];
                    for (var i = 0; i < selectField.options.length; i++) {
                        if (selectField.options[i].text == displayValue) {
                            selectField.options[i].selected = true;
                            $(selectField).trigger('change');
                            SUGAR.util.callOnChangeListers(selectField);
                            break;
                        }
                    }
                } else {
                    window.document.forms[form_name].elements[the_key].value = displayValue;
                    $(window.document.forms[form_name].elements[the_key]).trigger('change');
                    SUGAR.util.callOnChangeListers(window.document.forms[form_name].elements[the_key]);
                }
            }
        }
    }
}

function bindPrefillForBean(module, beanName) {
    if (!module || !beanName) {
        return;
    }

    var $beanId = $('#' + beanName.toLowerCase() + '_id');
    var $beanBtnClr = $('#btn_clr_' + beanName.toLowerCase() + '_name');

    $beanId.on('change', function () {
        fillBeanData(module, $beanId.val(), 'get' + beanName);
    });

    $beanBtnClr.on('click', function () {
        clearBeanFields(module);
    });

    subscribeOnSelectInAutocomplete(module, beanName);
    subscribeOnTextboxBlurAutocomplete(beanName);
}

function clearBeanFields(module) {
    $('input[id^="' + module.toLowerCase() + '_"]').val('');
}

function subscribeOnSelectInAutocomplete(module, beanName) {
    var contactInterval = setInterval(function () {
        var key = 'EditView_' + beanName.toLowerCase() + '_name';

        if (QSFieldsArray != undefined &&
                QSFieldsArray[key] != undefined &&
                QSFieldsArray[key].itemSelectEvent != undefined) {

            clearInterval(contactInterval);
            QSFieldsArray[key].itemSelectEvent.subscribe(function (e, args) {
                var record = args[2][1];

                if (module == 'Contacts') {
                    record = args[2][3];
                }

                fillBeanData(module, record, 'get' + beanName);
            });
        }
    }, 100);
}

function subscribeOnTextboxBlurAutocomplete(beanName) {
    var fieldName = beanName.toLowerCase() + '_name';
    var $beanName = $('#' + fieldName);
    
    var contactInterval = setInterval(function () {
        var key = 'EditView_' + beanName.toLowerCase() + '_name';

        if (QSFieldsArray != undefined &&
                QSFieldsArray[key] != undefined &&
                QSFieldsArray[key].textboxBlurEvent != undefined) {

            clearInterval(contactInterval);

            QSFieldsArray[key].textboxBlurEvent.subscribe(function (e, args) {
                
                $beanName.val(args[0]._savedBeanNameValueAfterClear);

                if (args[0]._savedBeanNameValueAfterClear != "") {
                    removeFromValidate('EditView', fieldName);
                } else {
                    addToValidate('EditView', fieldName, 'text', true, SUGAR.language.get('app_strings', "ERR_MISSING_REQUIRED_FIELDS"));
                }
            });
        }
    }, 100);

    $(document).ready(function () {
        if ($beanName.val() != "") {
            removeFromValidate('EditView', fieldName);
        } else {
            addToValidate('EditView', fieldName, 'text', true, SUGAR.language.get('app_strings', "ERR_MISSING_REQUIRED_FIELDS"));
        }
    });
}

YAHOO.widget.AutoComplete.prototype._onTextboxBlur = function (v, oSelf) {
    // Is a true blur
    oSelf._savedBeanNameValueAfterClear = v.target.value;

    if (!oSelf._bOverContainer || (oSelf._nKeyCode == 9)) {
        // Current query needs to be validated as a selection
        if (!oSelf._bItemSelected) {
            var elMatchListItem = oSelf._textMatchesOption();
            // Container is closed or current query doesn't match any result
            if (!oSelf._bContainerOpen || (oSelf._bContainerOpen && (elMatchListItem === null))) {
                // Force selection is enabled so clear the current query
                if (oSelf.forceSelection) {
                    oSelf._clearSelection();
                }
                // Treat current query as a valid selection
                else {
                    oSelf.unmatchedItemSelectEvent.fire(oSelf, oSelf._sCurQuery);
                }
            }
            // Container is open and current query matches a result
            else {
                // Force a selection when textbox is blurred with a match
                if (oSelf.forceSelection) {
                    oSelf._selectItem(elMatchListItem);
                }
            }
        }

        oSelf._clearInterval();
        oSelf._bFocused = false;
        if (oSelf._sInitInputValue !== oSelf._elTextbox.value) {
            oSelf.textboxChangeEvent.fire(oSelf);
        }
        oSelf.textboxBlurEvent.fire(oSelf);

        oSelf._toggleContainer(false);
    }
    // Not a true blur if it was a selection via mouse click
    else {
        oSelf._focus();
    }
};

function fillBeanData(module, record, method) {
    if (!module || !record || !method) {
        return;
    }

    LightAjax({
        module: module,
        record: record,
        call: method,
        params: {}
    }, function (data) {
        if (!data) {
            return
        }

        $.each(data, function (field, value) {
            var $field = $('#' + module.toLowerCase() + '_' + field);

            if ($field.attr('type') == 'checkbox') {
                if (value == '1') {
                    $field.attr('checked', 'true');
                } else {
                    $field.removeAttr('checked');
                }
            } else {
                $field.val(value);
            }
        })
    });
}

