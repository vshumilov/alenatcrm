var $doc = $(document);

$doc.ready(function () {
    var $beanType = $('#bean_type');
    var $inn = $('#inn');
    var $kpp = $('#kpp');
    var $ogrn = $('#ogrn');
    var $dateOgrn = $('#date_ogrn');

    var $kppRow = $kpp.closest('.edit-view-row-item');
    var $ogrnRow = $ogrn.closest('.edit-view-row-item');
    var $dateOgrnRow = $dateOgrn.closest('.edit-view-row-item');

    var $labelOgrn = $ogrnRow.find('.label');
    var $labelDateOgrn = $dateOgrnRow.find('.label');

    function changeBeanType(notFlashKpp)
    {
        if ($beanType.val() == 'ИП') {
            $inn.attr('maxlength', '12');
            $ogrn.attr('maxlength', '15');
            $kppRow.hide();
            $kppRow.next().hide();
            removeFromValidate('EditView', 'kpp');
            $labelOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_OGRN_IP"));
            $labelDateOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_DATE_OGRN_IP"));
        } else {
            $inn.attr('maxlength', '10');
            $ogrn.attr('maxlength', '13');
            $kppRow.show();
            $kppRow.next().show();
            
            if (notFlashKpp !== true) {
                $kpp.val('');
            }

            addToValidate('EditView', 'kpp', 'text', true, SUGAR.language.get('app_strings', "ERR_MISSING_REQUIRED_FIELDS"));
            $labelOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_OGRN"));
            $labelDateOgrn.html(SUGAR.language.get(module_sugar_grp1, "LBL_DATE_OGRN"));
        }
    }

    $beanType.on('change', changeBeanType);

    changeBeanType(true);
});

