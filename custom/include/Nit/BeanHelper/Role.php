<?php

require_once 'custom/include/Nit/BeanHelper.php';
require_once 'modules/SecurityGroups/SecurityGroup.php';

class BeanHelperRole extends BeanHelper
{

    public static function isHasRoleWithName($roles, $roleName)
    {
        if (empty($roles)) {
            global $current_user;
            $current_user->load_relationship('aclroles');
            $roles = $current_user->aclroles->getBeans();
        }

        if (empty($roles) || empty($roleName)) {
            return false;
        }

        foreach ($roles as $role) {
            if (strpos(mb_strtolower($role->name), mb_strtolower($roleName)) !== false) {
                return true;
            }
        }

        return false;
    }

    public static function getQueryAllUsersIdsByRoles($roleNames)
    {
        if (empty($roleNames)) {
            return;
        }

        $sql = "
        SELECT
            u.id
        FROM
            users AS u
        JOIN
            acl_roles_users AS aru
        ON
            u.id = aru.user_id AND
            aru.deleted = '0'
        JOIN
            acl_roles AS r
        ON
            aru.role_id = r.id AND
            r.deleted = '0'
        WHERE
            u.deleted = '0' AND
            r.name IN (<roleNames>)
        GROUP BY
            u.id
        ";

        $query = strtr($sql, ['<roleNames>' => "'" . implode("', '", $roleNames) . "'"]);

        return $query;
    }

    public function getFioWithInisials($fio)
    {
        if (empty($fio)) {
            return;
        }

        list($sername, $firstName, $secondName) = explode(' ', $fio);

        $fioWithInisialsData = [];
        $fioWithInisialsData[] = $sername;
        $fioWithInisialsData[] = mb_strtoupper(mb_substr($firstName, 0, 1, 'utf-8')) . '.';
        $fioWithInisialsData[] = mb_strtoupper(mb_substr($secondName, 0, 1, 'utf-8')) . '.';

        $fioWithInisials = implode(' ', $fioWithInisialsData);

        return $fioWithInisials;
    }

    public function getCurrentSecurityGroup()
    {
        $currentSecurityGroupId = $this->getCurrentSecurityGroupId();

        if (empty($currentSecurityGroupId)) {
            return;
        }

        $securityGroup = BeanFactory::getBean('SecurityGroups', $currentSecurityGroupId);

        return $securityGroup;
    }
    
    protected function getCurrentSecurityGroupId()
    {
        $sql = "
        SELECT
            id
        FROM
            securitygroups
        WHERE
            deleted = '0'
        LIMIT
            1
        ";
        
        return dbGetScalar($sql);
    }

}
