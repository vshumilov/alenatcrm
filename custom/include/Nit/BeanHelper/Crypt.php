<?php

class CryptHelper extends BeanHelper 
{
    public function encrypt($data) {
        if (empty($data)) {
            return $data;
        }
        
        $maskedStr = $this->getRandomStr() . json_encode($data) . $this->getRandomStr();
        
        $encryptedStr = $this->bean->encrpyt_before_save($maskedStr);
        
        return $encryptedStr;
    }
    
    public function decrypt($fieldName) {
        if (empty($this->bean->$fieldName)) {
            return $this->bean->$fieldName;
        }
        
        $decryptedStr = $this->bean->decrypt_after_retrieve($this->bean->$fieldName);
        
        $startPos = mb_strpos($decryptedStr, "{");
        $endPos = mb_strpos($decryptedStr, "}");
        $length = $endPos - $startPos + 1;
        
        $decryptedData = json_decode(mb_substr($decryptedStr, $startPos, $length));
        
        return $decryptedData;
    }

    private function getRandomStr()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstr = '';
        $keysCount = $this->getRandomKeysCount();
        $charactersCount = strlen($characters) - 1;
        
        for ($i = 0; $i < $keysCount; $i++) {
            $randstr .= $characters[rand(0, $charactersCount)];
        }
        return $randstr;
    }
    
    private function getRandomKeysCount() {
        $maxKeysCount = 100;
        $keysCount = rand(0, $maxKeysCount);
        
        return $keysCount;
    }
}

