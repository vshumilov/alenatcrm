<?php

require_once 'custom/include/Nit/BeanHelper/Email.php';
require_once 'modules/Smslogs/Smslog.php';

/* example config.php for sms
  'sms_login' => 'alenat',
  'sms_password' => 'alenat210567s',
  'sms_domain' => 'http://smsc.ru/',
  'sms_cost' => '3',
  'sms_sender' => '&sender=ООО Аленат',
  'sms_translit' => '',
  'sms_check_balance_request' => '<sms_domain>sys/balance.php?fmt=3&login=<sms_login>&charset=utf-8&psw=<sms_password>',
  'sms_send_request' => '<sms_domain>sys/send.php?fmt=3&login=<sms_login>&charset=utf-8&psw=<sms_password>&phones=<sms_phones>&mes=<sms_message>&cost=<sms_cost><sms_sender><sms_translit>',
  'sms_cost_request' => '<sms_domain>sys/send.php?fmt=3&login=<sms_login>&charset=utf-8&psw=<sms_password>&phones=<sms_phones>&mes=<sms_message>&cost=1',
 */

class BeanHelperSMS extends BeanHelperEmail
{

    protected $sms_login;  // логин клиента
    protected $sms_password; // пароль или MD5-хеш пароля в нижнем регистре
    protected $sms_domain; //домен sms центра
    protected $sms_cost; //стоимость sms
    protected $sms_sender = ''; //название отправителя sms
    protected $sms_translit = false; //делать или нет транслит текста в латиницу
    protected $sms_check_balance_request; //шаблон запроса на проверку баланса в sms центре
    protected $sms_send_request; //шаблон запроса на отправку sms
    protected $sms_cost_request; //шаблон запроса стоимости sms
    public $sms_phones; //массив телефонов, куда отправлять sms сообщение
    public $sms_message; // текст sms сообщения

    public function __construct($bean)
    {
        parent::__construct($bean);

        $this->fillParamsFromConfig();
    }

    protected function fillParamsFromConfig()
    {
        $sugarConfig = SugarConfig::getInstance();

        $params = get_object_vars($this);

        foreach ($params as $paramName => $value) {
            if (!$this->isSmsParam($paramName)) {
                continue;
            }

            $this->{$paramName} = $sugarConfig->get($paramName);

            if (empty($this->{$paramName})) {
                $GLOBALS['log']->fatal("no $paramName in config.php");
            }
        }
    }

    /**
     * Проверка баланса на счете
     * @return JSON 
     */
    protected function checkSmsBalance()
    {

        $url = $this->prepareSmsRequest($this->sms_check_balance_request);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * Отправка смс на сервер центра !!!
     * 
     * @param $msg - строка
     * @param $phones - строка через запятую
     */
    protected function sendSms($phones, $message, $toLog = false)
    {
        if (empty($message)) {
            $GLOBALS['log']->fatal("no message when sendSms");
            return;
        }

        if (empty($phones)) {
            $GLOBALS['log']->fatal("no phones when sendSms");
            return;
        }

        $this->sms_message = $message;
        $this->sms_phones = $phones;

        $url = $this->prepareSmsRequest($this->sms_send_request);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);

        if (!$this->hasSmsErrors($result) && $toLog) {
            $this->logSms($phones, $message);
        }

        return $result;
    }

    public function hasSmsErrors($result)
    {
        if (isset(json_decode($result, true)['error'])) {
            return true;
        }
        
        return false;
    }

    protected function logSms($phones, $message)
    {
        if (empty($phones) || empty($message)) {
            return;
        }

        foreach ($phones as $phone) {
            $smslog = new Smslog();
            $smslog->phone = $phone;
            $smslog->description = $message;
            $smslog->save();
        }
    }

    /**
     * Проверка стоимости сообщения без отправки!
     * 
     * @param str $msg
     * @param str $phones
     */
    protected function getSmsCost($phones, $message)
    {
        if (empty($message)) {
            $GLOBALS['log']->fatal("no message when sendSms");
            return;
        }

        if (empty($phones)) {
            $GLOBALS['log']->fatal("no phones when sendSms");
            return;
        }

        $this->sms_message = $message;
        $this->sms_phones = $phones;

        $url = $this->prepareSmsRequest($this->sms_cost_request);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    protected function prepareSmsRequest($url)
    {
        $params = get_object_vars($this);

        foreach ($params as $paramName => $value) {
            if (!$this->isSmsParam($paramName)) {
                continue;
            }

            if (empty($value)) {
                $value = '';
            }

            if (is_array($value)) {
                $url = strtr($url, ["<$paramName>" => implode(", ", $value)]);
            } else {
                $url = strtr($url, ["<$paramName>" => $value]);
            }
        }

        return $url;
    }

    protected function isSmsParam($paramName)
    {
        if (empty($paramName) || strpos($paramName, 'sms') === false) {
            return false;
        }

        return true;
    }

}
