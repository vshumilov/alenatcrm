<?php

require_once 'modules/AOP_Case_Updates/util.php';
require_once 'custom/include/Nit/BeanHelper/Role.php';

class BeanHelperEmail extends BeanHelperRole
{
    public $emailRegExp = '/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/';

    public function sendEmail($emails, $template, $signature = array(), $addDelimiter = true, $beans = [], $attachments = [])
    {
        if (empty($emails) || empty($template)) {
            return;
        }
        
        $GLOBALS['log']->fatal("BeanHelperEmail: sendEmail called");

        require_once("include/SugarPHPMailer.php");
        
        $mailer = new SugarPHPMailer();
        $admin = new Administration();
        $admin->retrieveSettings();

        $mailer->prepForOutbound();
        $mailer->setMailerForSystem();

        $signatureHTML = "";

        if ($signature && array_key_exists("signature_html", $signature)) {
            $signatureHTML = from_html($signature['signature_html']);
        }

        $signaturePlain = "";

        if ($signature && array_key_exists("signature", $signature)) {
            $signaturePlain = $signature['signature'];
        }
        
        $emailSettings = getPortalEmailSettings();
        
        $text = $this->populateTemplate($template, $addDelimiter, null, $beans);
        
        $mailer->Subject = $text['subject'];
        $mailer->Body = $text['body'] . $signatureHTML;
        $mailer->IsHTML(true);
        $mailer->AltBody = $text['body_alt'] . $signaturePlain;
        $mailer->From = $emailSettings['from_address'];
        $mailer->FromName = $emailSettings['from_name'];
        
        if (!empty($attachments) && is_array($attachments)) {
            foreach ($attachments as $attachment) {
                $mailer->addAttachment($attachment['path'], $attachment['name']);
            }    
        }
        
        foreach ($emails as $email) {
            $mailer->AddAddress($email);
        }

        if (!$mailer->Send()) {
            $GLOBALS['log']->fatal("BeanHelperEmail: Could not send email:  " . $mailer->ErrorInfo);
            return false;
        }

        require_once('modules/Emails/Email.php');
        $emailObj = new Email();
        $emailObj->to_addrs = implode(",", $emails);
        $emailObj->type = 'out';
        $emailObj->deleted = '0';
        $emailObj->name = $mailer->Subject;
        $emailObj->description = $mailer->AltBody;
        $emailObj->description_html = $mailer->Body;
        $emailObj->from_addr = $mailer->From;

        if ($this->bean->id) {
            $emailObj->parent_type = $this->bean->module_dir;
            $emailObj->parent_id = $this->bean->id;
        }

        $emailObj->date_sent = TimeDate::getInstance()->nowDb();
        $emailObj->modified_user_id = '1';
        $emailObj->created_by = '1';
        $emailObj->status = 'sent';
        $emailObj->save();

        return true;
    }

    public function populateTemplate(EmailTemplate $template, $addDelimiter = true, $bean = null, $beans = [])
    {
        global $app_strings, $sugar_config;
        //Order of beans seems to matter here so we place contact first.
        $userId = '';
        $user = $this->getUpdateUser();
        
        if (empty($bean->id)) {
            $bean = $this->bean;
        }
        
        if (!empty($beans[$bean->module_dir])) {
            $beans[$bean->module_dir] = $bean->id;
        }
        
        $beans["Users"] = $user->id;
        
        $ret = array();
        $ret['subject'] = from_html($this->parseTemplate($template->subject, $beans));
        
        $body = $this->parseTemplate(str_replace("\$sugarurl", $sugar_config['site_url'], $template->body_html), $beans);
        $bodyAlt = $this->parseTemplate(str_replace("\$sugarurl", $sugar_config['site_url'], $template->body), $beans);
        
        if ($addDelimiter) {
            $body = $app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'] . $body;
            $bodyAlt = $app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'] . $bodyAlt;
        }

        $ret['body'] = from_html($body);
        $ret['body_alt'] = strip_tags(from_html($bodyAlt));

        return $ret;
    }

    protected function parseTemplate($string, &$bean_arr)
    {
        global $beanFiles, $beanList;

        $typeMap = array('dynamicenum' => 'enum');

        foreach ($bean_arr as $bean_name => $bean_id) {
            require_once($beanFiles[$beanList[$bean_name]]);
            
            $className = $beanList[$bean_name];
            $focus = new $className();
            $focus->retrieve($bean_id);

            if ($bean_name == 'Leads' || $bean_name == 'Prospects') {
                $bean_name = 'Contacts';
            }

            foreach ($focus->field_defs as $key => $field_def) {

                if (array_key_exists($field_def['type'], $typeMap)) {
                    $focus->field_defs[$key]['type'] = $typeMap[$field_def['type']];
                }
            }


            $string = EmailTemplate::parse_template_bean($string, $bean_name, $focus);
        }

        return $string;
    }

    public function getAllUsersEmailsByRoles($roleNames)
    {
        if (empty($roleNames)) {
            return;
        }

        $usersQuery = BeanHelperRole::getQueryAllUsersIdsByRoles($roleNames);
        
        if (empty($usersQuery)) {
            return;
        }

        $usersResult = $this->bean->db->query($usersQuery);

        $emails = [];

        while (($row = $this->bean->db->fetchByAssoc($usersResult)) != null) {
            $user = new User();
            $user->retrieve($row['id']);
            
            if (empty($user->email1)) {
                continue;
            }

            $emails[] = $user->email1;
        }

        return $emails;
    }
    
    public function isValidEmail($email)
    {
        return !empty($email) && preg_match($this->emailRegExp, $email);
    }

    public function getEmailsByRoles($roleNames)
    {
        $users = $this->getUsersByRoles($roleNames);

        if (empty($users)) {
            return;
        }

        $emails = [];

        foreach ($users as $user) {
            $emails[] = $user->email1;
        }

        return $emails;
    }

    public function getUsersByRoles($roleNames)
    {
        $users = [];

        if (empty($roleNames)) {
            return;
        }
        
        $securityGroup = $this->getCurrentSecurityGroup();

        if (empty($securityGroup->id)) {
            return;
        }

        $securityGroup->load_relationship('users');

        $groupUsers = $securityGroup->users->getBeans();

        if (empty($groupUsers)) {
            return;
        }

        foreach ($groupUsers as $user) {
            $user->load_relationship('aclroles');

            $roles = $user->aclroles->getBeans();

            foreach ($roleNames as $roleName) {
                if (OpportunityBeanHelperOpportunity::isHasRoleWithName($roles, $roleName) && empty($users[$user->id])) {
                    $users[$user->id] = $user;
                }
            }
        }

        return $users;
    }
}
