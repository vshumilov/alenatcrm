<?php

class BeanHelperFile extends BeanHelper
{

    public function deleteAttachment($fileField = null, $isduplicate = "false")
    {
        if ($this->bean->ACLAccess('edit')) {
            if ($isduplicate == "true") {
                return true;
            }
            $removeFile = "upload://{$this->bean->id}";
        }
        if (file_exists($removeFile)) {
            if (!unlink($removeFile)) {
                $GLOBALS['log']->error("*** Could not unlink() file: [ {$removeFile} ]");
                return false;
            }  
        }

        if (!empty($fileField) && !empty($this->bean->field_defs[$fileField]['customFile'])) {
            $this->bean->{$fileField} = '';
            $this->bean->{$fileField . '_filename'} = '';
            $this->bean->{$fileField . '_mime_type'} = '';
            $this->bean->{$fileField . '_ext'} = '';
        } else {
            $this->bean->uploadfile = '';
            $this->bean->filename = '';
            $this->bean->file_mime_type = '';
            $this->bean->file_ext = '';
        }

        $this->bean->save();
        return true;

        return false;
    }

}
