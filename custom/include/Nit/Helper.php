<?php

class Nit_Helper
{

    /**
     * @var array
     */
    protected static $currencies = array();

    /**
     * @var array
     */
    protected static $queries = array();

    /**
     * @var array
     */
    protected static $beanHelpers = array();

    /**
     * @param string $id
     * @return Currency
     */
    public static function getCurrency($id)
    {
        if (!isset(self::$currencies[$id])) {
            self::$currencies[$id] = BeanFactory::getBean('Currencies', $id);
        }

        return self::$currencies[$id];
    }

    protected static function getClass(SugarBean $bean, $type, $name)
    {
        $className = $bean->object_name . '_' . $type . '_' . ucfirst($name);
        $filename = 'modules/' . $bean->module_dir . '/' . $type . '/' . ucfirst($name) . '.php';
        $customFilename = 'custom/' . $filename;

        if (file_exists($customFilename)) {
            require_once $customFilename;
            return $className;
        }

        if (file_exists($filename)) {
            require_once $filename;
            return $className;
        }
        
        $filename = 'custom/include/Vedisoft/' . $type . '/' . ucfirst($name) . '.php';
        
        if (file_exists($filename)) {
            require_once $filename;
            return 'Vedisoft_' . $type . '_' . ucfirst($name);;
        }
        
        throw new Vedisoft_Exception('File %file% for %type% %name% not exist', array(
            'type' => strtolower($type),
            'name' => $name,
            'file' => $filename,
        ));
    }

    /**
     * @param string|SugarBean $bean
     * @param string $query
     * @return \Vedisoft_Query
     * @throws Vedisoft_Exception
     */
    public static function getQuery($bean, $query)
    {
        if (is_string($bean)) {
            $bean = BeanFactory::newBean($bean);
        }
        
        if (isset(self::$queries[$bean->module_dir][$query])) {
            $query = self::$queries[$bean->module_dir][$query];
            $query->bean = $bean;
            
            return $query;
        }
        
        if (!($bean instanceof SugarBean)) {
            throw new Vedisoft_Exception('bean must be implemented SugarBean');
        }

        $className = self::getClass($bean, 'Query', $query);

        if (!class_exists($className, false)) {
            throw new Vedisoft_Exception('Class %class% for query %query% not define', array(
                'query' => $query,
                'class' => $className,
            ));
        }

        $queryObject = new $className;
        $queryObject->bean = $bean;

        self::$queries[$bean->module_dir][$query] = $queryObject;

        return $queryObject;
    }

    public static function getBeanHelper($bean, $beanHelper)
    {
        if (is_string($bean)) {
            $bean = BeanFactory::getBean($bean);
        }
        
        if (!($bean instanceof SugarBean)) {
            throw new Vedisoft_Exception('bean must be implemented SugarBean');
        }

        if (isset(self::$beanHelpers[$bean->module_name][$beanHelper])) {
            $beanHelper = self::$beanHelpers[$bean->module_name][$beanHelper];
            $beanHelper->bean = $bean;
            
            return $beanHelper;
        }
        
        $className = self::getClass($bean, 'BeanHelper', $beanHelper);

        if (!class_exists($className, false)) {
            throw new Vedisoft_Exception('Class %class% for beanHelper %beanHelper% not define', array(
                'beanHelper' => $beanHelper,
                'class' => $className,
            ));
        }

        $beanHelperObject = new $className;
        $beanHelperObject->bean = $bean;

        self::$beanHelpers[$bean->module_dir][$beanHelper] = $beanHelperObject;

        return $beanHelperObject;
    }

    /**
     * @param SugarBean $bean
     * @return array
     */
    public static function getPhoneFields(SugarBean $bean)
    {
        return self::getBeanHelper($bean, 'Phone')->getPhoneFields();
    }

    public static function getLocaleAmountString($amount, $currency, $with_pennies = true, $with_currency = true)
    {
        static $numeral;
        static $numeral_alt;
        static $numeral_use_alt;
        static $powers_of_ten;
        static $module_dir = 'Currencies';
        
        global $mod_strings;
        global $app_list_strings;
        
        if (empty($currency->currency_penny_forms)) {
            return 'Не заданы формы имени валюты';
        }
        
        if (empty($currency->currency_unit_forms)) {
            return 'Не заданы формы имен копеек';
        }

        // initialize currency settings
        if (!isset($numeral)) {
            global $current_language;
            $mod_strings = return_module_language($current_language, $currency->module_dir);
            $numeral = $app_list_strings['LIST_NUMERAL'];
            $numeral_alt = $numeral;
            $numeral_use_alt = $app_list_strings['LIST_NUMERAL_USE_ALT'];
            $powers_of_ten = $app_list_strings['LIST_POWERS_OF_TEN'];

            if (isset($app_list_strings['LIST_NUMERAL_ALT'])) {
                $numeral_alt = array_merge($numeral_alt, $app_list_strings['LIST_NUMERAL_ALT']);
            }
            foreach ($numeral as $key => & $numeral_item) {
                $numeral_item = array_map('trim', explode(',', $numeral_item));
            }
            foreach ($numeral_alt as $key => & $numeral_alt_item) {
                $numeral_alt_item = array_map('trim', explode(',', $numeral_alt_item));
            }
            foreach ($powers_of_ten as $key => & $powers_of_ten_item) {
                if ($key == -2 || $key == 0) {
                    continue;
                }
                $powers_of_ten_item = array_map('trim', explode(',', $powers_of_ten_item));
            }
            unset($key);
            unset($numeral_item);
            unset($numeral_alt_item);
            unset($powers_of_ten_item);
            unset($mod_strings);
        }
        unset($powers_of_ten[0]);
        unset($powers_of_ten[1]);
        $powers_of_ten[0] = array_map('trim', explode(',', $currency->currency_penny_forms));
        $powers_of_ten[1] = array_map('trim', explode(',', $currency->currency_unit_forms));

        // perfomes action
        $amount = (float) str_replace(',', '.', $amount);
        $amount = !empty($amount) ? $amount : 0;
        $amount_parts = explode('.', (string) $amount);

        // units
        $units = number_format($amount_parts[0], 0, '', '-');
        $units = explode('-', $units);

        $result = array();
        if (count($units) == 1 && $units[0] == 0) {
            $result[] = $numeral['units'][0];
            if ($with_currency) {
                $result[] = self::getLocaleAmountStringForm(0, $powers_of_ten[1]);
            }
        } else {
            while (($unit_part = array_shift($units)) != null) {
                $unit_part = str_pad($unit_part, 3, '0', STR_PAD_LEFT);
                $power_of_ten = count($units) + 1;
                if (in_array($powers_of_ten[$power_of_ten][0], $numeral_use_alt)) {
                    $numeral_current = & $numeral_alt;
                } else {
                    $numeral_current = & $numeral;
                }
                $unit_part_100 = (int) substr($unit_part, 0, 1);
                $unit_part_10 = (int) substr($unit_part, 1, 1);
                $unit_part_1 = (int) substr($unit_part, 2, 1);
                if ($unit_part_100 > 0) {
                    $result[] = $numeral_current['hundreds'][$unit_part_100];
                }
                if ($unit_part_10 == 1) {
                    $result[] = $numeral_current['units_second_decade'][$unit_part_1];
                } else {
                    if ($unit_part_10 > 1) {
                        $result[] = $numeral_current['tens'][$unit_part_10];
                    }
                    if ($unit_part_1 > 0) {
                        $result[] = $numeral_current['units'][$unit_part_1];
                    }
                }
                if ($power_of_ten != 1 || $with_currency) {
                    $result[] = self::getLocaleAmountStringForm($unit_part_10 . $unit_part_1, $powers_of_ten[$power_of_ten]);
                }
            }
        }
        if ($with_pennies) {
            // pennies
            $pennies = str_pad(isset($amount_parts[1]) ? $amount_parts[1] : 0, 2, '0', STR_PAD_RIGHT);
            $pennies = substr($pennies, 0, 2);
            $result[] = $pennies;
            if ($with_currency) {
                $result[] = self::getLocaleAmountStringForm($pennies, $powers_of_ten[0]);
            }
        }
        $result = implode(' ', $result);
        // use $encoding param, because tcpdf broke all system settings
        $result = mb_strtolower(preg_replace('/\s+/', ' ', $result), 'utf-8');
        return $result;
    }

    public static function getLocaleAmountStringForm($number, $forms)
    {
        $form = 1;
        $number = abs($number);
        $number = $number % 100;
        if ($number > 9 && $number < 21) {
            $form = 3;
        } else {
            $number = $number % 10;
            if ($number > 4 || $number == 0) {
                $form = 3;
            } elseif ($number > 1) {
                $form = 2;
            }
        }
        while ($form > -1) {
            if (isset($forms[--$form]))
                return $forms[$form];
        }
        return '';
    }

}
