<?php

class BeanHelper
{

    /**
     * @var SugarBean
     */
    public $bean;

    public function __construct($bean)
    {
        $this->bean = $bean;
    }

    public function getUpdateUser()
    {
        $user = BeanFactory::getBean('Users', $this->bean->assigned_user_id);
        return $user;
    }

    public function isNewBeanInSaving($bean)
    {
        $dateEntered = new DateTime($bean->date_entered);
        $dateCurrent = new DateTime(date('Y-m-d H:i:s'));
        $interval = $dateEntered->diff($dateCurrent);

        if ($bean->in_save && $interval->format('s') < 10) {
            return true;
        }
        
        return false;
    }

}
