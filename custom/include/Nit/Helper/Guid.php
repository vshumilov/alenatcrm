<?php

class HelperGuid
{

    protected $regExp = "/^(\{)?[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}(?(1)\})$/i";

    public function validate($guid)
    {
        return !empty($guid) && preg_match($this->regExp, $guid);
    }

}
