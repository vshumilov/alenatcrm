<?php

class HelperDate
{
    public function toDbDate($humanDate) {
        $date = new DateTime($humanDate);
        $dbDate = $date->format('Y-m-d');
        
        return $dbDate;
    }
    
    public function validate($dbDate) 
    {
        $date = new DateTime($dbDate);
        
        return checkdate($date->format('m'), $date->format('d'), $date->format('Y'));
    }
}

