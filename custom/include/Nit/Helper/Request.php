<?php

class HelperRequest
{

    public static function getStrFromReq($value)
    {
        //$value = xsltspecialchars( mysql_real_escape_string(trim(strip_tags($_REQUEST[$key]))) );
        $value = self::processReqStr($value);
    }

    /**
     * Получение целого положительного числа из HTTP запроса (с защитой от SQL-инъекций)
     * 
     * @author max
     * 
     * @param string $key имя переменной REQUEST
     * @param string $val результат
     * 
     * @return bool удалось ли "вытянуть" число 
     */
    public static function getIntFromReq($key, &$val, $default = 0)
    {
        if (!isset($_REQUEST[$key])) {
            $val = $default;
            return FALSE;
        }

        $value = trim(strip_tags($_REQUEST[$key]));

        if (preg_match("|^[\d]+$|", $value)) {
            $val = (integer) $value;
            return TRUE;
        } else {
            $val = $default;
            return FALSE;
        }
    }

    /**
     * Ловим CheckBox из формы
     * 
     * @author Ilya - с наилучшими пожеланиями
     * @return bool - существует переменная или нет
     */
    public static function getBoolFromReq($key, &$val)
    {
        if (isset($_REQUEST[$key])) {
            $val = true;
        } else {
            $val = false;
        }

        return $val;
    }

    public static function getArrayFromReq($key, &$array, $default = null)
    {
        if (!isset($_REQUEST[$key])) {
            $array = @$default;
            return FALSE;
        }

        if (is_array($_REQUEST[$key])) {
            $array = array();

            foreach ($_REQUEST[$key] as $row) {
                $array[] = mysql_real_escape_string(trim($row));
            }

            return $array;
        } else {
            $array = $default;
            return FALSE;
        }
    }

    /**
     * Получение ID из HTTP запроса (с защитой от SQL-инъекций)
     * 
     * @author max
     * 
     * @param string $key имя переменной REQUEST
     * @param string $id результат, ID
     * 
     * @return bool удалось ли "вытянуть" ID 
     */
    public static function getIdFromReq($key, &$id, $default = '')
    {
        if (!isset($_REQUEST[$key])) {
            $id = $default;
            return FALSE;
        }

        $value = trim(strip_tags($_REQUEST[$key]));

        if ($value == '') {
            $id = $default;
            return FALSE;
        }

        if (preg_match("|^[\d]*$|", $value)) {
            $id = $value;
            return TRUE;
        } else {
            $id = $default;
            return FALSE;
        }
    }

    public static function getJsonFromReq($key, &$value, $default = false)
    {
        if (!isset($_REQUEST[$key])) {
            $value = $default;
        } else {
            $value = $_REQUEST[$key];
            $value = self::processReqStr($value);

            if ($value = json_decode($value, true)) {
                return $value;
            } else {
                $value = false;
            }
        }
        return $value;
    }

    public static function processReqStr($str)
    {
        if (empty($str)) {
            return $str;
        }
        
        $db = getDatabaseInstance();
        
        return mysqli_real_escape_string($db->getDatabase(), trim(strip_tags($str)));
    }

    public static function getValueFromRequest($key)
    {
        if (isset($_REQUEST[$key])) {
            return $_REQUEST[$key];
        }

        if (isset($_REQUEST['params'][$key])) {
            return $_REQUEST['params'][$key];
        }

        return;
    }

}
