<?php

class HelperPhone
{
    public $phonePrefix = "+7";
    public $phoneMobileLength = 12;
    public $codeLength = 4;

    public function isValidMobilePhone($phone)
    {
        $phone = $this->formatPhone($phone);

        if (
        empty($phone) ||
        strpos($phone, $this->phonePrefix) === false ||
        strlen($phone) != $this->phoneMobileLength) {
            return false;
        }

        return true;
    }

    public function getSmallPhone($phone)
    {
        $smallPhone = intval(substr($phone, strlen($phone) - 4));
        return $smallPhone;
    }

    public function formatPhone($phone)
    {
        if (empty($phone)) {
            return $phone;
        }
        
        $phone = $this->getOnlyNumbers($phone);
        $len = strlen($phone);

        if (!in_array($len, [11, $this->phoneMobileLength])) {
            return $phone;
        }

        $phone = $this->phonePrefix . substr($phone, $len - 10);

        return $phone;
    }

    public function getOnlyNumbers($str)
    {
        $numbers = preg_replace("/[^0-9]/", '', $str);
        return $numbers;
    }

    public function isValidCode($code)
    {
        if (empty($code) || strlen($code) != $this->codeLength || !is_numeric($code)) {
            return false;
        }
        
        return true;
    }
}
