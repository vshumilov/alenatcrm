<?php

require_once('custom/include/SugarFields/Fields/Nitfile/SugarFieldNitfile.php');

function smarty_function_sugar_nitfile_process_field($params, &$smarty)
{
    if (empty($params['field'])) {
        $smarty->trigger_error("sugar_nitfile_process_field: missing 'field' parameter");
        return;
    }
    if (empty($smarty->_tpl_vars['bean']) ||
    !is_object($smarty->_tpl_vars['bean'])) {
        $smarty->trigger_error("sugar_nitfile_process_field: undefined Sugar module. Can\'t define current SugarBean object.");
        return;
    }
    $smarty->assign('NITFILE', SugarFieldNitfile::processNitfileField($smarty->_tpl_vars['bean'], $params['field']));

    return;
}
