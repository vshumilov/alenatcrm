<?php

require_once('custom/modules/Accounts/BeanHelper/Account.php');

function smarty_function_sugar_fio_inisials($params, &$smarty)
{
    $fullNameData = explode(' ', $params['fullname']);
 
    $inisialsData = array_reverse($fullNameData);
    
    $account = new Account();
    $accountHelper = new AccountBeanHelperAccount($account);
    $inisials = $accountHelper->getFioWithInisials(implode(' ', $inisialsData));

    return $inisials;
}

