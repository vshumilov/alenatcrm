<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/**
 * @class		SugarFieldYandexmap
 * @package		Yandex.maps for SugarCRM
 * @author		Vedisoft [http://www.vedisoft.info]
 */

require_once('include/SugarFields/Fields/Base/SugarFieldBase.php');

class SugarFieldDocumenttemplate extends SugarFieldBase
{
	/**
	 * Constructor
	 * @param $type [type of field, default 'Documenttemplate']
	 * @return void
	 */
	function __construct( $type = 'Documenttemplate' )
	{
		parent::SugarFieldBase( $type );
	}
	
	/**
	 * (non-PHPdoc)
	 * @see SugarFieldBase::setup()
	 */
	function setup($parentFieldArray, $vardef, $displayParams, $tabindex, $twopass = true)
	{
		parent::setup($parentFieldArray, $vardef, $displayParams, $tabindex, $twopass);
		if (!empty($displayParams['module']))
			$module = $displayParams['module'];
		elseif (!empty($vardef['module']))
			$module = $vardef['module'];
		else 
			global $module;
		$this->ss->assign('module', $module);
	}

	function getEditViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex) {
	    return $this->getDetailViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex);
	}
}