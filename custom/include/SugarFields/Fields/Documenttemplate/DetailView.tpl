{php}
    require_once('modules/DocumentTemplates/DocumentTemplate.php');
    $bean = $this->get_template_vars("bean");
    $this->assign('document_templates_options', DocumentTemplate::getDocumentTemplatesOptions($bean->module_dir, {{$displayParams.filters|default:null|@var_export:true}}));
    $this->assign('document_templates_value', DocumentTemplate::getDocumentTemplatesDefault($bean));
    $this->assign('document_templates_id', md5(microtime(true)));
{/php}
{if $document_templates_options|@count == 0}
    {sugar_translate label="MSG_NO_TEMPLATES_FOUND" module="DocumentTemplates"}
{elseif $document_templates_options|@count == 1}
    <input id="{$document_templates_id}" type="hidden" value="{$document_templates_options|@key|escape}" />
    <input id="{$document_templates_id}_btn" type="button" class="button" 
        value="{$document_templates_options|@array_shift|escape}" 
        title="{sugar_translate label="LBL_GENERATE_BUTTON_TITLE" module="DocumentTemplates"} &quot;{$document_templates_options|@array_shift|escape}&quot;" />
{else}
    <select id="{$document_templates_id}" size="{{$displayParams.size|default:1}}" style="width:{{$displayParams.width|default:"auto"}}">
        {html_options options=$document_templates_options selected=$document_templates_value}</select>
    <input id="{$document_templates_id}_btn" type="button" class="button" 
        value="{sugar_translate label="LBL_GENERATE_BUTTON_LABEL" module="DocumentTemplates"}" 
        title="{sugar_translate label="LBL_GENERATE_BUTTON_TITLE" module="DocumentTemplates"}" />
{/if}
<script src="custom/include/javascript/document-template/document-template.js"></script>
<script>
    (function () {ldelim}
        var documentTemplate = new SUGAR.DocumentTemplate({ldelim}
            btn_id: '{$document_templates_id}_btn',
            tpl_id: '{$document_templates_id}',
            bean_module: '{$module}',
            bean_id: '{$fields.id.value}',
            language: {ldelim}
                'MSG_SELECT_TEMPLATE_PLEASE': '{sugar_translate label="MSG_SELECT_TEMPLATE_PLEASE" module="DocumentTemplates"}',
                'MSG_CHANGES_WOULD_BE_SAVED': '{sugar_translate label="MSG_CHANGES_WOULD_BE_SAVED" module="DocumentTemplates"}'
            {rdelim}
        {rdelim});
    {rdelim}());
</script>