<?php

require_once('include/SugarFields/Fields/File/SugarFieldFile.php');
require_once 'modules/DocumentTemplates/UploadFile2.php';

class SugarFieldFilename extends SugarFieldFile
{

    public function save(&$bean, $params, $field, $vardef, $prefix = '')
    {
        $fileName = $prefix . $field . '_file';

        $upload_file = new UploadFile2($fileName);

        if (isset($_FILES[$fileName]) &&
        $upload_file->confirm_upload()) {
            $old_file = array(
                'bean_id' => $bean->id,
                'file_name' => $bean->file_name,
                'file_mime' => $bean->file_mime,
                'file_ext' => $bean->file_ext,
            );
            $bean->file_name = $upload_file->get_stored_file_name();
            $bean->file_mime = $upload_file->mime_type;
            $bean->file_ext = $upload_file->file_ext;
            $saved_id = $bean->save(!empty($bean->notify_on_save));
            if (!empty($old_file['bean_id']) &&
            !empty($old_file['file_name']) &&
            !empty($old_file['file_ext'])) {
                $upload_file->unlink_file($old_file['bean_id']);
            }
            if ($saved_id) {
                $upload_file->final_move($saved_id);
            }
        } else {
            $bean->save(!empty($bean->notify_on_save));
        }
    }

}
