
(function (windown, undefined) {
    if (window.Nitfile !== undefined)
        return;

    var _NITFILE_TIMER;

    window.Nitfile = function () {
        // type of current veiw
        this._type = null;
        // object mail container
        this._container = null;
        // conteinef for uploaded files
        this._container_ready = null;
        // container for added, but not uploaded files
        this._container_not_ready = null;
        // fake input of SugarBean field (uses at Sugar check_form function)
        this._input_hidden = null;
        // object if current file input
        this._input_file = null;
        // count of input files added
        this._input_file_count = 0;
        // count of already attached files
        this._ready_file_count = 0;
        // count of new added files (files which would attached)
        this._nitfiles_count = 0;
        //
        this._nitfiles = {};
        //
        this._inputs = {};

        // name of SugarBean object field
        this.field = '';
        // name of file input on form
        this.form_field = '';
        // type of thumbnail
        this.thumbnail = '';
        // hide control buttons or not?
        this.hide_buttons = null;
        return this;
    };

    Nitfile.prototype = {
        init: function () {
            switch (this.type()) {
                case 'view':
                    if (this.hide_buttons === null)
                        this.hide_buttons = true;
                    break;
                case 'edit':
                    if (this.hide_buttons === null)
                        this.hide_buttons = false;
                    this._input_hidden = $('<input />')
                            .attr('type', 'hidden')
                            .attr('id', this.option('field'))
                            .attr('name', this.option('field'))
                            .val('');
                    this.container().append(this._input_hidden);
                    this.createNitfileInput();
                    $(this._input_hidden.get(0).form)
                            .attr('enctype', 'multipart/form-data')
                            .attr('encoding', 'multipart/form-data');
                    break;
            }
        },
        type: function (type) {
            if (type !== undefined) {
                this._type = type;
            }
            return this._type;
        },
        getId: function (sugar_id) {
            for (var id in this._nitfiles) {
                if (sugar_id === this._nitfiles[id].id) {
                    return id;
                }
            }
            return null;
        },
        container: function (container) {
            if (container !== undefined) {
                if (container == 'ready') {
                    return this._container_ready;
                }
                if (container == 'not_ready') {
                    return this._container_not_ready;
                }
                this._container = typeof (container) == 'string'
                        ? $('#' + container)
                        : $(container);
                this._container
                        .attr('class', '')
                        .attr('style', '')
                        .addClass('sattach_container')
                        .data('Nitfile', this);
                if (this._container.length > 0) {
                    this._container_ready = $('<div></div>')
                            .addClass('sattach_container_inline')
                            .appendTo(this._container);
                    this._container_not_ready = $('<div></div>')
                            .addClass('sattach_container_inline')
                            .appendTo(this._container);
                }
            } else {
                return this._container;
            }
        },
        option: function (option, value) {
            if (value === undefined || option.slice(0, 1) == '_') {
                return (this[option] === undefined ? null : this[option]);
            }
            switch (option) {
                case 'container':
                    this.container(value);
                    break;
                case 'type':
                    this.type(value);
                    break;
                default:
                    this[option] = value;
                    break;
            }
            return this;
        },
        setTimeout: function (callback, timeout) {
            if (_NITFILE_TIMER !== undefined &&
                    _NITFILE_TIMER != null) {
                clearTimeout(_NITFILE_TIMER);
            }
            _NITFILE_TIMER = setTimeout(callback, timeout);
        },
        updateNitfileCount: function (up) {
            var up = up === undefined
                    ? 1
                    : (up == true ? 1 : -1)
            this._nitfiles_count += up;
            if (this._input_hidden !== undefined &&
                    this._input_hidden != null) {
                this._input_hidden.val(this._nitfiles_count > 0 ? '1' : '');
            }
        },
        deleteNotReadyFile: function (id) {
            this._inputs[id].file.attr('disabled', 'disabled');
            this._inputs[id].fake.attr('disabled', 'disabled');
            this._inputs[id].deleteBtn
                    .attr('title', SUGAR.language.get('app_strings', 'LBL_NITFILE_CANCEL_BUTTON_TITLE'))
                    .attr('value', SUGAR.language.get('app_strings', 'LBL_NITFILE_CANCEL_BUTTON_LABEL'));
            this._inputs[id].container.animate({'opacity': '0.4'}, 'fast');
            this._inputs[id].desc.css({'color': 'red'});
            this.updateNitfileCount(false);
            
            var self = this;
            
            setTimeout(function(){
                self._inputs[id].deleteBtn.addClass('recover-nitfile');
            }, 500);
            
        },
        restoreNotReadyFile: function (id) {
            this._inputs[id].file.removeAttr('disabled');
            this._inputs[id].fake.removeAttr('disabled');
            this._inputs[id].deleteBtn
                    .attr('title', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_TITLE'))
                    .attr('value', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_LABEL'));
            this._inputs[id].container.animate({'opacity': '1'}, 'fast');
            this._inputs[id].desc.css({'color': ''});
            this.updateNitfileCount(true);
            this._inputs[id].deleteBtn.removeClass('recover-nitfile');
        },
        deleteReadyFile: function (id) {
            var self = this;
            var params = {
                'entryPoint': 'ajax',
                'module': 'SugarNitfile',
                'call': 'delete',
                'options': {
                    'format': 'json'
                },
                'params': {
                    'id': this._nitfiles[id].id
                }
            };
            this._nitfiles[id].deleteBtn.bind('click.disable', function (e) {
                return false;
            });
            ajaxStatus.showStatus(SUGAR.language.get('app_strings', 'MSG_NITFILE_DELETE_FILE_PROGRESS'));
            $.get('index.php', params, function (data) {
                self.readyFileDeleted(data, id);
            })
        },
        readyFileDeleted: function (data, id) {
            if (data.error) {
                ajaxStatus.showStatus(data.error);
            } else {
                ajaxStatus.showStatus(SUGAR.language.get('app_strings', 'MSG_NITFILE_DELETE_FILE_SUCCESS'));
                $('a', this._nitfiles[id].container).bind('click.disable', function (e) {
                    return false;
                });
                this._nitfiles[id].deleteBtn.addClass('recover-nitfile');
                this._nitfiles[id].deleteBtn
                        .attr('title', SUGAR.language.get('app_strings', 'LBL_NITFILE_CANCEL_BUTTON_TITLE'))
                        .attr('value', SUGAR.language.get('app_strings', 'LBL_NITFILE_CANCEL_BUTTON_LABEL'));
                this._nitfiles[id].container.animate({'opacity': '0.4'}, 'fast');
                this._nitfiles[id].desc.css({'color': 'red'});
                this.updateNitfileCount(false);
            }
            this.setTimeout(function () {
                ajaxStatus.hideStatus();
            }, 2000);
        },
        restoreReadyFile: function (id) {
            var self = this;
            var params = {
                'entryPoint': 'ajax',
                'module': 'SugarNitfile',
                'call': 'restore',
                'options': {
                    'format': 'json'
                },
                'params': {
                    'id': this._nitfiles[id].id
                }
            };

            ajaxStatus.showStatus(SUGAR.language.get('app_strings', 'MSG_NITFILE_RESTORE_FILE_PROGRESS'));
            $.get('index.php', params, function (data) {
                self._nitfiles[id].deleteBtn.removeClass('recover-nitfile');
                self.readyFileRestored(data, id);
            })
        },
        readyFileRestored: function (data, id) {
            if (data.error) {
                ajaxStatus.showStatus(data.error);
            } else {
                ajaxStatus.showStatus(SUGAR.language.get('app_strings', 'MSG_NITFILE_RESTORE_FILE_SUCCESS'));
                $('a', this._nitfiles[id].container).unbind('click.disable');
                this._nitfiles[id].deleteBtn
                        .attr('title', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_TITLE'))
                        .attr('value', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_LABEL'));
                this._nitfiles[id].container.animate({'opacity': '1'}, 'fast');
                this._nitfiles[id].desc.css({'color': ''});
                this.updateNitfileCount(true);
            }
            this.setTimeout(function () {
                ajaxStatus.hideStatus();
            }, 2000);
        },
        clearReadyFiles: function () {
            this.container('ready').html('');
        },
        clearNotReadyFiles: function () {
            this.container('not_ready').html('');
        },
        createNitfileInput: function () {
            var self = this;
            var count = this._input_file_count++;
            var input = {
                changed: false,
                container: $('<div></div>')
                        .addClass('sattach_container_inline'),
                desc: $('<td valign="middle" style="vertical-align:middle">&nbsp;</td>'),
                file: $('<input type="file" />')
                        .attr('name', this.form_field + '[' + count + ']')
                        .addClass('sattach_file_input'),
                deleteBtn: $('<input type="button" class="button delete-new-nitfile" />')
                        .attr('title', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_TITLE'))
                        .attr('value', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_LABEL'))
                        .css('display', 'none'),
                fake: $('<div></div>')
                        .addClass('button search-nitfile')
                        .attr('title', SUGAR.language.get('app_strings', 'LBL_NITFILE_BROWSE_BUTTON_TITLE'))
                        .html($('<div></div>')
                                .html(SUGAR.language.get('app_strings', 'LBL_NITFILE_BROWSE_BUTTON_LABEL'))
                                .addClass('label'))
            }

            var tr = $('<tr></tr>');
            input.container.append($('<table class="sattach_file_input_table"></table>').append(tr));

            // Create input field section
            tr.append($('<td></td>').append(input.fake.append(input.file)));

            // Create file description section
            tr.append(input.desc);

            // Create delete section
            tr.append($('<td></td>').addClass('no-padding').append(input.deleteBtn));

            setTimeout(function () {

                input.deleteBtn.css('display', 'none');

            }, 200);

            // Event handlers
            // file input view enents
            input.file.mouseenter(function () {
                input.fake.addClass('buttonOn').find('div').addClass('buttonOn')
            });
            input.file.mouseleave(function () {
                input.fake.removeClass('buttonOn').find('div').removeClass('buttonOn')
            });
            // delete
            input.deleteBtn.on('click', function () {
                if ($(this).hasClass('recover-nitfile')) {
                    return;
                }
                
                self.deleteNotReadyFile(count);
            });

            input.deleteBtn.on('click', function () {
                if (!$(this).hasClass('recover-nitfile')) {
                    return;
                }
                self.restoreNotReadyFile(count);
            });
            // file select
            input.file.change(function () {
                self.nitfileNotReady(count);
            });

            this._inputs[count] = input;
            this.container().append(this._inputs[count].container);
        },
        generateFileDesc: function (file, isReadyFile) {
            var self = this;
            if (typeof (file) == 'string') {
                var file = {
                    'name': file
                }
            }
            var matches = /[^\\\/]+?(\.([^\.\\\/]+))$/.exec(file.name);
            if (matches == undefined) {
                matches = ['', '', ''];
            }
            if (matches[0] == undefined) {
                matches[0] = 'unknown_file';
            }
            if (matches[1] == undefined) {
                matches[1] = '';
            }
            if (matches[2] == undefined) {
                matches[2] = '';
            }
            file['name'] = matches[0];
            file['ext'] = file['ext'] === undefined
                    ? matches[2].toLowerCase()
                    : file['ext'].toLowerCase();
            switch (file['ext']) {
                case 'doc':
                case 'docx':
                case 'rtf':
                case 'odt':
                case 'ott':
                    var offset = '0px';
                    break;
                case 'xls':
                case 'xlc':
                case 'xlw':
                case 'xlt':
                case 'ods':
                case 'ots':
                case 'xlsx':
                case 'xltx':
                    var offset = '16px';
                    break;
                case 'ppt':
                case 'pps':
                case 'pot':
                case 'odp':
                case 'otp':
                    var offset = '32px';
                    break;
                case 'bmp':
                case 'wbmp':
                    var offset = '48px';
                    break;
                case 'jpg':
                case 'jpe':
                case 'jpeg':
                    var offset = '64px';
                    break;
                case 'png':
                    var offset = '80px';
                    break;
                case 'gif':
                    var offset = '96px';
                    break;
                case 'psd':
                case 'psb':
                case 'pdd':
                    var offset = '112px';
                    break;
                case 'mp3':
                case 'wav':
                case 'wma':
                case 'flac':
                    var offset = '128px';
                    break;
                case 'avi':
                case 'mpg':
                case 'mpeg':
                case 'mpe':
                case 'mp4':
                case 'wmv':
                case 'flv':
                case '3gp':
                    var offset = '144px';
                    break;
                case 'pdf':
                    var offset = '160px';
                    break;
                case 'zip':
                case 'rar':
                case 'tar':
                case 'gx':
                case '7z':
                case 'arj':
                    var offset = '176px';
                    break;
                case 'exe':
                case 'bin':
                    var offset = '192px';
                    break;
                case 'msi':
                    var offset = '208px';
                    break;
                case 'txt':
                case 'csv':
                case 'ini':
                case 'log':
                case 'cfg':
                case 'bat':
                case 'vbs':
                case 'vb':
                    var offset = '224px';
                    break;
                default:
                    var offset = '240px';
                    break;
            }

            if (file.access !== undefined &&
                    file.urls !== undefined) {
                if (file.access['download'] !== undefined &&
                        file.access['download'] == true &&
                        file.urls['download'] !== undefined) {
                    file.name = $('<a></a>')
                            .addClass('download')
                            .attr('href', file.urls['download'])
                            .html(file.name);
                    if (file.thumbnail !== false &&
                            file.thumbnail[this.option('thumbnail')] !== undefined) {
                        var format = file.thumbnail[this.option('thumbnail')];
                        file.thumbnail =
                                $('<a></a>', {
                                    'class': 'file_download_thumbnail',
                                    'target': '_blank',
                                    'href': file.urls['view'],
                                    'title': file.name.html()
                                })
                                .append($('<img />', {
                                    'src': 'index.php?entryPoint=ajax&module=SugarNitfile&call=getThumbnail&options[format]=file&record=' + encodeURIComponent(file.id) + '&params[format]=' + encodeURIComponent(this.option('thumbnail')),
                                    'width': format.width || '',
                                    'height': format.height || ''
                                })
                                        .mouseover(function () {
                                            $(this).css({'opacity': '1'})
                                        })
                                        .mouseout(function () {
                                            $(this).css({'opacity': ''})
                                        }));
                    }
                }
            }

            var descriptionClass = 'sattach_file_description';

            if (typeof (file.thumbnail) == 'object' &&
                    file.thumbnail.length !== undefined &&
                    file.thumbnail.length > 0) {

                if (isReadyFile) {
                    descriptionClass += ' no_margin_top';
                }

                var desc = $('<div style="float:left;"></div>')
                        .addClass(descriptionClass)
                        .addClass('thumbnail')
                        .css({'height': parseInt(format['height']) + 2 + 'px'})
                        .append(file.thumbnail);
            } else {
                if (isReadyFile) {
                    descriptionClass += ' is_ready_file';
                }

                var desc = $('<div></div>')
                        .addClass(descriptionClass)
                        .css({
                            'backgroundImage': 'url("custom/include/SugarFields/Fields/Nitfile/icons.png")',
                            'backgroundPosition': '0px -' + offset})
                        .html(file.name);
            }
            return desc;
        },
        nitfileNotReady: function (id) {
            this.updateNitfileCount(true);
            this._inputs[id].file.mouseleave();
            this._inputs[id].desc.html('').append(this.generateFileDesc(this._inputs[id].file.val()));
            if (!this._inputs[id].changed) {
                this._inputs[id].changed = true;
                this._inputs[id].deleteBtn.show();
                this.container('not_ready').append(this._inputs[id].container);
                this.createNitfileInput();
            }
        },
        nitfileReady: function (data) {
            var self = this;
            var count = this._ready_file_count++;
            this.updateNitfileCount(true);
            var nitfile = {
                id: data.id,
                urls: data.urls,
                container: $('<div></div>').addClass('sattach_container_block'),
                desc: this.generateFileDesc(data, true),
                deleteBtn: $('<input type="button" class="button delete-nitfile" />')
                        .attr('title', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_TITLE'))
                        .attr('value', SUGAR.language.get('app_strings', 'LBL_NITFILE_DELETE_BUTTON_LABEL'))
            };
            if (data.access['delete'] !== undefined &&
                    data.access['delete'] == true) {
                nitfile.deleteBtn.on('click', function () {
                    if ($(this).hasClass('recover-nitfile')) {
                        return;
                    }
                    
                    self.deleteReadyFile(count);
                });

                nitfile.deleteBtn.on('click', function () {
                    if (!$(this).hasClass('recover-nitfile')) {
                        return;
                    }
                    self.restoreReadyFile(count);
                });
            } else {
                nitfile.deleteBtn.attr('disabled', 'disabled');
            }
            nitfile.container.append(nitfile.desc);
            if (this.hide_buttons == false)
                nitfile.container.append(nitfile.deleteBtn);
            this._nitfiles[count] = nitfile;
            this.container('ready').append(this._nitfiles[count].container);
        }
    }
})(window);