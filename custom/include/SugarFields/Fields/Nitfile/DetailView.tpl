
<link type="text/css" rel="stylesheet" href="{{$CSS_SRC}}">
<script type="text/javascript" src="{{$JS_SRC}}"></script>
{sugar_nitfile_process_field field="{{$field_name|replace:'"':'\\"'}}" }
<div id="{$NITFILE.form_field|escape}_container"></div>
<script type="text/javascript">
    $(document).ready(function (){ldelim}
    {if $NITFILE.errors}
            var errors = [];
        {foreach from=$NITFILE.errors item=error}
            errors.push("{$error|replace:'"':'\\"'}");
        {/foreach}
            alert(SUGAR.language.get('app_strings', 'MSG_NITFILE_ERROR_LIST') + ':\n\n' + errors.join('\n\n'));
    {/if}
            var {{$field_name}} = new Nitfile();
    {{$field_name}}
            .option("type", "{{$VIEW}}")
                    .option("field", "{{$field_name|replace:'"':'\\"'}}")
                    .option("form_field", "{$NITFILE.form_field|replace:'"':'\\"'}")
                    .option("container", "{$NITFILE.form_field|replace:'"':'\\"'}_container")
                    .option("thumbnail", "{{$displayParams.thumbnail|replace:'"':'\\"'}}")
                    .option("hide_buttons", {{if isset($displayParams.hide_buttons)}}{{$displayParams.hide_buttons|@json}}{{else}}null{{/if}})
                                    .init();
    {foreach from=$NITFILE.nitfiles item=nitfile}
                            try {ldelim}
                                        eval("var item = {$nitfile.json|replace:'"':'\\"'}");
        {{$field_name}}.nitfileReady(item);
        {rdelim} catch (e){ldelim}
                    console.error(e);
        {rdelim}
    {/foreach}
    {rdelim});
</script>