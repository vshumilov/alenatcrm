<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('include/SugarFields/Fields/Base/SugarFieldBase.php');
require_once('modules/SugarNitfile/SugarNitfile.php');

class SugarFieldNitfile extends SugarFieldBase
{

    function __construct($type = 'Nitfile')
    {
        parent::SugarFieldBase($type);
    }

    function setup($parentFieldArray, $vardef, $displayParams, $tabindex, $twopass=true)
    {
        $displayParams = empty($displayParams) ? array() : $displayParams;
        // support old field definitions and display params
        if (isset($displayParams['preview']) && !isset($displayParams['thumbnail']))
            $displayParams['thumbnail'] = $displayParams['preview'];
        if (!isset($displayParams['thumbnail']))
            $displayParams['thumbnail'] = 'm';
        if ($displayParams['thumbnail'] !== false) {
            $thumbnail_formats = SugarNitfile::composeThumbnailFormats(isset($vardef['thumbnail_formats']) ? $vardef['thumbnail_formats'] : SugarNitfile::$THUMBNAIL_FORMATS);
            if (!isset($thumbnail_formats[$displayParams['thumbnail']]))
                $displayParams['thumbnail'] = false;
        }
        $this->ss->left_delimiter = '{{';
        $this->ss->right_delimiter = '}}';
        $this->ss->assign('field_name', $vardef['name']);
        $this->ss->assign('displayParams', $displayParams);
        $this->ss->assign('JS_SRC', 'custom/include/SugarFields/Fields/Nitfile/nitfile.js');
        
        if (empty($displayParams['no_css'])) {
            $this->ss->assign('CSS_SRC', 'custom/include/SugarFields/Fields/Nitfile/nitfile.css');
        }
    }

    function getDetailViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex)
    {
        $tpl = 'custom/include/SugarFields/Fields/Nitfile/DetailView.tpl';
        $this->setup($parentFieldArray, $vardef, $displayParams, $tabindex);
        $this->ss->assign('VIEW', 'view');
        return $this->ss->fetch($tpl);
    }

    function getEditViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex)
    {
        $tpl = 'custom/include/SugarFields/Fields/Nitfile/EditView.tpl';
        $this->setup($parentFieldArray, $vardef, $displayParams, $tabindex);
        $this->ss->assign('VIEW', 'edit');
        return $this->ss->fetch($tpl);
    }

    function getSearchViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex)
    {
        $tpl = 'custom/include/SugarFields/Fields/Nitfile/SearchView.tpl';
        $this->setup($parentFieldArray, $vardef, $displayParams, $tabindex);
        return $this->ss->fetch($tpl);
    }

    function getListViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex)
    {
        $tpl = 'custom/include/SugarFields/Fields/Nitfile/ListView.tpl';

        $errors = SugarNitfile::getErrors();

        if (!empty($errors)) {
            $errors = translate('MSG_NITFILE_ERROR_LIST') . ":\\n\\n" .
            implode('\\n\\n', $errors);

            $this->ss->assign('errors', str_replace('"', '\\\"', $errors));
        }

        $bean = BeanFactory::getBean($displayParams['module'], $parentFieldArray['ID']);
          
        $nitfiles = SugarNitfile::getBeanNitfiles($bean, $vardef['name']);
    
        $nitfilesArray = array();
        
        $smallFormat = $this->getSmallFormat($bean, $vardef['name']);
        
        $sugarConfig = new SugarConfig();
        $thumbnailsUrl = $sugarConfig->get('web_site_images_thumbnails_url');
        
        if (empty($thumbnailsUrl)) {
            $GLOBALS['log']->fatal("web_site_images_thumbnails_url dosen't exist in config.php: ");
        }

        foreach ($nitfiles as $nitfile) {

            $nitfilesArray[$nitfile->id]['id'] = urlencode($nitfile->id);
            $nitfilesArray[$nitfile->id]['fileName'] = htmlspecialchars($nitfile->file_name, ENT_NOQUOTES, 'UTF-8');
            $nitfilesArray[$nitfile->id]['ACLAccessView'] = $nitfile->ACLAccess('view');
            $nitfilesArray[$nitfile->id]['ext'] = $nitfile->file_ext;
            $nitfilesArray[$nitfile->id]['thumbnailSrc'] = "";
            
            if (!empty($smallFormat) && !empty($bean->thumbnail_name_s) && !empty($thumbnailsUrl)) {
                $nitfilesArray[$nitfile->id]['thumbnailSrc'] = $thumbnailsUrl . $bean->thumbnail_name_s;
            }
        }

        $this->ss->assign('nitfiles', $nitfilesArray);

        return $this->ss->fetch($tpl);
    }
    
    protected function getSmallFormat($bean, $fieldName)
    {
        $sugarNitfile = new SugarNitfile();
        $sugarNitfile->setBean($bean, $fieldName);
        $thumbnailFormats = $sugarNitfile->getThumbnailFormats();
        
        foreach ($thumbnailFormats as $formatKey => $format) {
            if ($formatKey == 's') {
                return $format;
            }
        }
    }

    public static function processNitfileField($bean, $field)
    {
        $data = array(
            'errors' => SugarNitfile::getErrors(),
            'urls' => array(
                'view' => 'index.php?entryPoint=ajax&options[format]=file&module=SugarNitfile&call=download&params[id]={id}',
                'download' => 'index.php?entryPoint=ajax&options[format]=file&options[force_download]=1&module=SugarNitfile&call=download&params[id]={id}',
                'delete' => 'index.php?entryPoint=ajax&options[format]=json&module=SugarNitfile&call=delete&params[id]={id}',
                'restore' => 'index.php?entryPoint=ajax&options[format]=json&module=SugarNitfile&call=restore&params[id]={id}',
            ),
            'form_field' => SugarNitfile::generateFormFieldName($bean, $field),
            'nitfiles' => array(),
        );

        $nitfiles = SugarNitfile::getBeanNitfiles($bean, $field);

        if (!empty($bean->id) && !empty($nitfiles)) {
            $json = getJSONobj();
            foreach ($nitfiles as $nitfile) {
                $item = array(
                    'id' => $nitfile->id,
                    'name' => $nitfile->file_name,
                    'size' => $nitfile->file_size,
                    'mime' => $nitfile->file_mime,
                    'ext' => $nitfile->file_ext,
                    'urls' => array(
                        'view' => str_replace('{id}', urlencode($nitfile->id), $data['urls']['view']),
                        'download' => str_replace('{id}', urlencode($nitfile->id), $data['urls']['download']),
                        'delete' => str_replace('{id}', urlencode($nitfile->id), $data['urls']['delete']),
                        'restore' => str_replace('{id}', urlencode($nitfile->id), $data['urls']['restore']),
                    ),
                    'access' => array(
                        'download' => $nitfile->ACLAccess('view'),
                        'delete' => $nitfile->ACLAccess('delete'),
                    ),
                    'thumbnail' => $nitfile->supportsThumbnail() ? $nitfile->getThumbnailFormats() : false,
                );
                $item['json'] = $json->encode($item);
                array_push($data['nitfiles'], $item);
            }
        }
        return $data;
    }

}
