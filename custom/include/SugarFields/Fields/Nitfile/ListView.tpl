<script type="text/javascript">
    window.onload = function () {ldelim}
            alert($errors);
    {rdelim}
</script>
{foreach from=$nitfiles item=nitfile}
    <div class="listViewBasic listViewOffset_{$nitfile.ext}}">
        {if $nitfile.ACLAccessView}
            <a href="index.php?entryPoint=ajax&options[format]=file&options[force_download]=1&module=SugarNitfile&call=download&params[id]={$nitfile.id}">
                {if $nitfile.thumbnailSrc}
                    <img src="{$nitfile.thumbnailSrc}"/>
                {else}
                    {$nitfile.fileName}
                {/if}
            </a>
        {else}
            <span>{$nitfile.fileName}</span>
        {/if}
    </div>
{/foreach}

