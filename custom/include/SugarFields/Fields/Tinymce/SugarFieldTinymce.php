<?php

require_once('include/SugarFields/Fields/Html/SugarFieldHtml.php');

class SugarFieldTinymce extends SugarFieldHtml {
    
    function getEditViewSmarty($parentFieldArray, $vardef, $displayParams, $tabindex){
    	$vardef['value'] = $this->getVardefValue($vardef);
				
        $this->setup($parentFieldArray, $vardef, $displayParams, $tabindex);
        return $this->fetch($this->findTemplate('EditView'));
    }
}

