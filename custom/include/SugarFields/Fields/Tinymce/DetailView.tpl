{literal}
    <script>
        $(document).ready(function () {
    {/literal}
            var field = '{{if empty($displayParams.idName)}}{{sugarvar key='name'}}{{else}}{{$displayParams.idName}}{{/if}}';
            $('#' + field).closest('.detail-view-field').css('width', '100%');
    {literal}
        });
    </script>
{/literal}

<span class="sugar_field" id="{{if empty($displayParams.idName)}}{{sugarvar key='name'}}{{else}}{{$displayParams.idName}}{{/if}}">{{$vardef.value}}</span>
{{if !empty($displayParams.enableConnectors)}}
{{sugarvar_connector view='DetailView'}}
{{/if}}