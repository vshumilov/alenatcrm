<link rel="stylesheet" type="text/css" href="custom/include/SugarFields/Fields/Int/RangeSearchForm.css">

{if strlen({{sugarvar key='value' string=true}}) <= 0}
    {assign var="value" value={{sugarvar key='default_value' string=true}} }
{else}
    {assign var="value" value={{sugarvar key='value' string=true}} }
{/if}

{{if empty($displayParams.idName)}}
{assign var="id" value={{sugarvar key='name' string=true}} }
{{else}}
{assign var="id" value={{$displayParams.idName}} }
{{/if}}

{if isset($smarty.request.{{$id_range_choice}})}
    {assign var="starting_choice" value=$smarty.request.{{$id_range_choice}}}
{else}
    {assign var="starting_choice" value="="}
{/if}

<script type='text/javascript'>
    function {$id}_range_change(val)
    {ldelim}
            calculate_between = (val == 'between');

            //Clear the values depending on the operation
            if (calculate_between) {ldelim}
                        document.getElementById("range_{$id}").value = '';
    {rdelim} else {ldelim}
                document.getElementById("start_range_{$id}").value = '';
                document.getElementById("end_range_{$id}").value = '';
    {rdelim}

            document.getElementById("{$id}_range_div").style.display = calculate_between ? 'none' : 'inline-block';
            document.getElementById("{$id}_between_range_div").style.display = calculate_between ? 'inline-block' : 'none';
    {rdelim}

        var {$id}_range_reset = function ()
    {ldelim}
    {$id}_range_change('=');
    {rdelim}

        YAHOO.util.Event.onDOMReady(function () {ldelim}
                if (document.getElementById('search_form_clear'))
    {ldelim}
                YAHOO.util.Event.addListener('search_form_clear', 'click', {$id}_range_reset);
    {rdelim}

    {rdelim});

        YAHOO.util.Event.onDOMReady(function () {ldelim}
                if (document.getElementById('search_form_clear_advanced'))
    {ldelim}
                YAHOO.util.Event.addListener('search_form_clear_advanced', 'click', {$id}_range_reset);
    {rdelim}
    {rdelim});

        YAHOO.util.Event.onDOMReady(function () {ldelim}
                //register on basic search form button if it exists
                if (document.getElementById('search_form_submit'))
    {ldelim}
                YAHOO.util.Event.addListener('search_form_submit', 'click',{$id}_range_validate);
    {rdelim}
            //register on advanced search submit button if it exists
            if (document.getElementById('search_form_submit_advanced'))
    {ldelim}
                YAHOO.util.Event.addListener('search_form_submit_advanced', 'click',{$id}_range_validate);
    {rdelim}

    {rdelim});

// this function is specific to range searches and will check that both start and end range fields have been
// filled prior to submitting search form.  It is called from the listener added above.
        function {$id}_range_validate(e){ldelim}
                if (
                        (document.getElementById("start_range_{$id}").value.length > 0 && document.getElementById("end_range_{$id}").value.length == 0)
                        || (document.getElementById("end_range_{$id}").value.length > 0 && document.getElementById("start_range_{$id}").value.length == 0)
                        )
    {ldelim}
                e.preventDefault();
                alert('{$APP.LBL_CHOOSE_START_AND_END_ENTRIES}');
                if (document.getElementById("start_range_{$id}").value.length == 0) {ldelim}
                                document.getElementById("start_range_{$id}").focus();
    {rdelim}
                else {ldelim}
                                document.getElementById("end_range_{$id}").focus();
    {rdelim}
    {rdelim}

    {rdelim}
</script>

<span style="white-space:nowrap !important;">
    <select class="custom-range-int-select"  id="{$id}_range_choice" name="{$id}_range_choice" style="width:156px" onchange="{$id}_range_change(this.value);">
        {html_options options={{sugarvar key='options' string=true}} selected=$starting_choice}
    </select>
    <div id="{$id}_range_div" style="{if $starting_choice=='between'}display:none;{else}display:inline-block;{/if}">
        <input type='text' name='range_{$id}' id='range_{$id}' style='width:75px !important;' size='{{$displayParams.size|default:20}}' 
               {{if isset($displayParams.maxlength)}}maxlength='{{$displayParams.maxlength}}'{{/if}} 
               value='{if empty($smarty.request.{{$id_range}}) && !empty($smarty.request.{{$original_id}})}{$smarty.request.{{$original_id}}}{else}{$smarty.request.{{$id_range}}}{/if}' tabindex='{{$tabindex}}' {{$displayParams.field}}>
    </div>
    <div id="{$id}_between_range_div" style="{if $starting_choice=='between'}display:inline-block;{else}display:none;{/if}">
        <input class="custom-range-int" type='text' name='start_range_{$id}' 
               id='start_range_{$id}' style='width:75px !important;' size='{{$displayParams.size|default:10}}' 
               {{if isset($displayParams.maxlength)}}maxlength='{{$displayParams.maxlength}}'{{/if}} 
               value='{$smarty.request.{{$id_range_start}} }' tabindex='{{$tabindex}}'>
        {$APP.LBL_AND}
        <input type='text' name='end_range_{$id}' 
               id='end_range_{$id}' style='width:75px !important;' size='{{$displayParams.size|default:10}}' 
               {{if isset($displayParams.maxlength)}}maxlength='{{$displayParams.maxlength}}'{{/if}} 
               value='{$smarty.request.{{$id_range_end}} }' tabindex='{{$tabindex}}'>    
    </div> 
</span>