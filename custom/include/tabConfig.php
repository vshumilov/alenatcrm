<?php

// created: 2016-10-23 14:07:26
$GLOBALS['tabStructure'] = array(
    'LBL_GROUPTAB0_1477213515' =>
    array(
        'label' => 'LBL_GROUPTAB0_1477213515',
        'modules' =>
        array(
            'Opportunities',
        ),
    ),
    'LBL_GROUPTAB1_1477213619' =>
    array(
        'label' => 'LBL_GROUPTAB1_1477213619',
        'modules' =>
        array(
            0 => 'Contacts',
            1 => 'Sanatoriums',
            2 => 'Tours',
            3 => 'Countries',
            4 => 'Cities',
            5 => 'Operators',
            6 => 'DocumentTemplates',
        ),
    ),
    'LBL_GROUPTAB2_1477213647' =>
    array(
        'label' => 'LBL_GROUPTAB2_1477213647',
        'modules' =>
        array(
            0 => 'EmailTemplates',
            1 => 'Dispatches',
            2 => 'Dispatcheslogs',
            3 => 'Schedulers',
        ),
    ),
    'LBL_GROUPTAB2_1477213646' =>
    array(
        'label' => 'LBL_GROUPTAB2_1477213646',
        'modules' =>
        array(
            0 => 'SecurityGroups',
        ),
    ),
);
