
sugarListView.prototype.send_form = function (select, currentModule, action, no_record_txt, action_module, return_info) {
    
    if (document.MassUpdate.select_entire_list.value == 1) {

        if (sugarListView.get_checks_count() < 1) {
            alert(no_record_txt);
            return false;
        }

        var href = action;
        if (action.indexOf('?') != -1)
            href += '&module=' + currentModule;
        else
            href += '?module=' + currentModule;

        if (return_info)
            href += return_info;
        var newForm = document.createElement('form');
        newForm.method = 'post';
        newForm.action = href;
        newForm.name = 'newForm';
        newForm.id = 'newForm';
        var postTa = document.createElement('textarea');
        postTa.name = 'current_post';
        postTa.value = document.MassUpdate.current_query_by_page.value;
        postTa.style.display = 'none';
        newForm.appendChild(postTa);
        document.MassUpdate.parentNode.appendChild(newForm);
        newForm.submit();
        return;
    } else if (document.MassUpdate.massall.checked == true)
        select = false;
    else
        select = true;

    sugarListView.get_checks();
    // create new form to post (can't access action property of MassUpdate form due to action input)
    var newForm = document.createElement('form');
    newForm.method = 'post';
    newForm.action = action;
    newForm.name = 'newForm';
    newForm.id = 'newForm';
    var uidTa = document.createElement('textarea');
    uidTa.name = 'uid';
    uidTa.style.display = 'none';
    uidTa.value = document.MassUpdate.uid.value;

    if (uidTa.value == '') {
        alert(no_record_txt);
        return false;
    }

    var $displayTabs = $('#display_tabs');

    if ($displayTabs.attr('id')) {
        var options = $displayTabs.find('option');
        for (var i = 0; i < options.length; i++) {
            var displayColumnsInput = document.createElement('input');
            displayColumnsInput.name = 'displayColumns[]';
            displayColumnsInput.value = $(options[i]).val().toLowerCase();
            displayColumnsInput.style.display = 'none';
            newForm.appendChild(displayColumnsInput);
        }
    }

    newForm.appendChild(uidTa);

    var moduleInput = document.createElement('input');
    moduleInput.name = 'module';
    moduleInput.type = 'hidden';
    moduleInput.value = currentModule;
    newForm.appendChild(moduleInput);

    var actionInput = document.createElement('input');
    actionInput.name = 'action';
    actionInput.type = 'hidden';
    actionInput.value = 'index';
    newForm.appendChild(actionInput);

    if (typeof action_module != 'undefined' && action_module != '') {
        var actionModule = document.createElement('input');
        actionModule.name = 'action_module';
        actionModule.type = 'hidden';
        actionModule.value = action_module;
        newForm.appendChild(actionModule);
    }
    //return_info must follow this pattern."&return_module=Accounts&return_action=index"
    if (typeof return_info != 'undefined' && return_info != '') {
        var params = return_info.split('&');
        if (params.length > 0) {
            for (var i = 0; i < params.length; i++) {
                if (params[i].length > 0) {
                    var param_nv = params[i].split('=');
                    if (param_nv.length == 2) {
                        returnModule = document.createElement('input');
                        returnModule.name = param_nv[0];
                        returnModule.type = 'hidden';
                        returnModule.value = param_nv[1];
                        newForm.appendChild(returnModule);
                    }
                }
            }
        }
    }

    document.MassUpdate.parentNode.appendChild(newForm);

    newForm.submit();
    // awu Bug 18624: Fixing issue where a canceled Export and unselect of row will persist the uid field, clear the field
    document.MassUpdate.uid.value = '';

    return false;
}