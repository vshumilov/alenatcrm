<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * gets the system default delimiter or an user-preference based override
 * @return string the delimiter
 */
function getDelimiter()
{
    global $sugar_config;
    global $current_user;

    if (!empty($sugar_config['export_excel_compatible'])) {
        return "\t";
    }

    $delimiter = ','; // default to "comma"
    $userDelimiter = $current_user->getPreference('export_delimiter');
    $delimiter = empty($sugar_config['export_delimiter']) ? $delimiter : $sugar_config['export_delimiter'];
    $delimiter = empty($userDelimiter) ? $delimiter : $userDelimiter;

    return $delimiter;
}

/**
 * builds up a delimited string for export
 * @param string type the bean-type to export
 * @param array records an array of records if coming directly from a query
 * @return string delimited string for export
 */
function export($type, $records = null, $members = false, $sample = false)
{
    global $locale;
    global $beanList;
    global $beanFiles;
    global $current_user;
    global $app_strings;
    global $app_list_strings;
    global $timedate;
    global $mod_strings;
    global $current_language;
    $sampleRecordNum = 5;

    //Array of fields that should not be exported, and are only used for logic
    $remove_from_members = array("ea_deleted", "ear_deleted", "primary_address");
    $focus = 0;

    $bean = $beanList[$type];
    require_once($beanFiles[$bean]);
    $focus = new $bean;
    $searchFields = array();
    $db = DBManagerFactory::getInstance();

    if ($records) {
        $records = explode(',', $records);
        $records = "'" . implode("','", $records) . "'";
        $where = "{$focus->table_name}.id in ($records)";
    } elseif (isset($_REQUEST['all'])) {
        $where = '';
    } else {
        if (!empty($_REQUEST['current_post'])) {
            $ret_array = generateSearchWhere($type, $_REQUEST['current_post']);

            $where = $ret_array['where'];
            $searchFields = $ret_array['searchFields'];
        } else {
            $where = '';
        }
    }
    $order_by = "";
    if ($focus->bean_implements('ACL')) {
        if (!ACLController::checkAccess($focus->module_dir, 'export', true)) {
            ACLController::displayNoAccess();
            sugar_die('');
        }
        if (ACLController::requireOwner($focus->module_dir, 'export')) {
            if (!empty($where)) {
                $where .= ' AND ';
            }
            $where .= $focus->getOwnerWhere($current_user->id);
        }
        /* BEGIN - SECURITY GROUPS */
        if (ACLController::requireSecurityGroup($focus->module_dir, 'export')) {
            require_once('modules/SecurityGroups/SecurityGroup.php');
            global $current_user;
            $owner_where = $focus->getOwnerWhere($current_user->id);
            $group_where = SecurityGroup::getGroupWhere($focus->table_name, $focus->module_dir, $current_user->id);
            if (!empty($owner_where)) {
                if (empty($where)) {
                    $where = " (" . $owner_where . " or " . $group_where . ")";
                } else {
                    $where .= " AND (" . $owner_where . " or " . $group_where . ")";
                }
            } else {
                if (!empty($where)) {
                    $where .= ' AND ';
                }
                $where .= $group_where;
            }
        }
        /* END - SECURITY GROUPS */
    }
    // Export entire list was broken because the where clause already has "where" in it
    // and when the query is built, it has a "where" as well, so the query was ill-formed.
    // Eliminating the "where" here so that the query can be constructed correctly.
    if ($members == true) {
        $query = $focus->create_export_members_query($records);
    } else {
        $beginWhere = substr(trim($where), 0, 5);
        if ($beginWhere == "where")
            $where = substr(trim($where), 5, strlen($where));

        $query = $focus->create_export_query($order_by, $where);
    }

    $result = '';
    $populate = false;
    if ($sample) {
        $result = $db->limitQuery($query, 0, $sampleRecordNum, true, $app_strings['ERR_EXPORT_TYPE'] . $type . ": <BR>." . $query);
        if ($focus->_get_num_rows_in_query($query) < 1) {
            $populate = true;
        }
    } else {
        $result = $db->query($query, true, $app_strings['ERR_EXPORT_TYPE'] . $type . ": <BR>." . $query);
    }


    $fields_array = $db->getFieldsArray($result, true);

    //set up the order on the header row
    $fields_array = get_field_order_mapping($focus->module_dir, $fields_array);



    //set up labels to be used for the header row
    $field_labels = array();
    foreach ($fields_array as $key => $dbname) {
        //Remove fields that are only used for logic
        if ($members && (in_array($dbname, $remove_from_members)))
            continue;

        //If labels should not be exportable skip them
        if (isset($focus->field_name_map[$key]) && isset($focus->field_name_map[$key]['exportable']) && $focus->field_name_map[$key]['exportable'] === false) {
            continue;
        }

        //default to the db name of label does not exist
        $field_labels[$key] = translateForExport($dbname, $focus);
    }

    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    if ($locale->getExportCharset() == 'UTF-8' &&
            !preg_match('/macintosh|mac os x|mac_powerpc/i', $user_agent)) { // Bug 60377 - Mac Excel doesn't support UTF-8
        //Bug 55520 - add BOM to the exporting CSV so any symbols are displayed correctly in Excel
        $BOM = "\xEF\xBB\xBF";
        $content = $BOM;
    } else {
        $content = '';
    }

    // setup the "header" line with proper delimiters
    $content .= "\"" . implode("\"" . getDelimiter() . "\"", array_values($field_labels)) . "\"\r\n";
    $pre_id = '';

    if ($populate) {
        //this is a sample request with no data, so create fake datarows
        $content .= returnFakeDataRow($focus, $fields_array, $sampleRecordNum);
    } else {
        $records = array();

        //process retrieved record
        while ($val = $db->fetchByAssoc($result, false)) {

            $new_arr = array();
            if ($members) {
                if ($pre_id == $val['id'])
                    continue;
                if ($val['ea_deleted'] == 1 || $val['ear_deleted'] == 1) {
                    $val['primary_email_address'] = '';
                }
                unset($val['ea_deleted']);
                unset($val['ear_deleted']);
                unset($val['primary_address']);
            }
            $pre_id = $val['id'];

            if ($members)
                $focus = BeanFactory::getBean($val['related_type']);
            else { // field order mapping is not applied for member-exports, as they include multiple modules
                //order the values in the record array
                $val = get_field_order_mapping($focus->module_dir, $val);
            }

            foreach ($fields_array as $key => $value) {
                //getting content values depending on their types
                $fieldNameMapKey = $fields_array[$key];

                //Dont export fields that have been explicitly marked not to be exportable
                if (isset($focus->field_name_map[$fieldNameMapKey]) && isset($focus->field_name_map[$fieldNameMapKey]['exportable']) &&
                        $focus->field_name_map[$fieldNameMapKey]['exportable'] === false) {
                    continue;
                }

                if (isset($focus->field_name_map[$fieldNameMapKey]) && $focus->field_name_map[$fieldNameMapKey]['type']) {
                    $fieldType = $focus->field_name_map[$fieldNameMapKey]['type'];
                    switch ($fieldType) {
                        //if our value is a currency field, then apply the users locale
                        case 'currency':
                            require_once('modules/Currencies/Currency.php');
                            $val[$key] = currency_format_number($val[$key]);
                            break;

                        //if our value is a datetime field, then apply the users locale
                        case 'datetime':
                        case 'datetimecombo':
                            $val[$key] = $timedate->to_display_date_time($db->fromConvert($val[$key], 'datetime'));
                            $val[$key] = preg_replace('/([pm|PM|am|AM]+)/', ' \1', $val[$key]);
                            break;

                        //kbrill Bug #16296
                        case 'date':
                            $val[$key] = $timedate->to_display_date($db->fromConvert($val[$key], 'date'), false);
                            break;

                        // Bug 32463 - Properly have multienum field translated into something useful for the client
                        case 'multienum':
                            $valueArray = unencodeMultiEnum($val[$key]);

                            if (isset($focus->field_name_map[$fields_array[$key]]['options']) && isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']])) {
                                foreach ($valueArray as $multikey => $multivalue) {
                                    if (isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$multivalue])) {
                                        $valueArray[$multikey] = $app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$multivalue];
                                    }
                                }
                            }
                            $val[$key] = implode(",", $valueArray);

                            break;

                        case 'enum':
                            if (isset($focus->field_name_map[$fields_array[$key]]['options']) &&
                                    isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']]) &&
                                    isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$val[$key]])
                            )
                                $val[$key] = $app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$val[$key]];

                            break;
                    }
                }


                // Keep as $key => $value for post-processing
                $new_arr[$key] = preg_replace("/\"/", "\"\"", $val[$key]);
            }

            unset($new_arr['id']);

            // Use Bean ID as key for records
            $records[$pre_id] = $new_arr;
        }

        // Check if we're going to export non-primary emails
        if ($focus->hasEmails() && in_array('email_addresses_non_primary', $fields_array)) {
            // $records keys are bean ids
            $keys = array_keys($records);

            // Split the ids array into chunks of size 100
            $chunks = array_chunk($keys, 100);

            foreach ($chunks as $chunk) {
                // Pick all the non-primary mails for the chunk
                $query = "
                      SELECT eabr.bean_id, ea.email_address
                      FROM email_addr_bean_rel eabr
                      LEFT JOIN email_addresses ea ON ea.id = eabr.email_address_id
                      WHERE eabr.bean_module = '{$focus->module_dir}'
                      AND eabr.primary_address = '0'
                      AND eabr.bean_id IN ('" . implode("', '", $chunk) . "')
                      AND eabr.deleted != '1'
                      ORDER BY eabr.bean_id, eabr.reply_to_address, eabr.primary_address DESC
                    ";

                $result = $db->query($query, true, $app_strings['ERR_EXPORT_TYPE'] . $type . ": <BR>." . $query);

                while ($val = $db->fetchByAssoc($result, false)) {
                    if (empty($records[$val['bean_id']]['email_addresses_non_primary'])) {
                        $records[$val['bean_id']]['email_addresses_non_primary'] = $val['email_address'];
                    } else {
                        // No custom non-primary mail delimeter yet, use semi-colon
                        $records[$val['bean_id']]['email_addresses_non_primary'] .= ';' . $val['email_address'];
                    }
                }
            }
        }

        $customRelateFields = array();
        $selects = array();
        foreach ($records as $record) {
            foreach ($record as $recordKey => $recordValue) {
                if (preg_match('/{relate\s+from=""([^"]+)""\s+to=""([^"]+)""}/', $recordValue, $matches)) {
                    $marker = $matches[0];
                    $relatedValue = '';

                    $splits = explode('.', $matches[1]);
                    $currentModule = $splits[0];
                    $currentField = $splits[1];
                    $currentBean = BeanFactory::getBean($currentModule);
                    $currentTable = $currentBean->table_name;

                    $splits = explode('.', $matches[2]);
                    $relatedModule = $splits[0];
                    $relatedField = $splits[1];
                    $relatedBean = BeanFactory::getBean($relatedModule);
                    $relatedTable = $relatedBean->table_name;

                    $relatedLabel = "$relatedTable.name AS related_label, NULL AS related_label1";
                    if (isset($relatedBean->field_defs['name']['source']) && $relatedBean->field_defs['name']['source'] == 'non-db') {
                        //$relatedLabel = 'NULL AS related_label, NULL AS related_label1';
                        if (
                                !isset($relatedBean->field_defs['first_name']['source']) || $relatedBean->field_defs['first_name']['source'] != 'non-db' &&
                                !isset($relatedBean->field_defs['last_name']['source']) || $relatedBean->field_defs['last_name']['source'] != 'non-db'
                        ) {
                            $relatedLabel = "$relatedTable.last_name AS related_label, $relatedTable.first_name AS related_label1";
                        }
                    }

                    $relatedTableCustomJoin = '';
                    $relatedFieldSelect = "NULL AS related_value";
                    if (!isset($existsTables["{$relatedTable}_cstm"])) {
                        $existsTables["{$relatedTable}_cstm"] = $db->tableExists("{$relatedTable}_cstm");
                    }
                    if ($existsTables["{$relatedTable}_cstm"]) {
                        $relatedTableCustomJoin = "
                        JOIN {$relatedTable}_cstm ON {$relatedTable}_cstm.id_c = {$currentTable}_cstm.$relatedField
                        ";
                        $relatedFieldSelect = "{$currentTable}_cstm.$relatedField AS related_value";
                    }

                    $relatedTableJoin = "LEFT JOIN $relatedTable ON $relatedTable.id = {$currentTable}_cstm.id_c";
                    if (isset($currentBean->field_defs[$relatedField])) {
                        $relatedTableJoin = "LEFT JOIN $relatedTable ON $relatedTable.id = {$currentTable}_cstm.$relatedField";
                    }

                    //-- $relatedTable.id AS related_id,
                    //-- {$currentTable}_cstm.id_c AS current_id_c,
                    //-- {$relatedTable}_cstm.id_c AS related_id_c,
                    $selects[] = "(SELECT $currentTable.id AS current_id,'$currentModule' AS current_module,'$currentField' AS current_field,'$relatedModule' AS related_module,'$relatedField' AS related_field,$relatedFieldSelect,$relatedLabel FROM $currentTable JOIN {$currentTable}_cstm ON {$currentTable}_cstm.id_c=$currentTable.id $relatedTableCustomJoin $relatedTableJoin WHERE $currentTable.id='{$record['id']}')";
                }
            }
        }

        $selects = array_unique($selects);


        // grab custom related fields information
        // query max length optimization, measured by mssql FreeTDS connection too
        $queryMaxLength = 620000;
        $query = '';
        $i = 0;
        $selectsCount = count($selects) - 1;
        foreach ($selects as $select) {
            $queryTemp = $query . ($i == 0 ? $select : " UNION $select");
            if ($i == $selectsCount || strlen($queryTemp) > $queryMaxLength) {
                $result = $db->query($query, 'export error on custom related type: ' . $query);
                while ($val = $db->fetchByAssoc($result, false)) {
                    $customRelateFields[$val['current_module']][$val['current_id']][$val['related_module']][$val['related_field']] = trim($val['related_label'] . ' ' . $val['related_label1']);
                }
                $query = $select;
            } else {
                $query = $queryTemp;
            }
            $i++;
        }


        foreach ($records as $record) {
            $line = implode("\"" . getDelimiter() . "\"", $record);
            $line = "\"" . $line;
            $line .= "\"\r\n";
            $line = parseRelateFields($line, $record, $customRelateFields);
            $content .= $line;
        }
    }

    return $content;
}

/**
 * Parse custom related fields
 * @param $line string CSV line
 * @param $record array of current line
 * @return mixed string CSV line
 */
function parseRelateFields($line, $record, $customRelateFields)
{
    while (preg_match('/{relate\s+from=""([^"]+)""\s+to=""([^"]+)""}/', $line, $matches)) {
        $marker = $matches[0];
        $relatedValue = '';

        $splits = explode('.', $matches[1]);
        $currentModule = $splits[0];
        $currentField = $splits[1];

        $splits = explode('.', $matches[2]);
        $relatedModule = $splits[0];
        $relatedField = $splits[1];

        if ($currentModule != $record['related_type']) {
            $GLOBALS['log']->debug('incorrect related type in export');
        } else {
            if (isset($customRelateFields[$currentModule][$record['id']][$relatedModule][$relatedField])) {
                $relatedValue = $customRelateFields[$currentModule][$record['id']][$relatedModule][$relatedField];
            } else {
                $relatedValue = '';
            }
        }

        $line = str_replace($marker, $relatedValue, $line);
    }
    return $line;
}

function generateSearchWhere($module, $query)
{//this function is similar with function prepareSearchForm() in view.list.php
    $seed = loadBean($module);
    if (file_exists('modules/' . $module . '/SearchForm.html')) {
        if (file_exists('modules/' . $module . '/metadata/SearchFields.php')) {
            require_once('include/SearchForm/SearchForm.php');
            $searchForm = new SearchForm($module, $seed);
        } elseif (!empty($_SESSION['export_where'])) { //bug 26026, sometimes some module doesn't have a metadata/SearchFields.php, the searchfrom is generated in the ListView.php.
            // Currently, massupdate will not generate the where sql. It will use the sql stored in the SESSION. But this will cause bug 24722, and it cannot be avoided now.
            $where = $_SESSION['export_where'];
            $whereArr = explode(" ", trim($where));
            if ($whereArr[0] == trim('where')) {
                $whereClean = array_shift($whereArr);
            }
            $where = implode(" ", $whereArr);
            //rrs bug: 31329 - previously this was just returning $where, but the problem is the caller of this function
            //expects the results in an array, not just a string. So rather than fixing the caller, I felt it would be best for
            //the function to return the results in a standard format.
            $ret_array['where'] = $where;
            $ret_array['searchFields'] = array();
            return $ret_array;
        } else {
            return;
        }
    } else {
        require_once('include/SearchForm/SearchForm2.php');

        if (file_exists('custom/modules/' . $module . '/metadata/metafiles.php')) {
            require('custom/modules/' . $module . '/metadata/metafiles.php');
        } elseif (file_exists('modules/' . $module . '/metadata/metafiles.php')) {
            require('modules/' . $module . '/metadata/metafiles.php');
        }

        if (file_exists('custom/modules/' . $module . '/metadata/searchdefs.php')) {
            require_once('custom/modules/' . $module . '/metadata/searchdefs.php');
        } elseif (!empty($metafiles[$module]['searchdefs'])) {
            require_once($metafiles[$module]['searchdefs']);
        } elseif (file_exists('modules/' . $module . '/metadata/searchdefs.php')) {
            require_once('modules/' . $module . '/metadata/searchdefs.php');
        }

        //fixing bug #48483: Date Range search on custom date field then export ignores range filter
        // first of all custom folder should be checked
        if (file_exists('custom/modules/' . $module . '/metadata/SearchFields.php')) {
            require_once('custom/modules/' . $module . '/metadata/SearchFields.php');
        } elseif (!empty($metafiles[$module]['searchfields'])) {
            require_once($metafiles[$module]['searchfields']);
        } elseif (file_exists('modules/' . $module . '/metadata/SearchFields.php')) {
            require_once('modules/' . $module . '/metadata/SearchFields.php');
        }
        if (empty($searchdefs) || empty($searchFields)) {
            //for some modules, such as iframe, it has massupdate, but it doesn't have search function, the where sql should be empty.
            return;
        }
        $searchForm = new SearchForm($seed, $module);
        $searchForm->setup($searchdefs, $searchFields, 'SearchFormGeneric.tpl');
    }
    $searchForm->populateFromArray(json_decode(html_entity_decode($query), true));
    $where_clauses = $searchForm->generateSearchWhere(true, $module);
    if (count($where_clauses) > 0)
        $where = '(' . implode(' ) AND ( ', $where_clauses) . ')';
    $GLOBALS['log']->info("Export Where Clause: {$where}");
    $ret_array['where'] = $where;
    $ret_array['searchFields'] = $searchForm->searchFields;
    return $ret_array;
}

/**
 * calls export method to build up a delimited string and some sample instructional text on how to use this file
 * @param string type the bean-type to export
 * @return string delimited string for export with some tutorial text
 */
function exportSample($type)
{
    global $app_strings;

    //first grab the
    $_REQUEST['all'] = true;

    //retrieve the export content
    $content = export($type, null, false, true);

    // Add a new row and add details on removing the sample data
    // Our Importer will stop after he gets to the new row, ignoring the text below 
    return $content . "\n" . $app_strings['LBL_IMPORT_SAMPLE_FILE_TEXT'];
}

//this function will take in the bean and field mapping and return a proper value
function returnFakeDataRow($focus, $field_array, $rowsToReturn = 5)
{

    if (empty($focus) || empty($field_array))
        return;

    //include the file that defines $sugar_demodata
    include('install/demoData.en_us.php');

    $person_bean = false;
    if (isset($focus->first_name)) {
        $person_bean = true;
    }

    global $timedate;
    $returnContent = '';
    $counter = 0;
    $new_arr = array();

    //iterate through the record creation process as many times as defined.  Each iteration will create a new row
    while ($counter < $rowsToReturn) {
        $counter++;
        //go through each field and populate with dummy data if possible
        foreach ($field_array as $field_name) {

            if (empty($focus->field_name_map[$field_name]) || empty($focus->field_name_map[$field_name]['type'])) {
                //type is not set, fill in with empty string and continue;
                $returnContent .= '"",';
                continue;
            }
            $field = $focus->field_name_map[$field_name];
            //fill in value according to type
            $type = $field['type'];

            switch ($type) {

                case "id":
                case "assigned_user_name":
                    //return new guid string
                    $returnContent .= '"' . create_guid() . '",';
                    break;
                case "int":
                    //return random number`
                    $returnContent .= '"' . mt_rand(0, 4) . '",';
                    break;
                case "name":
                    //return first, last, user name, or random name string
                    if ($field['name'] == 'first_name') {
                        $count = count($sugar_demodata['first_name_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['last_name_array'][mt_rand(0, $count)] . '",';
                    } elseif ($field['name'] == 'last_name') {
                        $count = count($sugar_demodata['last_name_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['last_name_array'][mt_rand(0, $count)] . '",';
                    } elseif ($field['name'] == 'user_name') {
                        $count = count($sugar_demodata['first_name_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['last_name_array'][mt_rand(0, $count)] . '_' . mt_rand(1, 111) . '",';
                    } else {
                        //return based on bean
                        if ($focus->module_dir == 'Accounts') {
                            $count = count($sugar_demodata['company_name_array']) - 1;
                            $returnContent .= '"' . $sugar_demodata['company_name_array'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'Bugs') {
                            $count = count($sugar_demodata['bug_seed_names']) - 1;
                            $returnContent .= '"' . $sugar_demodata['bug_seed_names'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'Notes') {
                            $count = count($sugar_demodata['note_seed_names_and_Descriptions']) - 1;
                            $returnContent .= '"' . $sugar_demodata['note_seed_names_and_Descriptions'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'Calls') {
                            $count = count($sugar_demodata['call_seed_data_names']) - 1;
                            $returnContent .= '"' . $sugar_demodata['call_seed_data_names'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'Tasks') {
                            $count = count($sugar_demodata['task_seed_data_names']) - 1;
                            $returnContent .= '"' . $sugar_demodata['task_seed_data_names'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'Meetings') {
                            $count = count($sugar_demodata['meeting_seed_data_names']) - 1;
                            $returnContent .= '"' . $sugar_demodata['meeting_seed_data_names'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'ProductCategories') {
                            $count = count($sugar_demodata['productcategory_seed_data_names']) - 1;
                            $returnContent .= '"' . $sugar_demodata['productcategory_seed_data_names'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'ProductTypes') {
                            $count = count($sugar_demodata['producttype_seed_data_names']) - 1;
                            $returnContent .= '"' . $sugar_demodata['producttype_seed_data_names'][mt_rand(0, $count)] . '",';
                        } elseif ($focus->module_dir == 'ProductTemplates') {
                            $count = count($sugar_demodata['producttemplate_seed_data']) - 1;
                            $returnContent .= '"' . $sugar_demodata['producttemplate_seed_data'][mt_rand(0, $count)] . '",';
                        } else {
                            $returnContent .= '"Default Name for ' . $focus->module_dir . '",';
                        }
                    }
                    break;
                case "relate":
                    if ($field['name'] == 'team_name') {
                        //apply team names and user_name
                        $teams_count = count($sugar_demodata['teams']) - 1;
                        $users_count = count($sugar_demodata['users']) - 1;

                        $returnContent .= '"' . $sugar_demodata['teams'][mt_rand(0, $teams_count)]['name'] . ',' . $sugar_demodata['users'][mt_rand(0, $users_count)]['user_name'] . '",';
                    } else {
                        //apply GUID
                        $returnContent .= '"' . create_guid() . '",';
                    }
                    break;
                case "bool":
                    //return 0 or 1
                    $returnContent .= '"' . mt_rand(0, 1) . '",';
                    break;

                case "text":
                    //return random text
                    $returnContent .= '"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna",';
                    break;

                case "team_list":
                    $teams_count = count($sugar_demodata['teams']) - 1;
                    //give fake team names (East,West,North,South)
                    $returnContent .= '"' . $sugar_demodata['teams'][mt_rand(0, $teams_count)]['name'] . '",';
                    break;

                case "date":
                    //return formatted date
                    $timeStamp = strtotime('now');
                    $value = date($timedate->dbDayFormat, $timeStamp);
                    $returnContent .= '"' . $timedate->to_display_date_time($value) . '",';
                    break;

                case "datetime":
                case "datetimecombo":
                    //return formatted date time
                    $timeStamp = strtotime('now');
                    //Start with db date
                    $value = date($timedate->dbDayFormat . ' ' . $timedate->dbTimeFormat, $timeStamp);
                    //use timedate to convert to user display format
                    $value = $timedate->to_display_date_time($value);
                    //finally forma the am/pm to have a space so it can be recognized as a date field in excel
                    $value = preg_replace('/([pm|PM|am|AM]+)/', ' \1', $value);
                    $returnContent .= '"' . $value . '",';

                    break;
                case "phone":
                    $value = '(' . mt_rand(300, 999) . ') ' . mt_rand(300, 999) . '-' . mt_rand(1000, 9999);
                    $returnContent .= '"' . $value . '",';
                    break;
                case "varchar":
                    //process varchar for possible values
                    if ($field['name'] == 'first_name') {
                        $count = count($sugar_demodata['first_name_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['last_name_array'][mt_rand(0, $count)] . '",';
                    } elseif ($field['name'] == 'last_name') {
                        $count = count($sugar_demodata['last_name_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['last_name_array'][mt_rand(0, $count)] . '",';
                    } elseif ($field['name'] == 'user_name') {
                        $count = count($sugar_demodata['first_name_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['last_name_array'][mt_rand(0, $count)] . '_' . mt_rand(1, 111) . '",';
                    } elseif ($field['name'] == 'title') {
                        $count = count($sugar_demodata['titles']) - 1;
                        $returnContent .= '"' . $sugar_demodata['titles'][mt_rand(0, $count)] . '",';
                    } elseif (strpos($field['name'], 'address_street') > 0) {
                        $count = count($sugar_demodata['street_address_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['street_address_array'][mt_rand(0, $count)] . '",';
                    } elseif (strpos($field['name'], 'address_city') > 0) {
                        $count = count($sugar_demodata['city_array']) - 1;
                        $returnContent .= '"' . $sugar_demodata['city_array'][mt_rand(0, $count)] . '",';
                    } elseif (strpos($field['name'], 'address_state') > 0) {
                        $state_arr = array('CA', 'NY', 'CO', 'TX', 'NV');
                        $count = count($state_arr) - 1;
                        $returnContent .= '"' . $state_arr[mt_rand(0, $count)] . '",';
                    } elseif (strpos($field['name'], 'address_postalcode') > 0) {
                        $returnContent .= '"' . mt_rand(12345, 99999) . '",';
                    } else {
                        $returnContent .= '"",';
                    }
                    break;
                case "url":
                    $returnContent .= '"https://www.sugarcrm.com",';
                    break;

                case "enum":
                    //get the associated enum if available
                    global $app_list_strings;

                    if (isset($focus->field_name_map[$field_name]['type']) && !empty($focus->field_name_map[$field_name]['options'])) {
                        if (!empty($app_list_strings[$focus->field_name_map[$field_name]['options']])) {

                            //get the values into an array
                            $dd_values = $app_list_strings[$focus->field_name_map[$field_name]['options']];
                            $dd_values = array_values($dd_values);

                            //grab the count
                            $count = count($dd_values) - 1;

                            //choose one at random
                            $returnContent .= '"' . $dd_values[mt_rand(0, $count)] . '",';
                        } else {
                            //name of enum options array was found but is empty, return blank
                            $returnContent .= '"",';
                        }
                    } else {
                        //name of enum options array was not found on field, return blank
                        $returnContent .= '"",';
                    }
                    break;
                default:
                    //type is not matched, fill in with empty string and continue;
                    $returnContent .= '"",';
            }
        }
        $returnContent .= "\r\n";
    }
    return $returnContent;
}

//expects the field name to translate and a bean of the type being translated (to access field map and mod_strings)
function translateForExport($field_db_name, $focus)
{
    global $mod_strings, $app_strings;

    if (empty($field_db_name) || empty($focus)) {
        return false;
    }

    //grab the focus module strings
    $temp_mod_strings = $mod_strings;
    global $current_language;
    $mod_strings = return_module_language($current_language, $focus->module_dir);
    $fieldLabel = '';

    //!! first check to see if we are overriding the label for export.
    if (!empty($mod_strings['LBL_EXPORT_' . strtoupper($field_db_name)])) {
        //entry exists which means we are overriding this value for exporting, use this label
        $fieldLabel = $mod_strings['LBL_EXPORT_' . strtoupper($field_db_name)];
    }
    //!! next check to see if we are overriding the label for export on app_strings.
    elseif (!empty($app_strings['LBL_EXPORT_' . strtoupper($field_db_name)])) {
        //entry exists which means we are overriding this value for exporting, use this label
        $fieldLabel = $app_strings['LBL_EXPORT_' . strtoupper($field_db_name)];
    }//check to see if label exists in mapping and in mod strings
    elseif (!empty($focus->field_name_map[$field_db_name]['vname']) && !empty($mod_strings[$focus->field_name_map[$field_db_name]['vname']])) {
        $fieldLabel = $mod_strings[$focus->field_name_map[$field_db_name]['vname']];
    }//check to see if label exists in mapping and in app strings
    elseif (!empty($focus->field_name_map[$field_db_name]['vname']) && !empty($app_strings[$focus->field_name_map[$field_db_name]['vname']])) {
        $fieldLabel = $app_strings[$focus->field_name_map[$field_db_name]['vname']];
    }//field is not in mapping, so check to see if db can be uppercased and found in mod strings
    elseif (!empty($mod_strings['LBL_' . strtoupper($field_db_name)])) {
        $fieldLabel = $mod_strings['LBL_' . strtoupper($field_db_name)];
    }//check to see if db can be uppercased and found in app strings
    elseif (!empty($app_strings['LBL_' . strtoupper($field_db_name)])) {
        $fieldLabel = $app_strings['LBL_' . strtoupper($field_db_name)];
    } else {
        //we could not find the label in mod_strings or app_strings based on either a mapping entry
        //or on the db_name itself or being overwritten, so default to the db name as a last resort
        $fieldLabel = $field_db_name;
    }
    //strip the label of any columns
    $fieldLabel = preg_replace("/([:]|\xEF\xBC\x9A)[\\s]*$/", '', trim($fieldLabel));

    //reset the bean mod_strings back to original import strings
    $mod_strings = $temp_mod_strings;
    return $fieldLabel;
}

//call this function to return the desired order to display columns for export in.
//if you pass in an array, it will reorder the array and send back to you.  It expects the array
//to have the db names as key values, or as labels
function get_field_order_mapping($name = '', $reorderArr = '', $exclude = true)
{
    //die(var_dump($name, $reorderArr));
    //define the ordering of fields, note that the key value is what is important, and should be the db field name
    $field_order_array = array();
    $fields_to_exclude = array();

    //get an instance of the bean
    global $beanList;
    global $beanFiles;

    $bean = $beanList[$_REQUEST['module']];
    require_once($beanFiles[$bean]);
    $focus = new $bean;

    $lname = strtolower($name);
    $fields_to_exclude[$lname] = array_keys($focus->field_defs);
    $displayColumns = [];
    
    if ($lname == 'contacts') {
        $displayColumns[] = 'first_name';
        $displayColumns[] = 'last_name';
    }

    if (!empty($_REQUEST['displayColumns'])) {
        $displayColumns = $_REQUEST['displayColumns'];
    } else {
        $sugarView = new SugarView();
        $sugarView->module = $name;
        $sugarView->type = 'list';
        $metadataFile = $sugarView->getMetaDataFile();

        if (!empty($metadataFile)) {
            require $metadataFile;

            foreach ($listViewDefs[$name] as $fieldName => $field) {
                if (!($field['default'])) {
                    continue;
                }
                $displayColumns[] = strtolower($fieldName);
            }
        }
    }

    foreach ($displayColumns as $column) {
        $field_order_array[$lname][$column] = lcfirst($column);
    }

    $fields_to_exclude[$lname] = array_diff($fields_to_exclude[$lname], $displayColumns);
    $fields_to_exclude['accounts'][] = 'account_name';
    $fields_to_exclude[$lname] = array_merge($fields_to_exclude[$lname], ['system_id', 'modified_by_name', 'modified_by_name_owner', 'modified_by_name_mod', 'created_by_name', 'created_by_name_owner', 'created_by_name_mod', 'assigned_user_id', 'assigned_user_name_owner', 'assigned_user_name_mod', 'team_count', 'team_count_owner', 'team_count_mod', 'team_name_owner', 'team_name_mod', 'account_name_owner', 'account_name_mod', 'modified_user_name',  'modified_user_name_owner', 'modified_user_name_mod', 'city_owner', 'city_mod']);

    //of array is passed in for reordering, process array
    if (!empty($name) && !empty($reorderArr) && is_array($reorderArr)) {

        //make sure reorderArr has values as keys, if not then iterate through and assign the value as the key
        $newReorder = array();
        foreach ($reorderArr as $rk => $rv) {
            if (is_int($rk)) {
                $newReorder[$rv] = $rv;
            } else {
                $newReorder[$rk] = $rv;
            }
        }

        //lets iterate through and create a reordered temporary array using
        //the  newly formatted copy of passed in array
        $temp_result_arr = array();
        $lname = strtolower($name);
        if (!empty($field_order_array[$lname])) {
            foreach ($field_order_array[$lname] as $fk => $fv) {

                //if the value exists as a key in the passed in array, add to temp array and remove from reorder array.
                //Do not force into the temp array as we don't want to violate acl's
                if (array_key_exists($fk, $newReorder)) {
                    $temp_result_arr[$fk] = $newReorder[$fk];
                    unset($newReorder[$fk]);
                }
            }
        }
        //add in all the left over values that were not in our ordered list
        //array_splice($temp_result_arr, count($temp_result_arr), 0, $newReorder);
        foreach ($newReorder as $nrk => $nrv) {
            $temp_result_arr[$nrk] = $nrv;
        }

        if ($exclude) {
            //Some arrays have values we wish to exclude
            if (isset($fields_to_exclude[$lname])) {
                foreach ($fields_to_exclude[$lname] as $exclude_field) {
                    unset($temp_result_arr[$exclude_field]);
                }
            }
        }

        //return temp ordered list
        return $temp_result_arr;
    }

    //if no array was passed in, pass back either the list of ordered columns by module, or the entireorder array
    if (empty($name)) {
        return $field_order_array;
    } else {
        return $field_order_array[strtolower($name)];
    }
}

?>
