<?php

require_once 'include/MVC/Controller/SugarController.php';
require_once 'custom/include/Nit/BeanHelper/File.php';

class CustomSugarController extends SugarController
{

    protected function action_deleteattachment()
    {
        $beanHelperFile = new BeanHelperFile($this->bean);

        $fileField = $this->bean->db->quote($_REQUEST['fileField']);

        $retval = $beanHelperFile->deleteAttachment($fileField);

        echo json_encode($retval);
        sugar_cleanup(true);
    }

}
