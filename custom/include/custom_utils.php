<?php

define('VEDISOFT_CUSTOM_INCLUDE_LOADED', 1);

/**
 * Old php versions does not have sys_get_temp_dir function.
 * Function returns name of directory for temporary	files
 * @return	string 
 */
if (!function_exists('sys_get_temp_dir')) {

    function sys_get_temp_dir()
    {
        static $sys_get_temp_dir;
        if (!isset($sys_get_temp_dir)) {
            $temp_file = tmpfile();
            $stream_meta = stream_get_meta_data($temp_file);
            switch (true) {
                case ($path = realpath(dirname($stream_meta['uri']))) !== false:
                    break;
                case ($path = getenv('TMP')) !== false:
                    break;
                case ($path = getenv('TEMP')) !== false:
                    break;
                case ($path = getenv('TMPDIR')) !== false:
                    break;
                case is_windows() && (($path = getenv('WINDIR')) !== false):
                    $path = $path . DIRECTORY_SEPARATOR . 'TEMP';
                    break;
                default:
                    $path = '/var/tmp';
                    break;
            }
            $sys_get_temp_dir = $path;
        }
        return $sys_get_temp_dir;
    }

}

/**
 * Extracts subpanel id list from request
 * 
 * @return	array
 */
function getSelectedIdsFromRequest($request_key = 'subpanel_id', $module = null)
{
    require_once('include/SearchForm/SearchForm2.php');

    $id_list = array();
    if (isset($_REQUEST[$request_key]))
        $id_list = !is_array($_REQUEST[$request_key]) ? array($_REQUEST[$request_key]) : $_REQUEST[$request_key];

    if (strval($_REQUEST['select_entire_list']) !== '1')
        return $id_list;

    if (empty($module)) {
        $focus = loadBean($_REQUEST['module']);

        if (!($focus instanceof SugarBean))
            return $id_list;

        $loaded = $focus->load_relationship($_REQUEST['subpanel_field_name']);

        if (!$loaded)
            return $id_list;

        $link = & $focus->{$_REQUEST['subpanel_field_name']};
        $is_lhs = $link->_get_bean_position();
        $module = $is_lhs ? $link->_relationship->rhs_module : $link->_relationship->lhs_module;
    }
    $focus2 = loadBean($module);

    if (!($focus2 instanceof SugarBean))
        return $id_list;

    $where = SearchForm::generateSearchWhere(false, $module, true);
    $where = !empty($where) ? (is_array($where) ? (count($where) > 1 ? '(' . implode(') AND (', $where) . ')' : array_shift($where)) : $where) : 'TRUE';
    $list_query = $focus2->create_new_list_query('', $where, array('deleted' => true), array('include_custom_fields' => false), false, '', false, $focus2, true);
    $result = $focus2->db->query($list_query);
    $id_list = array();
    while (($row = $focus2->db->fetchByAssoc($result)) != null)
        array_push($id_list, $row['id']);

    return $id_list;
}

/**
 * Returns the bean object of the given bean name
 *
 * @param  string $bean
 * @return object
 */
function loadBeanByBeanName($bean)
{
    global $beanList, $beanFiles;

    if (!isset($beanList) || !isset($beanFiles))
        require('include/modules.php');

    if (!in_array($bean, $beanList) && isset($beanList[$bean]))
        $bean = $beanList[$bean];

    if (in_array($bean, $beanList)) {
        if (isset($beanFiles[$bean])) {
            require_once( $beanFiles[$bean] );
            $focus = new $bean( );
        } else {
            return false;
        }
    } else {
        return false;
    }

    return $focus;
}

/**
 * Filter non-DB fields from ORDER condition
 * 
 * @param	object	$bean	# SugarBean instance
 * @param	mixed	$order	# string with order condition or array with fields to sort by
 */
function fixOrderFieldsForQuery(& $bean, $order)
{
    if (empty($order))
        return $order;

    $is_array = is_array($order);
    if ($is_array) {
        $order_by = '';
    } else {
        preg_match('/^\s*(ORDER BY)/i', $order, $matches);
        $order_by = !empty($matches) ? $matches[1] : '';
        $order = explode(',', $order);
    }
    $new_order = array();

    $db_sources = array('db', 'custom_fields');
    foreach ($order as $key => $item) {

        if (!preg_match('/(`?([^\s`]+)`?\.)?`?([^\s`]+)`?\s*((asc|desc)\s*)?$/i', $item, $matches)) {
            continue;
        }
        if (empty($matches[2])) {
            $def = $bean->getFieldDefinition($matches[3]);
            $source = isset($def['source']) ? $def['source'] : 'db';
            $type = isset($def['type']) ? $def['type'] : 'varchar';
            if (!in_array($source, $db_sources) && $type != 'relate') {
                continue;
            }
            switch ($source) {
                case 'db':
                    $table = isset($def['table']) ? $def['table'] : $bean->table_name;
                    $item = '' . $table . '.' . $matches[3] . ' ' . $matches[5];
                    break;
                case 'custom_fields':
                    $table = $bean->table_name . '_cstm';
                    $item = '' . $table . '.' . $matches[3] . ' ' . $matches[5];
                    break;
                default:
                    $item = $matches[3] . ' ' . $matches[5];
                    break;
            }
            array_push($new_order, $item);
        } else {
            array_push($new_order, $matches[0]);
        }
    }
    if ($is_array) {
        return $new_order;
    } else {
        $order = (empty($order_by) ? '' : $order_by . ' ') .
        (count($new_order) > 0 ? implode(', ', $new_order) : 'TRUE');
    }
    return $order;
}

/**
 * function arr2urlparam needs to convert arrays
 * 		(or other types variables) to part of URL query string
 * 		for example:
 * 		$a = array('p1' => 'v1', 'p2' => 'v2', 'p3' => array( 1, 2, 3 ))
 * 		$urlPart = arr2urlparam( $a );
 * 		// $urlPart = 'p1=v1 & p2=v2 & p3[0]=1 & p3[1]=2 & p3[2]=2'
 * 		$urlPart = arr2urlparam( $a, 'ARR' );
 * 		// $urlPart = 'ARRp1=v1 & ARRp2=v2 & ARRp3[0]=1 & ARRp3[1]=2 & ARRp3[2]=2'
 * 		$urlPart = arr2urlparam( $a, '', 'ARR' );
 * 		// $urlPart = 'ARR[p1]=v1 & ARR[p2]=v2 & ARR[p3][0]=1 & ARR[p3][1]=2 & ARR[p3][2]=2'
 * 
 * @param mixed $obj object, which need would converted
 * @param string $prefix prefix will prepend
 * 		to all request valiables names, which will
 * 		create by this function
 * @param string $key name of request array,
 * 		which would content convered data, if not set
 * 
 * @return string part of ULR query string
 */
function arr2urlparam($obj, $prefix = '', $key = false)
{
    if ($prefix === '' && $key === false) {
        return http_build_query($obj);
    } else {
        $url_str = '';
        if (is_array($obj)) {
            $url_param = Array();
            foreach ($obj as $k => $o) {
                $item = arr2urlparam($o, $prefix, $key ? $key . '[' . $k . ']' : $k );
                if ($item)
                    $url_param[] = $item;
            }
            $url_str.= implode('&', $url_param);
        } else if (is_scalar($obj)) {
            $key = $prefix . $key;
            if (!empty($key))
                $url_str.= $key . '=' . urlencode($obj);
        } else if (is_null($obj)) {
            $key = $prefix . $key;
            if (!empty($key))
                $url_str.= $key . '=';
        }
        return $url_str;
    }
}

/**
 * function arr2hiddenInputs needs to convert arrays
 * 		(or other types variables) to HTML code, wich contains hidden inputs
 * 
 * @param mixed $obj object, which need would converted
 * @param string $prefix prefix will prepend
 * 		to all request valiables names, which will
 * 		create by this function
 * @param string $key name of request array,
 * 		which would content convered data, if not set
 * 
 * @return string HTML with hidden inputs
 */
function arr2hiddenInputs($obj, $prefix = '', $key = false)
{
    $str = '';
    if (is_array($obj)) {
        $param = Array();
        foreach ($obj as $k => $o) {
            $item = arr2hiddenInputs($o, $prefix, $key ? $key . '[' . $k . ']' : $k );
            if ($item)
                $param[] = $item;
        }
        $str.= implode("\n", $param);
    } else if (is_scalar($obj)) {
        $key = $prefix . $key;
        if (!empty($key))
            $str.= '<input type="hidden" name="' . $prefix . $key . '" value="' . htmlspecialchars($obj, ENT_COMPAT) . '" />';
    }
    return $str;
}

function arr2OptionStr($obj, $selected = false, $value_key = 'value', $label_key = 'label')
{
    $obj = DbRes2OptionArray($obj, false, $value_key, $label_key);
    $options = array();
    foreach ($obj as $key => $value) {
        array_push($options, '<option value="' . htmlspecialchars($key) . '"' .
        ( (string) $key == (string) $selected ? ' selected="selected"' : '' ) . '>' .
        htmlspecialchars($value) . '</option>');
    }
    return implode("\n", $options);
}

/**
 * DbRes2OptionArray convert database query resource to options array
 * 
 * @param mixed $query: recource returned by custom function (DB recource or array)
 * @param mixed $default: if false, then don`t use default options,
 * 		else if $default not empty, then use it as first element in
 * 		returned associative array
 * @param string $id: field`s name in $query, which contains 'VALUE' attr for options array
 * @param string $label: field`s name in $query, which contains 'LABEL' attr for options array
 * 
 * @return array: array of options [associative array]
 */
function DbRes2OptionArray($query, $default = false, $id = 'value', $label = 'label')
{
    global $db;
    $options = array();
    // define query is a DB resource or array
    $is_array = is_array($query);
    // if query is an array check for
    // is this array an options array (associative) or not
    $is_options = !$is_array ? null : !is_array($query[key($query)]);

    if ($is_array ? (count($query) == 0) : ($db->getRowCount($query) < 1))
    // empty options list, return default options, if posible
        return $default ? $default : $options;

    // if $query isn`t associative array, we should check id and label keys
    if (!$is_options) {
        $row = $is_array ? $query[key($query)] : $db->fetchByAssoc($query);
        if (!is_array($row) || count($row) < 1)
        // empty query row, return default options, if posible
        // we don`t want to check other rows
            return $default ? $default : $options;

        // we need to define `id` and `label` fields in row array map
        if (!isset($row[$id]) || !isset($row[$label])) {
            $keys = array_keys($row);
            $id = array_shift($keys);
            $label = array_shift($keys);
            // if empty array, then array_shift return NULL
            if (is_null($label))
                $label = $id;
        }
    }

    if ($default) {
        // define first option like first value in standart options array
        // standart options array define at language pack
        $options = is_array($default) ? $default : array(0 => $default);
    }
    if (!$is_options) {
        while ($row) {
            $options[$row[$id]] = $row[$label];
            $row = $is_array ? next($query) : $db->fetchByAssoc($query);
        }
    } else {
        foreach ($query as $key => $value) {
            $options[$key] = (string) $value;
        }
    }
    return $options;
}

/**
 * Returns calendar datetime format
 * @return	string
 */
function get_cal_datetime_format()
{
    global $timedate;

    $time_format = $timedate->get_user_time_format();
    $date_format = $timedate->get_cal_date_format();
    $time_separator = ":";
    $match = array();
    if (preg_match('/\d+([^\d])\d+([^\d]*)/s', $time_format, $match)) {
        $time_separator = $match[1];
    }
    $t23 = strpos($time_format, '23') !== false ? '%H' : '%I';
    if (!isset($match[2]) || $match[2] == '') {
        $format = "$date_format $t23$time_separator%M";
    } else {
        $pm = $match[2] == "pm" ? "%P" : "%p";
        $format = "$date_format $t23$time_separator%M$pm";
    }
    return $format;
}

/**
 * Returns current date offsetted to user's timezone
 * @param	boolean	$get		# 'date', 'time' 'stamp' or 'datetime', 'datetime' is default
 * @param	boolean	$db_format	# return date in Database format
 */
function get_current_user_datetime($format = null, $db_format = false, $offset = null)
{
    global $timedate;

    $date = $timedate->get_gmt_db_datetime();
    if ($offset) {
        $date = gmdate($timedate->get_db_date_time_format(), strtotime($date . ' ' . $offset));
    }
    $date = $timedate->to_display_date_time($date);
    $from_format = $timedate->get_date_time_format();
    switch ($format) {
        case 'date':
            $to_format = $db_format ? $timedate->dbDayFormat : $timedate->get_date_format();
            break;
        case 'time':
            $to_format = $db_format ? $timedate->dbTimeFormat : $timedate->get_time_format();
            break;
        case 'stamp':
            return strtotime($date);
            break;
        case 'datetime':
            $to_format = $db_format ? $timedate->get_db_date_time_format() : $timedate->get_date_time_format();
            break;
        default:
            $to_format = $db_format ? $timedate->get_db_date_time_format() : $from_format;
            break;
    }
    if ($to_format != $from_format) {
        $date = $timedate->swap_formats($date, $from_format, $to_format);
    }
    return $date;
}

/**
 * Convert date to database format
 * @param	date	$date
 * @param	boolean	$offset
 * @param	UserObj	$user
 */
function convert_date_to_display_format($date, $offset = true, $user = null)
{
    static $db_date_format;
    static $db_date_time_format;
    global $timedate;

    if (!isset($db_date_format)) {
        $db_date_format = $timedate->dbDayFormat;
        $db_date_time_format = $timedate->get_db_date_time_format();
    }

    if (empty($date))
        return $date;

    if (is_array($date)) {
        $date = $timedate->merge_date_time(array_shift($date), array_shift($date));
    }

    $user_date_format = $timedate->get_date_format($user);
    $user_date_time_format = $timedate->get_date_time_format(true, $user);

    if ($timedate->check_matching_format($date, $db_date_time_format)) {

        $date = $timedate->to_display_date_time($date, true, $offset, $user);
    } elseif ($timedate->check_matching_format($date . ':00', $db_date_time_format)) {

        $date = $timedate->to_display_date_time($date . ':00', true, $offset, $user);
    } elseif ($timedate->check_matching_format($date, $db_date_format)) {

        $date = $timedate->to_display_date($date, false);
    } elseif (!$timedate->check_matching_format($date, $user_date_time_format) &&
    !$timedate->check_matching_format($date . ':00', $user_date_time_format) &&
    !$timedate->check_matching_format($date, $user_date_format) &&
    function_exists('strtotime')) {

        $date = date($db_date_time_format, strtotime($date));
        $date = $timedate->to_display_date_time($date, true, false, $user);
    }

    return $date;
}

/**
 * Convert date to display format
 * @param	date	$date
 * @param	boolean	$offset
 * @param	UserObj	$user
 */
function convert_date_to_db_format($date, $offset = true, $user = null)
{
    static $db_date_format;
    static $db_date_time_format;
    global $timedate;

    if (!isset($db_date_format)) {
        $db_date_format = $timedate->dbDayFormat;
        $db_date_time_format = $timedate->get_db_date_time_format();
    }

    if (empty($date))
        return $date;

    if (is_array($date)) {
        $date = $timedate->merge_date_time(array_shift($date), array_shift($date));
    }

    $user_date_format = $timedate->get_date_format($user);
    $user_date_time_format = $timedate->get_date_time_format(true, $user);

    if ($timedate->check_matching_format($date, $user_date_time_format)) {

        $date = $timedate->merge_date_time(
        $timedate->to_db_date($date, $offset), $timedate->to_db_time($date, $offset)
        );
    } elseif ($timedate->check_matching_format($date . ':00', $user_date_time_format)) {

        $date = $timedate->merge_date_time(
        $timedate->to_db_date($date . ':00', $offset), $timedate->to_db_time($date . ':00', $offset)
        );
    } elseif ($timedate->check_matching_format($date, $user_date_format)) {

        $date = $timedate->to_db_date($date, false);
    } elseif (!$timedate->check_matching_format($date, $db_date_time_format) &&
    !$timedate->check_matching_format($date . ':00', $db_date_time_format) &&
    !$timedate->check_matching_format($date, $db_date_format) &&
    function_exists('strtotime')) {

        $date = date($user_date_time_format, strtotime($date));
        $date = $timedate->merge_date_time(
        $timedate->to_db_date($date, false), $timedate->to_db_time($date, false)
        );
    }
    return $date;
}

/**
 * returns file mime type
 * @param  string $file file path
 * @return mixed       mime type or false if undefined
 */
function getMimeType($file)
{
    if (function_exists('finfo_file')) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $file);
        finfo_close($finfo);
    } else {
        $type = mime_content_type($file);
    }

    return $type;
}

/**
 * Class contains onlu static methods
 */
final class UTIL
{

    private static $_memory_limit;
    private static $_client_ip;
    private static $_user_agent;
    private static $_random_symbols = 'abcdefjhijklnmopqrstuvwzyx0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * Define memory usage level
     * 2 types of returned result:
     * 	- in bytes ( memory_get_usage analog )
     * 	- in percents from php memory limit
     * @param	boolean	$percent # define type of returned result, if true percents will be returned
     * @return	integer/float
     */
    public static function getMemoryUsage($percent = true)
    {
        if ($percent) {
            return ( memory_get_usage() / self::getMemoryLimit() );
        } else {
            return memory_get_usage();
        }
    }

    /**
     * Return memoty limit in bytes
     * @return	integer
     */
    public static function getMemoryLimit()
    {
        if (!isset(self::$_memory_limit)) {
            self::$_memory_limit = ini_get('memory_limit');
            if (!is_numeric(self::$_memory_limit)) {
                $lastSymbol = substr(self::$_memory_limit, -1);
                self::$_memory_limit = intval(substr(self::$_memory_limit, 0, -1));
                switch ($lastSymbol) {
                    case 'G':
                    case 'g':
                        self::$_memory_limit = 1024 * self::$_memory_limit;
                    case 'M':
                    case 'm':
                        self::$_memory_limit = 1024 * self::$_memory_limit;
                    case 'K':
                    case 'k':
                        self::$_memory_limit = 1024 * self::$_memory_limit;
                        break;
                    default:
                        self::$_memory_limit = intval(self::$_memory_limit);
                        break;
                }
            }
        }
        return self::$_memory_limit;
    }

    /**
     * Increase memoty limit for $value bytes
     * @param	integer	$increase
     * @return	boolean
     */
    public function increaseMemoryLimit($increase = 0)
    {
        if (ini_set('memory_limit', self::getMemoryLimit(false) + $increase) === false) {
            return false;
        } else {
            self::$_memory_limit = null;
            return true;
        }
    }

    /**
     * Defines client ip address
     * @return	string or false in case no ip address detected
     */
    public static function defineClientIP()
    {
        if (!isset(self::$_client_ip)) {
            $client_ip = false;
            $server_keys = array(
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR'
            );
            foreach ($server_keys as $key) {
                if (array_key_exists($key, $_SERVER) === true) {
                    foreach (explode(',', $_SERVER[$key]) as $ip) {
                        if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                            self::$_client_ip = $ip;
                            break;
                        }
                    }
                }
            }
        }
        return self::$_client_ip;
    }

    /**
     * Define user agemt browser
     * supported: IE, Firefox, Chrome, Safari, Opera
     * @return string
     */
    public static function defineUserAgent()
    {
        if (!isset(self::$_user_agent)) {
            self::$_user_agent = mb_strtolower($_SERVER['HTTP_USER_AGENT']);
            if (strpos(self::$_user_agent, 'firefox') !== false)
                self::$_user_agent = 'FIREFOX';
            elseif (strpos(self::$_user_agent, 'chrome') !== false)
                self::$_user_agent = 'CHROME';
            elseif (strpos(self::$_user_agent, 'safari') !== false)
                self::$_user_agent = 'SAFARI';
            elseif (strpos(self::$_user_agent, 'opera') !== false)
                self::$_user_agent = 'OPERA';
            elseif (strpos(self::$_user_agent, 'msie') !== false)
                self::$_user_agent = 'IE';
        }
        return self::$_user_agent;
    }

    /**
     * alias to setcookie
     * 
     * @param	string	$name
     * @param	mixed	$value
     * @param	integer	$expire
     * @param	string	$path
     * @param	string	$domain
     * @param	boolean	$secure
     * @param	boolean	$httponly
     */
    public static function setCookie(
    $name, $value, $expire = 0, $path = null, $domain = null, $secure = false, $httponly = false
    )
    {
        $_COOKIE[$name] = $value;
        if (headers_sent())
            return false;

        if (is_null($domain))
            $domain = preg_replace('/\:[0-9]+$/', '', $_SERVER['HTTP_HOST']);

        if (is_null($path))
            $path = preg_replace('/[^\/]*$/', '', $_SERVER['SCRIPT_NAME']);

        $domain = $domain == 'localhost' ? false : $domain;
        return setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
    }

    /**
     * Generate random string of 62 symbols (a-zA-A0-9)
     * 
     * @param	integer	$length
     * @return	string
     */
    public static function createRandomString($length = 16)
    {
        $string = '';
        while (strlen($string) < $length) {
            $string.= substr(self::$_random_symbols, mt_rand(0, 61), 1);
        }
        return $string;
    }

    /**
     * @see http://stackoverflow.com/a/5268056/1075298.
     * 
     * Parse URL address for parts.
     * Return associative array of valid URI components, or FALSE if $url is not
     * RFC-3986 compliant. If the passed URL begins with: "www." or "ftp.", then
     * "http://" or "ftp://" is prepended and the corrected full-url is stored in
     * the return array with a key name "url". This value should be used by the caller.
     * 
     * Return value: FALSE if $url is not valid, otherwise array of URI components:
     * e.g.
     * Given: "http://www.jmrware.com:80/articles?height=10&width=75#fragone"
     * Array(
     *    [scheme] => http
     *    [authority] => www.jmrware.com:80
     *    [userinfo] =>
     *    [host] => www.jmrware.com
     *    [IP_literal] =>
     *    [IPV6address] =>
     *    [ls32] =>
     *    [IPvFuture] =>
     *    [IPv4address] =>
     *    [regname] => www.jmrware.com
     *    [port] => 80
     *    [path_abempty] => /articles
     *    [query] => height=10&width=75
     *    [fragment] => fragone
     *    [url] => http://www.jmrware.com:80/articles?height=10&width=75#fragone
     * )
     * @param	string	$url
     * @return	array or null on fail
     * @author	ridgerunner
     */
    public static function parseURL($url)
    {
        if (strpos($url, 'www.') === 0)
            $url = 'http://' . $url;
        if (strpos($url, 'ftp.') === 0)
            $url = 'ftp://' . $url;
        if (!preg_match(
        '/# Valid absolute URI having a non-empty, valid DNS host.
			        ^
			        (?P<scheme>[A-Za-z][A-Za-z0-9+\-.]*):\/\/
			        (?P<authority>
			          (?:(?P<userinfo>(?:[A-Za-z0-9\-._~!$&\'()*+,;=:]|%[0-9A-Fa-f]{2})*)@)?
			          (?P<host>
			            (?P<IP_literal>
			              \[
			              (?:
			                (?P<IPV6address>
			                  (?:                                                (?:[0-9A-Fa-f]{1,4}:){6}
			                  |                                                ::(?:[0-9A-Fa-f]{1,4}:){5}
			                  | (?:                          [0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){4}
			                  | (?:(?:[0-9A-Fa-f]{1,4}:){0,1}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){3}
			                  | (?:(?:[0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){2}
			                  | (?:(?:[0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})?::   [0-9A-Fa-f]{1,4}:
			                  | (?:(?:[0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})?::
			                  )
			                  (?P<ls32>[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}
			                  | (?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}
			                       (?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)
			                  )
			                |   (?:(?:[0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})?::   [0-9A-Fa-f]{1,4}
			                |   (?:(?:[0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})?::
			                )
			              | (?P<IPvFuture>[Vv][0-9A-Fa-f]+\.[A-Za-z0-9\-._~!$&\'()*+,;=:]+)
			              )
			              \]
			            )
			          | (?P<IPv4address>(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}
			                               (?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))
			          | (?P<regname>(?:[A-Za-z0-9\-._~!$&\'()*+,;=]|%[0-9A-Fa-f]{2})+)
			          )
			          (?::(?P<port>[0-9]*))?
			        )
			        (?P<path_abempty>(?:\/(?:[A-Za-z0-9\-._~!$&\'()*+,;=:@]|%[0-9A-Fa-f]{2})*)*)
			        (?:\?(?P<query>       (?:[A-Za-z0-9\-._~!$&\'()*+,;=:@\\/?]|%[0-9A-Fa-f]{2})*))?
			        (?:\#(?P<fragment>    (?:[A-Za-z0-9\-._~!$&\'()*+,;=:@\\/?]|%[0-9A-Fa-f]{2})*))?
			        $
			        /mx', $url, $matches))
            return null;

        switch ($matches['scheme']) {
            case 'https':
            case 'http':
                // HTTP scheme does not allow userinfo.
                if ($matches['userinfo'])
                    return null;
                break;
            case 'ftps':
            case 'ftp':

                break;
            // Unrecognized URI scheme. Default to FALSE.
            default:
                return null;
        }
        // Validate host name conforms to DNS "dot-separated-parts".
        if ($matches['regname']) {
            // If host regname specified, check for DNS conformance.
            if ($matches['host'] !== 'localhost' &&
            !preg_match(
            '/# HTTP DNS host name.
		            ^                      # Anchor to beginning of string.
		            (?!.{256})             # Overall host length is less than 256 chars.
		            (?:                    # Group dot separated host part alternatives.
		              [A-Za-z0-9]\.        # Either a single alphanum followed by dot
		            |                      # or... part has more than one char (63 chars max).
		              [A-Za-z0-9]          # Part first char is alphanum (no dash).
		              [A-Za-z0-9\-]{0,61}  # Internal chars are alphanum plus dash.
		              [A-Za-z0-9]          # Part last char is alphanum (no dash).
		              \.                   # Each part followed by literal dot.
		            )*                     # Zero or more parts before top level domain.
		            (?:                    # Explicitly specify top level domains.
		              com|edu|gov|int|mil|net|org|biz|
		              info|name|pro|aero|coop|museum|
		              asia|cat|jobs|mobi|tel|travel|xxx|
		              [A-Za-z]{2})         # Country codes are exactly two alpha chars.
		              \.?                  # Top level domain can end in a dot.
		            $                      # Anchor to end of string.
		            /ix', $matches['host']))
                return null;
        }
        $matches['url'] = $url;
        for ($i = 0; isset($matches[$i]); ++$i)
            unset($matches[$i]);

        return $matches;
    }

}

/**
 * Store errors data in external storage or session
 */
class GLOBAL_ERRORS
{

    static $globals_key = 'global_error';
    static $default_key = 'default_key';

    /**
     * Check available
     *
     * @access	public
     * @return	boolean
     */
    static function enabled()
    {
        return isset($_SESSION);
    }

    /**
     * Store error
     *
     * @access	public
     * @param	string	$error
     * @param	string	$key
     * @return	void
     */
    static function set($error, $key = null)
    {
        if (!self::enabled()) {
            return false;
        }
        if (!isset($_SESSION[self::$globals_key])) {
            $_SESSION[self::$globals_key] = array();
        }
        $key = isset($key) ? $key : self::$default_key;
        if (!isset($_SESSION[self::$globals_key][$key])) {
            $_SESSION[self::$globals_key][$key] = array();
        }
        $_SESSION[self::$globals_key][$key][] = $error;
    }

    /**
     * Get error under special key
     *
     * @access	public
     * @param	string	$error
     * @param	string	$key
     * @return	void
     */
    static function get($key = null, $clear = true)
    {
        if (!self::enabled()) {
            return false;
        }
        $key = isset($key) ? $key : self::$default_key;
        $errors = isset($_SESSION[self::$globals_key]) &&
        isset($_SESSION[self::$globals_key][$key]) &&
        !empty($_SESSION[self::$globals_key][$key]) ? $_SESSION[self::$globals_key][$key] : false;
        if ($clear)
            self::clear($key);
        return $errors;
    }

    /**
     * Clear storage
     * 
     * @param	string	$key
     */
    static function clear($key = null)
    {
        if (!self::enabled()) {
            return false;
        }
        $key = isset($key) ? $key : self::$default_key;
        if (isset($_SESSION[self::$globals_key]) &&
        isset($_SESSION[self::$globals_key][$key]))
            unset($_SESSION[self::$globals_key][$key]);
    }

}
