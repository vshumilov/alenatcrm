<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('include/ListView/ListViewSmarty.php');
require_once('custom/include/ListView/ListViewData2.php');
require_once('include/contextMenus/contextMenu.php');

class ListViewSmarty2 extends ListViewSmarty
{
    public $targetList = false;
    public $email = false;
    public $mergeduplicates = false;
    public $mailMerge = false;
    public $showMassupdateFields = false;
    /**
     * Constructor, Smarty object immediately available after
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->ss = new Sugar_Smarty();
        
        $this->lvd = new ListViewData2();
        $this->searchColumns = array();
    }

}
