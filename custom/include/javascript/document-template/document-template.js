/*global $, SUGAR, LightAjax*/
/*jslint browser:true, newcap:true, devel:true*/

(function () {
    'use strict';

    function refreshNitfiles(fields) {
        var field_name, field_data, $container, i;

        for (field_name in fields) {
            if (fields.hasOwnProperty(field_name)) {
                field_data = fields[field_name];
                $container = $('#' + field_data.form_field + '_container');

                $container.data('Nitfile').clearReadyFiles();

                for (i = 0; i < field_data.nitfiles.length; i += 1) {
                    $container.data('Nitfile').nitfileReady(field_data.nitfiles[i]);
                }
            }
        }
    }

    SUGAR.DocumentTemplate = function (params) {
        function process(bean_module, bean_id) {
            var template = $('#' + params.tpl_id).val();

            if (!template) {
                alert(params.language.MSG_SELECT_TEMPLATE_PLEASE);
                template.focus();

                return false;
            }

            function ajax(template, method, download, callback) {
                var config = {
                        module: "DocumentTemplates",
                        record: template,
                        call: method,
                        params: {
                            bean_module: bean_module,
                            bean_id: bean_id
                        }
                    };

                if (download) {
                    config.options = {
                        format: "file",
                        download: true
                    };
                }

                LightAjax(config, function (data) {
                    if (!data) {
                        return;
                    }

                    callback(data);
                });
            }

            ajax(template, 'generate', true, function () {
                ajax(template, 'refreshNitfiles', false, refreshNitfiles);
            });
        }

        $('#' + params.btn_id).on('click', function () {
            var $form = $(this).closest('form'),
                $cloned_form = $form.clone(),
                self = this;

            if ($cloned_form.attr('name') === 'EditView') {

                if (confirm(params.language.MSG_CHANGES_WOULD_BE_SAVED)) {
                    $cloned_form
                        .find('[name="action"').val('Save').end()
                        .find('[name^="return_"]').remove();

                    $.post('http://localhost/poseidon/index.php', $cloned_form.serialize(), function (response) {
                        var record = $(response).find('[name="record"]').val();

                        if (record) {
                            params.bean_id = record;
                            $form.find('[name="record"]').val(record);

                            process(params.bean_module, params.bean_id);
                        }
                    });
                }

                return false;
            }

            process(params.bean_module, params.bean_id);
        });
    };
}());