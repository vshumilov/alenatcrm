<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * @package		Vedisoft.LightAjax
 * @author		Vedisoft.Semenov Pavel [http://www.vedisoft.info]
 * 
 * @desc		AjaxHelper basic class
 * 				Using to generate response data
 */
class AjaxHelper
{

    // definition of access types for
    // different ajax queryies
    protected $_access_types_tist = array(
    );
    // definr default access type
    protected $_default_access_type = 'list';
    // name of loaded module
    protected $_module = null;
    // module bean
    protected $_bean = null;
    // ajax manager
    private $_ajax_manager = null;

    /**
     * Object constructor
     * 
     * @access	default
     * @param	object	$ajaxManager
     * @param	string	$module
     * @return	void
     */
    function __construct(& $ajax_manager, $module = null, $record = null)
    {
        $this->_ajax_manager = $ajax_manager;

        if (isset($module)) {
            global $beanList, $beanFiles;

            if (isset($beanList[$module]) && isset($beanFiles[$beanList[$module]])) {
                require_once($beanFiles[$beanList[$module]]);
                $this->_bean = new $beanList[$module];
                $this->_module = $module;
            } else {
                $this->setError(translate('ERR_LAJAX_UNDEFINED_MODULE_ACCESS') . var_export($module, true), false);
                $this->_module = $module;
                $this->_bean = null;
            }
            if (isset($record) && isset($this->_bean)) {
                if (!$this->_bean->retrieve($record)) {
                    $this->setError(translate('ERR_LAJAX_BEAN_NOT_FOUND'), false);
                }
            }
        } else {
            $this->_bean = null;
            $this->_module = 'Home';
        }
    }

    /**
     * Object destructor
     * 
     * @access	default
     * @return	void
     */
    function __destruct()
    {
        // nothing todo
    }

    /**
     * Check method avialability for external call
     * 
     * @param	string	$method
     * @return	boolean
     */
    final public function checkMethodExists($method = 'undefined')
    {
        return (method_exists($this, $method) && in_array($method, $this->getInterface()));
    }

    /**
     * Check access permition for execution of operation
     * 
     * @access	public
     * @param	string	$function
     * @return	boolean
     */
    public function checkAccess($function = 'nonexisting')
    {
        $type = isset($this->_access_types_tist[$function]) ? $this->_access_types_tist[$function] : $this->_default_access_type;
        if ($type == 'admin') {
            global $current_user;
            return (isset($current_user) &&
            ($current_user instanceof User) &&
            is_admin($current_user));
        }
        if ($this->beanLoaded()) {
            $return = $this->_bean->ACLAccess($type);
        } else {
            $type = strtolower($type);
            $return = ACLController::checkAccess($this->_module, $type, true);
        }
        return $return;
    }

    /**
     * Defines bean loaded or not
     * 
     * @access	protected
     * @return	boolean
     */
    protected function beanLoaded()
    {
        return ($this->_bean != null && $this->_bean instanceOf SugarBean);
    }

    /**
     * Calls parent object (AjaxManager Oject) methods
     * 
     * @access	protected
     * @param	string	method name
     * @return	mixed - returns of AjaxManager metho call
     */
    final protected function ajaxManagerCall($method = null)
    {
        $arguments = func_get_args();
        $method = array_shift($arguments);

        if (!method_exists($this->_ajax_manager, $method))
            die('call undefined method ' . get_class($this->_ajax_manager) . '::' . $method);

        return call_user_func_array(array($this->_ajax_manager, $method), $arguments);
    }

    /**
     * Sets error to parent object (AjaxManager Oject)
     * 
     * @access	protected
     * @param	mixed	$error
     * @param	boolean	$fatal
     * @return	void
     */
    protected function setError($error = null, $fatal = true)
    {
        $this->ajaxManagerCall('setError', $error, $fatal);
    }

    /**
     * Get errors from parent object (AjaxManager Oject)
     * 
     * @access	protected
     * @param	boolean	$fatal
     * @param	boolean	$all
     * @return	void
     */
    protected function getError($fatal = false, $all = false)
    {
        return $this->ajaxManagerCall('getError', $fatal, $all);
    }

    /**
     * Set debuging data to parent object (AjaxManager Oject)
     * 
     * @access	protected
     * @param	mixed	$data
     * @param	boolean	$var_export
     */
    protected function debugIn($data = '', $var_export = false)
    {
        return $this->ajaxManagerCall('debugIn', $data, $var_export);
    }

    /**
     * Method to test ajax works
     * returns same values as passed in
     * 
     * @access	public
     * @param	mixed	$echo
     * @return	mixed
     */
    final public function echoTest($echo = 'echo test: success')
    {
        if (count(func_num_args()) > 1)
            return func_get_args();
        else
            return $echo;
    }

    /**
     * Retun Ajax helper interface
     * 
     * @return	array
     */
    final public function getInterface()
    {
        $class_reflection = new ReflectionClass(get_class($this));
        $class_methods = $class_reflection->getMethods(ReflectionMethod::IS_PUBLIC);
        $interface = array();
        foreach ($class_methods as $class_methods) {
            if (strpos($class_methods->name, '__') === 0) {
                continue;
            }
            $interface[] = $class_methods->name;
        }
        return $interface;
    }

    public function updateRole($object_class, $object_id, $parent_id, $role, $rel_name)
    {
        $object = loadBeanByBeanName($object_class);
        $object->retrieve($object_id);

        return $object->updateRole($parent_id, $role, $rel_name);
    }

    /**
     * Returns return autocomplete options array
     * 
     * @param  array $options
     * @return array
     */
    public function autocomplete(array $options)
    {
        $options = array_merge(array(
            // substring to search for
            'term' => null,
            // db field to search in
            'source_fields' => array(),
            // in case source_fields aren't specified,
            // we can get them by given target field
            'target_field' => null,
            // search rules
            'like' => 'left', // left | right | both
            // simple sql limit
            'limit' => '6',
        ), $options);

        if (!empty($options['term'])) {
            switch ($options['like']) {
                case 'right':
                    $mask = '%' . $options['term'];
                    break;
                case 'both':
                    $mask = '%' . $options['term'] . '%';
                    break;
                case 'left':
                default:
                    $mask = $options['term'] . '%';
                    break;
            }
        }

        if ($options['limit'] !== null)
            $limit = (int) $options['limit'];

        // @TODO implement getSourceFields
        if (empty($options['source_fields']))
            $source_fields = $this->_bean->getAutocompleteSourceFields($options['target_field']);
        else if (!is_array($options['source_fields']))
            $source_fields = array($options['source_fields']);
        else
            $source_fields = $options['source_fields'];

        if (empty($source_fields))
            return array();

        $sql_queries = array();

        foreach ($source_fields as $field) {
            if (strpos($field, '.') > 0)
                list($table, $field) = explode('.', $field);
            else
                $table = $this->_bean->table_name;

            $sql_queries[] = ""
            . "SELECT DISTINCT " . dbQuoteName($field) . " AS option_name "
            . "FROM " . dbQuoteName($table) . " "
            . "WHERE "
            . "`$field` IS NOT NULL "
            . ($mask ? " AND " . dbQuoteName($field) . " LIKE " . dbQuote($mask) . " " : "")
            ;
        }

        $sql_query = implode(' UNION ', $sql_queries)
        . " ORDER BY option_name"
        . ($limit ? " LIMIT $limit " : "");

        $options = array_values(dbGetAssocArray($sql_query));

        return $options;
    }

    /**
     * Returns list of unseen notifications for user
     * @param	integer	$offset_from
     * @param	integer	$offset_to
     * @param	array	$exclude
     * @return	array
     */
    public function getUnseenNotifications($offset_from, $offset_to, $exclude = array())
    {
        return SugarNotification::getUnseenNotifications(Registry::get('current_user')->id, intval($offset_from, 10), intval($offset_to, 10), $exclude);
    }

    /**
     * Mark notification as seen
     * @param	string	$id
     * @return	boolean
     */
    public function setNotificationSeen($id)
    {
        try {
            SugarNotification::setSeen($id);
            Mission::detachNotification($id);
            return true;
        } catch (Exception $exception) {
            $GLOBALS['log']->fatal(__METHOD__ . ' - FAILED - ' . $exception->getMessage());
            return false;
        }
    }

    public function getInlineData($type, $module, $id)
    {
        if (!($bean = loadBean($module)) || !$bean->retrieve($id)) {
            Logger::trace()->fatal(__METHOD__ . ' - FAILED - cannot load module ' . $module . ' with id ' . $id);
            return array();
        }

        switch ($type) {
            case 'details':
                return $bean->getInlineDetails();
            case 'history':
                return $bean->getInlineHistory();
            default:
                Logger::trace()->fatal(__METHOD__ . ' - FAILED - type ' . $type . ' is not implemented');
                return array();
        }
    }

}
