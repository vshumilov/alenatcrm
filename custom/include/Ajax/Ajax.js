/**
 * @package		Vedisoft.LightAjax
 * @author		Vedisoft.Semenov Pavel [http://www.vedisoft.info]
 * @desc		javascript interface for LightAjax
 */
(function (window, $, undefined) {
    // reposition the status div, top and centered
    SUGAR.ajaxStatusClass.prototype.positionStatus = function () {
        this.statusDiv.style.top = document.body.scrollTop + 50 + 'px';
        statusDivRegion = YAHOO.util.Dom.getRegion(this.statusDiv);
        statusDivWidth = statusDivRegion.right - statusDivRegion.left;
        this.statusDiv.style.left = YAHOO.util.Dom.getViewportWidth() / 2 - statusDivWidth / 2 + 'px';
    }
    // do nothing if LightAjax defined
    if (window.LightAjax != undefined)
        return;
    // private LightAjax object
    var LightAjax = {
        // default options
        'default': (function () {
            var options = {
                // request options
                request: {
                    // form with additional request data
                    // @important duplicated params from  will override original request param
                    form: null,
                    // request method (get, post)
                    type: 'get',
                    // base ulr
                    base: window.location.href.replace(/\/(index.php.*)?$/, '/index.php'),
                    // request arguments
                    args: {
                        // entry point (ajax, ajaxLight, ajaxShared)
                        entryPoint: 'ajax',
                        // sugar module, optional param
                        //module: null,
                        // object record, optional param
                        //record: null,
                        // light ajax options, optional param
                        //options: {
                        // response format
                        //format: 'json',
                        // debug level (true, false)
                        //debug: false,
                        // force download (only for response type: file)
                        //download: false,
                        //},
                        // called method
                        call: null//,
                                // called method arguments
                                //params: {}
                    }
                },
                callback: {
                    success: null,
                    error: null
                },
                status_load: 'load',
                status_done: 'done'
            };
            if (window.module_sugar_grp1 != undefined)
                options.request.args.module = window.module_sugar_grp1;
            else if (window.moduleName != undefined)
                options.request.args.module = window.moduleName;
            return options;
        })(),
        // timer to manage ajaxStatus displaying
        statusTimer: null,
        // preset status messages
        statusList: function (message) {
            var statusList = {
                load: SUGAR.language.get('app_strings', 'MSG_LAJAX_LOADING'),
                save: SUGAR.language.get('app_strings', 'MSG_LAJAX_SAVING'),
                done: SUGAR.language.get('app_strings', 'MSG_LAJAX_DONE'),
                debug: SUGAR.language.get('app_strings', 'MSG_LAJAX_DEBUG'),
                error: SUGAR.language.get('app_strings', 'ERR_LAJAX_ERROR'),
                errorServer: SUGAR.language.get('app_strings', 'ERR_LAJAX_SERVER_ERROR')
            };

            return statusList[message];
        },
        // hide ajax status
        hideStatus: function () {
            ajaxStatus.hideStatus();
        },
        // shows ajax status
        showStatus: function (message, time) {
            if (this.statusTimer != undefined) {
                clearTimeout(this.statusTimer);
            }

            ajaxStatus.showStatus(message);

            if (time != undefined) {
                this.statusTimer = window.setTimeout(this.hideStatus, time);
            }
        },
        // perfoms server request
        call: function (request, callback, context, status_load, status_done) {
            if (typeof (request) == 'string')
                request = {call: request};
            if (request.form == undefined &&
                    request.type == undefined &&
                    request.base == undefined &&
                    request.args == undefined)
                request = {args: request};
            request = $.extend(true, {}, this['default'].request, request);

            if (request.args.entryPoint == 'ajaxLight')
                request.base = request.base.replace(/index\.php/, 'ajax.php');

            if (typeof (callback) == 'string') {
                status_done = context;
                status_load = callback;
                callback = null;
                context = null;
            }
            if (typeof (context) == 'string') {
                status_done = status_load;
                status_load = context;
                context = null;
            }

            if ($.isFunction(callback))
                callback = {success: callback};
            callback = $.extend(true, {}, this['default'].callback, callback);

            if (status_load == undefined)
                status_load = this['default'].status_load;
            if (status_done == undefined)
                status_done = status_load !== 'none'
                        ? this['default'].status_done
                        : 'none';

            if (request.form != undefined) {
                var formBody = $(request.form).serializeArray();
                for (var i in formBody) {
                    request.args[formBody[i].name] = formBody[i].value;
                }
            }
            var ajaxBody = this.serialize(request.args);

            // file downloading
            // TODO errors process
            if (request.args.options != undefined &&
                    request.args.options.format != undefined &&
                    request.args.options.format == 'file') {

                this.showStatus(this.statusList(status), 10000);
                var file_window = window.open(request.base + '?' + ajaxBody);
                var NS = 'LightAjax-file-' + (new Date()).getTime();
                $(file_window).on('load.' + NS, function () {
                    $(window).off('focus.' + NS);
                    $(file_window).off('load.' + NS);
                    LightAjax.success(file_window.data, null, null, callback.success, context, status_done);
                    $(file_window).get(0).close();
                    $(window).focus();
                });
                $(window).on('focus.' + NS, function () {
                    $(window).off('focus.' + NS);
                    $(file_window).off('load.' + NS);
                    LightAjax.success({
                        error: null,
                        debug: null,
                        data: true
                    }, null, null, callback.success, context, status_done);
                });
                return;
            }

            if (status_load !== 'none')
                this.showStatus(status_load);
            else
                this.hideStatus();

            $.ajax({
                url: request.base,
                type: request.type,
                data: ajaxBody,
                context: LightAjax,
                success: function (data, textStatus, XMLHttpRequest) {
                    this.success(data, textStatus, XMLHttpRequest, callback.success, context, status_done);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    this.error(XMLHttpRequest, textStatus, errorThrown, callback.error, context, status_done);
                }
            });
        },
        // process successfull request
        success: function (data, textStatus, XMLHttpRequest, callback, context, status_done) {
            if (status_done !== 'none')
                this.showStatus(this.statusList(status_done), 10000);
            else
                this.hideStatus();
            if (data.debug != undefined)
                this.showStatus(this.statusList('debug') + ': <br />' + data.debug);
            if (data.error != undefined)
                this.showStatus(this.statusList('error') + ': <br /> - ' + data.error.join('<br /> - '), 60000);
            if (typeof (data.data) != 'undefined')
                data = data.data;
            if (callback != undefined && $.isFunction(callback)) {
                if (context != undefined) {
                    callback.call(context, data);
                } else {
                    callback(data);
                }
            }
        },
        // process failed request
        error: function (XMLHttpRequest, textStatus, errorThrown, callback, context, status_done) {
            if (status_done !== 'none')
                this.showStatus(this.statusList('errorServer') + ": \n" + textStatus, 10000);
            else
                this.hideStatus();
            if (callback != undefined && $.isFunction(callback)) {
                if (context != undefined) {
                    callback.call(context, null);
                } else {
                    callback(null);
                }
            }
        },
        // serizlize request data to url encoded string
        serialize: function (obj, key) {
            var key = key || '';

            if ($.isFunction(obj))
                obj = obj();

            if ($.isArray(obj)) {
                var encoded = [];
                for (var i = 0; i < obj.length; i++) {
                    encoded.push(this.serialize(obj[i], (key !== '' ? (key + '[' + i + ']') : i)));
                }
                return encoded.join('&');
            } else if ($.isPlainObject(obj)) {
                var encoded = [];
                for (var i in obj) {
                    encoded.push(this.serialize(obj[i], (key !== '' ? (key + '[' + i + ']') : i)));
                }
                return encoded.join('&');
            } else {
                if (typeof (obj) == 'boolean')
                    obj = obj ? '1' : '0';
                else if (obj === null)
                    obj = '';
                obj = encodeURIComponent(obj);
                return (key !== '' ? (key + '=' + obj) : obj);
            }
        }
    };

    /**
     * Public interface perofms standart LightAjax request 
     * @see /include/Ajax/Help.txt # "Light Ajax JS interface"
     * @param	request
     * @param	callback
     * @param	context
     * @param	ststus
     */
    window.LightAjax = function (request, callback, context, status_load, status_done) {
        LightAjax.call(request, callback, context, status_load, status_done);
    };
// build LightAjax interface immediately
})(window, jQuery);