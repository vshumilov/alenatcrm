<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
/**
 * @package		Vedisoft.LightAjax
 * @author		Vedisoft.Semenov Pavel [http://www.vedisoft.info]
 * 
 * @desc		AjaxManager class
 */
require_once('custom/include/Ajax/AjaxHelper.php');
require_once('custom/include/Ajax/AjaxHelperLight.php');
require_once('custom/include/Ajax/AjaxHelperShared.php');

class AjaxManager
{

    // directory for temporary files
    private $_tmp_dir = '-tmp';
    // Errors
    protected $_errors = array();
    protected $_errors_fatal = array();
    // Debug
    protected $_debug_data = array();
    protected $_debug_log = false;
    // Helper object
    protected $_helper = null;
    // response data
    protected $_response_data = null;
    // header data
    protected $_headers = array();
    // default type of response
    var $response_format_default = 'json';
    // type of response
    var $response_format = null;
    // forces downloading (send special headers)
    var $force_download = true;
    // authenticate data
    var $authentication = null;
    // for light and fast requests
    var $light = false;
    // shared acces to ajax
    var $shared = false;

    /**
     * Object constructor
     * 
     * @access	default
     * @param	string	$module
     * @param	array	$options
     * @return	void
     */
    function __construct($module = null, $record = null, $options)
    {
        if (isset($options['debug']))
            $this->_debug_log = $options['debug'];
        if (isset($options['format']))
            $this->response_format = $options['format'];
        if (isset($options['light']))
            $this->light = $options['light'];
        if (isset($options['shared']))
            $this->shared = $options['shared'];
        if (isset($options['download']))
            $this->force_download = !empty($options['download']);
        if (isset($options['auth']))
            $this->authentication = $options['auth'];

        // init default Ajax helper class
        $helper_class = 'AjaxHelper';
        if ($this->light)
            $helper_class .= 'Light';
        elseif ($this->shared)
            $helper_class .= 'Shared';
        $helper_file = $helper_class . '.php';

        if (!empty($module)) {
            global $beanList, $beanFiles;
            if (isset($beanList[$module]) && isset($beanFiles[$beanList[$module]])) {
                $special_file = dirname($beanFiles[$beanList[$module]]) . DIRECTORY_SEPARATOR . $helper_file;
                $special_class = $module . $helper_class;
                $alternate_class = $beanList[$module] . $helper_class;
            } else {
                $special_file = 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . $helper_file;
                $special_class = $module . $helper_class;
            }
            if (file_exists($special_file))
                require_once($special_file);
            if (!class_exists($special_class) && isset($alternate_class))
                $special_class = $alternate_class;
            if (class_exists($special_class))
                $helper_class = $special_class;
        }
        $this->_helper = new $helper_class($this, $module, $record, $this->authentication);
    }

    /**
     * Object destructor
     * 
     * @access	default
     * @return	void
     */
    function __destruct()
    {
        // nothing todo
    }

    /**
     * Returns object errors
     * 
     * @access	public
     * @param	boolean	$fatal	# get fatal error
     * @param	boolean	$all	# get all errors
     * @return	mixed
     */
    public function getError($fatal = false, $all = false)
    {
        if (count($this->_errors) == 0)
            return null;

        if ($all)
            return ($fatal ? $this->_errors_fatal : $this->_errors);

        return ($fatal ? $this->_errors_fatal[count($this->_errors_fatal) - 1] : $this->_errors[count($this->_errors) - 1]);
    }

    /**
     * Sets errors
     * 
     * @access	public
     * @param	string	$error
     * @param	bollean	$fatal
     * @return	void
     */
    public function setError($error = null, $fatal = true)
    {
        array_push($this->_errors, $error);
        if ($fatal)
            array_push($this->_errors_fatal, $error);
    }

    /**
     * Log debugging data
     * 
     * @access	public
     * @param	string	$data
     * @param	boolean	$var_export
     * @return	void
     */
    public function debugIn($data = '', $var_export = false)
    {
        if ($var_export)
            $data = var_export($data, true);
        array_push(
        $this->_debug_data, array(
            'time' => microtime(true),
            'data' => (string) $data
        )
        );
    }

    /**
     * Return or print debug data
     * 
     * @access	public
     * @param	boolean	
     * @return	mixed
     */
    public function debugOut($print = false)
    {
        $process = create_function('$a', "\$a['time'] = explode('.', \$a['time']);return '['.date('H:i:s',intval(\$a['time'][0])).' '.sprintf('%04u', \$a['time'][1]).'] '.\$a['data'];");
        $data = array_map($process, $this->_debug_data);
        $data = implode("\n", $data);
        if ($print)
            echo($data);
        return $data;
    }

    /**
     * Register headers
     * 
     * @access	public
     * @param	string	$header
     * @param	string	$value
     * @return	void
     */
    public function setHeader($header, $value = null)
    {
        if ($value === null) {
            preg_match('/^([^:]+)\:(.+)$/ism', $header, $matches);
            $header = trim($matches[1]);
            $value = trim($matches[2]);
        }
        $this->_headers[strtolower($header)] = $header . ': ' . $value;
    }

    /**
     * Returns current header value
     * 
     * @access	public
     * @param	string	$header
     * @return	string
     */
    public function getHeader($header)
    {
        $header = strtolower($header);
        return (isset($this->_headers[$header]) ? $this->_headers[$header] : null);
    }

    /**
     * Clear header
     * 
     * @access	public
     * @param	string	$header
     * @return	void
     */
    public function clearHeader()
    {
        $header = strtolower($header);
        if (isset($this->_headers[$header]))
            unset($this->_headers[$header]);
    }

    /**
     * Perfoms header out
     * 
     * @access	public
     * @return	void
     */
    public function headerSend()
    {
        foreach ($this->_headers as $header) {
            header($header, true);
        }
    }

    /**
     * Do request
     * 
     * @access	public
     * @param	string	$function	# name of function
     * @param	array	$arguments	# func args
     * @return	mixed
     */
    public function request($function = 'nonexisting', $arguments = array())
    {
        // start ob to prevent garbage contents out
        ob_start();

        if (empty($arguments))
            $arguments = array();
        if (!is_array($arguments))
            $arguments = array($arguments);

        if (!$this->_helper->checkMethodExists($function)) {
            $this->_response_data = null;
            $this->setError(sprintf(translate('ERR_LAJAX_UNDEFINED_METHOD_CALL'), get_class($this->_helper) . '::' . $function), true);
        } elseif (!$this->_helper->checkAccess($function)) {
            $this->_response_data = null;
            $this->setError(translate('ERR_LAJAX_ACCESS_DENIED'), true);
        } else {
            try {
                $this->_response_data = call_user_func_array(array($this->_helper, $function), $arguments);
            } catch (Exception $ex) {
                $this->_response_data = null;
                $this->setError($ex->getMessage(), true);
            }
        }

        // clean and close ob
        ob_end_clean();

        // output response data
        $this->response();
    }

    /**
     * Formating response
     * 
     * @access	protected
     * @return	mixed
     */
    protected function response()
    {
        $response_format = !empty($this->response_format) ? (in_array($this->response_format, array('default', 'text', 'simple', 'raw')) ? '' : $this->response_format) : $this->response_format_default;
        $method = 'responseOut' . ucfirst(strtolower($response_format));
        if (!method_exists($this, $method)) {
            $this->setError(translate('ERR_LAJAX_UNDEFINED_RESPONSE_FORMAT') . $response_format, true);
            $this->_response_data = null;
            $method = 'responseOut';
        }
        $errors = $this->getError(!$this->_debug_log, true);
        $debug = $this->_debug_log ? $this->debugOut() : null;
        $response = array(
            'debug' => $debug,
            'data' => $this->_response_data,
            'error' => empty($errors) ? null : (!is_array($errors) ? array($errors) : $errors),
        );

        $this->$method($response);
    }

    /**
     * Returns raw data or fatal error (if exists)
     * 
     * @access	protected
     * @param	mixed	$data
     * @return	void
     */
    protected function responseOut($data)
    {
        if (empty($data['debug'])) {
            unset($data['debug']);
        } else {
            $data['debug'] = 'DEBUG: ' . "\n" . $data['debug'];
        }
        if (empty($data['error'])) {
            unset($data['error']);
        } else {
            $data['error'] = 'ERRORS: ' . "\n" . '@ERROR ' . implode("\n" . '@ERROR ', $data['error']);
        }
        $data = implode("\n", $data);
        $this->setHeader('Content-Type', 'text/plain; charset=UTF-8');
        $this->setHeader('Content-Length', strlen($data));
        $this->headerSend();
        echo($data);
    }

    /**
     * Returns passed data in JSON format
     * 
     * @param	mixed	$data
     * @return	void
     */
    protected function responseOutJson($data)
    {
        $json = getJSONobj();
        $data = $json->encode($data);
        $this->setHeader('Content-Type', 'application/json; charset=UTF-8');
        $this->setHeader('Content-Length', strlen($data));
        $this->headerSend();
        echo($data);
    }

    protected function responseOutHTML($data)
    {
        $this->setHeader('Content-Type', 'text/html; charset=UTF-8');
        $this->setHeader('Content-Length', strlen($data));
        $this->headerSend();
        echo($data);
    }

    /**
     * Returns stream contents
     * 
     * @param	mixed	$data
     * @return	void
     */
    protected function responseOutFile($data)
    {
        // disable gzip compression
        if (function_exists('gzclose')) {
            ini_set('zlib.output_compression', 'Off');
        }
        // clear ob if exists
        while (@ob_end_clean()) {
            
        }

        // start new OB to prewent errors display
        ob_start();

        $this->prepareFileInfoForOuput($data['data']);

        // refresh errors
        $data['error'] = $this->getError(true, true);

        // report about errors [break file out] or 
        // report about debug info [break file out]
        if (!empty($data['error']) || !empty($data['debug'])) {
            $data['data'] = null;
            $data = '<!DOCTYPE html>' . "\n" .
            '<html><head>' .
            '<script type="text/javascript">' .
            'window.data = ' . getJSONobj()->encode($data) . ';' .
            (!empty($data['debug']) ? 'if (typeof(window.console) != "undefined") window.console.debug(' . getJSONobj()->encode($data['debug']) . ');' : '') .
            (!empty($data['error']) ? 'if (typeof(window.console) != "undefined") ' . getJSONobj()->encode($data['error']) . '.map(function (error) {window.console.error(error);});' : '') .
            '</script>' .
            '</head><body></body></html>';
            $this->responseOutHTML($data);
            return;
        }

        // set headers
        if ($this->getHeader('Pragma') === null)
            $this->setHeader('Pragma', 'public');
        if ($this->getHeader('Cache-Control') === null)
            $this->setHeader('Cache-Control', 'no-cache');
        if ($this->getHeader('Content-Type') === null)
            $this->setHeader('Content-Type', $data['data']['mime']);
        if ($this->getHeader('Content-Transfer-Encoding') === null)
            $this->setHeader('Content-Transfer-Encoding', 'binary');
        if ($this->getHeader('Content-Disposition') === null && $this->force_download)
            $this->setHeader('Content-Disposition', 'attachment; filename="' . $data['data']['name'] . '"');

        // by default set content length like file size, so we set $byte_start = 0 and $byte_end = last file byte
        if (!isset($_SERVER['HTTP_RANGE']) || strpos($_SERVER['HTTP_RANGE'], 'bytes=') !== 0) {
            $byte_start = 0;
            $byte_end = $data['data']['size'] - 1;
            if (!isset($data['data']['stream']) || empty($data['data']['stream'])) {
                $this->setHeader('Content-Length', $data['data']['size']);
            }
        }
        // if client request only range of target file's so we should set right bytes range to transfer
        // and send special headers
        else {
            list($byte_start, $byte_end) = explode('-', preg_replace('/([0-9]*\,[0-9]*-)+/', '', substr($_SERVER['HTTP_RANGE'], 6)));
            if ($byte_end === '') {
                $byte_end = $data['data']['size'] - 1;
            }
            if ($byte_start === '') {
                $byte_start = $data['data']['size'] - $byte_end - 1;
                $byte_end = $data['data']['size'] - 1;
            }

            // set special headers for audio-records
            // this headers make browser to understand, that it got all bytes of audio file
            header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0') . ' 206 Partial Content');
            $this->setHeader('Accept-Ranges', 'bytes');
            $this->setHeader('Content-Range', 'bytes ' . $byte_start . '-' . $byte_end . '/' . $data['data']['size']);
            $this->setHeader('Content-Length', $byte_end - $byte_start + 1);
        }

        // output headers
        ob_end_clean();
        $this->headerSend();
        flush();
        set_time_limit(0);

        // set buffer size (64k)
        $buffer = 64 * 1024;
        // start file output
        rewind($data['data']['file']);
        if ($byte_start > 0) {
            fseek($data['data']['file'], $byte_start);
        }

        $bytes_must_send = $byte_end - $byte_start + 1;
        $bytes_sent = 0;
        while (!feof($data['data']['file']) && ($bytes_must_send > $bytes_sent)) {
            if ($bytes_sent + $buffer > $bytes_must_send) {
                $buffer = $bytes_must_send - $bytes_sent;
            }
            $bytes_sent += $buffer;
            echo(fread($data['data']['file'], $buffer));
            flush();
        }

        if (isset($data['data']['unlink']) && $data['data']['unlink'] === true) {
            $stream_meta = stream_get_meta_data($data['data']['file']);
            $remove_file_path = realpath($stream_meta['uri']);
            fclose($data['data']['file']);
            if (!empty($remove_file_path)) {
                @unlink($remove_file_path);
            }
        } else {
            fclose($data['data']['file']);
        }
    }

    /**
     * Fix file information (extension, length and so on)
     * 
     * @access	protected
     * @param	array	$file information about file would sent
     */
    protected function prepareFileInfoForOuput(& $file)
    {
        if (!isset($file['file'])) {
            $this->setError(translate('ERR_LAJAX_UNDEFINED_FILE_RESOURCE'), true);
            return;
        }
        if (is_windows() && is_string($file['file'])) {
            $file['file'] = mb_convert_encoding($file['file'], 'CP1251', 'UTF-8');
        }
        if (is_string($file['file']) && file_exists($file['file'])) {
            $file['file'] = @fopen($file['file'], 'r');
        }
        if (!is_resource($file['file'])) {
            $this->setError(translate('ERR_LAJAX_UNDEFINED_FILE_RESOURCE'), true);
            return;
        }
        $stream_meta = stream_get_meta_data($file['file']);
        $filePath = realpath($stream_meta['uri']);
        if (empty($file['mime'])) {
            if (file_exists($filePath) &&
            function_exists('mime_content_type')) {
                $file['mime'] = mime_content_type($filePath);
            }
            $file['mime'] = !empty($file['mime']) ? $file['mime'] : 'application/octet-stream';
        }
        if (empty($file['name'])) {
            $pathinfo = pathinfo($filePath);
            if (empty($pathinfo['extension'])) {
                $pathinfo['extension'] = 'txt';
                $file['name'] = $pathinfo['basename'] . '.' . $pathinfo['extension'];
            } else {
                $file['name'] = $pathinfo['basename'];
            }
            $file['name'] = str_replace('"', "'", $file['name']);
        }
        if (empty($file['size']) && empty($file['stream'])) {
            fseek($file['file'], 0, SEEK_END);
            $file['size'] = ftell($file['file']);
        }
        if (strpos(mb_strtolower($_SERVER['HTTP_USER_AGENT']), 'msie') !== false) {
            $file['name'] = urlencode($file['name']);
            $file['name'] = str_replace('+', ' ', $file['name']);
        }
        return $file;
    }

    /**
     * Generate path name for temporary file
     * @param	string	$user_id
     * @param	string	$salt
     * @return	string
     */
    public function getTMPFilename($salt = '')
    {
        return ($this->getTMPDirname() . DIRECTORY_SEPARATOR . md5(microtime(true) . $salt) . '.tmp');
    }

    /**
     * Return name of temp directory
     * @return	string
     */
    public function getTMPDirname()
    {
        if (!is_dir($this->_tmp_dir)) {
            sugar_mkdir($this->_tmp_dir);
        }
        return $this->_tmp_dir;
    }

}
