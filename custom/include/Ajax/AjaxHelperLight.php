<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * @package		Vedisoft.LightAjax
 * @author		Vedisoft.Semenov Pavel [http://www.vedisoft.info]
 * 
 * @desc		AjaxHelperLight basic class
 * 				Used for generate response data
 * 				Uses only for process requests that do not require authorization
 * 				and do not required SugarBean object using
 */
class AjaxHelperLight extends AjaxHelper
{

    /**
     * Object constructor
     * 
     * @access	default
     * @param	object	$ajaxManager
     * @param	string	$module
     * @param	string	$record
     * @return	void
     */
    function __construct(& $ajax_manager, $module = null, $record = null)
    {
        parent::__construct($ajax_manager);
        $this->_module = $module;
        $this->_record = $record;
    }

    /**
     * Check access permition for execution of operation
     * @TODO: rewrite method
     * 
     * @access	public
     * @param	string	$function
     * @return	boolean
     */
    public function checkAccess($function = 'nonexisting')
    {
        return true;
    }

}
