<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
/**
 * @package		Vedisoft.LightAjax
 * @author		Vedisoft.Semenov Pavel [http://www.vedisoft.info]
 * 
 * @desc		Init sugar crm for light requests,
 * 				Init itme ~0.05s
 * 				Loaded objects:
 * 				- sugar_config
 * 				- database
 * 				- timedate
 * 				- localozation
 * 				... etc
 * 
 * 				Nor SugarBean, nor child classes would loaded
 */
// set level for error reporting
if (version_compare(PHP_VERSION, '5.3.0', '<'))
    $error_reporting = E_ALL ^ E_NOTICE ^ E_WARNING;
else if (version_compare(PHP_VERSION, '5.4.0', '>='))
    $error_reporting = E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED ^ E_STRICT;
else
    $error_reporting = E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED;
ini_set('error_reporting', $error_reporting);
ini_set('display_errors', true);
mb_internal_encoding('utf-8');

// init config array
require_once('config.php');
if (file_exists('config_override.php'))
    require_once('config_override.php');
$GLOBALS['sugar_config'] = $sugar_config;


// init utils
require_once('include/utils.php');

// clean request from xss
clean_special_arguments();
clean_incoming_data();

// init php special setting, needed for sugar
setPhpIniSettings();

// load featrures needed for sugar works
require_once('sugar_version.php');
require_once('include/database/DBManager.php');
require_once('include/database/DBManagerFactory.php');
require_once('include/dir_inc.php');
require_once('include/modules.php');
require_once('include/Localization/Localization.php');
require_once('include/TimeDate.php');
require_once('include/utils/file_utils.php');
require_once('include/SugarLogger/LoggerManager.php');

// Start session
if (!empty($sugar_config['session_dir']))
    session_save_path($sugar_config['session_dir']);
if (can_start_session())
    session_start();

// init cpesial objects
$GLOBALS['log'] = LoggerManager::getLogger('SugarCRM');
$GLOBALS['timedate'] = new TimeDate();
$GLOBALS['locale'] = new Localization();
$GLOBALS['db'] = DBManagerFactory::getInstance();
$GLOBALS['db']->resetQueryCount();

$GLOBALS['sugar_version'] = $sugar_version;
$GLOBALS['sugar_flavor'] = $sugar_flavor;
$GLOBALS['js_version_key'] = md5($GLOBALS['sugar_config']['unique_key'] . $GLOBALS['sugar_version']);

// prevent sugar_cleanup fails
unset($sugar_config['dbconfig']);
