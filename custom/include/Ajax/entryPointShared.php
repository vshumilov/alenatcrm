<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
/**
 * @package		Vedisoft.LightAjax
 * @author		Vedisoft.Semenov Pavel [http://www.vedisoft.info]
 * 
 * @desc		Entry point for shared requests
 */
// set reporting level
if (version_compare(PHP_VERSION, '5.3.0', '<'))
    $error_reporting = E_ALL ^ E_NOTICE ^ E_WARNING;
else if (version_compare(PHP_VERSION, '5.4.0', '>='))
    $error_reporting = E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED ^ E_STRICT;
else
    $error_reporting = E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED;
ini_set('error_reporting', $error_reporting);

// init AjaxObject class
require_once('include/Ajax/AjaxManager.php');

// define ajax request params
$module = isset($_REQUEST['module']) ? $_REQUEST['module'] : null;
$record = isset($_REQUEST['record']) ? $_REQUEST['record'] : null;
$ajaxOptions = isset($_REQUEST['options']) ? $_REQUEST['options'] : array();
$ajaxCall = isset($_REQUEST['call']) ? $_REQUEST['call'] : null;
$ajaxCallParams = isset($_REQUEST['params']) ? $_REQUEST['params'] : array();
$ajaxOptions['shared'] = true;

// init AjaxManager
$ajax = new AjaxManager($module, $record, $ajaxOptions);

// process request
$ajax->request($ajaxCall, $ajaxCallParams);

// exit without saving
die();
