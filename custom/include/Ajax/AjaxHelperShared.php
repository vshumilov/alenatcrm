<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * @package		Vedisoft.LightAjax
 * @author		Vedisoft.Semenov Pavel [http://www.vedisoft.info]
 * 
 * @desc		AjaxHelperShared basic class
 * 				Used for generate response data
 * 				Uses only for process requests that do not require authorization
 */
class AjaxHelperShared extends AjaxHelper
{

    private $_authentication_salt = null;
    private $_current_user = null;

    /**
     * Object constructor
     * 
     * @access	default
     * @param	object	$ajax_manager
     * @param	string	$module
     * @param	string	$record
     * @param	string	$authentication
     * @return	void
     */
    function __construct(& $ajax_manager, $module = null, $record = null, $authentication = null)
    {
        global $sugar_config;
        parent::__construct($ajax_manager, $module, $record);
        $this->_authentication_salt = isset($sugar_config['lajax_auth_salt']) ? $sugar_config['lajax_auth_salt'] : 'sugar-crm';
        $this->login($authentication);
    }

    /**
     * Object destructor
     * 
     * @access	default
     * @return	void
     */
    function __destruct()
    {
        $this->logout();
        parent::__destruct();
    }

    /**
     * Check access permition for execution of operation
     * @TODO: rewrite method
     * 
     * @access	public
     * @param	string	$function
     * @return	boolean
     */
    public function checkAccess($function = 'nonexisting')
    {
        return true;
    }

    /**
     * Decode user authentication data
     * see self::encodeAuthentication for more details
     * 
     * @access	protected
     * @param	string	$authentication
     * @return	array
     */
    final protected function decodeAuthentication($authentication)
    {
        if (strlen($authentication) >= 64) {
            $authentication_decoded = array();
            $authentication_decoded['salt'] = substr($authentication, 0, -64);
            $authentication_decoded['user'] = substr($authentication, -64, -32);
            $authentication_decoded['hash'] = substr($authentication, -32);
            return $authentication_decoded;
        } else {
            return null;
        }
    }

    /**
     * Encode user authentication data
     * 
     * @access	public
     * @param	string	$user
     * @param	string	$hash
     * @return	string
     */
    final public function encodeAuthentication($user, $hash, $salt = null)
    {
        if (!isset($salt)) {
            $salt = $this->_authentication_salt;
        }
        $client_address = str_replace(' ', '', strtolower(UTIL::defineClientIP()));
        $client_agent = str_replace(' ', '', strtolower($_SERVER['HTTP_USER_AGENT']));
        if (!preg_match('/^[0-9a-f]{32}$/i', $hash)) {
            $hash = md5($hash);
        }
        $hash = str_replace(' ', '', strtolower($hash));
        $salt = str_replace(' ', '', strtolower($salt));
        $user = md5($user);
        $hash = md5($salt . $hash . $client_address . $client_agent);
        return $salt . $user . $hash;
    }

    /**
     * Simulate sugar user login
     * 
     * @access	protected
     * @param	array	user auth data (user_name, user_hash)
     * @return	boolean
     */
    final protected function login($authentication = null)
    {
        global $current_user;

        $this->logout();
        $authentication = $this->decodeAuthentication($authentication);
        if (empty($authentication)) {
            return false;
        }
        $client_address = str_replace(' ', '', strtolower(UTIL::defineClientIP()));
        $client_agent = str_replace(' ', '', strtolower($_SERVER['HTTP_USER_AGENT']));

        $colunm_user_name = 'MD5(`user_name`)';
        $colunm_user_hash = 'MD5(CONCAT(' .
        '\'' . $current_user->db->quote($authentication['salt']) . '\', ' .
        'LOWER(REPLACE(`user_hash`, \' \', \'\')), ' .
        '\'' . $current_user->db->quote($client_address) . '\', ' .
        '\'' . $current_user->db->quote($client_agent) . '\'' .
        '))';
        $sql = 'SELECT `id` ' .
        'FROM `users` ' .
        'WHERE ' .
        '`deleted` = 0 AND ' .
        '`status` = \'Active\' AND ' .
        $colunm_user_name . ' = \'' . $current_user->db->quote($authentication['user']) . '\' AND ' .
        $colunm_user_hash . ' = \'' . $current_user->db->quote($authentication['hash']) . '\'';

        $result = $current_user->db->query($sql);
        $result = $current_user->db->fetchByAssoc($result);
        if (isset($result) &&
        isset($result['id'])) {
            $this->_current_user = clone $current_user;
            $current_user = new User();
            $current_user->retrieve($result['id']);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Restore original current_user, ends of login simulation
     * 
     * @access	protected
     * @return	boolean
     */
    final protected function logout()
    {
        if (!isset($this->_current_user)) {
            return false;
        } else {
            global $current_user;
            $current_user = clone $this->_current_user;
            $this->_current_user = null;
            return true;
        }
    }

}
