
</div>
</div>
<!-- END of container-fluid, pageContainer divs -->
<!-- Start Footer Section -->
{if $AUTHENTICATED}
    <!-- Start generic footer -->
    <footer>
    	<div class="footer_left">
    		<a id="admin_options">&copy; {$MOD.LBL_SUITE_SUPERCHARGED}</a>
            <a id="powered_by">&copy; {$MOD.LBL_SUITE_POWERED_BY}</a>
    	</div>
    	<div class="footer_right">
    		
    		<a onclick="SUGAR.util.top();" href="javascript:void(0)">Вверх</a>
    	</div>
        <div id="copyright_data">
            <div id="dialog2" title="{$MOD.LBL_SUITE_SUPERCHARGED}">
                <p>{$MOD.LBL_SUITE_DESC1}</p>
                <br>
                <p>{$MOD.LBL_SUITE_DESC2}</p>
                <br>
                <p>{$MOD.LBL_SUITE_DESC3}</p>
                <br>
            </div>
            <div id="dialog" title="&copy; {$MOD.LBL_SUITE_POWERED_BY}">
                <p>{$COPYRIGHT}</p>
            </div>
        </div>
    </footer>
    <!-- END Generic Footer -->
{/if}
<!-- END Footer Section -->
{literal}
    <script>

        //qe_init function sets listeners to click event on elements of 'quickEdit' class
        if (typeof(DCMenu) != 'undefined') {
            DCMenu.qe_refresh = false;
            DCMenu.qe_handle;
        }
        function qe_init() {

            //do not process if YUI is undefined
            if (typeof(YUI) == 'undefined' || typeof(DCMenu) == 'undefined') {
                return;
            }


            //remove all existing listeners.  This will prevent adding multiple listeners per element and firing multiple events per click
            if (typeof(DCMenu.qe_handle) != 'undefined') {
                DCMenu.qe_handle.detach();
            }

            //set listeners on click event, and define function to call
            YUI().use('node', function (Y) {
                var qe = Y.all('.quickEdit');
                var refreshDashletID;
                var refreshListID;

                //store event listener handle for future use, and define function to call on click event
                DCMenu.qe_handle = qe.on('click', function (e) {
                    //function will flash message, and retrieve data from element to pass on to DC.miniEditView function
                    ajaxStatus.flashStatus(SUGAR.language.get('app_strings', 'LBL_LOADING'), 800);
                    e.preventDefault();
                    if (typeof(e.currentTarget.getAttribute('data-dashlet-id')) != 'undefined') {
                        refreshDashletID = e.currentTarget.getAttribute('data-dashlet-id');
                    }
                    if (typeof(e.currentTarget.getAttribute('data-list')) != 'undefined') {
                        refreshListID = e.currentTarget.getAttribute('data-list');
                    }
                    DCMenu.miniEditView(e.currentTarget.getAttribute('data-module'), e.currentTarget.getAttribute('data-record'), refreshListID, refreshDashletID);
                });

            });
        }

        qe_init();

        SUGAR_callsInProgress++;
        SUGAR._ajax_hist_loaded = true;
        if (SUGAR.ajaxUI)
            YAHOO.util.Event.onContentReady('ajaxUI-history-field', SUGAR.ajaxUI.firstLoad);




        $(function(){

            // fix for campaign wizard
            if($('#wizard').length) {

                // footer fix
                var bodyHeight = $('body').height();
                var contentHeight = $('#pagecontent').height() + $('#wizard').height();
                var fieldsetHeight = $('#pagecontent').height() + $('#wizard fieldset').height();
                var height = bodyHeight < contentHeight ? contentHeight : bodyHeight;
                if(fieldsetHeight > height) {
                    height = fieldsetHeight;
                }
                height += 50;
                $('#content').css({
                    'min-height': height + 'px'
                });

                // uploader fix
                $('#step1_uploader').css({
                    position: 'relative',
                    top: ($('#wizard').height() - 90) + 'px'
                });
            }
        });

    </script>
{/literal}
</div>
</body>
</html>