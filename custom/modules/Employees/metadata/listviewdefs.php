<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Employees'] = array(
    'NAME' => array(
        'width' => '20',
        'label' => 'LBL_LIST_NAME',
        'link' => true,
        'related_fields' => array('last_name', 'first_name'),
        'orderBy' => 'last_name',
        'default' => true),
    'ACLROLE_NAME' => array(
        'width' => '15',
        'label' => 'LBL_ACLROLE_NAME',
        'link' => true,
        'module' => 'ACLRoles',
        'id' => 'ACLROLE_ID',
        'default' => true,
        'sortable' => false,
        'related_fields' =>
        array(
            'aclrole_id',
        ),
    ),
    'REPORTS_TO_NAME' => array(
        'width' => '15',
        'label' => 'LBL_LIST_REPORTS_TO_NAME',
        'link' => true,
        'sortable' => false,
        'default' => false),
    'EMAIL1' => array(
        'width' => '15',
        'label' => 'LBL_LIST_EMAIL',
        'link' => true,
        'customCode' => '{$EMAIL1_LINK}{$EMAIL1}</a>',
        'default' => true,
        'sortable' => false),
    'PHONE_WORK' => array(
        'width' => '10',
        'label' => 'LBL_LIST_PHONE',
        'link' => true,
        'default' => true),
    'EMPLOYEE_STATUS' => array(
        'width' => '10',
        'label' => 'LBL_LIST_EMPLOYEE_STATUS',
        'link' => false,
        'default' => true),
    'DATE_ENTERED' => array(
        'width' => '10',
        'label' => 'LBL_DATE_ENTERED',
        'default' => false),
);
