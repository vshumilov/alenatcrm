<?php 
 //WARNING: The contents of this file are auto-generated



if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Employee']['fields']['aclrole_name'] = [
    'name' => 'aclrole_name',
    'rname' => 'name',
    'id_name' => 'aclrole_id',
    'vname' => 'LBL_ACLROLE_NAME',
    'type' => 'relate',
    'table' => 'acl_roles',
    'join_name' => 'aclroles',
    'isnull' => 'true',
    'module' => 'ACLRoles',
    'dbType' => 'varchar',
    'link' => 'aclroles',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
];
$dictionary['Employee']['fields']['aclrole_id'] = [
    'name' => 'aclrole_id',
    'vname' => 'LBL_ACLROLE_ID',
    'type' => 'id',
    'audited' => true,
    'source' => 'non-db',
];
$dictionary['Employee']['fields']['aclroles'] = [
    'name' => 'aclroles',
    'type' => 'link',
    'source' => 'non-db',
    'side' => 'right',
    'vname' => 'LBL_ROLES',
];

?>