<?php

$viewdefs ['Documents'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'maxColumns' => '2',
            'form' =>
            array(
                'hidden' =>
                array(
                    0 => '<input type="hidden" name="old_id" value="{$fields.document_revision_id.value}">',
                ),
                'buttons' =>
                array(
                    'EDIT',
                    'DELETE',
                ),
            ),
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'LBL_DOCUMENT_INFORMATION' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'lbl_document_information' =>
            array(
                array(
                    array(
                        'name' => 'filename',
                        'displayParams' =>
                        array(
                            'onchangeSetFileNameTo' => 'document_name',
                        ),
                    ),
                    
                ),
                array(
                    'document_name',
                    
                ),
                array(
                    array('name' => 'active_date', 'displayParams' => array('required' => true)),
                    
                ),
                array(
                    array(
                        'name' => 'description',
                        
                    ),
                ),
            ),
        ),
    ),
);
