<?php

$searchdefs ['Documents'] = array(
    'layout' =>
    array(
        'basic_search' =>
        array(
            0 => 'name',
        ),
        'advanced_search' =>
        array(
            'document_name' =>
            array(
                'name' => 'document_name',
                'default' => true,
                'width' => '10%',
            ),
            'status' =>
            array(
                'type' => 'varchar',
                'label' => 'LBL_DOC_STATUS',
                'width' => '10%',
                'default' => true,
                'name' => 'status',
            ),
            'template_type' =>
            array(
                'type' => 'enum',
                'label' => 'LBL_TEMPLATE_TYPE',
                'width' => '10%',
                'default' => true,
                'name' => 'template_type',
                'displayParams' => array(
                    'size' => '1',
                    'width' => '47%'
                )
            ),
            'category_id' =>
            array(
                'name' => 'category_id',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'size' => '1',
                    'width' => '47%'
                )
            ),
            'subcategory_id' =>
            array(
                'name' => 'subcategory_id',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'size' => '1',
                    'width' => '47%'
                )
            ),
            'assigned_user_id' =>
            array(
                'name' => 'assigned_user_id',
                'label' => 'LBL_ASSIGNED_TO',
                'default' => true,
                'width' => '10%',
            ),
            'active_date' =>
            array(
                'name' => 'active_date',
                'default' => true,
                'width' => '10%',
            ),
            'exp_date' =>
            array(
                'name' => 'exp_date',
                'default' => true,
                'width' => '10%',
            ),
        ),
    ),
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
);
