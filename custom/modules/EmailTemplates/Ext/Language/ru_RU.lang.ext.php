<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_EMAIL_ATTACHMENT'] = 'Вложение уведомления';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Список шаблонов уведомлений';
$mod_strings['LBL_MODULE_NAME'] = 'Шаблоны уведомлений';
$mod_strings['LBL_MODULE_TITLE'] = 'Шаблоны уведомлений: ГЛАВНАЯ';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Создать шаблон уведомления';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Поиск шаблонов уведомлений';
$mod_strings['LNK_NEW_EMAIL_TEMPLATE'] = 'Создать шаблон уведомления';
$mod_strings['LNK_NEW_SEND_EMAIL'] = 'Создать уведомление';
$mod_strings['LNK_SENT_EMAIL_LIST'] = 'Отправленные уведомления';
$mod_strings['LNK_EMAIL_TEMPLATE_LIST'] = 'Шаблоны уведомлений';


?>