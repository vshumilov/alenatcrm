<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(77, 'updateMeetingGeocodeInfo', 'modules/Meetings/MeetingsJjwg_MapsLogicHook.php','MeetingsJjwg_MapsLogicHook', 'updateMeetingGeocodeInfo'); 

$hook_array['before_save'] = [];

$hook_array['before_save'][] = [
    1,
    'set system number',
    'custom/modules/Meetings/Hook/Meeting.php',
    'MeetingHookMeeting',
    'setSystemNumber'
];

$hook_array['before_save'][] = [
    2,
    'set name',
    'custom/modules/Meetings/Hook/Meeting.php',
    'MeetingHookMeeting',
    'setName'
];

$hook_array['before_save'][] = [
    3,
    'set notifing mail scan',
    'custom/modules/Meetings/Hook/Meeting.php',
    'MeetingHookMeeting',
    'setNotifingMailScan'
];

$hook_array['before_save'][] = [
    4,
    'is changed notifing date',
    'custom/modules/Meetings/Hook/Email.php',
    'MeetingHookEmail',
    'isChangedNotifingDate'
];

$hook_array['after_save'][] = [
    1,
    'send email to pedestrian if notifing date was set',
    'custom/modules/Meetings/Hook/Email.php',
    'MeetingHookEmail',
    'sendEmailSetNotifingDate'
];

