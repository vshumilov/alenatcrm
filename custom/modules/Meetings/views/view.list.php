<?php

require_once 'custom/include/MVC/View/views/view.list.php';
require_once 'custom/modules/Meetings/BeanHelper/Meeting.php';
require_once 'custom/include/ListView/ListViewSmarty2.php';
require_once 'custom/modules/Users/BeanHelper/User.php';

class MeetingsViewList extends ViewList {

    function prepareSearchForm() {
        $searchMode = 'basic';

        if ($_REQUEST['searchFormTab'] == 'advanced_search') {
            $searchMode = 'advanced';
        }

        if (empty($_REQUEST['status_' . $searchMode])) {
            $statusList = MeetingBeanHelperMeeting::getStatusList($this->bean);
            $selectedList[] = array_keys($statusList)[0];
            $_REQUEST['status_' . $searchMode] = $selectedList;
        }

        global $current_user;
        $userHelper = new UserBeanHelperUser($current_user);
        $userHelper->setSearchByCurrentOrganization();

        parent::prepareSearchForm();
    }

    function preDisplay() {
        $this->lv = new ListViewSmarty2();
    }

}
