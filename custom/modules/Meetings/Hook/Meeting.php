<?php

require_once 'custom/modules/Meetings/BeanHelper/Meeting.php';
require_once 'custom/modules/Meetings/BeanHelper/Email.php';
require_once 'custom/modules/Checkwatersupplysystems/BeanHelper/Checkwatersupplysystem.php';
require_once 'custom/modules/Checkwatersupplysystems/BeanHelper/Email.php';

class MeetingHookMeeting
{

    public function setSystemNumber(Meeting $bean)
    {
        global $timedate;
        
        $bean->date_start = date($timedate->get_db_date_time_format(), time());
        $bean->date_end = date($timedate->get_db_date_time_format(), time());
        
        if (!empty($_REQUEST['record'])) {
            return;
        }

        $meetingBeanHelper = new MeetingBeanHelperMeeting($bean);

        $bean->system_number = $meetingBeanHelper->getSystemNumber();
    }
    
    public function setName(Meeting $bean)
    {
        $meetingBeanHelper = new MeetingBeanHelperMeeting($bean);

        $bean->name = $meetingBeanHelper->getName();
    }

    public function setNotifingMailScan(Meeting $bean)
    {
        SugarNitfile::saveBeanNitfiles($bean, 'notifing_mail_scan');
        
        if (empty($_REQUEST['record'])) {
            return;
        }
        
        global $timedate;
        
        $meeting = new Meeting();
        $record = $meeting->db->quote($_REQUEST['record']);
        $meeting->retrieve($record);
        
        if ($timedate->to_display_date($bean->date_abonent_registration) == $meeting->date_abonent_registration) {
            return;
        }
        
        $meetingBeanHelper = new MeetingBeanHelperMeeting($bean);

        $bean = $meetingBeanHelper->setNotifingMailScanUser();
        
        $emailBeanHelper = new MeetingBeanHelperEmail($bean);

        $result = $emailBeanHelper->sendEmailVezirAbonentNotify();
        
        $checkwatersupplysystem = new Checkwatersupplysystem();
        
        $checkwatersupplysystemBeanHelper = new CheckwatersupplysystemBeanHelperCheckwatersupplysystem($checkwatersupplysystem);
        $result = $checkwatersupplysystemBeanHelper->createByAccount($bean->account_id);
        
        if (!$result) {
            return;
        }
        
        $emailCheckwatersupplysystem = new CheckwatersupplysystemBeanHelperEmail($checkwatersupplysystemBeanHelper->bean);
        $emailCheckwatersupplysystem->sendEmailCreationCheckwatersupplysystem();
    }
}
