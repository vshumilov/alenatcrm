<?php

require_once 'custom/modules/Meetings/BeanHelper/Email.php';
require_once 'custom/modules/Meetings/BeanHelper/Meeting.php';

class MeetingHookEmail
{
    public function isChangedNotifingDate(Meeting $bean)
    {
        $record = $bean->db->quote($_REQUEST['record']);

        if (empty($record) && empty($bean->date_notifing)) {
            return;
        }
        
        global $timedate;
        
        if (!empty($record)) {
            $meeting = new Meeting();
            $meeting->retrieve($record);

            if ($meeting->date_notifing == $timedate->to_display_date_time($bean->date_notifing)) {
                return;
            }
        }
        
        $bean->is_changed_notified_date = true;
    }
    
    public function sendEmailSetNotifingDate(Meeting $bean)
    {
        if (empty($bean->is_changed_notified_date)) {
            return;
        }

        $emailBeanHelper = new MeetingBeanHelperEmail($bean);
        
        $emailBeanHelper->sendEmailSetNotifingDate();
    }
}

