<?php

require_once 'custom/modules/Opportunities/BeanHelper/Email.php';

class MeetingBeanHelperEmail extends OpportunityBeanHelperEmail
{
    public function sendEmailVezirAbonentNotify()
    {
        $sugarConfig = SugarConfig::getInstance();
        $templateId = $sugarConfig->get('vezir_abonent_notify_template_id');

        if (empty($templateId)) {
            $GLOBALS['log']->info("sendEmailVezirAbonentNotify: vezir_abonent_notify_template_id doesn't exist in config.php");
            return;
        }

        $emailTemplate = BeanFactory::getBean('EmailTemplates', $templateId);

        $emails = $this->getEmailsByRoles(['администратор']);

        $result = $this->sendEmail($emails, $emailTemplate);

        return $result;
    }
    
    public function sendEmailSetNotifingDate()
    {
        $sugarConfig = SugarConfig::getInstance();
        $templateId = $sugarConfig->get('set_notifingdate_meeting_notification_template_id');

        if (empty($templateId)) {
            $GLOBALS['log']->info("sendEmailSetNotifingDate: set_notifingdate_meeting_notification_template_id doesn't exist in config.php");
            return;
        }

        $emailTemplate = BeanFactory::getBean('EmailTemplates', $templateId);

        $emails = $this->getEmailsByRoles(['пешеход']);

        $email = $this->sendEmail($emails, $emailTemplate);

        return $email;
    }
}

