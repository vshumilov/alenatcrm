<?php

require_once 'custom/modules/Meetings/BeanHelper/Email.php';
require_once 'custom/include/Nit/BeanHelper/Role.php';

class MeetingBeanHelperMeeting extends BeanHelperRole {

    public function getName() {
        if (empty($this->bean->account_id)) {
            return '';
        }

        $account = BeanFactory::getBean('Accounts', $this->bean->account_id);

        if (empty($account->id)) {
            return;
        }
        
        $name = translate('LBL_PART_OF_NAME') . ' ' . $account->name;
        
        return $name;
    }

    public function getSystemNumber() {
        $count = $this->getCountSystemNumbers();

        $system_number = $count + 1;

        return $system_number;
    }

    public function getCountSystemNumbers() {
        $query = "
        SELECT
            count(id) AS count
        FROM
            meetings
        ";

        $count = $this->bean->db->getOne($query);

        return $count;
    }

    public function setNotifingMailScanUser() {
        if (empty($this->bean->id)) {
            return $this->bean;
        }

        global $timedate;

        $meeting = new Meeting();
        $record = $meeting->db->quote($this->bean->id);
        $meeting->retrieve($record);

        if ($timedate->to_display_date($this->bean->date_abonent_registration) == $meeting->date_abonent_registration) {
            return $this->bean;
        }

        global $current_user;

        $this->bean->notifing_mail_user_id = $current_user->id;

        return $this->bean;
    }

    public static function getStatusList($focus, $field = 'status', $value = '', $view = 'DetailView') {
        global $current_user;
        $opportunityTypeDom = translate('meeting_status_dom');
        
        $statusList = [];
        
        if (!empty($_REQUEST['searchFormTab']) || (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'index')) {
            $opportunityTypeDom = array_merge(['' => 'Все'], $opportunityTypeDom);
            $statusList[''] = $opportunityTypeDom[''];
        }
        
        $current_user->load_relationship('aclroles');

        $roles = $current_user->aclroles->getBeans();

        $statusList['Planned'] = $opportunityTypeDom['Planned'];

        if (self::isHasRoleWithName($roles, 'пешеход')) {
            $statusList['Sent'] = $opportunityTypeDom['Sent'];
            return $statusList;
        }

        if (self::isHasRoleWithName($roles, 'начальник') || self::isHasRoleWithName($roles, 'администратор')) {
            $statusList = $opportunityTypeDom;
            return $statusList;
        }

        return $statusList;
    }

}
