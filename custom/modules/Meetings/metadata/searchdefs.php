<?php

$searchdefs ['Meetings'] = array(
    'layout' =>
    array(
        'basic_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'status' =>
            array(
                'name' => 'status',
                'default' => true,
                'width' => '10%',
                'options' => 'meeting_status_dom_with_empty'
            ),
            'city' =>
            array(
                'name' => 'city',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'date_notifing' =>
            array(
                'name' => 'date_notifing',
                'label' => 'LBL_DATE_NOTIFING_SHORT',
                'default' => true,
                'width' => '10%',
            ),
        ),
        'advanced_search' =>
        array(
            'system_number' =>
            array(
                'name' => 'system_number',
                'default' => true,
                'width' => '10%',
            ),
            'incoming_number' =>
            array(
                'name' => 'incoming_number',
                'default' => true,
                'width' => '10%',
            ),
            'description' =>
            array(
                'name' => 'description',
                'default' => true,
                'type' => 'varchar',
                'width' => '10%',
            ),
            'date_notifing' =>
            array(
                'name' => 'date_notifing',
                'default' => true,
                'width' => '10%',
            ),
            'date_entered' =>
            array(
                'name' => 'date_entered',
                'default' => true,
                'width' => '10%',
            ),
            'date_abonent_registration' =>
            array(
                'name' => 'date_abonent_registration',
                'default' => true,
                'width' => '10%',
            ),
            'account_name' =>
            array(
                'label' => 'LBL_ACCOUNT_NAME',
                'width' => '10%',
                'default' => true,
                'name' => 'account_name',
            ),
            'assigned_user_id' =>
            array(
                'name' => 'assigned_user_id',
                'label' => 'LBL_ASSIGNED_TO',
                'default' => true,
                'width' => '10%',
            ),
            'city' =>
            array(
                'name' => 'city',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'status' =>
            array(
                'name' => 'status',
                'default' => true,
                'width' => '10%',
                'options' => 'meeting_status_dom_with_empty',
                'displayParams' => array(
                    'width' => '47%',
                )
            ),
            
        ),
    ),
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
);
