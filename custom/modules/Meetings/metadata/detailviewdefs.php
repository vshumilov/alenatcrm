<?php

$viewdefs ['Meetings'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'includes' => array(
                array('file' => 'modules/Reminders/Reminders.js'),
            ),
            'form' =>
            array(
                'buttons' =>
                array(
                    'EDIT',
                    'DELETE',
                ),
                'hidden' => array(
                    '<input type="hidden" name="isSaveAndNew">',
                    '<input type="hidden" name="status">',
                    '<input type="hidden" name="isSaveFromDetailView">',
                    '<input type="hidden" name="isSave">',
                ),
                'headerTpl' => 'modules/Meetings/tpls/detailHeader.tpl',
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
        ),
        'panels' =>
        array(
            'LBL_MEETING_INFORMATION' =>
            array(
                array(
                    array(
                        'name' => 'system_number',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'status',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'date_entered',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'date_notifing',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'account_name',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'assigned_user_name',
                        'label' => 'LBL_ASSIGNED_TO_NAME',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'description',
                        'comment' => 'Full text of the note',
                        'label' => 'LBL_DESCRIPTION',
                    ),
                ),
            ),
            'LBL_NOTIFING_MAIL_PANEL' => array(
                array(
                    'notifing_mail_scan',
                ),
                array(
                    'date_abonent_registration',
                ),
                array(
                    'incoming_number',
                ),
                array(
                    'notifing_mail_user_name'
                )
            ),
            'LBL_DOCUMENT_TEMPLATE_PANEL' => array(
                array(
                    'document_template',
                ),
                array(
                    'nitfiles',
                )
            ),
        ),
    ),
);
