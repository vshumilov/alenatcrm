<?php

$viewdefs ['Meetings'] = array(
            'QuickCreate' =>
            array(
                'templateMeta' =>
                array(
                    'includes' => array(
                        array('file' => 'modules/Reminders/Reminders.js'),
                    ),
                    'maxColumns' => '2',
                    'form' =>
                    array(
                        'hidden' =>
                        array(
                            '<input type="hidden" name="isSaveAndNew" value="false">',
                            '<input type="hidden" name="is_ajax_call" value="1">',
                        ),
                        'buttons' =>
                        array(
                            array(
                                'customCode' => '<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button" onclick="SUGAR.meetings.fill_invitees();this.form.action.value=\'Save\'; this.form.return_action.value=\'DetailView\'; {if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}this.form.return_id.value=\'\'; {/if}return check_form(\'EditView\');" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}">',
                            ),
                            'CANCEL',
                            array(
                                'customCode' => '<input title="{$MOD.LBL_SEND_BUTTON_TITLE}" class="button" onclick="this.form.send_invites.value=\'1\';SUGAR.meetings.fill_invitees();this.form.action.value=\'Save\';this.form.return_action.value=\'EditView\';this.form.return_module.value=\'{$smarty.request.return_module}\';return check_form(\'EditView\');" type="submit" name="button" value="{$MOD.LBL_SEND_BUTTON_LABEL}">',
                            ),
                            array(
                                'customCode' => '{if $fields.status.value != "Held"}<input title="{$APP.LBL_CLOSE_AND_CREATE_BUTTON_TITLE}" accessKey="{$APP.LBL_CLOSE_AND_CREATE_BUTTON_KEY}" class="button" onclick="SUGAR.meetings.fill_invitees(); this.form.status.value=\'Held\'; this.form.action.value=\'Save\'; this.form.return_module.value=\'Meetings\'; this.form.isDuplicate.value=true; this.form.isSaveAndNew.value=true; this.form.return_action.value=\'EditView\'; this.form.return_id.value=\'{$fields.id.value}\'; return check_form(\'EditView\');" type="submit" name="button" value="{$APP.LBL_CLOSE_AND_CREATE_BUTTON_LABEL}">{/if}',
                            ),
                        ),
                    ),
                    'widths' =>
                    array(
                        array(
                            'label' => '10',
                            'field' => '30',
                        ),
                        array(
                            'label' => '10',
                            'field' => '30',
                        ),
                    ),
                    'javascript' => '<script type="text/javascript">{$JSON_CONFIG_JAVASCRIPT}</script>
{sugar_getscript file="cache/include/javascript/sugar_grp_jsolait.js"}
<script>toggle_portal_flag();function toggle_portal_flag()  {literal} { {/literal} {$TOGGLE_JS} {literal} } {/literal} </script>',
                    'useTabs' => false,
                ),
                'panels' =>
                array(
                    'LBL_MEETING_INFORMATION' =>
                    array(
                        array(
                            array(
                                'name' => 'status',
                                'fields' =>
                                array(
                                    array(
                                        'name' => 'status',
                                    ),
                                ),
                            ),

                        ),
                        array(
                            array(
                                'name' => 'date_notifing',
                            ),

                        ),
                        array(
                            array(
                                'name' => 'account_name',
                            ),

                        ),
                        array(
                            array(
                                'name' => 'assigned_user_name',
                                'label' => 'LBL_ASSIGNED_TO_NAME',
                            ),

                        ),
                        array(
                            array(
                                'name' => 'description',
                                'comment' => 'Full text of the note',
                                'label' => 'LBL_DESCRIPTION',
                            ),
                        ),
                    ),
                    'LBL_NOTIFING_MAIL_PANEL' => array(
                        array(
                            'notifing_mail_scan',
                        ),
                        array(
                            'date_abonent_registration',
                        ),
                        array(
                            'incoming_number',

                        ),
                    ),
                    'LBL_DOCUMENT_TEMPLATE_PANEL' => array(
                        array(
                            'nitfiles',
                        ),
                    ),
                ),
            ),
);
