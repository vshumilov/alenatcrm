<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'Meetings'),
    ),
    'where' => '',
    'list_fields' => array(
        'system_number' => array(
            'name' => 'system_number',
            'vname' => 'LBL_SYSTEM_NUMBER',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '20%',
        ),
        'name' => array(
            'name' => 'name',
            'vname' => 'LBL_LIST_SUBJECT',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '30%',
        ),
        'status' => array(
            'name' => 'status',
            'vname' => 'LBL_STATUS',
            'width' => '20%',
        ),
        'date_notifing' => array(
            'name' => 'date_notifing',
            'vname' => 'LBL_DATE_NOTIFING',
            'width' => '10%',
        ),
        'incoming_number' => array(
            'name' => 'incoming_number',
            'vname' => 'LBL_INCOMING_NUMBER',
            'width' => '10%',
        ),
        'edit_button' => array(
            'vname' => 'LBL_EDIT_BUTTON',
            'widget_class' => 'SubPanelEditButton',
            'width' => '2%',
        ),
        'remove_button' => array(
            'vname' => 'LBL_REMOVE',
            'widget_class' => 'SubPanelRemoveButton',
            'width' => '2%',
        ),
        'recurring_source' => array(
            'usage' => 'query_only',
        ),
    ),
);
?>
