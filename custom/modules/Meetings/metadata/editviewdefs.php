<?php

$viewdefs ['Meetings'] = array(
    'EditView' =>
    array(
        'templateMeta' =>
        array(
            'includes' => array(
                array('file' => 'modules/Reminders/Reminders.js'),
            ),
            'maxColumns' => '2',
            'form' =>
            array(
                'hidden' =>
                array(
                    '<input type="hidden" name="isSaveAndNew" value="false">',
                ),
                'buttons' =>
                array(
                    array(
                        'customCode' => '<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" id ="SAVE_HEADER" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="SUGAR.meetings.fill_invitees();document.EditView.action.value=\'Save\'; document.EditView.return_action.value=\'DetailView\'; {if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}document.EditView.return_id.value=\'\'; {/if} formSubmitCheck();"type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}">',
                    ),
                    'CANCEL',
                    array(
                        'customCode' => '{if $fields.status.value != "Held"}<input title="{$APP.LBL_CLOSE_AND_CREATE_BUTTON_TITLE}" id="close_and_create_new_header" class="button" onclick="SUGAR.meetings.fill_invitees(); document.EditView.status.value=\'Held\'; document.EditView.action.value=\'Save\'; document.EditView.return_module.value=\'Meetings\'; document.EditView.isDuplicate.value=true; document.EditView.isSaveAndNew.value=true; document.EditView.return_action.value=\'EditView\'; document.EditView.return_id.value=\'{$fields.id.value}\'; formSubmitCheck();"type="button" name="button" value="{$APP.LBL_CLOSE_AND_CREATE_BUTTON_LABEL}">{/if}',
                    ),
                ),
                'headerTpl' => 'modules/Meetings/tpls/header.tpl',
                'buttons_footer' =>
                array(
                    array(
                        'customCode' => '<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" id ="SAVE_FOOTER" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="SUGAR.meetings.fill_invitees();document.EditView.action.value=\'Save\'; document.EditView.return_action.value=\'DetailView\'; {if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}document.EditView.return_id.value=\'\'; {/if} formSubmitCheck();"type="button" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}">',
                    ),
                    'CANCEL',
                    array(
                        'customCode' => '{if $fields.status.value != "Held"}<input title="{$APP.LBL_CLOSE_AND_CREATE_BUTTON_TITLE}" id="close_and_create_new_footer" class="button" onclick="SUGAR.meetings.fill_invitees(); document.EditView.status.value=\'Held\'; document.EditView.action.value=\'Save\'; document.EditView.return_module.value=\'Meetings\'; document.EditView.isDuplicate.value=true; document.EditView.isSaveAndNew.value=true; document.EditView.return_action.value=\'EditView\'; document.EditView.return_id.value=\'{$fields.id.value}\'; formSubmitCheck();"type="button" name="button" value="{$APP.LBL_CLOSE_AND_CREATE_BUTTON_LABEL}">{/if}',
                    ),
                ),
                'footerTpl' => 'modules/Meetings/tpls/footer.tpl',
            ),
            'widths' =>
            array(
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'javascript' => '<script type="text/javascript">{$JSON_CONFIG_JAVASCRIPT}</script>
{sugar_getscript file="cache/include/javascript/sugar_grp_jsolait.js"}
<script>toggle_portal_flag();function toggle_portal_flag()  {ldelim} {$TOGGLE_JS} {rdelim} 
function formSubmitCheck(){ldelim}if(check_form(\'EditView\')){ldelim}document.EditView.submit();{rdelim}{rdelim}</script>',
            'useTabs' => false,
        ),
        'panels' =>
        array(
            'LBL_MEETING_INFORMATION' =>
            array(
                array(
                    array(
                        'name' => 'status',
                        'fields' =>
                        array(
                            array(
                                'name' => 'status',
                            ),
                        ),
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'date_notifing',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'account_name',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'assigned_user_name',
                        'label' => 'LBL_ASSIGNED_TO_NAME',
                    ),
                    
                ),
                array(
                    array(
                        'name' => 'description',
                        'comment' => 'Full text of the note',
                        'label' => 'LBL_DESCRIPTION',
                    ),
                ),
            ),
            'LBL_NOTIFING_MAIL_PANEL' => array(
                array(
                    'notifing_mail_scan',
                ),
                array(
                    'date_abonent_registration',
                ),
                array(
                    'incoming_number',
                    
                ),
            ),
            'LBL_DOCUMENT_TEMPLATE_PANEL' => array(
                array(
                    'nitfiles',
                ),
            ),
        ),
    ),
);
