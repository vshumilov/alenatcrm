<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$searchFields['Meetings'] = array(
    'name' => array('query_type' => 'default'),
    'city' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'city_id' =>
    array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            m.id 
                       FROM 
                            meetings AS m
                       JOIN
                            accounts AS a 
                       ON
                            m.account_id = a.id
                       WHERE 
                            a.city_id LIKE '%{0}%' AND 
                            a.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'description' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            m1.id 
                       FROM 
                            meetings AS m1 
                       WHERE 
                            m1.description LIKE '%{0}%' AND 
                            m1.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'contact_name' => array('query_type' => 'default', 'db_field' => array('contacts.first_name', 'contacts.last_name')),
    'date_start' => array('query_type' => 'default'),
    'current_user_only' => array('query_type' => 'default', 'db_field' => array('assigned_user_id'), 'my_items' => true, 'vname' => 'LBL_CURRENT_USER_FILTER', 'type' => 'bool'),
    'assigned_user_id' => array('query_type' => 'default'),
    'status' => array(
        'query_type' => 'default',
    ),
    'favorites_only' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'checked_only' => true,
        'subquery' => "SELECT favorites.parent_id FROM favorites
			                    WHERE favorites.deleted = 0
			                        and favorites.parent_type = 'Meetings'
			                        and favorites.assigned_user_id = '{1}'",
        'db_field' => array('id')),
    //Range Search Support
    'range_date_entered' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_entered' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_entered' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_date_modified' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_modified' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_modified' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_date_start' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_start' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_start' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_date_end' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_end' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_end' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_date_abonent_registration' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_abonent_registration' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_abonent_registration' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_date_notifing' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_notifing' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_notifing' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
        //Range Search Support 				
);
