<?php

$listViewDefs ['Meetings'] = array(
    'SET_COMPLETE' =>
    array(
        'width' => '1%',
        'label' => 'LBL_LIST_CLOSE',
        'link' => true,
        'sortable' => false,
        'default' => true,
        'related_fields' =>
        array(
            0 => 'status',
        ),
    ),
    'SYSTEM_NUMBER' =>
    array(
        'width' => '20%',
        'label' => 'LBL_SYSTEM_NUMBER',
        'link' => true,
        'default' => true,
    ),
    'NAME' =>
    array(
        'width' => '40%',
        'label' => 'LBL_LIST_SUBJECT',
        'link' => true,
        'default' => true,
    ),
    'STATUS' =>
    array(
        'width' => '20%',
        'label' => 'LBL_STATUS',
        'default' => true,
    ),
    'ACCOUNT_NAME' =>
    array(
        'width' => '20%',
        'label' => 'LBL_ACCOUNT_NAME',
        'module' => 'Accounts',
        'id' => 'ACCOUNT_ID',
        'link' => true,
        'default' => true,
        'sortable' => false,
        'related_fields' =>
        array(
            'account_id',
        ),
    ),
    'DATE_NOTIFING' =>
    array(
        'width' => '15%',
        'label' => 'LBL_DATE',
        'default' => false,
    ),
    'INCOMING_NUMBER' => array(
        'width' => '10%',
        'label' => 'LBL_INCOMING_NUMBER',
        'default' => false
    ),
    'DATE_ABONENT_REGISTRATION' => array(
        'width' => '15%',
        'label' => 'LBL_DATE_ABONENT_REGISTRATION',
        'default' => false
    ),
    'ASSIGNED_USER_NAME' =>
    array(
        'width' => '2%',
        'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => false,
    ),
    
);
