<?php 
 //WARNING: The contents of this file are auto-generated


$mod_strings['LBL_DOCUMENT_TEMPLATE_PANEL'] = 'Создать документы';
$mod_strings['LBL_DOCUMENT_TEMPLATE'] = 'Документ';
$mod_strings['LBL_NITFILES'] = 'Файлы';
$mod_strings['LBL_NITFILES_PANEL'] = 'Файлы';


$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Уведомления абонентов';
$mod_strings['LBL_MEETING'] = 'Уведомление';
$mod_strings['LBL_MODULE_NAME'] = 'Уведомления абонентов';
$mod_strings['LBL_MODULE_TITLE'] = 'Уведомления абонентов - ГЛАВНАЯ';
$mod_strings['LNK_MEETING_LIST'] = 'Уведомления абонентов';
$mod_strings['LBL_EXTNOSTART_HEADER'] = 'Ошибка: Уведомление не может быть создано';
$mod_strings['ERR_DELETE_RECORD'] = 'Для удаления уведомления должен быть определен номер записи.';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Список уведомлений';
$mod_strings['LBL_LIST_MY_MEETINGS'] = 'Мои уведомления';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Создать уведомление';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Поиск уведомления';
$mod_strings['LBL_CREATOR'] = 'Автор уведомления';
$mod_strings['LNK_NEW_APPOINTMENT'] = $mod_strings['LBL_NEW_FORM_TITLE'];
$mod_strings['LNK_NEW_MEETING'] = $mod_strings['LBL_NEW_FORM_TITLE'];
$mod_strings['LNK_IMPORT_MEETINGS'] = 'Импорт уведомлений';	
$mod_strings['NTC_REMOVE_INVITEE'] = 'Вы действительно хотите удалить это приглашение из данного уведомления?';
$mod_strings['LBL_HOST_EXT_MEETING'] = 'Дата отправки уведомления';
$mod_strings['LBL_EXTNOT_HEADER'] = 'Ошибка: Это не Ваше уведомление';
$mod_strings['LBL_DATE'] = 'Дата отправки уведомления';
$mod_strings['LBL_DATE_NOTIFING'] = 'Дата отправки уведомления';
$mod_strings['LBL_DATE_NOTIFING_SHORT'] = 'Дата отправки';

$mod_strings['LBL_DATE_ENTERED'] = 'Дата формирования';
$mod_strings['LBL_DOCUMENTS_SUBPANEL_TITLE'] = 'Документы';
$mod_strings['LBL_SYSTEM_NUMBER'] = 'Системный номер';
$mod_strings['LBL_LIST_CLOSE'] = 'Завизировать';

$mod_strings['LBL_INCOMING_NUMBER'] = 'Номер входящего у абонента';
$mod_strings['LBL_DATE_ABONENT_REGISTRATION'] = 'Дата регистрации у абонента';
$mod_strings['LBL_NOTIFING_MAIL_SCAN'] = 'Скан уведомления';
$mod_strings['LBL_NOTIFING_MAIL_PANEL'] = 'Скан уведомительного письсма';
$mod_strings['LBL_NOTIFING_MAIL_TO_NAME'] = 'Загрузил';

$mod_strings['LBL_EMAILS_SUBPANEL_TITLE'] = 'Emails';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Абонент';
$mod_strings['LBL_PART_OF_NAME'] = 'Уведомление';

?>