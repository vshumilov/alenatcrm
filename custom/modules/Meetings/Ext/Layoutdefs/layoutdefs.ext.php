<?php 
 //WARNING: The contents of this file are auto-generated



unset($layout_defs['Meetings']['subpanel_setup']['contacts']);
unset($layout_defs['Meetings']['subpanel_setup']['users']);
unset($layout_defs['Meetings']['subpanel_setup']['leads']);
unset($layout_defs['Meetings']['subpanel_setup']['history']);
unset($layout_defs['Meetings']['subpanel_setup']['securitygroups']);

$layout_defs['Meetings']['subpanel_setup']['documents_meetings'] = [
    'order' => 1,
    'module' => 'Documents',
    'subpanel_name' => 'default',
    'sort_order' => 'asc',
    'sort_by' => 'id',
    'title_key' => 'LBL_DOCUMENTS_SUBPANEL_TITLE',
    'get_subpanel_data' => 'documents',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
        array(
            'widget_class' => 'SubPanelTopSelectButton',
            'mode' => 'MultiSelect',
        ),
    ),
];

global $current_user;
if ($current_user->isAdmin()) {
    $layout_defs['Meetings']['subpanel_setup']['emails'] = [
        'order' => 2,
        'module' => 'Emails',
        'subpanel_name' => 'ForHistory',
        'get_subpanel_data' => 'emails',
        'generate_select' => true,
        'title_key' => 'LBL_EMAILS_SUBPANEL_TITLE',
        'top_buttons' => array(
            array('widget_class' => 'SubPanelTopArchiveEmailButton'),
        ),
    ];
}

?>