<?php

$viewdefs ['Opportunities'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    'EDIT',
                    'DELETE',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'LBL_CONTACTS_PANEL' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'LBL_CONTACTS_PANEL' => array(
                array(
                    'opportunity_type',
                ),
                array(
                    'contact_name',
                ),
                array(
                    'contacts_gender',
                ),
                array(
                    'contacts_birth_date',
                ),
                array(
                    'contacts_phone_mobile',
                ),
                array(
                    'contacts_phone_work',
                ),
                array(
                    'contacts_email',
                ),
            ),
            'LBL_CONTACTS_PASSPORT_PANEL' => array(
                array(
                    array(
                        'name' => 'contacts_passport_series',
                    ),
                ),
                array(
                    array(
                        'name' => 'contacts_passport_number',
                    ),
                ),
                array(
                    array(
                        'name' => 'contacts_passport_date_of_issue',
                    ),
                ),
                array(
                    array(
                        'name' => 'contacts_passport_division_code',
                    ),
                ),
                array(
                    array(
                        'name' => 'contacts_passport_issued_by',
                    ),
                ),
                array(
                    array(
                        'name' => 'contacts_passport_address',
                    ),
                ),
            ),
            'LBL_TOURS_PANEL' => array(
                array(
                    'tour_name',
                ),
                array(
                    'tours_country_name',
                ),
                array(
                    'tours_operator_name',
                ),
                array(
                    'tours_city_name',
                ),
                array(
                    'tours_route',
                ),
                array(
                    'tours_hotel',
                ),
                array(
                    'tours_foodoption_name',
                ),
                array(
                    'tours_apartment_name',
                ),
                array(
                    array('name' => 'start_date_tour'),
                ),
                array(
                    array('name' => 'end_date_tour'),
                ),
                array(
                    'tours_days',
                ),
                array(
                    array(
                        'name' => 'tourists',
                        'label' => 'LBL_TOURISTS_BIG'
                    ),
                ),
                array(
                    'count_places',
                ),
                array(
                    'count_places_with_treatment',
                ),
                array(
                    array('name' => 'price_name'),
                ),
                array(
                array(
                        'name' => 'prepayment',
                        'label' => '{$MOD.LBL_PREPAYMENT} ({$CURRENCY})',
                    ),
                ),
                array(
                    array(
                        'name' => 'amount',
                        'label' => '{$MOD.LBL_AMOUNT} ({$CURRENCY})',
                    ),
                ),
                array(
                    array(
                        'name' => 'amount_with_discount',
                        'label' => '{$MOD.LBL_AMOUNT_WITH_DISCOUNT} ({$CURRENCY})',
                    ),
                ),
                array(
                    'tours_transfer',
                ),
                array(
                    'tours_excursion',
                ),
                array(
                    'tours_meeting',
                ),
                array(
                    'tours_visa_support',
                ),
                array(
                    'tours_medical_insurance',
                ),
                array(
                    'tours_insurance_on_his_own_recognizance',
                ),
                array(
                    'tours_additional_offers',
                ),
            ),
            'default' =>
            array(
                array(
                    'system_number',
                ),
                array(
                    array('name' => 'name'),
                ),
                array(
                    array(
                        'name' => 'date_entered',
                        'type' => 'datetimecombo'
                    ),
                ),
                array(
                    array(
                        'name' => 'booked_datetime',
                        'type' => 'datetimecombo'
                    ),
                ),
                array(
                    array(
                        'name' => 'agreement_sent_datetime',
                        'type' => 'datetimecombo'
                    ),
                ),
                array(
                    array(
                        'name' => 'prepayment_datetime',
                        'type' => 'datetimecombo'
                    ),
                ),
                array(
                    array(
                        'name' => 'paid_datetime',
                        'type' => 'datetimecombo'
                    ),
                ),
                array(
                    array(
                        'name' => 'closed_datetime',
                        'type' => 'datetimecombo'
                    ),
                ),
                array(
                    array(
                        'name' => 'canceled_datetime',
                        'type' => 'datetimecombo'
                    ),
                ),
                array(
                    'client_from_source',
                ),
                array(
                    'assigned_user_name',
                ),
            ),
            'LBL_DOCUMENT_TEMPLATE_PANEL' => array(
                array(
                    'document_template',
                ),
                array(
                    'nitfiles',
                ),
                array(
                    'agreement_pdf',
                ),
            ),
        ),
    ),
);
