<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$viewdefs = array(
    'Opportunities' =>
    array(
        'QuickCreate' =>
        array(
            'templateMeta' =>
            array(
                'maxColumns' => '2',
                'widths' =>
                array(
                    0 =>
                    array(
                        'label' => '10',
                        'field' => '30',
                    ),
                    1 =>
                    array(
                        'label' => '10',
                        'field' => '30',
                    ),
                ),
                'javascript' => '{$PROBABILITY_SCRIPT}',
            ),
            'panels' =>
            array(
                'default' =>
                array(
                    array(
                        'account_name',
                    ),
                    array(
                        'opportunity_type',
                    ),
                    array(
                        'application_scan',
                    ),
                ),
                'LBL_COVERING_LETTER_SCAN_PANEL' => array(
                    array(
                        'covering_letter_scan',
                    ),
                    array(
                        'date_abonent_received',
                    )
                ),
                'LBL_DIFFERENCES_ACT_PANEL' => array(
                    array(
                        'differences_act_scan',
                    ),
                    array(
                        'date_differences_act_registration'
                    )
                ),
                'LBL_AGREEMENT_PANEL' => array(
                    array(
                        'agreement_scan',
                    ),
                    array(
                        'date_agreement_registration'
                    )
                ),
                'LBL_RESPOND_ON_REJECT_PANEL' => array(
                    array(
                        'respond_on_reject',
                    ),
                    array(
                        'date_respond_on_reject'
                    )
                ),
                'LBL_RESPOND_ON_REJECT_SCAN_PANEL' => array(
                    array(
                        'respond_on_reject_scan',
                    ),
                    array(
                        'date_respond_on_reject_registration'
                    ),
                    array(
                        'respond_on_reject_scan_number',
                    )
                ),
                'LBL_DOCUMENT_TEMPLATE_PANEL' => array(
                    array(
                        'nitfiles',
                    ),
                ),
            ),
        ),
    ),
);
