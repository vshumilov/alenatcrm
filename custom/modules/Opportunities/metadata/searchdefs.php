<?php

$searchdefs['Opportunities'] = array(
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout' =>
    array(
        'basic_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
        ),
        'advanced_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'system_number' =>
            array(
                'name' => 'system_number',
                'default' => true,
                'width' => '10%',
            ),
            'contact_name' =>
            array(
                'name' => 'contact_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'tour_name' =>
            array(
                'name' => 'tour_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'email' =>
            array(
                'name' => 'email',
                'label' => 'LBL_ANY_EMAIL',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'phone' =>
            array(
                'name' => 'phone',
                'label' => 'LBL_ANY_PHONE',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'tours_country_name' =>
            array(
                'name' => 'tours_country_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'tours_city_name' =>
            array(
                'name' => 'tours_city_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'tours_operator_name' =>
            array(
                'name' => 'tours_operator_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'tours_route' =>
            array(
                'name' => 'tours_route',
                'default' => true,
                'width' => '10%',
            ),
            'tours_hotel' =>
            array(
                'name' => 'tours_hotel',
                'default' => true,
                'width' => '10%',
            ),
            'tours_foodoption_name' =>
            array(
                'name' => 'tours_foodoption_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'tours_apartment_name' =>
            array(
                'name' => 'tours_apartment_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'tours_days' =>
            array(
                'name' => 'tours_days',
                'default' => true,
                'width' => '10%',
            ),
            'tours_transfer' =>
            array(
                'name' => 'tours_transfer',
                'default' => true,
                'width' => '10%',
            ),
            'tours_excursion' =>
            array(
                'name' => 'tours_excursion',
                'default' => true,
                'width' => '10%',
            ),
            'tours_meeting' =>
            array(
                'name' => 'tours_meeting',
                'default' => true,
                'width' => '10%',
            ),
            'tours_visa_support' =>
            array(
                'name' => 'tours_visa_support',
                'default' => true,
                'width' => '10%',
            ),
            'tours_medical_insurance' =>
            array(
                'name' => 'tours_medical_insurance',
                'default' => true,
                'width' => '10%',
            ),
            'tours_insurance_on_his_own_recognizance' =>
            array(
                'name' => 'tours_insurance_on_his_own_recognizance',
                'default' => true,
                'width' => '10%',
            ),
            'tours_additional_offers' =>
            array(
                'name' => 'tours_additional_offers',
                'default' => true,
                'width' => '10%',
            ),
        ),
    ),
);
