<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Opportunities'] = array(
    'NAME' => array(
        'width' => '30',
        'label' => 'LBL_LIST_OPPORTUNITY_NAME',
        'link' => true,
        'default' => true),
    'CONTACT_NAME' => array(
        'width' => '20',
        'label' => 'LBL_CONTACT',
        'id' => 'CONTACT_ID',
        'module' => 'Contacts',
        'link' => true,
        'default' => true,
        'sortable' => true,
        'ACLTag' => 'CONTACT',
        'related_fields' => array('contact_id')),
    'TOUR_NAME' => array(
        'width' => '20',
        'label' => 'LBL_TOUR_NAME',
        'id' => 'TOUR_ID',
        'module' => 'Tours',
        'link' => true,
        'default' => true,
        'sortable' => true,
        'ACLTag' => 'TOUR',
        'related_fields' => array('tour_id')),
    'DATE_ENTERED' => array(
        'width' => '20',
        'label' => 'LBL_LIST_DATE_ENTERED',
        'default' => true),
);
