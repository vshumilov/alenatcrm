<?php

$viewdefs['Opportunities']['EditView'] = array(
    'templateMeta' => array(
        'maxColumns' => '2',
        'form' =>
        array(
            'enctype' => 'multipart/form-data',
        ),
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
        'includes' =>
        array(
            array(
                'file' => 'custom/include/Nit/js/form.js',
            ),
            array(
                'file' => 'custom/modules/Contacts/js/contactExist.js',
            ),
            array(
                'file' => 'custom/modules/Opportunities/js/editview.js',
            ),
            array(
                'file' => 'custom/include/Nit/js/notValidateAllFields.js',
            ),
        ),
    ),
    'panels' => array(
        'LBL_CONTACTS_PANEL' => array(
            array(
                'not_validate',
            ),
            array(
                'opportunity_type',
            ),
            array(
                'contact_name',
            ),
            array(
                'contacts_gender',
            ),
            array(
                'contacts_birth_date',
            ),
            array(
                'contacts_phone_mobile',
            ),
            array(
                'contacts_phone_work',
            ),
            array(
                'contacts_email',
            ),
        ),
        'LBL_CONTACTS_PASSPORT_PANEL' => array(
            array(
                array(
                    'name' => 'contacts_passport_series',
                ),
            ),
            array(
                array(
                    'name' => 'contacts_passport_number',
                ),
            ),
            array(
                array(
                    'name' => 'contacts_passport_date_of_issue',
                ),
            ),
            array(
                array(
                    'name' => 'contacts_passport_division_code',
                ),
            ),
            array(
                array(
                    'name' => 'contacts_passport_issued_by',
                ),
            ),
            array(
                array(
                    'name' => 'contacts_passport_address',
                ),
            ),
        ),
        'LBL_TOURS_PANEL' => array(
            array(
                'tour_name',
            ),
            array(
                'tours_country_name',
            ),
            array(
                'tours_operator_name',
            ),
            array(
                'tours_city_name',
            ),
            array(
                'tours_route',
            ),
            array(
                'tours_hotel',
            ),
            array(
                'tours_foodoption_name',
            ),
            array(
                'tours_apartment_name',
            ),
            array(
                array('name' => 'start_date_tour'),
            ),
            array(
                array('name' => 'end_date_tour'),
            ),
            array(
                'tours_days',
            ),
            array(
                array(
                    'name' => 'tourists',
                    'label' => 'LBL_TOURISTS_BIG'
                ),
            ),
            array(
                array('name' => 'currency_id', 'label' => 'LBL_CURRENCY'),
            ),
            array(
                'count_places',
            ),
            array(
                'count_places_with_treatment',
            ),
            array(
                array('name' => 'price_name'),
            ),
            array(
                array('name' => 'prepayment'),
            ),
            array(
                array('name' => 'amount'),
            ),
            array(
                array('name' => 'amount_with_discount'),
            ),
            array(
                'tours_transfer',
            ),
            array(
                'tours_excursion',
            ),
            array(
                'tours_meeting',
            ),
            array(
                'tours_visa_support',
            ),
            array(
                'tours_medical_insurance',
            ),
            array(
                'tours_insurance_on_his_own_recognizance',
            ),
            array(
                'tours_additional_offers',
            ),
        ),
        'default' =>
        array(
            array(
                'client_from_source',
            ),
            array(
                'assigned_user_name',
            ),
        ),
        'LBL_DOCUMENT_TEMPLATE_PANEL' => array(
            array(
                'nitfiles',
            ),
        ),
    )
);
