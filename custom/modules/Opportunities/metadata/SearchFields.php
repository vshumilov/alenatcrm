<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';

$searchFields['Opportunities'] = array(
    'tours_country_name' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'tours_country_id' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.country_id'
        ),
    ),
    'tours_city_name' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'tours_city_id' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.city_id'
        ),
    ),
    'tours_operator_name' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'tours_operator_id' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.operator_id'
        ),
    ),
    'tours_route' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.route'
        ),
    ),
    'tours_hotel' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.hotel'
        ),
    ),
    'tours_foodoption' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.foodoption'
        ),
    ),
    'tours_foodoption_name' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'tours_foodoption_id' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'opportunities.tours_foodoption_id'
        ),
    ),
    'tours_apartment_name' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'tours_apartment_id' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'opportunities.tours_apartment_id'
        ),
    ),
    'tours_apartment' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.apartment'
        ),
    ),
    'tours_days' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.days'
        ),
    ),
    'tours_transfer' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.transfer'
        ),
    ),
    'tours_excursion' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.excursion'
        ),
    ),
    'tours_meeting' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.meeting'
        ),
    ),
    'tours_visa_support' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.visa_support'
        ),
    ),
    'medical_insurance' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.medical_insurance'
        ),
    ),
    'tours_insurance_on_his_own_recognizance' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.insurance_on_his_own_recognizance'
        ),
    ),
    'tours_additional_offers' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.additional_offers'
        ),
    ),
    'name' => array('query_type' => 'default'),
    'account_name' => array('query_type' => 'default', 'db_field' => array('accounts.name')),
    'amount' => array('query_type' => 'default'),
    'next_step' => array('query_type' => 'default'),
    'probability' => array('query_type' => 'default'),
    'lead_source' => array('query_type' => 'default', 'operator' => '=', 'options' => 'lead_source_dom', 'template_var' => 'LEAD_SOURCE_OPTIONS'),
    'sales_stage' => array('query_type' => 'default', 'operator' => '=', 'options' => 'sales_stage_dom', 'template_var' => 'SALES_STAGE_OPTIONS', 'options_add_blank' => true),
    'current_user_only' => array('query_type' => 'default', 'db_field' => array('assigned_user_id'), 'my_items' => true, 'vname' => 'LBL_CURRENT_USER_FILTER', 'type' => 'bool'),
    'assigned_user_id' => array('query_type' => 'default'),
    'open_only' => array(
        'query_type' => 'default',
        'db_field' => array('sales_stage'),
        'operator' => 'not in',
        'closed_values' => array('Closed Won', 'Closed Lost'),
        'type' => 'bool',
    ),
    'favorites_only' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'checked_only' => true,
        'subquery' => "SELECT favorites.parent_id FROM favorites
			                    WHERE favorites.deleted = 0
			                        and favorites.parent_type = 'Opportunities'
			                        and favorites.assigned_user_id = '{1}'",
        'db_field' => array('id')),
    //Range Search Support 
    'range_date_entered' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_entered' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_entered' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_date_modified' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_modified' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_modified' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_date_closed' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_closed' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_closed' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'range_amount' => array('query_type' => 'default', 'enable_range_search' => true),
    'start_range_amount' => array('query_type' => 'default', 'enable_range_search' => true),
    'end_range_amount' => array('query_type' => 'default', 'enable_range_search' => true),
    'range_date_signed' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'start_range_date_signed' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'end_range_date_signed' => array('query_type' => 'default', 'enable_range_search' => true, 'is_date_field' => true),
    'contact_id' => array(
        'operator' => '=',
        'query_type' => 'default'
    ),
    'contact_name' => array(
        'query_type' => 'default',
        'db_field' => array(
        )
    ),
    'tour_id' => array(
        'operator' => '=',
        'query_type' => 'default'
    ),
    'tour_name' => array(
        'query_type' => 'default',
        'db_field' => array(
        )
    ),
    'phone' => array(
        'query_type' => 'default',
        'db_field' => array(
        )
    ),
    'email' => array(
        'query_type' => 'default',
        'db_field' => array(
        ),
    ),
 //Range Search Support 			
);
