<?php 
 //WARNING: The contents of this file are auto-generated


$mod_strings['LBL_DOCUMENT_TEMPLATE_PANEL'] = 'Создать документы';
$mod_strings['LBL_DOCUMENT_TEMPLATE'] = 'Документ';
$mod_strings['LBL_NITFILES'] = 'Файлы';
$mod_strings['LBL_NITFILES_PANEL'] = 'Файлы';


$mod_strings['LBL_NAME'] = 'Сокращенное наименование';
$mod_strings['LBL_FULL_NAME'] = 'Полное наименование';
$mod_strings['LBL_INN'] = 'ИНН';
$mod_strings['LBL_KPP'] = 'КПП';
$mod_strings['LBL_OGRN'] = 'ОГРН';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Юридический адрес';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Фактический адрес';
$mod_strings['LBL_ACCOUNT_DETAILS_PANEL'] = 'Банковские реквизиты';
$mod_strings['LBL_BANK_NAME'] = 'Банк';
$mod_strings['LBL_PAYMENT_ACCOUNT'] = 'Расчетный счет';
$mod_strings['LBL_CORRESPONDENT_ACCOUNT'] = 'Корсчет';
$mod_strings['LBL_BIC'] = 'БИК';
$mod_strings['LBL_DESCRIPTION'] = 'Основание работы на территории';
$mod_strings['LBL_SIGNED_PANEL'] = 'Подписан';
$mod_strings['LBL_GENITIVE_FIO'] = 'ФИО в родительном падеже';
$mod_strings['LBL_NOMINATIVE_FIO'] = 'ФИО в именительном падеже';
$mod_strings['LBL_BASE_ACTS_SIGNATORY'] = 'Основание действия подписанта';
$mod_strings['LBL_ADDRESS_PANEL'] = 'Адреса';

$mod_strings['LBL_MODULE_NAME'] = 'Договоры';
$mod_strings['LBL_MODULE_TITLE'] = 'Договоры - ГЛАВНАЯ';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Поиск договоров';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Обзор договора';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Список договоров';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Название договора:';
$mod_strings['LBL_OPPORTUNITY'] = 'Договор:';

$mod_strings['LBL_DUPLICATE'] = 'Возможно дублирующийся договор';
$mod_strings['MSG_DUPLICATE'] = 'Создаваемый вами договор возможно дублирует уже имеющийся договор. Договоры, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания новой договоры или кнопку "Отмена" для возврата в модуль.';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Создать договор';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Создать договор';
$mod_strings['LNK_IMPORT_OPPORTUNITIES'] = 'Импорт заявок';
$mod_strings['LNK_OPPORTUNITY_LIST'] = 'Договоры';
$mod_strings['ERR_DELETE_RECORD'] = 'Перед удалением договора должен быть определён номер записи.';
$mod_strings['LBL_TOP_OPPORTUNITIES'] = 'Мои основные открытые договоры';
$mod_strings['NTC_REMOVE_OPP_CONFIRMATION'] = 'Вы действительно хотите удалить этот контакт из договора?';
$mod_strings['OPPORTUNITY_REMOVE_PROJECT_CONFIRM'] = 'Вы действительно хотите удалить данный договор из проекта?';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Договоры';
$mod_strings['LBL_OPPORTUNITY_TYPE'] = 'Статус';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Абонент';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Абонент';
$mod_strings['LBL_PHONE'] = 'Телефон';
$mod_strings['LBL_EMAIL'] = 'Email';
$mod_strings['LBL_INCOMING_NUMBER'] = 'Номер входящего';
$mod_strings['LBL_APPLICATION_SCAN'] = 'Скан заявления';

$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Юридический адрес - город:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'Юридический адрес - страна:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Юридический адрес - индекс:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Юридический адрес - область:';
$mod_strings['LBL_BILLING_ADDRESS_STREET_2'] = 'Юридический адрес - улица 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET_3'] = 'Юридический адрес - улица 3';
$mod_strings['LBL_BILLING_ADDRESS_STREET_4'] = 'Юридический адрес - улица 4';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Юридический адрес - улица:';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Юридический адрес';

$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Фактический адрес - город:';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'Фактический адрес - страна:';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Фактический адрес - индекс:';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Фактический адрес - область:';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_2'] = 'Фактический адрес - улица 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_3'] = 'Фактический адрес - улица 3';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_4'] = 'Фактический адрес - улица 4';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Фактический адрес - улица:';

$mod_strings['LBL_DOCUMENTS_SUBPANEL_TITLE'] = 'Приложения';
$mod_strings['LBL_EMAILS_SUBPANEL_TITLE'] = 'Письма';

$mod_strings['LBL_ENDORSED_TO_NAME'] = 'Завизировал';
$mod_strings['LBL_DATE_ENDORSED'] = 'Дата визирования';
$mod_strings['LBL_CREATE_CONTRACT'] = 'Создать договор на основе договора';
$mod_strings['LBL_SIGNED_TO_NAME'] = 'Подписал';
$mod_strings['LBL_DATE_SIGNED'] = 'Дата подписания';

$mod_strings['LBL_COVERING_LETTER_SCAN_PANEL'] = 'Сопроводительное письмо';
$mod_strings['LBL_COVERING_LETTER_SCAN'] = 'Скан-копия';
$mod_strings['LBL_DATE_ABONENT_RECEIVED'] = 'Дата получения абонентом';
$mod_strings['LBL_ABONENT_REQUESITES_PANEL'] = 'Реквизиты абонета';
$mod_strings['LBL_COVERING_LETTER_TO_NAME'] = 'Загрузил скан';

$mod_strings['LBL_AGREEMENT_SCAN'] = 'Скан договора';
$mod_strings['LBL_DATE_AGREEMENT_REGISTRATION'] = 'Дата регистрации';
$mod_strings['LBL_AGREEMENT_TO_NAME'] = 'Загрузил скан';
$mod_strings['LBL_AGREEMENT_PANEL'] = 'Скан договора';

$mod_strings['LBL_DIFFERENCES_ACT_SCAN'] = 'Скан акта разногласий';
$mod_strings['LBL_DATE_DIFFERENCES_ACT_REGISTRATION'] = 'Дата регистрации';
$mod_strings['LBL_DIFFERENCES_ACT_TO_NAME'] = 'Загрузил скан';
$mod_strings['LBL_DIFFERENCES_ACT_PANEL'] = 'Акт разногласий';

$mod_strings['LBL_RESPOND_ON_REJECT'] = 'Ответ на отклоненный договор';
$mod_strings['LBL_DATE_RESPOND_ON_REJECT'] = 'Дата загрузки';
$mod_strings['LBL_RESPOND_ON_REJECT_TO_NAME'] = 'Загрузил';
$mod_strings['LBL_RESPOND_ON_REJECT_PANEL'] = 'Ответ юриста на отклоненный договор';

$mod_strings['LBL_RESPOND_ON_REJECT_SCAN'] = 'Скан ответа';
$mod_strings['LBL_DATE_RESPOND_ON_REJECT_REGISTRATION'] = 'Дата регистрации';
$mod_strings['LBL_RESPOND_ON_REJECT_SCAN_TO_NAME'] = 'Загрузил';
$mod_strings['LBL_RESPOND_ON_REJECT_SCAN_NUMBER'] = 'Номер';
$mod_strings['LBL_RESPOND_ON_REJECT_SCAN_PANEL'] = 'Скан ответа на отклоненный договор с отметкой о получении абонентом';
$mod_strings['LBL_SYSTEM_NUMBER'] = 'Номер договора';
$mod_strings['LNK_OPPORTUNITY_LIST_SHORT'] = 'Договоры';
$mod_strings['LBL_CONTACT'] = 'Клиент';
$mod_strings['LBL_TYPE'] = 'Статус';
$mod_strings['LBL_TOUR_NAME'] = 'Тур';

$mod_strings['LBL_CONTACTS_PHONE_WORK'] = 'Тел. (раб.)';
$mod_strings['LBL_CONTACTS_PHONE_MOBILE'] = 'Тел. (моб.)';
$mod_strings['LBL_CONTACTS_EMAIL'] = 'Эл. почта';
$mod_strings['LBL_CONTACTS_PANEL'] = 'Информация о клиенте';
$mod_strings['LBL_CONTACTS_PASSPORT_SERIES'] = 'Серия';
$mod_strings['LBL_CONTACTS_PASSPORT_NUMBER'] = 'Номер';
$mod_strings['LBL_CONTACTS_PASSPORT_DATE_OF_ISSUE'] = 'Дата выдачи';
$mod_strings['LBL_CONTACTS_PASSPORT_ISSUED_BY'] = 'Кем выдан';
$mod_strings['LBL_CONTACTS_PASSPORT_DIVISION_CODE'] = 'Код подразделения';
$mod_strings['LBL_CONTACTS_PASSPORT_ADDRESS'] = 'Адрес регистрации';
$mod_strings['LBL_CONTACTS_PASSPORT_PANEL'] = 'Паспортные данные';

$mod_strings['LBL_COUNTRY_NAME'] = 'Страна';
$mod_strings['LBL_OPERATOR_NAME'] = 'Оператор';
$mod_strings['LBL_CITY_NAME'] = 'Город (курорт)';
$mod_strings['LBL_ROUTE'] = 'Маршрут';
$mod_strings['LBL_HOTEL'] = 'Отель';
$mod_strings['LBL_FOOD'] = 'Питание';
$mod_strings['LBL_TOURS_FOODOPTION_NAME'] = 'Питание';
$mod_strings['LBL_APARTMENT'] = 'Размещение';
$mod_strings['LBL_TOURS_APARTMENT_NAME'] = 'Размещение';
$mod_strings['LBL_DAYS'] = 'Срок нахождения (дней)';
$mod_strings['LBL_TRANSFER'] = 'Трансфер (перевозка наземным транспортом)';
$mod_strings['LBL_EXCURSION'] = 'Экскурсионная программа';
$mod_strings['LBL_MEETING'] = 'Встреча и проводы с русскоговорящим гидом';
$mod_strings['LBL_VISA_SUPPORT'] = 'Визовая поддержка';
$mod_strings['LBL_MEDICAL_INSURANCE'] = 'Медицинская страховка';
$mod_strings['LBL_INSURANCE_ON_HIS_OWN_RECOGNIZANCE'] = 'Cтраховка от невыезда';
$mod_strings['LBL_ADDITIONAL_OFFERS'] = 'Дополнительные услуги';
$mod_strings['LBL_TOURS_PANEL'] = 'Информация о туре';
$mod_strings['LBL_TOURISTS'] = 'Договор на туристов';
$mod_strings['LBL_TOURISTS_BIG'] = 'Договор на туристов<br/> (Например: Болотову Н.В., Гуровских Ю.Л.)';
$mod_strings['LBL_START_DATE_TOUR'] = 'Дата поездки, с';
$mod_strings['LBL_END_DATE_TOUR'] = 'Дата поездки, по';
$mod_strings['LBL_PREPAYMENT'] = 'Сумма предоплаты';
$mod_strings['LBL_CONTACTS_BIRTH_DATE'] = 'Дата рождения';
$mod_strings['LBL_ANY_EMAIL'] = 'Любой E-mail';
$mod_strings['LBL_ANY_PHONE'] = 'Любой тел.';
$mod_strings['LBL_CONTACTS_GENDER'] = 'Пол';
$mod_strings['LBL_NOT_VALIDATE'] = 'Не проверять обязательные поля';
$mod_strings['LBL_AMOUNT_WITH_DISCOUNT'] = 'Сумма со скидкой';
$mod_strings['LBL_CLIENT_FROM_SOURCE'] = 'Откуда узнал о нас';
$mod_strings['LBL_COUNT_PLACES'] = 'Кол-во людей без лечения';
$mod_strings['LBL_COUNT_PLACES_WITH_TREATMENT'] = 'Кол-во людей с лечением';
$mod_strings['LBL_BOOKED_DATETIME'] = 'Дата бронирования';
$mod_strings['LBL_AGREEMENT_SENT_DATETIME'] = 'Дата отправки договора клиенту';
$mod_strings['LBL_PREPAYMENT_DATETIME'] = 'Дата предоплаты';
$mod_strings['LBL_PAID_DATETIME'] = 'Дата оплаты';
$mod_strings['LBL_CLOSED_DATETIME'] = 'Дата закрытия договора';
$mod_strings['LBL_CANCELED_DATETIME'] = 'Дата отмены договора';
$mod_strings['LBL_AGREEMENT_PDF'] = 'Договор в pdf';
$mod_strings['LBL_HREF_AGREEMENT_CONFIRM'] = 'Ссылка на подтверждение договора';
$mod_strings['LBL_PRICE_NAME'] = 'Цена';




?>