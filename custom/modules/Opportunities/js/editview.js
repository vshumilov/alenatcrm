$(document).ready(function () {

    bindPrefillForBean('Contacts', 'Contact');
    bindPrefillForBean('Tours', 'Tour');

    subscribeOnTextboxBlurAutocomplete('tours_country');
    subscribeOnTextboxBlurAutocomplete('tours_city');
    subscribeOnTextboxBlurAutocomplete('tours_operator');
    subscribeOnTextboxBlurAutocomplete('tours_food');
    subscribeOnTextboxBlurAutocomplete('tours_placement');

    var $contactName = $('#contact_name');
    var $birthDate = $('#contacts_birth_date');
    var $phoneWork = $('#contacts_phone_work');
    var $phoneMobile = $('#contacts_phone_mobile');
    var $email = $('#contacts_email');
    var $contactId = $('#contact_id');
    var $startDateTour = $('#start_date_tour');
    var $endDateTour = $('#end_date_tour');
    var $toursDays = $('#tours_days');
    var isEditAgreement = $contactId.val();
    var contactExist = new ContactExist("contacts_");

    $contactName.on("change", function () {
        checkIsExistsContact();
    });

    $birthDate.on("change", function () {
        checkIsExistsContact();
    });

    $phoneWork.on("change", function () {
        checkIsExistsContact();
    });

    $phoneMobile.on("change", function () {
        checkIsExistsContact();
    });

    $email.on("change", function () {
        checkIsExistsContact();
    });

    function checkIsExistsContact() {
        setTimeout(function () {
            var contactId = $contactId.val();
            var contactName = $contactName.val();
            var spacePos = contactName.indexOf(" ");
            var lastName = contactName.substring(0, spacePos);
            var firstName = contactName.substring(spacePos + 1);

            contactExist.checkIsExists(
                    firstName,
                    lastName,
                    contactId,
                    $birthDate.val(),
                    $phoneWork.val(),
                    $phoneMobile.val(),
                    $email.val());
        }, 500);

    }

    YAHOO.util.Event.addListener($startDateTour[0], 'change', function () {
        calculateDays();
    });

    YAHOO.util.Event.addListener($endDateTour[0], 'change', function () {
        calculateDays();
    });

    function calculateDays() {
        if (!$startDateTour.val() || !$endDateTour.val()) {
            return;
        }

        var startDateArray = $startDateTour.val().split('.');
        var endDateArray = $endDateTour.val().split('.');

        var startDate = new Date(startDateArray[2], parseInt(startDateArray[1])-1, startDateArray[0]);
        var endDate = new Date(endDateArray[2], parseInt(endDateArray[1])-1, endDateArray[0]);

        if (!isDateCorrect(startDateArray[2], startDateArray[1], startDateArray[0], startDate)) {
            return;
        }

        if (!isDateCorrect(endDateArray[2], endDateArray[1], endDateArray[0], endDate)) {
            return;
        }
        
        if (endDate.getTime() < startDate.getTime()) {
            $toursDays.val('');
            return;
        }

        var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;
        $toursDays.val(diffDays);
    }

    function isDateCorrect(year, mon, day, jsDate) {
        if (jsDate.getFullYear() != year) {
            return false;
        }

        if ((jsDate.getMonth() + 1) != parseInt(mon)) {
            return false;
        }

        if (jsDate.getDate() != day) {
            return false;
        }

        return true;
    }
});

