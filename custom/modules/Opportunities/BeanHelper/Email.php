<?php

require_once 'custom/include/Nit/BeanHelper/Email.php';
require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';

class OpportunityBeanHelperEmail extends BeanHelperEmail
{

    public function sendEmailCompleteSetOfDocuments()
    {
        $sugarConfig = SugarConfig::getInstance();
        $templateId = $sugarConfig->get('vezir_agreement_request_template_id');

        if (empty($templateId)) {
            $GLOBALS['log']->info("sendEmailCompleteSetOfDocuments: vezir_agreement_request_template_id doesn't exist in config.php");
            return;
        }

        $emailTemplate = BeanFactory::getBean('EmailTemplates', $templateId);

        $emails = $this->getEmailsByRoles(['юрист', 'администратор']);

        $result = $this->sendEmail($emails, $emailTemplate);

        return $result;
    }
    
    public function getSecurityGroupWithAccountCity()
    {
        global $current_user;

        $current_user->load_relationship('SecurityGroups');

        $securityGroups = $current_user->SecurityGroups->getBeans();
        
        $account = BeanFactory::getBean('Accounts', $this->bean->account_id);

        foreach ($securityGroups as $securityGroup) {
            if ($securityGroup->city_id == $account->city_id) {
                return $securityGroup;
            }
        }
    }

}
