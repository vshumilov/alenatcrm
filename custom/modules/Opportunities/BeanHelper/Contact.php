<?php

require_once 'custom/include/Nit/BeanHelper.php';
require_once 'custom/modules/Contacts/BeanHelper/Contact.php';

class OpportunityBeanHelperContact extends BeanHelper
{

    public $key = 'contacts_';
    public $module = 'Contacts';

    public function saveBean()
    {
        global $timedate;
        $timedate = new TimeDate();

        $opportunity = $this->bean;

        $bean = BeanFactory::getBean($this->module);

        $beanIdName = $this->getBeanIdName($bean);

        $beanNameField = strtolower($bean->object_name) . '_name';

        if (empty($opportunity->{$beanNameField}) && $bean->module_dir == 'Contacts') {
            return;
        }

        if (!empty($opportunity->{$beanIdName})) {
            $beanTmp = BeanFactory::getBean($this->module);
            $className = $beanTmp->object_name;
            $bean = new $className();
            $bean->retrieve($opportunity->{$beanIdName});
        }

        $fields = $opportunity->field_defs;

        foreach ($fields as $field) {

            if (!$this->isBeanField($field, $bean) ||
                    (empty($opportunity->{$field['name']}) && !in_array($field['name'], ['contacts_gender']))
            ) {
                continue;
            }

            $fieldName = $this->getBeanFieldName($field['name']);

            if ($field['type'] == 'relate') {
                $this->setChildBean($field, $opportunity, $bean);
            } else {
                $bean->{$fieldName} = $opportunity->{$field['name']};
            }
        }

        $bean->name = $opportunity->{$beanNameField};

        if (empty($bean->id) && $bean->module_dir == 'Contacts') {
            $arrayName = explode(" ", $opportunity->contact_name);
            $bean->last_name = $arrayName[0];

            array_shift($arrayName);

            $bean->first_name = implode(" ", $arrayName);
        }

        if ($this->isNewBeanInSaving($opportunity) && $bean->module_dir == 'Contacts') {
            $bean->opportunity_id = $opportunity->id;
            $bean->last_agreement_datetime = $timedate->to_display_date_time(date('Y-m-d H:i:s'));
        }

        $contactBeanHelper = new ContactBeanHelperContact($bean);
        $row = $contactBeanHelper->getDuplicateContact(
                $bean->first_name, $bean->last_name, null, $bean->birth_date, $bean->phone_work, $bean->phone_mobile, ""
        );

        if (!empty($row['id'])) {
            $bean->id = $row['id'];
        }

        $beanId = $bean->save();

        return $beanId;
    }

    protected function setChildBean($field, &$opportunity, &$bean)
    {
        if (empty($opportunity->{$field['id_name']}) && !empty($opportunity->{$field['name']})) {
            $childBean = BeanFactory::getBean($field['module']);
            $childBean->name = $opportunity->{$field['name']};
            $childBean->save();

            if (empty($childBean->id)) {
                return;
            }

            $opportunity->{$field['id_name']} = $childBean->id;
        }

        $fieldNameId = $this->getBeanFieldName($field['id_name']);
        $bean->{$fieldNameId} = $opportunity->{$field['id_name']};

        $fieldNameName = $this->getBeanFieldName($field['name']);
        $bean->{$fieldNameName} = $opportunity->{$field['name']};
    }

    public function fillParentFields()
    {
        $opportunity = $this->bean;
        $bean = BeanFactory::getBean($this->module);
        $beanIdName = $this->getBeanIdName($bean);

        if (empty($opportunity->id) || empty($opportunity->{$beanIdName})) {
            return $opportunity;
        }

        $beanTmp = BeanFactory::getBean($this->module);
        $className = $beanTmp->object_name;
        $bean->retrieve($opportunity->{$beanIdName});;

        $fields = $opportunity->field_defs;

        foreach ($fields as $field) {
            if (!$this->isBeanField($field, $bean)) {
                continue;
            }

            $fieldName = $this->getBeanFieldName($field['name']);

            $opportunity->{$field['name']} = $bean->{$fieldName};
        }

        return $opportunity;
    }

    protected function getBeanIdName($bean)
    {
        $beanIdName = strtolower($bean->object_name) . '_id';

        return $beanIdName;
    }

    protected function isBeanField($field, $bean)
    {
        $keyPos = strpos($field['name'], $this->key);
        $fieldName = $this->getBeanFieldName($field['name']);

        if ($keyPos !== false &&
                (
                (!empty($field['source']) && $field['source'] == 'non-db') ||
                (
                (
                empty($field['source']) || $field['source'] != 'non-db') &&
                empty($bean->{$fieldName}
                )
                )
                )
        ) {
            return true;
        }

        return false;
    }

    protected function getBeanFieldName($fieldName)
    {
        $beanfieldName = substr($fieldName, strpos($fieldName, $this->key) + strlen($this->key));

        return $beanfieldName;
    }

}
