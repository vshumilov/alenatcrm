<?php

require_once 'custom/include/Nit/BeanHelper/Sms.php';
require_once 'custom/modules/Opportunities/BeanHelper/Contact.php';
require_once 'custom/modules/Opportunities/BeanHelper/Tour.php';
require_once 'custom/modules/Accounts/BeanHelper/Account.php';
require_once 'custom/modules/Dispatches/BeanHelper/Dispatch.php';
require_once 'custom/include/Nit/Helper/Phone.php';
require_once 'custom/include/Nit/Helper/Guid.php';
require_once 'vendor/autoload.php';
require_once 'modules/SugarNitfile/SugarNitfile.php';
require_once 'modules/Opportunities/Opportunity.php';
require_once 'modules/Dispatches/Dispatch.php';

class OpportunityBeanHelperOpportunity extends DispatchBeanHelperDispatch
{

    public function setName()
    {
        if (!empty($this->bean->name)) {
            return $this->bean->name;
        }

        $sign = ' №';

        $this->bean->system_number = $this->getSystemNumber() + 1;

        $contact = BeanFactory::getBean('Contacts', $this->bean->contact_id);
        $accountBeanHelper = new AccountBeanHelperAccount();
        $contactInisials = $accountBeanHelper->getFioWithInisials($contact->full_name);

        $this->bean->name = 'Договор' . $sign . $this->bean->system_number . " " . $contactInisials;

        return $this->bean;
    }

    public function getIncomingNumber()
    {
        $count = $this->getCountTotal();

        $incomingNumber = ($count + 1);

        return $incomingNumber;
    }

    public function getSystemNumber()
    {
        $count = $this->getCountTotal();

        $systemNumber = ($count + 1);

        return $systemNumber;
    }

    public function getCountWithContact()
    {
        if (empty($this->bean->contact_id)) {
            return;
        }

        $query = "
        SELECT
            count(o.id) AS count
        FROM
            opportunities AS o
        WHERE
            o.contact_id = <contactId>
        ";

        $sql = strtr($query, ['<contactId>' => $this->bean->db->quoted($this->bean->contact_id)]);

        $count = $this->bean->db->getOne($sql);

        return $count;
    }

    public function getCountTotal()
    {
        $query = "
        SELECT
            count(o.id) AS count
        FROM
            opportunities AS o
        ";

        $count = $this->bean->db->getOne($query);

        return $count;
    }

    public static function getTypeList($focus, $field = 'opportunity_type', $value = '', $view = 'DetailView')
    {
        global $current_user;
        $opportunityTypeDom = translate('opportunity_type_dom');
        $typeList = [];

        if (!empty($_REQUEST['searchFormTab']) || (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'index')) {
            $opportunityTypeDom = array_merge(['' => 'Все'], $opportunityTypeDom);
            $typeList[''] = $opportunityTypeDom[''];
        }

        $current_user->load_relationship('aclroles');

        $roles = $current_user->aclroles->getBeans();

        $typeList['incomplete_set_of_documents'] = $opportunityTypeDom['incomplete_set_of_documents'];
        $typeList['complete_set_of_documents'] = $opportunityTypeDom['complete_set_of_documents'];

        if (self::isHasRoleWithName($roles, 'начальник')) {
            $typeList = $opportunityTypeDom;
            return $typeList;
        }

        if (self::isHasRoleWithName($roles, 'юрист') || self::isHasRoleWithName($roles, 'администратор')) {
            $typeList['endorsed_by_lawyer_and_administrator'] = $opportunityTypeDom['endorsed_by_lawyer_and_administrator'];
        }

        if (self::isHasRoleWithName($roles, 'юрист')) {
            $typeList['rejected'] = $opportunityTypeDom['rejected'];
            $typeList['rejected_30'] = $opportunityTypeDom['rejected_30'];
            $typeList['rejected_card'] = $opportunityTypeDom['rejected_card'];
            return $typeList;
        }

        if (self::isHasRoleWithName($roles, 'пешеход')) {
            $typeList = $opportunityTypeDom;
            unset($typeList['incomplete_set_of_documents']);
            unset($typeList['endorsed_by_lawyer_and_administrator']);

            return $typeList;
        }

        return $typeList;
    }

    public static function getRejectedStatuses()
    {
        $statuses = ['rejected', 'rejected_30', 'rejected_card'];

        return $statuses;
    }

    public function getOpportunitiesQuery()
    {
        $sql = "
        SELECT
            o.id
        FROM
            opportunities AS o
        WHERE
            o.deleted = '0'
        ";

        return $sql;
    }

    public function fillChildrenData()
    {
        $contactHelper = new OpportunityBeanHelperContact($this->bean);
        $this->bean = $contactHelper->fillParentFields();

        $tourHelper = new OpportunityBeanHelperTour($this->bean);
        $this->bean = $tourHelper->fillParentFields();

        return $this->bean;
    }

    public function setTypeDatetime()
    {
        if (empty($this->bean->id)) {
            return $this->bean;
        }

        $oldOpportunity = $this->getOpportunityById($this->bean->id);

        $timeDateField = $this->bean->opportunity_type . '_datetime';

        if ($this->bean->opportunity_type == $oldOpportunity['opportunity_type'] || !empty($oldOpportunity[$timeDateField])) {
            return $this->bean;
        }

        global $timedate;
        $timedate = new TimeDate();

        $this->bean->{$timeDateField} = date($timedate->get_db_date_time_format(), time());

        if ($this->bean->opportunity_type == 'booked') {
            $this->generateDocxAgreement();
        } else if ($this->bean->opportunity_type == 'agreement_sent') {
            $this->sendAgreementToClient();
        }

        return $this->bean;
    }

    protected function generateDocxAgreement()
    {
        if (empty($this->bean->id)) {
            return;
        }
        
        $documentTemplateId = $this->getOpportunityDocumentTemplateId();
        
        if (empty($documentTemplateId)) {
            return;
        }
        
        $documentTemplate = new DocumentTemplate();
        $documentTemplate->retrieve($documentTemplateId);
        
        if (empty($documentTemplate->id)) {
            return;
        }
        
        $documentTemplate->generateDocument($this->bean);
    }

    protected function getOpportunityDocumentTemplateId()
    {
        if (empty($this->bean->id)) {
            return;
        }
        
        $sql = "
        SELECT
            id
        FROM
            documenttemplates
        WHERE
            deleted = '0' AND
            parent_type = <moduleName>
        ";
        
        $query = strtr($sql, ['<moduleName>' => dbQuote($this->bean->module_dir)]);
        
        return dbGetScalar($query);
    }

    protected function sendAgreementToClient()
    {
        if (empty($this->bean->id)) {
            return;
        }

        $pdfFilePath = $this->convertAgreementDocxToPdf();

        if (empty($pdfFilePath)) {
            return;
        }

        $this->attachPdfFileToOpportunity($pdfFilePath);
        
        unlink($pdfFilePath);

        $contact = BeanFactory::getBean('Contacts', $this->bean->contact_id);

        if (empty($contact->id)) {
            return;
        }

        $sugarConfig = new SugarConfig();
        $sendAgreementEmailTemplateId = $sugarConfig->get('send_agreement_email_template_id');

        if (empty($sendAgreementEmailTemplateId)) {
            $GLOBALS['log']->fatal("send_agreement_email_template_id not exist in config.php");
            return;
        }

        $template = BeanFactory::getBean('EmailTemplates', $sendAgreementEmailTemplateId);

        $beans = [];
        $beans['Opportunities'] = $this->bean->id;

        $text = $this->populateTemplate($template, false, $contact, $beans);

        $attachment = [];
        $attachment['path'] = $pdfFilePath;
        $attachment['name'] = $this->bean->name;

        $resultEmail = $this->sendEmail([$contact->email], $template, [], true, $beans, [$attachment]);

        $dispatchHelper = new DispatchBeanHelperDispatch($this->bean);

        if ($resultEmail) {
            $dispatchHelper->createDispatcheslog($contact);
        }

        $helperPhone = new HelperPhone();

        if (!$helperPhone->isValidMobilePhone($contact->phone_mobile)) {
            return;
        }

        if (empty($text['body_alt'])) {
            $GLOBALS['log']->fatal("in EmailTemplate with send_agreement_email_template_id body_alt is empty");
            return;
        }

        $resultSms = $this->sendSms([$contact->phone_mobile], $text['body_alt']);

        if (!$dispatchHelper->hasSmsErrors($resultSms)) {
            $dispatchHelper->createDispatcheslog($contact, 'Sms', $text['body_alt']);
        }
    }

    protected function attachPdfFileToOpportunity($pdfFilePath)
    {
        if (empty($pdfFilePath) || !file_exists($pdfFilePath)) {
            return;
        }

        $file = [];
        $file['name'] = $this->bean->name . '.pdf';
        $file['type'] = 'application/pdf';
        $file['tmp_name'] = $pdfFilePath;
        $file['size'] = filesize($pdfFilePath);
        $file['error'] = '';

        SugarNitfile::createBeanNitfile($this->bean, 'agreement_pdf', $file);
    }

    protected function convertAgreementDocxToPdf()
    {
        $files = SugarNitfile::getBeanNitfiles($this->bean, 'nitfiles');

        if (empty($files)) {
            return;
        }

        $agreementFile = array_shift($files);

        $docxPath = $agreementFile->getFilePath() . '.docx';

        copy($agreementFile->getFilePath(), $docxPath);

        Gears\Pdf::convert($docxPath, $agreementFile->getPdfFilePath());

        unlink($docxPath);

        return $agreementFile->getPdfFilePath();
    }

    public function getOpportunityById($id)
    {
        if (empty($id)) {
            return;
        }

        $sql = "
        SELECT
            *
        FROM
            opportunities
        WHERE
            deleted = '0' AND
            id = <id>
        LIMIT 
            1
        ";

        $query = strtr($sql, ['<id>' => dbQuote($id)]);

        return dbGetRow($query);
    }
    
    public function getByIdWithCode($opportunityId, $code = null, $onlyShortData = true) {
        $guidHelper = new HelperGuid();

        if (!$guidHelper->validate($opportunityId)) {
            throw new Exception('opportunityId is not valid');
        }

        $opportunity = new Opportunity();
        $opportunity->retrieve($opportunityId);

        $data = [];

        if (!empty($onlyShortData)) {
            $data['id'] = $opportunity->id;
            $data['opportunity_type'] = $opportunity->opportunity_type;
            return $data;
        }

        $beanHelper = new DispatchBeanHelperDispatch(new Dispatch());
        $result = $beanHelper->checkSmsCode($code, $opportunity->contacts_phone_mobile);

        if (!empty($result['error'])) {
            throw new Exception("check sms code error");
        }

        foreach ($opportunity->field_defs as $field) {
            $data[$field['name']] = $opportunity->{$field['name']};
        }

        if (!empty($opportunity->price_id)) {
            $price = new Price();
            $price->retrieve($opportunity->price_id);

            $data['price'] = [];

            foreach ($price->field_defs as $field) {
                $data['price'][$field['name']] = $price->{$field['name']};
            }

            if (!empty($price->parent_id)) {
                $sanatorium = new Sanatorium();
                $sanatorium->retrieve($price->parent_id);
                $data['sanatorium'] = [];

                foreach ($sanatorium->field_defs as $field) {
                    $data['sanatorium'][$field['name']] = htmlspecialchars_decode($sanatorium->{$field['name']});
                }
            }
        }
        
        if (!empty($opportunity->tours_apartment_id)) {
            $apartment = new Apartment();
            $apartment->retrieve($opportunity->tours_apartment_id);
            
            $data['apartment'] = [];
            
            foreach ($apartment->field_defs as $field) {
                $data['apartment'][$field['name']] = $apartment->{$field['name']};
            }
        }

        return $data;
    }
    
    public function getPdfAgreementNitFileWithCode($opportunityId, $code)
    {
        $guidHelper = new HelperGuid();

        if (!$guidHelper->validate($opportunityId)) {
            throw new Exception('opportunityId is not valid');
        }

        $opportunity = new Opportunity();
        $opportunity->retrieve($opportunityId);
        
        $beanHelper = new DispatchBeanHelperDispatch(new Dispatch());
        $result = $beanHelper->checkSmsCode($code, $opportunity->contacts_phone_mobile);

        if (!empty($result['error'])) {
            throw new Exception("check sms code error");
        }
        
        $pdfFilesData = SugarNitfile::getBeanNitfiles($opportunity, 'agreement_pdf');
        
        if (empty($pdfFilesData)) {
            throw new Exception("agreement pdf file not exists");
        }
        
        $sugarNitFilePdf = array_shift($pdfFilesData);
        
        return $sugarNitFilePdf;
    }

}
