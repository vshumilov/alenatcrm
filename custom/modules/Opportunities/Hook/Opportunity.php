<?php

require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';
require_once 'custom/modules/Opportunities/BeanHelper/Contact.php';
require_once 'custom/modules/Opportunities/BeanHelper/Tour.php';
require_once 'modules/SugarNitfile/SugarNitfile.php';

class OpportunityHookOpportunity
{

    public function setName(Opportunity $bean)
    {
        $opportunityBeanHelper = new OpportunityBeanHelperOpportunity($bean);

        $bean = $opportunityBeanHelper->setName();
    }

    public function saveContact(Opportunity $bean)
    {
        $contactBeanHelper = new OpportunityBeanHelperContact($bean);
        $bean->contact_id = $contactBeanHelper->saveBean();
    }

    public function saveTour(Opportunity $bean)
    {
        $tourBeanHelper = new OpportunityBeanHelperTour($bean);
        $bean->tour_id = $tourBeanHelper->saveBean();
    }

    public function saveNitfiles(Opportunity $bean)
    {
        SugarNitfile::saveBeanNitfiles($bean, 'nitfiles');
    }

    public function fillChildrenData(Opportunity $bean)
    {
        $opportunityHelper = new OpportunityBeanHelperOpportunity($bean);
        $bean = $opportunityHelper->fillChildrenData();
    }

    public function setHrefBean(Opportunity $bean)
    {
        $sugarConfig = new SugarConfig();
        $crmSiteUrl = $sugarConfig->get('site_url');
        
        if (empty($crmSiteUrl)) {
            $GLOBALS['log']->fatal("site_url not exists in config.php");
            return;
        }
        
        $bean->href_bean = $crmSiteUrl. '/index.php?module=Opportunities&action=DetailView&record=' . $bean->id;
    }
    
    public function setHrefAgreementConfirm(Opportunity $bean)
    {
        if (empty($bean->id)) {
            return;
        }
        
        $sugarConfig = new SugarConfig();
        $webSiteUrl = $sugarConfig->get('web_site_url');
        
        if (empty($webSiteUrl)) {
            $GLOBALS['log']->fatal("web_site_url not exists in config.php");
            return;
        }
        
        $agreementConfirmUrl = $sugarConfig->get('agreement_confirm_url');
        
        if (empty($agreementConfirmUrl)) {
            $GLOBALS['log']->fatal("agreement_confirm_url not exists in config.php");
            return;
        }
        
        $url = $webSiteUrl . $agreementConfirmUrl;
        
        $bean->href_agreement_confirm = strtr($url, ['{id}' => $bean->id]);
    }
    
    
    public function setTypeDatetime(Opportunity $bean)
    {
        $opportunityHelper = new OpportunityBeanHelperOpportunity($bean);
        $bean = $opportunityHelper->setTypeDatetime();
    }
}
