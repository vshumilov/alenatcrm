<?php

require_once 'custom/modules/Opportunities/BeanHelper/Email.php';
require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';

class OpportunityHookEmail
{
    public function sendEmailCompleteSetOfDocuments(Opportunity $bean)
    {
        if (empty($bean->is_set_type_complete_set_of_documents)) {
            return;
        }

        $emailBeanHelper = new OpportunityBeanHelperEmail($bean);

        $emailBeanHelper->sendEmailCompleteSetOfDocuments();
    }
}
