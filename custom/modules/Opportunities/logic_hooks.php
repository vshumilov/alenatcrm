<?php

// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
$hook_version = 1;
$hook_array = Array();
// position, file, function 
$hook_array['before_save'] = Array();
$hook_array['after_retrieve'] = [];
$hook_array['before_save'][] = Array(1, 'Opportunities push feed', 'modules/Opportunities/SugarFeeds/OppFeed.php', 'OppFeed', 'pushFeed');
$hook_array['before_save'][] = Array(77, 'updateGeocodeInfo', 'modules/Opportunities/OpportunitiesJjwg_MapsLogicHook.php', 'OpportunitiesJjwg_MapsLogicHook', 'updateGeocodeInfo');
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(77, 'updateRelatedMeetingsGeocodeInfo', 'modules/Opportunities/OpportunitiesJjwg_MapsLogicHook.php', 'OpportunitiesJjwg_MapsLogicHook', 'updateRelatedMeetingsGeocodeInfo');
$hook_array['after_save'][] = Array(78, 'updateRelatedProjectGeocodeInfo', 'modules/Opportunities/OpportunitiesJjwg_MapsLogicHook.php', 'OpportunitiesJjwg_MapsLogicHook', 'updateRelatedProjectGeocodeInfo');
$hook_array['after_relationship_add'] = Array();
$hook_array['after_relationship_add'][] = Array(77, 'addRelationship', 'modules/Opportunities/OpportunitiesJjwg_MapsLogicHook.php', 'OpportunitiesJjwg_MapsLogicHook', 'addRelationship');
$hook_array['after_relationship_delete'] = Array();
$hook_array['after_relationship_delete'][] = Array(77, 'deleteRelationship', 'modules/Opportunities/OpportunitiesJjwg_MapsLogicHook.php', 'OpportunitiesJjwg_MapsLogicHook', 'deleteRelationship');

$hook_array['before_save'][] = [
    2,
    'save contact',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'saveContact'
];

$hook_array['before_save'][] = [
    3,
    'save tour',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'saveTour'
];

$hook_array['before_save'][] = [
    4,
    'set name',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'setName'
];

$hook_array['before_save'][] = [
    5,
    'save nitfiles',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'saveNitfiles'
];

$hook_array['before_save'][] = [
    6,
    'set type Datetime',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'setTypeDatetime'
];

$hook_array['after_retrieve'][] = [
    1,
    'fill children Data',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'fillChildrenData'
];

$hook_array['after_retrieve'][] = [
    2,
    'set href bean',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'setHrefBean'
];

$hook_array['after_retrieve'][] = [
    3,
    'set href agreement confirm',
    'custom/modules/Opportunities/Hook/Opportunity.php',
    'OpportunityHookOpportunity',
    'setHrefAgreementConfirm'
];

