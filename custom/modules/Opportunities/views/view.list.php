<?php

require_once 'custom/include/MVC/View/views/view.list.php';
require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';
require_once 'custom/include/ListView/ListViewSmarty2.php';
require_once 'custom/modules/Users/BeanHelper/User.php';
require_once 'custom/modules/Contacts/BeanHelper/Contact.php';
require_once 'custom/modules/Tours/BeanHelper/Tour.php';

class OpportunitiesViewList extends ViewList {


    function preDisplay() {
        parent::preDisplay();
        
        $this->lv = new ListViewSmarty2();
    }
    
    public function listViewPrepare()
    {
        parent::listViewPrepare();
        
        $this->params['custom_where'] = "";
        $this->params['custom_from'] = "";
        
        if (!empty($_REQUEST['phone_advanced'])) {
            $contactHelper = new ContactBeanHelperContact($this->bean);
            $this->params['custom_where'] .= $contactHelper->getWherePhone($_REQUEST['phone_advanced']);
        }
        
        if (!empty($_REQUEST['email_advanced'])) {
            $contactHelper = new ContactBeanHelperContact($this->bean);
            $this->params['custom_where'] .= $contactHelper->getWhereEmail($_REQUEST['email_advanced']);
        }
        
        $tourFields = $this->getNotEmptyToursFields();
        
        if (!empty($tourFields)) {
            $tourHelper = new TourBeanHelperTour($this->bean);
            $this->params['custom_from'] .= $tourHelper->getCustomFrom();
            $this->params['custom_where'] .= $tourHelper->getCustomWhere($tourFields);
        }
    }
    
    private function getNotEmptyToursFields() {
        $keys = array_keys($_REQUEST);
        $toursKeys = [];
        
        foreach ($keys as $key) {
            if (!empty($_REQUEST[$key]) && strpos($key, "tours") !== false) {
                $toursKeys[] = $key;
            }
        }
        
        return $toursKeys;
    }

}
