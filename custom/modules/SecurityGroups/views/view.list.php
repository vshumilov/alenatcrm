<?php

require_once('custom/include/MVC/View/views/view.list.php');
require_once('custom/include/ListView/ListViewSmarty2.php');

class SecurityGroupsViewList extends ViewList
{
    /**
     * @see ViewList::preDisplay()
     */
    public function preDisplay(){
        
        parent::preDisplay();

        $this->lv = new ListViewSmarty2();
    }
}
