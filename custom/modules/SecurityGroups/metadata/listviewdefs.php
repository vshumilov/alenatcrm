<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');


$module_name = 'SecurityGroups';
$listViewDefs[$module_name] = array(
    'NAME' => array(
        'width' => '32',
        'label' => 'LBL_NAME',
        'default' => true,
        'link' => true),
    'BILLING_ADDRESS_CITY' =>
    array(
        'width' => '10%',
        'label' => 'LBL_CITY',
        'default' => true,
    ),
    'PHONE_OFFICE' =>
    array(
        'width' => '10%',
        'label' => 'LBL_LIST_PHONE',
        'default' => true,
    ),
    'EMAIL' =>
    array(
        'width' => '15%',
        'label' => 'LBL_EMAIL',
        'sortable' => false,
        'default' => true,
    ),
    'ASSIGNED_USER_NAME' =>
    array(
        'width' => '10%',
        'label' => 'LBL_LIST_ASSIGNED_USER',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => false,
    ),
);
