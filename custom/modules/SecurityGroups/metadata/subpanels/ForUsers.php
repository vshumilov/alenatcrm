<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton'),
    ),
    'where' => '',
    'list_fields' => array(
        'name' => array(
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '25%',
        ),
        'billing_address_city' => array(
            'name' => 'billing_address_city',
            'vname' => 'LBL_CITY',
        ),
        'phone_office' => array(
            'name' => 'phone_office',
            'vname' => 'LBL_LIST_PHONE',
        ),
        'email' => array(
            'name' => 'email',
            'vname' => 'LBL_EMAIL',
        ),
        'assigned_user_name' => array(
            'name' => 'assigned_user_name',
            'vname' => 'LBL_LIST_ASSIGNED_USER',
        ),
        'edit_button' => array(
            'widget_class' => 'SubPanelEditButton',
            'securitygroup_noninherit_id' => 'securitygroup_noninherit_id',
            'module' => 'SecurityGroups',
            'width' => '5%',
        ),
        'remove_button' => array(
            'widget_class' => 'SubPanelRemoveButton',
            'module' => 'SecurityGroups',
            'width' => '5%',
            'refresh_page' => true,
        ),
    ),
);