<?php

class SmslogHookSmslog
{

    protected $maxNameLength = 50;

    public function setName(Smslog $bean)
    {
        $descriptionData = explode(" ", $bean->description);

        $bean->name = array_pop($descriptionData);
    }

}
