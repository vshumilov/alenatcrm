<?php

require_once 'custom/include/Nit/BeanHelper.php';

class TourBeanHelperCountry extends BeanHelper
{

    public $module = 'Countries';

    public function saveBean()
    {
        $tour = $this->bean;

        $bean = BeanFactory::getBean($this->module);

        $beanIdName = strtolower($bean->object_name) . '_id';
        $beanNameField = strtolower($bean->object_name) . '_name';
        
        if (empty($tour->{$beanNameField})) {
            return;
        }

        if (!empty($tour->{$beanIdName})) {
            $bean = BeanFactory::getBean($this->module, $tour->{$beanIdName});
        } else {
            $beanId = $this->getBeanIdByName($tour->{$beanNameField});
            
            if (!empty($beanId)) {
                $bean = BeanFactory::getBean($this->module, $beanId);
            }
        }
        
        $bean->name = ucfirst($tour->{$beanNameField});
        
        $beanId = $bean->save();

        return $beanId;
    }

    protected function getBeanIdByName($name)
    {
        if (empty($name)) {
            return;
        }
        
        $sql = "
        SELECT
            id
        FROM
            <tableName>
        WHERE
            name LIKE '<name>' AND
            deleted = '0'
        LIMIT
            1
        ";

        $query = strtr($sql, [
            '<tableName>' => strtolower($this->module),
            '<name>' => $name
        ]);
        
        $beanId = dbGetScalar($query);
        
        return $beanId;
    }

}
