<?php

require_once 'custom/include/Nit/BeanHelper.php';

class TourBeanHelperCity extends BeanHelper
{

    public $module = 'Cities';

    public function saveBean()
    {
        $tour = $this->bean;

        $bean = BeanFactory::getBean($this->module);

        $beanIdName = strtolower($bean->object_name) . '_id';
        $beanNameField = strtolower($bean->object_name) . '_name';
        
        if (empty($tour->{$beanNameField})) {
            return;
        }

        if (!empty($tour->{$beanIdName})) {
            $bean = BeanFactory::getBean($this->module, $tour->{$beanIdName});
        } else {
            $beanId = $this->getBeanIdByNameAndCountry($tour->{$beanNameField}, $tour->country_id);
            
            if (!empty($beanId)) {
                $bean = BeanFactory::getBean($this->module, $beanId);
            }
        }
        
        $bean->name = ucfirst($tour->{$beanNameField});
        $bean->country_id = $tour->country_id;
        
        $beanId = $bean->save();

        return $beanId;
    }

    protected function getBeanIdByNameAndCountry($name, $countryId)
    {
        if (empty($name) || empty($countryId)) {
            return;
        }
        
        $sql = "
        SELECT
            id
        FROM
            <tableName>
        WHERE
            name LIKE '<name>' AND
            country_id = '<countryId>' AND
            deleted = '0'
        LIMIT
            1
        ";

        $query = strtr($sql, [
            '<tableName>' => strtolower($this->module),
            '<name>' => $name,
            '<countryId>' => $countryId
        ]);
        
        $beanId = dbGetScalar($query);
        
        return $beanId;
    }

}
