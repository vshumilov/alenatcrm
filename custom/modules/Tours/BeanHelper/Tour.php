<?php

require_once 'custom/modules/Contacts/BeanHelper/Contact.php';
require_once 'custom/include/Nit/Helper/Email.php';
require_once 'custom/include/Nit/Helper/Phone.php';
require_once 'custom/include/Nit/Helper/Guid.php';
require_once 'custom/include/Nit/Helper/Date.php';

class TourBeanHelperTour extends ContactBeanHelperContact
{

    public function getName()
    {
        $nameData = [];

        if (!empty($this->bean->hotel)) {
            $nameData[] = $this->bean->hotel;
        }

        if (!empty($this->bean->city_name)) {
            $nameData[] = $this->bean->city_name;
        }

        return implode(" ", $nameData);
    }

    public function getCustomFrom()
    {
        $query = "
            LEFT JOIN
                tours
            ON
                opportunities.tour_id = tours.id AND
                tours.deleted = '0'
        ";

        return $query;
    }

    public function getCustomWhere($toursFields)
    {
        if (empty($toursFields)) {
            return;
        }

        $query = "";

        $fields = $this->bean->field_defs;

        foreach ($fields as $field) {
            if (!in_array($field['name'], $toursFields)) {
                continue;
            }

            $query .= " 
                AND tours.<field> = ";

            if ($field['type'] == 'varchar') {
                $query .= "'%<value>%'";
            } else if ($field['type'] == 'text') {
                $query .= "'%<value>%'";
            } else {
                $query = "'<value>'";
            }

            $query = strtr($query, [
                "<field>" => $field['name'],
                "<value>" => $_REQUEST[$field['name']]
            ]);
        }

        return $query;
    }

    public function getToursQuery()
    {
        $sql = "
        SELECT
            t.id
        FROM
            tours AS t
        WHERE
            t.deleted = '0'
        ";

        return $sql;
    }

    public function createFromSanatorium($priceToApartmentId, $dateFrom, $dateTo, $countPlaces, $countPlacesWithTreatment, $lastName, $firstName, $middleName, $birthDate, $email, $phoneMobile)
    {
        if (empty($priceToApartmentId) ||
                empty($dateFrom) ||
                empty($dateTo) ||
                !isset($countPlaces) ||
                !isset($countPlacesWithTreatment) ||
                empty($lastName) ||
                empty($firstName) ||
                empty($middleName) ||
                empty($birthDate) ||
                empty($email) ||
                empty($phoneMobile)) {
            throw new Exception('Some of required params are empty');
        }

        $guidHelper = new HelperGuid();

        if (!$guidHelper->validate($priceToApartmentId)) {
            throw new Exception('priceToApartmentId is not valid');
        }

        $dateHelper = new HelperDate();
        //TODO: check if $dateFrom > $dateTo
        if (!$dateHelper->validate($dateFrom)) {
            throw new Exception('dateFrom is not valid');
        }

        if (!$dateHelper->validate($dateTo)) {
            throw new Exception('dateTo is not valid');
        }

        //TODO: check if $birthDate is adult
        if (!$dateHelper->validate($birthDate)) {
            throw new Exception('birthDate is not valid');
        }
        
        if (!($countPlaces > 0 || $countPlacesWithTreatment > 0)) {
            throw new Exception('countPlaces + countPlacesWithTreatment = 0');
        }

        $phoneHelper = new HelperPhone();

        if (!$phoneHelper->isValidMobilePhone($phoneMobile)) {
            throw new Exception('Phone is not valid mobile phone');
        }

        $emailHelper = new HelperEmail();

        if (!$emailHelper->validate($email)) {
            throw new ServerErrorHttpException('Email is not valid');
        }

        $row = $this->getPriceAndApartmentIds($priceToApartmentId);

        $apartment = BeanFactory::getBean('Apartments', $row['apartment_id']);

        $price = BeanFactory::getBean('Prices', $row['price_id']);

        if (empty($apartment->id) || empty($price->parent_id)) {
            throw new Exception("Price or apartment doesn't exist");
        }

        $sanatorium = BeanFactory::getBean('Sanatoriums', $price->parent_id);

        if (empty($sanatorium->id)) {
            throw new Exception("sanatorium doesn't exist");
        }

        $timeDate = new TimeDate();

        $agreement = new Opportunity();

        $agreement->contact_name = "$lastName $firstName $middleName"; //????
        //TODO:
        //contacts_gender
        $agreement->contacts_birth_date = $timeDate->asUserDate(new DateTime($birthDate));
        $agreement->contacts_phone_mobile = $phoneMobile;
        $agreement->contacts_email = $email;

        $agreement->tours_country_name = $sanatorium->country_name;
        $agreement->tours_country_id = $sanatorium->country_id;
        $agreement->tours_operator_name = ''; //TODO: change in future
        $agreement->tours_city_name = $sanatorium->city_name;
        $agreement->tours_city_id = $sanatorium->city_id;

        //tours_route
        $agreement->tours_hotel = $sanatorium->name;
        $agreement->tours_foodoption_name = $price->foodoption_name;
        $agreement->tours_foodoption_id = $price->foodoption_id;
        $agreement->tours_apartment_name = $apartment->name;
        $agreement->tours_apartment_id = $apartment->id;


        $dateFromDateTime = new DateTime($dateFrom);
        $dateToDateTime = new DateTime($dateTo);

        $agreement->start_date_tour = $timeDate->asUserDate($dateFromDateTime);
        $agreement->end_date_tour = $timeDate->asUserDate($dateToDateTime);
        $interval = $dateFromDateTime->diff($dateToDateTime);
        $agreement->tours_days = (int) $interval->format('%d');
        $agreement->tourists = $agreement->contact_name; //TODO: tourists

        $agreement->currency_id = $price->currency_id;
        
        $agreement->count_places = $countPlaces;
        $agreement->count_places_with_treatment = $countPlacesWithTreatment;

        $agreement->amount = $this->calculateAmount($price->cost, $agreement->tours_days, $countPlaces) + $this->calculateAmount($price->cost_with_treatment, $agreement->tours_days, $countPlacesWithTreatment);
        $agreement->prepayment = $agreement->amount;
        
        $agreement->price_id = $price->id;
        $agreement->price_name = $agreement->name;
        
        $agreement->save();

        return $agreement;
    }
    
    protected function calculateAmount($cost, $days, $countPlaces) {
        return (float)$cost * (int)$days * (int)$countPlaces;
    }

    public function getPriceAndApartmentIds($priceToApartmentId)
    {
        if (empty($priceToApartmentId)) {
            return;
        }

        $sql = "
        SELECT 
            apartment_id,
            price_id
        FROM
            prices_apartments
        WHERE
            id = <priceToApartmentId> AND
            deleted = '0'
        ";

        $query = strtr($sql, ['<priceToApartmentId>' => dbQuote($priceToApartmentId)]);

        return dbGetRow($query);
    }

}
