<?php

$hook_version = 1;
$hook_array = [];

$hook_array['before_save'] = [];

$hook_array['before_save'][] = [
    1,
    'save country',
    'custom/modules/Tours/Hook/Tour.php',
    'TourHookTour',
    'saveCountry'
];

$hook_array['before_save'][] = [
    2,
    'save city',
    'custom/modules/Tours/Hook/Tour.php',
    'TourHookTour',
    'saveCity'
];

$hook_array['before_save'][] = [
    3,
    'save operator',
    'custom/modules/Tours/Hook/Tour.php',
    'TourHookTour',
    'saveOperator'
];

$hook_array['before_save'][] = [
    4,
    'set name',
    'custom/modules/Tours/Hook/Tour.php',
    'TourHookTour',
    'setName'
];

