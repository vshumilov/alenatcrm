<?php

require_once 'custom/modules/Tours/BeanHelper/Tour.php';
require_once 'custom/modules/Tours/BeanHelper/Country.php';
require_once 'custom/modules/Tours/BeanHelper/City.php';
require_once 'custom/modules/Tours/BeanHelper/Operator.php';

class TourHookTour
{
    public function saveCountry(Tour $bean)
    {
        $countryBeanHelper = new TourBeanHelperCountry($bean);
        $bean->country_id = $countryBeanHelper->saveBean();
    }
    
    public function saveCity(Tour $bean)
    {
        $cityBeanHelper = new TourBeanHelperCity($bean);
        $bean->city_id = $cityBeanHelper->saveBean();
    }
    
    public function saveOperator(Tour $bean)
    {
        $operatorBeanHelper = new TourBeanHelperOperator($bean);
        $bean->operator_id = $operatorBeanHelper->saveBean();
    }
    
    public function setName(Tour $bean)
    {
        $tourBeanHelper = new TourBeanHelperTour($bean);
        $bean->name = $tourBeanHelper->getName();
    }
}
