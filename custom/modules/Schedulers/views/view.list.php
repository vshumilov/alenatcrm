<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('custom/include/MVC/View/views/view.list.php');

class SchedulersViewList extends ViewList {

    public function display() {
        parent::display();
        $this->seed->displayCronInstructions();
    }

}
