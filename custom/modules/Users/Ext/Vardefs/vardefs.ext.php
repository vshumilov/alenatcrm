<?php 
 //WARNING: The contents of this file are auto-generated



if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['User']['fields']['city'] = [
    'name' => 'city',
    'rname' => 'name',
    'id_name' => 'city_id',
    'vname' => 'LBL_CITY',
    'type' => 'relate',
    'table' => 'cities',
    'join_name' => 'cities',
    'isnull' => 'true',
    'module' => 'Cities',
    'dbType' => 'varchar',
    'link' => 'cities',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['User']['fields']['city_id'] = [
    'name' => 'city_id',
    'vname' => 'LBL_CITY_ID',
    'type' => 'id',
    'audited' => true,
    'source' => 'non-db',
];

$dictionary['User']['fields']['cities'] = [
    'name' => 'cities',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Cities',
    'bean_name' => 'City',
    'vname' => 'LBL_CITIES',
];

$dictionary['User']['fields']['aclrole_name'] = [
    'name' => 'aclrole_name',
    'rname' => 'name',
    'id_name' => 'aclrole_id',
    'vname' => 'LBL_ACLROLE_NAME',
    'type' => 'relate',
    'table' => 'acl_roles',
    'join_name' => 'aclroles',
    'isnull' => 'true',
    'module' => 'ACLRoles',
    'dbType' => 'varchar',
    'link' => 'aclroles',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
];
$dictionary['User']['fields']['aclrole_id'] = [
    'name' => 'aclrole_id',
    'vname' => 'LBL_ACLROLE_ID',
    'type' => 'id',
    'audited' => true,
    'source' => 'non-db',
];


?>