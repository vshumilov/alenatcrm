<?php

global $mod_strings;

$popupMeta = array('moduleMain' => 'User',
    'varName' => 'USER',
    'orderBy' => 'user_name',
    'whereClauses' => array(
        'first_name' => 'users.first_name',
        'last_name' => 'users.last_name',
        'user_name' => 'users.user_name',
        'is_group' => 'users.is_group',
    ),
    'searchInputs' =>
    array(
        'first_name',
        'last_name',
        'user_name',
    ),
    'listviewdefs' => array(
        'NAME' => array(
            'width' => '20',
            'label' => 'LBL_LIST_NAME',
            'link' => true,
            'related_fields' => array('last_name', 'first_name'),
            'orderBy' => 'last_name',
            'default' => true),
        'TITLE' => array(
            'width' => '15',
            'label' => 'LBL_TITLE',
            'link' => true,
            'default' => true),
        'REPORTS_TO_NAME' => array(
            'width' => '15',
            'label' => 'LBL_LIST_REPORTS_TO_NAME',
            'link' => true,
            'sortable' => false,
            'default' => false),
        'EMAIL1' => array(
            'width' => '15',
            'label' => 'LBL_LIST_EMAIL',
            'link' => true,
            'customCode' => '{$EMAIL1_LINK}{$EMAIL1}</a>',
            'default' => true,
            'sortable' => false),
        'PHONE_WORK' => array(
            'width' => '10',
            'label' => 'LBL_LIST_PHONE',
            'link' => true,
            'default' => true),
    ),
    'searchdefs' => array(
        'first_name' =>
        array(
            'name' => 'first_name',
            'default' => true,
            'width' => '10%',
        ),
        'last_name' =>
        array(
            'name' => 'last_name',
            'default' => true,
            'width' => '10%',
        ),
        'user_name' =>
        array(
            'name' => 'user_name',
            'default' => true,
            'width' => '10%',
        ),
        'city' =>
        array(
            'name' => 'city',
            'default' => true,
            'width' => '10%',
            'displayParams' => array(
                'useIdSearch' => true,
            )
        ),
    )
);
