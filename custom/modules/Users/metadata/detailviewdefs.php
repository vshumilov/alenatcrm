<?php

$viewdefs['Users']['DetailView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
        'form' => array(
            'headerTpl' => 'modules/Users/tpls/DetailViewHeader.tpl',
            'footerTpl' => 'modules/Users/tpls/DetailViewFooter.tpl',
        ),
    ),
    'panels' => array(
        'LBL_USER_INFORMATION' => array(
            array(
                'full_name'
            ),
            array(
                'user_name'
            ),
            array(
                'status',
            ),
            array(
                array(
                    'name' => 'UserType',
                    'customCode' => '{$USER_TYPE_READONLY}',
                ),
            ),
            array(
                'picture'
            ),
        ),
        'LBL_EMPLOYEE_INFORMATION' => array(
            array(
                'employee_status',
            ),
            array(
                'show_on_employees'
            ),
            array(
                'title'
            ),
            array(
                'phone_work'
            ),
            array(
                'department',
            ),
            array(
                'phone_mobile'
            ),
            array(
                'reports_to_name',
            ),
            array(
                'phone_other'
            ),
            array(
                'phone_fax'
            ),
            array(
                'phone_home'
            ),
            array(
                'messenger_type',
            ),
            array(
                'messenger_id'
            ),
            array(
                'address_street',
            ),
            array(
                'address_city'
            ),
            array(
                'address_state',
            ),
            array(
                'address_postalcode'
            ),
            array(
                'address_country'
            ),
            array(
                'description'
            ),
        ),
    ),
);
