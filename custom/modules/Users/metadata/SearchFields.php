<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$searchFields['Users'] = array(
    'user_name' => array('query_type' => 'default'),
    'first_name' => array('query_type' => 'default'),
    'last_name' => array('query_type' => 'default'),
    'search_name' => array('query_type' => 'default', 'db_field' => array('first_name', 'last_name'), 'force_unifiedsearch' => true),
    'is_admin' => array('query_type' => 'default', 'operator' => '=', 'input_type' => 'checkbox'),
    'is_group' => array('query_type' => 'default', 'operator' => '=', 'input_type' => 'checkbox'),
    'status' => array('query_type' => 'default', 'options' => 'user_status_dom', 'template_var' => 'STATUS_OPTIONS', 'options_add_blank' => true),
    'email' => array(
        'query_type' => 'default',
        'operator' => 'subquery',
        'subquery' => 'SELECT eabr.bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (ea.id = eabr.email_address_id) WHERE eabr.deleted=0 and ea.email_address LIKE',
        'db_field' => array(
            'id',
        )
    ),
    'phone' => array(
        'query_type' => 'default',
        'operator' => 'subquery',
        'subquery' => array('SELECT id FROM users where phone_home LIKE ',
            'SELECT id FROM users where phone_fax LIKE',
            'SELECT id FROM users where phone_other LIKE',
            'SELECT id FROM users where phone_work LIKE',
            'SELECT id FROM users where phone_mobile LIKE',
            'OR' => true
        ),
        'db_field' => array(
            'id',
        )
    ),
    'city' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'city_id' =>
    array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            u.id 
                       FROM 
                            users AS u
                       JOIN
                            securitygroups_users AS su 
                       ON
                            su.user_id = u.id AND
                            su.deleted = '0'
                       JOIN
                            securitygroups AS s
                       ON
                            su.securitygroup_id = s.id
                       WHERE 
                            s.city_id LIKE '%{0}%' AND 
                            s.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
);
