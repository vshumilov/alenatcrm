<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$listViewDefs['Users'] = array(
    'NAME' => array(
        'width' => '30', 
        'label' => 'LBL_LIST_NAME', 
        'link' => true,
        'related_fields' => array('last_name', 'first_name'),
        'orderBy' => 'last_name',
        'default' => true),
    'USER_NAME' => array(
        'width' => '5', 
        'label' => 'LBL_USER_NAME', 
        'link' => true,
        'default' => true),
    'ACLROLE_NAME' => array(
        'width' => '15',
        'label' => 'LBL_ACLROLE_NAME',
        'link' => true,
        'module' => 'ACLRoles',
        'id' => 'ACLROLE_ID',
        'default' => true,
        'sortable' => false,
        'related_fields' =>
        array(
            'aclrole_id',
        ),
    ),
    'EMAIL1' => array(
        'width' => '30',
        'sortable' => false, 
        'label' => 'LBL_LIST_EMAIL', 
        'link' => true,
        'default' => true),
    'PHONE_WORK' => array(
        'width' => '25', 
        'label' => 'LBL_LIST_PHONE', 
        'link' => true,
        'default' => true),
    'STATUS' => array(
        'width' => '10', 
        'label' => 'LBL_STATUS', 
        'link' => false,
        'default' => true),
    'IS_ADMIN' => array(
        'width' => '10', 
        'label' => 'LBL_ADMIN', 
        'link' => false,
        'default' => true),
    'IS_GROUP' => array(
        'width' => '10', 
        'label' => 'LBL_LIST_GROUP', 
        'link' => true,
        'default' => false),
);
?>
