<?php

require_once 'custom/include/Nit/BeanHelper/Role.php';

class UserBeanHelperUser extends BeanHelperRole {

    public function getCurrentOrganization() {
        $this->bean->load_relationship('SecurityGroups');

        $securityGroups = $this->bean->SecurityGroups->getBeans();

        if (empty($securityGroups)) {
            return;
        }

        $currentOrganization = array_shift($securityGroups);

        return $currentOrganization;
    }
    
    public function setSearchByCurrentOrganization()
    {
        $searchMode = 'basic';
        
        if (empty($_REQUEST['searchFormTab']) || $_REQUEST['searchFormTab'] == 'advanced_search') {
            $searchMode = 'advanced';
        }
        
        if (empty($_REQUEST['city_id_' . $searchMode])) {
            global $current_user;

            $currentOrganization = $this->getCurrentOrganization();

            if (!empty($currentOrganization->city_id)) {
                $_REQUEST['city_id_' . $searchMode] = $currentOrganization->city_id;
                $_REQUEST['city_' . $searchMode] = $currentOrganization->city;
            }
        }
    }

}
