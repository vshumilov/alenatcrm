<?php

require_once 'custom/include/Nit/BeanHelper.php';

class PriceBeanHelperPrice extends BeanHelper
{

    public $multienumDelimiter = '^,^';

    public function prepareName()
    {
        global $timedate;

        $nameArray = [];

        if (!empty($this->bean->date_from) && !empty($this->bean->date_to)) {
            $nameArray[] = $timedate->to_display_date($this->bean->date_from) . " - " . $timedate->to_display_date($this->bean->date_to);
        }

        if (!empty($this->bean->pricelevel_name)) {
            $nameArray[] = $this->bean->pricelevel_name;
        }

        if (!empty($this->bean->foodoption_name)) {
            $nameArray[] = $this->bean->foodoption_name;
        }
        
        $housesNames = $this->getHousesNamesStr();
        
        if (!empty($housesNames)) {
            sort($housesNames);
            $nameArray[] = implode(", ", $housesNames);
        }
        
        $apartmentsNames = $this->getApartmentsNamesStr();
        
        if (!empty($apartmentsNames)) {
            sort($apartmentsNames);
            $nameArray[] = implode(", ", $apartmentsNames);
        }

        $this->name = implode(", ", $nameArray);

        return $this->name;
    }
    
    protected function getHousesNamesStr()
    {
        $houses = $this->getRelBeans('houses');
        $housesNames = [];
        
        if (!empty($houses)) {
            foreach ($houses as $house) {
                $housesNames[] = 'к.' . $house->number;
            }
        }
        
        return $housesNames;
    }
    
    protected function getApartmentsNamesStr()
    {
        $apartments = $this->getRelBeans('apartments');
        $apartmentsNames = [];
        
        if (!empty($apartments)) {
            foreach ($apartments as $apartment) {
                $strArray = [];
                
                if ($apartment->rooms_count > 1) {
                    $strArray[] = $apartment->rooms_count . 'ком.';
                }
                
                if (!empty($apartment->places_count)) {
                    $strArray[] = $apartment->places_count . 'м.';
                }
                
                if (!empty($apartment->description) && mb_strlen($apartment->description) < 15) {
                    $strArray[] = $apartment->description;
                }
                
                $apartmentsNames[] = implode(" ", $strArray);
            }
        }
        
        return $apartmentsNames;
    }

    public function setHouses()
    {
        return $this->setVirtualMultienum('house');
    }

    public function setApartaments()
    {
        return $this->setVirtualMultienum('apartment');
    }

    protected function setVirtualMultienum($beanName = 'house')
    {
        $relName = $this->getRelName($beanName);
        
        $beans = $this->getRelBeans($relName);
        
        $multienumFieldName = $this->getMultienumFieldName($beanName);

        $newBeansIds = explode(',', strtr($this->bean->{$multienumFieldName}, ['^' => '']));

        if (!empty($beans)) {
            foreach ($beans as $bean) {
                if (empty($newBeansIds) || !in_array($bean->id, $newBeansIds)) {
                    $this->bean->{$relName}->delete($this->bean->id, $bean->id);
                    continue;
                }
            }
        }

        if (empty($newBeansIds)) {
            return $this->bean;
        }

        foreach ($newBeansIds as $newBeanId) {
            $this->bean->{$relName}->add($newBeanId);
        }

        return $this->bean;
    }

    public function getHouses()
    {
        return $this->getVirtualMultienum('house');
    }

    public function getApartaments()
    {
        return $this->getVirtualMultienum('apartment');
    }

    protected function getVirtualMultienum($beanName = 'house')
    {
        $relName = $this->getRelName($beanName);
        
        $beans = $this->getRelBeans($relName);

        $multienumFieldName = $this->getMultienumFieldName($beanName);

        if (empty($beans)) {
            return $this->bean;
        }

        $beansIds = [];

        foreach ($beans as $bean) {
            $beansIds[] = $bean->id;
        }

        $this->bean->{$multienumFieldName} = '^' . implode($this->multienumDelimiter, $beansIds) . '^';

        return $this->bean;
    }

    protected function getRelBeans($relName)
    {
        $this->bean->load_relationship($relName);

        $beans = [];

        if (!empty($this->bean->{$relName})) {
            $beans = $this->bean->{$relName}->getBeans();
        }
        
        return $beans;
    }

    protected function getRelName($beanName)
    {
        $relName = $beanName . 's';

        return $relName;
    }

    protected function getMultienumFieldName($beanName)
    {
        $multienumFieldName = $beanName . 's_multienum';

        return $multienumFieldName;
    }

}
