<?php

require_once 'custom/modules/Prices/BeanHelper/Price.php';

class PriceHookPrice
{
    public function setName(Price $bean)
    {
        if ($bean->is_after_save) {
            return;
        }
        
        $priceHelper = new PriceBeanHelperPrice($bean);
        
        $bean->name = $priceHelper->prepareName();
        
        $bean->is_after_save = true;
        
        $bean->save();
    }
    
    public function setHouses(Price $bean) 
    {
        if ($bean->is_after_save) {
            return;
        }
        
        $priceHelper = new PriceBeanHelperPrice($bean);
        
        $bean = $priceHelper->setHouses();
    }
    
    public function setApartaments(Price $bean) 
    {
        if ($bean->is_after_save) {
            return;
        }
        
        $priceHelper = new PriceBeanHelperPrice($bean);
        
        $bean = $priceHelper->setApartaments();
    }
    
    public function getHouses(Price $bean) 
    {
        $priceHelper = new PriceBeanHelperPrice($bean);
        
        $bean = $priceHelper->getHouses();
    }
    
    public function getApartaments(Price $bean) 
    {
        $priceHelper = new PriceBeanHelperPrice($bean);
        
        $bean = $priceHelper->getApartaments();
    }
}
