<?php

$hook_version = 1;
$hook_array = Array();

$hook_array['before_save'] = [];
$hook_array['after_save'] = [];
$hook_array['after_retrieve'] = [];

$hook_array['after_save'][] = [
    1,
    'set houses',
    'custom/modules/Prices/Hook/Price.php',
    'PriceHookPrice',
    'setHouses'
];

$hook_array['after_save'][] = [
    2,
    'set apartaments',
    'custom/modules/Prices/Hook/Price.php',
    'PriceHookPrice',
    'setApartaments'
];

$hook_array['after_save'][] = [
    3,
    'set name',
    'custom/modules/Prices/Hook/Price.php',
    'PriceHookPrice',
    'setName'
];

$hook_array['after_retrieve'][] = [
    1,
    'get houses',
    'custom/modules/Prices/Hook/Price.php',
    'PriceHookPrice',
    'getHouses'
];

$hook_array['after_retrieve'][] = [
    2,
    'get apartaments',
    'custom/modules/Prices/Hook/Price.php',
    'PriceHookPrice',
    'getApartaments'
];
