<?php

require_once 'custom/include/Nit/BeanHelper.php';
require_once 'modules/SugarNitfile/SugarNitfile.php';

class ImageBeanHelperImage extends BeanHelper
{

    public $fileFieldName = 'file';
    public $thumbnailFieldNameKey = 'thumbnail_name_';

    public function saveFile()
    {
        if ($this->bean)
        
        if (empty($this->bean)) {
            return $this->bean;
        }
        
        if (empty($this->bean->parent_id)) {
            $this->bean->parent_type = '';
        }
        
        if (empty($_FILES)) {
            return $this->bean;
        }
        
        $savedNitfiles = SugarNitfile::saveBeanNitfiles($this->bean, $this->fileFieldName);
        
        if (empty($savedNitfiles)) {
            return $this->bean;
        }
        
        $sugarNitfile = array_shift($savedNitfiles);

        if (empty($sugarNitfile)) {
            return $this->bean;
        }
        
        $this->bean->file_id = $sugarNitfile->id;
        $this->bean->file_ext = $sugarNitfile->file_ext;
        
        $this->bean = $this->createThumbnails($sugarNitfile);
        
        return $this->bean;
    }
    
    public function createThumbnails($sugarNitfile)
    {
        if (empty($this->bean)) {
            return $this->bean;
        }

        $thumbnailFormats = $sugarNitfile->getThumbnailFormats();

        if (empty($thumbnailFormats)) {
            $GLOBALS['log']->fatal("thumbnail_formats of file field not added to Images vardefs");
            return $this->bean;
        }

        foreach ($thumbnailFormats as $formatKey => $format) {
            $sugarNitfile->createThumbnail($formatKey, true);
            $thumbnailFieldName = $this->thumbnailFieldNameKey . $formatKey;
            $this->bean->{$thumbnailFieldName} = $sugarNitfile->getThumbnailFileName($format);
        }
        
        return $this->bean;
    }

    protected function getFileField()
    {
        if (empty($this->bean)) {
            return $this->bean;
        }

        $fields = $this->bean->field_defs;

        foreach ($fields as $field) {
            if ($field['name'] == $this->fileFieldName) {
                return $field;
            }
        }
    }
}
