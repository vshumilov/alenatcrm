<?php

require_once 'custom/modules/Images/BeanHelper/Image.php';

class ImageHookImage
{
    public function saveFile(Image $bean)
    {
        $imageBeanHelper = new ImageBeanHelperImage($bean);
        
        $bean = $imageBeanHelper->saveFile();
        
        return $bean;
    }
}
