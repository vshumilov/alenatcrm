<?php

require_once 'custom/include/Nit/BeanHelper/Role.php';

class AccountBeanHelperAccount extends BeanHelperRole
{

    public function setStatuses()
    {
        $this->setLastAgreementStatus();
        $this->setLastAcceptmeteringdevicesStatus();

        return $this->bean;
    }
    
    public function setLastAgreementStatus()
    {
        $agreement = $this->getLastAgreement();

        if (empty($agreement['id'])) {
            return;
        }

        $opportunityTypeDom = translate('opportunity_type_dom');

        $this->bean->last_agreement_id = $agreement['id'];

        if (!empty($opportunityTypeDom[$agreement['opportunity_type']])) {
            $this->bean->last_agreement_status = $opportunityTypeDom[$agreement['opportunity_type']];
        }
    }

    public function getLastAgreement()
    {
        $sql = "
        SELECT
            o.*
        FROM
            accounts_opportunities AS ao
        JOIN
            opportunities AS o
        ON
            ao.opportunity_id = o.id AND
            o.deleted = '0'
        WHERE
            ao.account_id = <accountId> AND
            ao.deleted = '0'
        ORDER BY
            ao.date_modified DESC
        ";

        $query = strtr($sql, ['<accountId>' => $this->bean->db->quoted($this->bean->id)]);

        $agreementResult = $this->bean->db->query($query);

        if (!$agreementResult) {
            return;
        }

        $agreement = $this->bean->db->fetchByAssoc($agreementResult);

        return $agreement;
    }

    public function setLastAcceptmeteringdevicesStatus()
    {
        $acceptmeteringdevice = $this->getLastAcceptmeteringdevices();

        if (empty($acceptmeteringdevice['id'])) {
            return;
        }

        $acceptmeteringdeviceTypeDom = translate('acceptmeteringdevice_type_dom');

        $this->bean->last_acceptmeteringdevice_id = $acceptmeteringdevice['id'];

        if (!empty($acceptmeteringdeviceTypeDom[$acceptmeteringdevice['acceptmeteringdevice_type']])) {
            $this->bean->last_acceptmeteringdevice_status = $acceptmeteringdeviceTypeDom[$acceptmeteringdevice['acceptmeteringdevice_type']];
        }
    }

    public function getLastAcceptmeteringdevices()
    {
        $sql = "
        SELECT
            a.*
        FROM
            acceptmeteringdevices AS a
        WHERE
            a.account_id = <accountId> AND
            a.deleted = '0'
        ORDER BY
            a.date_modified DESC
        ";

        $query = strtr($sql, ['<accountId>' => $this->bean->db->quoted($this->bean->id)]);

        $agreementResult = $this->bean->db->query($query);

        if (!$agreementResult) {
            return;
        }

        $agreement = $this->bean->db->fetchByAssoc($agreementResult);

        return $agreement;
    }

    public function getOrganization()
    {
        if (empty($this->bean->city_id)) {
            return;
        }

        $sql = "
        SELECT
            id
        FROM
            securitygroups
        WHERE
            city_id = <cityId> AND
            deleted = '0'
        ";

        $query = strtr($sql, ['<cityId>' => $this->bean->db->quoted($this->bean->city_id)]);

        $securityGroupId = $this->bean->db->getOne($query);

        if (empty($securityGroupId)) {
            return;
        }

        $securityGroup = BeanFactory::getBean('SecurityGroups', $securityGroupId);

        return $securityGroup;
    }

}
