<?php

require_once('custom/include/MVC/View/views/view.list.php');
require_once('custom/modules/Accounts/AccountsListViewSmarty2.php');
require_once 'custom/modules/Users/BeanHelper/User.php';

class AccountsViewList extends ViewList {

    function prepareSearchForm() {
        global $current_user;
        $userHelper = new UserBeanHelperUser($current_user);
        $userHelper->setSearchByCurrentOrganization();

        parent::prepareSearchForm();
    }

    /**
     * @see ViewList::preDisplay()
     */
    public function preDisplay() {
        require_once('modules/AOS_PDF_Templates/formLetter.php');
        formLetter::LVPopupHtml('Accounts');
        parent::preDisplay();

        $this->lv = new AccountsListViewSmarty2();
        $this->lv->targetList = false;
        $this->lv->email = false;
        $this->lv->mergeduplicates = false;
        $this->lv->mailMerge = false;
        $this->lv->showMassupdateFields = false;
    }

}
