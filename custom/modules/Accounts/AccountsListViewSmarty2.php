<?php

require_once('modules/Accounts/AccountsListViewSmarty.php');

class AccountsListViewSmarty2 extends AccountsListViewSmarty
{

    public function process($file, $data, $htmlVar)
    {
        parent::process($file, $data, $htmlVar);
    }

    public function buildExportLink($loc = 'top')
    {
        $this->actionsMenuExtraItems = [];
        
        global $app_strings;
        return "<a href='javascript:void(0)' id=\"export_listview_" . $loc . " \" onclick=\"return sListView.send_form(true, '{$this->seed->module_dir}', 'index.php?entryPoint=export','{$app_strings['LBL_LISTVIEW_NO_SELECTED']}')\">{$app_strings['LBL_EXPORT']}</a>";
    }

}
