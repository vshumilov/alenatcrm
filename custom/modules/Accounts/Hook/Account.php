<?php

require_once 'custom/modules/Accounts/BeanHelper/Account.php';

class AccountHookAccount
{

    public function setStatuses(Account $bean)
    {
        $checkwatersupplysystemBeanHelper = new AccountBeanHelperAccount($bean);

        $bean = $checkwatersupplysystemBeanHelper->setStatuses();
    }

}
