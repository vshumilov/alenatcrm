<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs ['Accounts'] = array(
    'NAME' =>
    array(
        'width' => '25%',
        'label' => 'LBL_LIST_ACCOUNT_NAME',
        'link' => true,
        'default' => true,
    ),
    'BILLING_ADDRESS_CITY' =>
    array(
        'width' => '25%',
        'label' => 'LBL_LIST_CITY',
        'default' => true,
    ),
    'BILLING_ADDRESS_STREET' =>
    array(
        'width' => '25%',
        'label' => 'LBL_STREET',
        'default' => true,
    ),
    'PHONE_OFFICE' =>
    array(
        'width' => '25%',
        'label' => 'LBL_LIST_PHONE',
        'default' => true,
    ),
    'ASSIGNED_USER_NAME' =>
    array(
        'width' => '10%',
        'label' => 'LBL_LIST_ASSIGNED_USER',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => false,
    ),
    'EMAIL1' =>
    array(
        'width' => '15%',
        'label' => 'LBL_EMAIL_ADDRESS',
        'sortable' => false,
        'link' => true,
        'customCode' => '{$EMAIL1_LINK}{$EMAIL1}</a>',
        'default' => false,
    ),
    'BILLING_ADDRESS_STATE' =>
    array(
        'width' => '7%',
        'label' => 'LBL_BILLING_ADDRESS_STATE',
        'default' => false,
    ),
    'BILLING_ADDRESS_POSTALCODE' =>
    array(
        'width' => '10%',
        'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
        'default' => false,
    ),
    'SHIPPING_ADDRESS_STREET' =>
    array(
        'width' => '15%',
        'label' => 'LBL_SHIPPING_ADDRESS_STREET',
        'default' => false,
    ),
    'SHIPPING_ADDRESS_CITY' =>
    array(
        'width' => '10%',
        'label' => 'LBL_SHIPPING_ADDRESS_CITY',
        'default' => false,
    ),
    'SHIPPING_ADDRESS_STATE' =>
    array(
        'width' => '7%',
        'label' => 'LBL_SHIPPING_ADDRESS_STATE',
        'default' => false,
    ),
    'SHIPPING_ADDRESS_POSTALCODE' =>
    array(
        'width' => '10%',
        'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
        'default' => false,
    ),
    'SHIPPING_ADDRESS_COUNTRY' =>
    array(
        'width' => '10%',
        'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
        'default' => false,
    ),
    'DATE_MODIFIED' =>
    array(
        'width' => '5%',
        'label' => 'LBL_DATE_MODIFIED',
        'default' => false,
    ),
    'CREATED_BY_NAME' =>
    array(
        'width' => '10%',
        'label' => 'LBL_CREATED',
        'default' => false,
    ),
    'MODIFIED_BY_NAME' =>
    array(
        'width' => '10%',
        'label' => 'LBL_MODIFIED',
        'default' => false,
    ),
    'FULL_NAME' => array(
        'width' => '20%',
        'label' => 'LBL_FULL_NAME',
        'default' => false,
    ),
    'INN' => array(
        'width' => '10%',
        'label' => 'LBL_INN',
        'default' => false,
    ),
    'KPP' => array(
        'width' => '10%',
        'label' => 'LBL_KPP',
        'default' => false,
    ),
    'OGRN' => array(
        'width' => '20%',
        'label' => 'LBL_OGRN',
        'default' => false,
    ),
    'PAYMENT_ACCOUNT' => array(
        'width' => '10%',
        'label' => 'LBL_PAYMENT_ACCOUNT',
        'default' => false,
    ),
    'BANK_NAME' => array(
        'width' => '10%',
        'label' => 'LBL_BANK_NAME',
        'default' => false,
    ),
    'CORRESPONDENT_ACCOUNT' => array(
        'width' => '10%',
        'label' => 'LBL_CORRESPONDENT_ACCOUNT',
        'default' => false,
    ),
    'BIC' => array(
        'width' => '10%',
        'label' => 'LBL_BIC',
        'default' => false,
    ),
    'GENITIVE_FIO' => array(
        'width' => '20%',
        'label' => 'LBL_GENITIVE_FIO',
        'default' => false,
    ),
    'NOMINATIVE_FIO' => array(
        'width' => '20%',
        'label' => 'LBL_NOMINATIVE_FIO',
        'default' => false,
    ),
);
