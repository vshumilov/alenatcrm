<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$popupMeta = array(
    'moduleMain' => 'Account',
    'varName' => 'ACCOUNT',
    'orderBy' => 'name',
    'whereClauses' => array(
        'name' => 'accounts.name',
        'billing_address_city' => 'accounts.billing_address_city',
        'account_type' => 'accounts.account_type',
        'industry' => 'accounts.industry',
        'billing_address_state' => 'accounts.billing_address_state',
        'billing_address_country' => 'accounts.billing_address_country',
        'email' => 'accounts.email',
        'assigned_user_id' => 'accounts.assigned_user_id',
    ),
    'searchInputs' => array(
        0 => 'name',
        1 => 'billing_address_city',
        3 => 'account_type',
        4 => 'industry',
        5 => 'billing_address_state',
        6 => 'billing_address_country',
        7 => 'email',
        8 => 'assigned_user_id',
    ),
    'create' => array(
        'formBase' => 'AccountFormBase.php',
        'formBaseClass' => 'AccountFormBase',
        'getFormBodyParams' =>
        array(
            0 => '',
            1 => '',
            2 => 'AccountSave',
        ),
        'createButton' => 'LNK_NEW_ACCOUNT',
    ),
    'searchdefs' => array(
        'name' =>
        array(
            'name' => 'name',
            'width' => '10%',
        ),
        'city' =>
        array(
            'name' => 'city',
            'default' => true,
            'width' => '10%',
            'displayParams' => array(
                'useIdSearch' => true,
            )
        ),
    ),
    'listviewdefs' => array(
        'NAME' =>
        array(
            'width' => '25%',
            'label' => 'LBL_LIST_ACCOUNT_NAME',
            'link' => true,
            'default' => true,
        ),
        'BILLING_ADDRESS_CITY' =>
        array(
            'width' => '25%',
            'label' => 'LBL_LIST_CITY',
            'default' => true,
        ),
        'BILLING_ADDRESS_STREET' =>
        array(
            'width' => '25%',
            'label' => 'LBL_STREET',
            'default' => true,
        ),
        'PHONE_OFFICE' =>
        array(
            'width' => '25%',
            'label' => 'LBL_LIST_PHONE',
            'default' => true,
        ),
    ),
);
