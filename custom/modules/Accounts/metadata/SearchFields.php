<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$searchFields['Accounts'] = array(
    'name' =>
    array(
        'query_type' => 'default',
    ),
    'city' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'city_id' =>
    array(
        'query_type' => 'default',
    ),
    'full_name' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            a.id 
                       FROM 
                            accounts AS a 
                       WHERE 
                            a.full_name LIKE '%{0}%' AND 
                            a.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'genitive_fio' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            a2.id 
                       FROM 
                            accounts AS a2 
                       WHERE 
                            a2.genitive_fio LIKE '%{0}%' AND 
                            a2.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'nominative_fio' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            a3.id 
                       FROM 
                            accounts AS a3 
                       WHERE 
                            a3.nominative_fio LIKE '%{0}%' AND 
                            a3.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'base_acts_signatory' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            a6.id 
                       FROM 
                            accounts AS a6 
                       WHERE 
                            a6.base_acts_signatory LIKE '%{0}%' AND 
                            a6.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'description' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            a5.id 
                       FROM 
                            accounts AS a5 
                       WHERE 
                            a5.description LIKE '%{0}%' AND 
                            a5.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'address_street' =>
    array(
        'query_type' => 'default',
        'db_field' =>
        array(
            0 => 'billing_address_street',
            1 => 'shipping_address_street',
        ),
    ),
    'address_city' =>
    array(
        'query_type' => 'default',
        'db_field' =>
        array(
            0 => 'billing_address_city',
            1 => 'shipping_address_city',
        ),
        'vname' => 'LBL_CITY',
    ),
    'address_state' =>
    array(
        'query_type' => 'default',
        'db_field' =>
        array(
            0 => 'billing_address_state',
            1 => 'shipping_address_state',
        ),
        'vname' => 'LBL_STATE',
    ),
    'address_postalcode' =>
    array(
        'query_type' => 'default',
        'db_field' =>
        array(
            0 => 'billing_address_postalcode',
            1 => 'shipping_address_postalcode',
        ),
        'vname' => 'LBL_POSTAL_CODE',
    ),
    'address_country' =>
    array(
        'query_type' => 'default',
        'db_field' =>
        array(
            0 => 'billing_address_country',
            1 => 'shipping_address_country',
        ),
        'vname' => 'LBL_COUNTRY',
    ),
    'phone_office' =>
    array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'subquery' => "SELECT
                            a4.id 
                       FROM 
                            accounts AS a4 
                       WHERE 
                            a4.phone_office LIKE '%{0}%' AND 
                            a4.deleted = '0'
                      ",
        'db_field' => array('id'),
    ),
    'email' =>
    array(
        'query_type' => 'default',
        'operator' => 'subquery',
        'subquery' => 'SELECT eabr.bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (ea.id = eabr.email_address_id) WHERE eabr.deleted=0 AND ea.email_address LIKE',
        'db_field' =>
        array(
            0 => 'id',
        ),
        'vname' => 'LBL_ANY_EMAIL',
    ),
    'current_user_only' =>
    array(
        'query_type' => 'default',
        'db_field' =>
        array(
            0 => 'assigned_user_id',
        ),
        'my_items' => true,
        'vname' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
    ),
    'assigned_user_id' =>
    array(
        'query_type' => 'default',
    ),
    'range_date_entered' =>
    array(
        'query_type' => 'default',
        'enable_range_search' => true,
        'is_date_field' => true,
    ),
    'start_range_date_entered' =>
    array(
        'query_type' => 'default',
        'enable_range_search' => true,
        'is_date_field' => true,
    ),
    'end_range_date_entered' =>
    array(
        'query_type' => 'default',
        'enable_range_search' => true,
        'is_date_field' => true,
    ),
    'range_date_modified' =>
    array(
        'query_type' => 'default',
        'enable_range_search' => true,
        'is_date_field' => true,
    ),
    'start_range_date_modified' =>
    array(
        'query_type' => 'default',
        'enable_range_search' => true,
        'is_date_field' => true,
    ),
    'end_range_date_modified' =>
    array(
        'query_type' => 'default',
        'enable_range_search' => true,
        'is_date_field' => true,
    ),
    'favorites_only' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'checked_only' => true,
        'subquery' => "SELECT favorites.parent_id FROM favorites
			                    WHERE favorites.deleted = 0
			                        and favorites.parent_type = 'Accounts'
			                        and favorites.assigned_user_id = '{1}'",
        'db_field' => array('id')),
);
