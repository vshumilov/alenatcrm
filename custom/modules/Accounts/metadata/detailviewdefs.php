<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$viewdefs ['Accounts'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    0 => 'EDIT',
                    1 => 'DELETE',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'includes' =>
            array(
                array(
                    'file' => 'modules/Accounts/Account.js',
                ),
                array(
                    'file' => 'custom/include/Nit/js/detailviewRequisites.js',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'LBL_ACCOUNT_INFORMATION' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ADVANCED' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'LBL_ACCOUNT_INFORMATION' =>
            array(
                array(
                    'bean_type'
                ),
                array(
                    array(
                        'name' => 'name',
                        'label' => 'LBL_NAME',
                        'displayParams' =>
                        array(
                            'required' => true,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'full_name',
                        'label' => 'LBL_FULL_NAME',
                    ),
                ),
                array(
                    array(
                        'name' => 'inn',
                        'label' => 'LBL_INN',
                        'displayParams' =>
                        array(
                            'required' => true,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'kpp',
                        'label' => 'LBL_KPP',
                        'displayParams' =>
                        array(
                            'required' => true,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'ogrn',
                        'label' => 'LBL_OGRN',
                        'displayParams' =>
                        array(
                            'required' => true,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'date_ogrn',
                    ),
                ),
                array(
                    array(
                        'name' => 'phone_office',
                        'label' => 'LBL_PHONE',
                    ),
                ),
                array(
                    array(
                        'name' => 'website',
                        'type' => 'link',
                        'label' => 'LBL_WEBSITE',
                    ),
                ),
                array(
                    array(
                        'name' => 'email1',
                        'studio' => 'false',
                        'label' => 'LBL_EMAIL',
                    ),
                ),
                array(
                    array(
                        'name' => 'city',
                    ),
                ),
                array(
                    array(
                        'name' => 'receivable_debt',
                    ),
                ),
                array(
                    array(
                        'name' => 'last_agreement_status',
                    ),
                ),
                array(
                    array(
                        'name' => 'last_acceptmeteringdevice_status',
                    ),
                ),
                array(
                    array(
                        'name' => 'description',
                        'label' => 'LBL_DESCRIPTION',
                    ),
                ),
                array(
                    array(
                        'name' => 'assigned_user_name',
                        'label' => 'LBL_ASSIGNED_TO',
                    ),
                ),
            ),
            'LBL_ADDRESS_PANEL' => array(
                array(
                    array(
                        'name' => 'billing_address_street',
                        'label' => 'LBL_BILLING_ADDRESS',
                        'hideLabel' => true,
                        'type' => 'address',
                        'displayParams' =>
                        array(
                            'key' => 'billing',
                            'rows' => 2,
                            'cols' => 30,
                            'maxlength' => 150,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'shipping_address_street',
                        'label' => 'LBL_SHIPPING_ADDRESS',
                        'hideLabel' => true,
                        'type' => 'address',
                        'displayParams' =>
                        array(
                            'key' => 'shipping',
                            'copy' => 'billing',
                            'rows' => 2,
                            'cols' => 30,
                            'maxlength' => 150,
                        ),
                    ),
                ),
            ),
            'LBL_ACCOUNT_DETAILS_PANEL' => array(
                array(
                    array(
                        'name' => 'payment_account',
                        'displayParams' => array(
                            'size' => 50,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'bank_name',
                        'displayParams' => array(
                            'size' => 50,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'correspondent_account',
                        'displayParams' => array(
                            'size' => 50,
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => 'bic',
                        'displayParams' => array(
                            'size' => 50,
                        ),
                    ),
                ),
            ),
            'LBL_SIGNED_PANEL' => array(
                array(
                    array(
                        'name' => 'genitive_fio',
                    ),
                ),
                array(
                    array(
                        'name' => 'nominative_fio',
                    ),
                ),
                array(
                    array(
                        'name' => 'genitive_position',
                    ),
                ),
                array(
                    array(
                        'name' => 'nominative_position',
                    ),
                ),
                array(
                    array(
                        'name' => 'base_acts_signatory',
                        'label' => 'LBL_BASE_ACTS_SIGNATORY',
                        'displayParams' => array(
                            'rows' => 6,
                            'cols' => 80
                        ),
                    ),
                ),
            ),
        ),
    ),
);
