<?php

$searchdefs ['Accounts'] = array(
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout' =>
    array(
        'basic_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'city' =>
            array(
                'name' => 'city',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
        ),
        'advanced_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'full_name' =>
            array(
                'name' => 'full_name',
                'default' => true,
                'width' => '10%',
            ),
            'inn' =>
            array(
                'name' => 'inn',
                'default' => true,
                'width' => '10%',
            ),
            'kpp' =>
            array(
                'name' => 'kpp',
                'default' => true,
                'width' => '10%',
            ),
            'ogrn' =>
            array(
                'name' => 'ogrn',
                'default' => true,
                'width' => '10%',
            ),
            'payment_account' =>
            array(
                'name' => 'payment_account',
                'default' => true,
                'width' => '10%',
            ),
            'bank_name' =>
            array(
                'name' => 'bank_name',
                'default' => true,
                'width' => '10%',
            ),
            'correspondent_account' =>
            array(
                'name' => 'correspondent_account',
                'default' => true,
                'width' => '10%',
            ),
            'bic' =>
            array(
                'name' => 'bic',
                'default' => true,
                'width' => '10%',
            ),
            'genitive_fio' =>
            array(
                'name' => 'genitive_fio',
                'default' => true,
                'width' => '10%',
            ),
            'nominative_fio' =>
            array(
                'name' => 'nominative_fio',
                'default' => true,
                'width' => '10%',
            ),
            'phone_office' =>
            array(
                'name' => 'phone_office',
                'label' => 'LBL_PHONE',
                'default' => true,
                'width' => '10%',
            ),
            'email' =>
            array(
                'name' => 'email',
                'label' => 'LBL_ANY_EMAIL',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'address_street' =>
            array(
                'name' => 'address_street',
                'label' => 'LBL_STREET',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'address_city' =>
            array(
                'name' => 'address_city',
                'label' => 'LBL_CITY',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'address_state' =>
            array(
                'name' => 'address_state',
                'label' => 'LBL_STATE',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'address_postalcode' =>
            array(
                'name' => 'address_postalcode',
                'label' => 'LBL_POSTAL_CODE',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'billing_address_country' =>
            array(
                'name' => 'billing_address_country',
                'label' => 'LBL_COUNTRY',
                'type' => 'name',
                'options' => 'countries_dom',
                'default' => true,
                'width' => '10%',
            ),'base_acts_signatory' =>
            array(
                'name' => 'base_acts_signatory',
                'default' => true,
                'width' => '10%',
                'type' => 'varchar'
            ),
            'city' =>
            array(
                'name' => 'city',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'description' =>
            array(
                'name' => 'description',
                'default' => true,
                'width' => '10%',
                'type' => 'varchar'
            ),
            'assigned_user_name' =>
            array(
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO',
                'displayParams' => array(
                    'useIdSearch' => true,
                ),
                'default' => true,
            ),
        ),
    ),
);
