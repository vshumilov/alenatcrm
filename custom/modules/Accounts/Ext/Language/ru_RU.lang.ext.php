<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_NAME'] = 'Сокращ. наименование';
$mod_strings['LBL_NAME_SHORT'] = 'Сокращ. наименование';
$mod_strings['LBL_FULL_NAME'] = 'Полное наименование';
$mod_strings['LBL_INN'] = 'ИНН';
$mod_strings['LBL_KPP'] = 'КПП';
$mod_strings['LBL_OGRN'] = 'ОГРН';
$mod_strings['LBL_OGRN_IP'] = 'ОГРНИП';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Юридический адрес';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Фактический адрес';
$mod_strings['LBL_ACCOUNT_DETAILS_PANEL'] = 'Банковские реквизиты';
$mod_strings['LBL_BANK_NAME'] = 'Банк';
$mod_strings['LBL_PAYMENT_ACCOUNT'] = 'Расчетный счет';
$mod_strings['LBL_CORRESPONDENT_ACCOUNT'] = 'Корсчет';
$mod_strings['LBL_BIC'] = 'БИК';
$mod_strings['LBL_DESCRIPTION'] = 'Основание работы на территории';
$mod_strings['LBL_SIGNED_PANEL'] = 'Подписан';
$mod_strings['LBL_GENITIVE_FIO'] = 'ФИО в родительном падеже';
$mod_strings['LBL_NOMINATIVE_FIO'] = 'ФИО в именительном падеже';
$mod_strings['LBL_GENITIVE_POSITION'] = 'Должность в родительном падеже';
$mod_strings['LBL_NOMINATIVE_POSITION'] = 'Должность в именительном падеже';
$mod_strings['LBL_BASE_ACTS_SIGNATORY'] = 'Основание действия подписанта';
$mod_strings['LBL_ADDRESS_PANEL'] = 'Адреса';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Абонент:';
$mod_strings['LBL_ACCOUNT'] = 'Абонент:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Абоненты';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Абоненты';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Абонент';
$mod_strings['LBL_MODULE_NAME'] = 'Абоненты';
$mod_strings['LBL_MODULE_TITLE'] = 'Абоненты - ГЛАВНАЯ';
$mod_strings['LBL_MODULE_ID'] = 'Абоненты';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Абоненты';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Абоненты';
$mod_strings['ACCOUNT_REMOVE_PROJECT_CONFIRM'] = 'Вы действительно хотите удалить этого абонента из проекта?';
$mod_strings['LBL_DUPLICATE'] = 'Возможно дублирующийся абонент';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Мои абоненты';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Список абонентов';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Новый абонент';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID родительского абонента';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Сохранение абонента';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Поиск абонентов';
$mod_strings['LBL_VIEW_FORM_TITLE'] = 'Обзор абонента';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Создать абонента';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Импорт абонентов';
$mod_strings['MSG_DUPLICATE'] = 'Создаваемая запись, возможно, дублирует уже существующего абонента. Схожие записи приведены в списке.<br>Нажмите на кнопку <b>Создать абонента</b> для создания новой записи или выберите существующую запись из списка.';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'Создаваемая запись, возможно, дублирует уже существующего абонента. Схожие записи приведены в списке.<br>Нажмите на кнопку <b>Сохранить</b> для создания новой записи или на кнопку <b>Отмена</b> для возврата в модуль без создания новой записи.';
$mod_strings['LBL_RECEIVABLES_SUBPANEL_TITLE'] = 'Учет дебиторской задолженности';
$mod_strings['LBL_RECEIVABLE_DEBT'] = 'Дебиторская задолженность';
$mod_strings['LBL_CITY'] = 'Город';
$mod_strings['LBL_AGREEMENTS_SUBPANEL_TITLE'] = 'Договоры';
$mod_strings['LBL_ACCEPTMETERINGDEVICES_SUBPANEL_TITLE'] = 'Прием в эксплуатацию ПУ';
$mod_strings['LBL_CHECKWATERSUPPLYSYSTEMS_SUBPANEL_TITLE'] = 'Проверка системы ВС и ВО';
$mod_strings['LBL_INFOMETERINGDEVICES_SUBPANEL_TITLE'] = 'Приборы учета абонентов';
$mod_strings['LBL_LAST_AGREEMENT_STATUS'] = 'Статус договора';
$mod_strings['LBL_LAST_ACCEPTMETERINGDEVICE_STATUS'] = 'Статус приема приборов учета в эксплуатацию';
$mod_strings['LBL_DATE_OGRN'] = 'Дата выдачи ОГРН';
$mod_strings['LBL_DATE_OGRN_IP'] = 'Дата выдачи ОГРНИП';
$mod_strings['LBL_DIRECTOR_BASIC_ACT'] = 'Руководитель действует на основании';
$mod_strings['LBL_BEAN_TYPE'] = 'Тип';

$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Юридический адрес - город:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'Юридический адрес - страна:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Юридический адрес - индекс:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Юридический адрес - область:';
$mod_strings['LBL_BILLING_ADDRESS_STREET_2'] = 'Юридический адрес - улица 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET_3'] = 'Юридический адрес - улица 3';
$mod_strings['LBL_BILLING_ADDRESS_STREET_4'] = 'Юридический адрес - улица 4';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Юридический адрес - улица:';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Юридический адрес';

$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Фактический адрес - город:';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'Фактический адрес - страна:';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Фактический адрес - индекс:';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Фактический адрес - область:';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_2'] = 'Фактический адрес - улица 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_3'] = 'Фактический адрес - улица 3';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_4'] = 'Фактический адрес - улица 4';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Фактический адрес - улица:';

$mod_strings['LBL_MEETINGS_SUBPANEL_TITLE'] = 'Уведомления абонентов';









?>