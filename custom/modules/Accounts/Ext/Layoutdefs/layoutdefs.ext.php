<?php 
 //WARNING: The contents of this file are auto-generated



unset($layout_defs['Accounts']['subpanel_setup']['activities']);
unset($layout_defs['Accounts']['subpanel_setup']['history']);
unset($layout_defs['Accounts']['subpanel_setup']['documents']);
unset($layout_defs['Accounts']['subpanel_setup']['opportunities']);
unset($layout_defs['Accounts']['subpanel_setup']['leads']);
unset($layout_defs['Accounts']['subpanel_setup']['cases']);
unset($layout_defs['Accounts']['subpanel_setup']['accounts']);
unset($layout_defs['Accounts']['subpanel_setup']['bugs']);
unset($layout_defs['Accounts']['subpanel_setup']['project']);
unset($layout_defs['Accounts']['subpanel_setup']['campaigns']);
unset($layout_defs['Accounts']['subpanel_setup']['account_aos_quotes']);
unset($layout_defs['Accounts']['subpanel_setup']['account_aos_invoices']);
unset($layout_defs['Accounts']['subpanel_setup']['account_aos_contracts']);
unset($layout_defs['Accounts']['subpanel_setup']['products_services_purchased']);
unset($layout_defs['Accounts']['subpanel_setup']['securitygroups']);

$layout_defs['Accounts']['subpanel_setup']['receivables'] = [
    'order' => 6,
    'module' => 'Receivables',
    'subpanel_name' => 'ForAccounts',
    'sort_order' => 'DESC',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_RECEIVABLES_SUBPANEL_TITLE',
    'get_subpanel_data' => 'receivables',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
    ),
];

$layout_defs['Accounts']['subpanel_setup']['meetings'] = [
    'order' => 1,
    'module' => 'Meetings',
    'subpanel_name' => 'customdefault',
    'sort_order' => 'DESC',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_MEETINGS_SUBPANEL_TITLE',
    'get_subpanel_data' => 'meetings_link',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
    ),
];

$layout_defs['Accounts']['subpanel_setup']['agreements'] = [
    'order' => 2,
    'module' => 'Opportunities',
    'subpanel_name' => 'agreements',
    'sort_order' => 'DESC',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_AGREEMENTS_SUBPANEL_TITLE',
    'get_subpanel_data' => 'opportunities',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
    ),
];

$layout_defs['Accounts']['subpanel_setup']['acceptmeteringdevices'] = [
    'order' => 4,
    'module' => 'Acceptmeteringdevices',
    'subpanel_name' => 'default',
    'sort_order' => 'DESC',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_ACCEPTMETERINGDEVICES_SUBPANEL_TITLE',
    'get_subpanel_data' => 'acceptmeteringdevices',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
    ),
];

$layout_defs['Accounts']['subpanel_setup']['checkwatersupplysystems'] = [
    'order' => 5,
    'module' => 'Checkwatersupplysystems',
    'subpanel_name' => 'default',
    'sort_order' => 'DESC',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_CHECKWATERSUPPLYSYSTEMS_SUBPANEL_TITLE',
    'get_subpanel_data' => 'checkwatersupplysystems',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
    ),
];

$layout_defs['Accounts']['subpanel_setup']['infometeringdevices'] = [
    'order' => 3,
    'module' => 'Infometeringdevices',
    'subpanel_name' => 'default',
    'sort_order' => 'DESC',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_INFOMETERINGDEVICES_SUBPANEL_TITLE',
    'get_subpanel_data' => 'infometeringdevices',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
        array(
            'widget_class' => 'SubPanelTopSelectButton',
            'mode' => 'MultiSelect',
        ),
    ),
];


?>