<?php 
 //WARNING: The contents of this file are auto-generated



if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');


global $mod_strings, $app_strings, $sugar_config;

$module_menu = [];

if (ACLController::checkAccess('Accounts', 'edit', true))
    $module_menu[] = Array("index.php?module=Accounts&action=EditView&return_module=Accounts&return_action=index", $mod_strings['LNK_NEW_ACCOUNT'], "Create", 'Accounts');

if (ACLController::checkAccess('Accounts', 'list', true))
    $module_menu[] = Array("index.php?module=Accounts&action=index&return_module=Accounts&return_action=DetailView", $mod_strings['LNK_ACCOUNT_LIST'], "List", 'Accounts');
?>