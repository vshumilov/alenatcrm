<?php 
 //WARNING: The contents of this file are auto-generated



$dictionary['Email']['fields']['meetings'] = [
    'name' => 'meetings',
    'vname' => 'LBL_EMAILS_MEETINGS_REL',
    'type' => 'link',
    'relationship' => 'emails_meetings_rel',
    'module' => 'Meetings',
    'bean_name' => 'Meeting',
    'source' => 'non-db',
];

?>