<?php

require_once 'custom/modules/Contacts/BeanHelper/Contact.php';

class ContactHookContact
{
    public function setSecureData(Contact $bean)
    {
        $contactBeanHelper = new ContactBeanHelperContact($bean);

        $bean = $contactBeanHelper->setSecureData();
        
        if (empty($bean->is_sms_dispatch_blocked)) {
            $bean->is_sms_dispatch_blocked = 0;
        }
        
        if (empty($bean->is_email_dispatch_blocked)) {
            $bean->is_email_dispatch_blocked = 0;
        }
    }

    public function getSecureData(Contact $bean)
    {
        $contactBeanHelper = new ContactBeanHelperContact($bean);

        $bean = $contactBeanHelper->getSecureData();
    }

    public function getPatronymic(Contact $bean)
    {
        $contactBeanHelper = new ContactBeanHelperContact($bean);

        $bean = $contactBeanHelper->setPatronymicAndFirstNameOnly();
    }

    public function setPatronymicToFirstName(Contact $bean)
    {
        $contactBeanHelper = new ContactBeanHelperContact($bean);

        $bean = $contactBeanHelper->setPatronymicToFirstName();
    }

}
