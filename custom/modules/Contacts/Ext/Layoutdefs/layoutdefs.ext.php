<?php 
 //WARNING: The contents of this file are auto-generated



unset($layout_defs['Contacts']['subpanel_setup']['activities']);
unset($layout_defs['Contacts']['subpanel_setup']['history']);
unset($layout_defs['Contacts']['subpanel_setup']['leads']);
unset($layout_defs['Contacts']['subpanel_setup']['opportunities']);
unset($layout_defs['Contacts']['subpanel_setup']['cases']);
unset($layout_defs['Contacts']['subpanel_setup']['bugs']);
unset($layout_defs['Contacts']['subpanel_setup']['project']);
unset($layout_defs['Contacts']['subpanel_setup']['campaigns']);
unset($layout_defs['Contacts']['subpanel_setup']['contact_aos_quotes']);
unset($layout_defs['Contacts']['subpanel_setup']['contact_aos_invoices']);
unset($layout_defs['Contacts']['subpanel_setup']['contact_aos_contracts']);
unset($layout_defs['Contacts']['subpanel_setup']['fp_events_contacts']);
unset($layout_defs['Contacts']['subpanel_setup']['securitygroups']);
unset($layout_defs['Contacts']['subpanel_setup']['contacts']);
unset($layout_defs['Contacts']['subpanel_setup']['documents']);

$layout_defs['Contacts']['subpanel_setup']['agreements'] = [
    'order' => 1,
    'module' => 'Opportunities',
    'subpanel_name' => 'agreements',
    'get_subpanel_data' => 'agreements',
    'generate_select' => true,
    'title_key' => 'LBL_AGREEMENTS_SUBPANEL_TITLE',
    'sort_order' => 'desc',
    'sort_by' => 'date_entered',
    'top_buttons' =>
    array(
        array(
            'widget_class' => 'SubPanelTopCreateButton',
        ),
    ),
];

?>