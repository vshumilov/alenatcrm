<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['after_retrieve'] = [];
$hook_array['before_save'][] = Array(1, 'Contacts push feed', 'modules/Contacts/SugarFeeds/ContactFeed.php','ContactFeed', 'pushFeed'); 
$hook_array['before_save'][] = Array(77, 'updateGeocodeInfo', 'modules/Contacts/ContactsJjwg_MapsLogicHook.php','ContactsJjwg_MapsLogicHook', 'updateGeocodeInfo'); 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(1, 'Update Portal', 'modules/Contacts/updatePortal.php','updatePortal', 'updateUser'); 
$hook_array['after_save'][] = Array(77, 'updateRelatedMeetingsGeocodeInfo', 'modules/Contacts/ContactsJjwg_MapsLogicHook.php','ContactsJjwg_MapsLogicHook', 'updateRelatedMeetingsGeocodeInfo'); 

$hook_array['before_save'][] = [
    3,
    'set patronymic to firstname',
    'custom/modules/Contacts/Hook/Contact.php',
    'ContactHookContact',
    'setPatronymicToFirstName'
];

$hook_array['before_save'][] = [
    4,
    'set secure data',
    'custom/modules/Contacts/Hook/Contact.php',
    'ContactHookContact',
    'setSecureData'
];

$hook_array['after_retrieve'][] = [
    1,
    'get secure data',
    'custom/modules/Contacts/Hook/Contact.php',
    'ContactHookContact',
    'getSecureData'
];

$hook_array['after_retrieve'][] = [
    2,
    'get patronymic',
    'custom/modules/Contacts/Hook/Contact.php',
    'ContactHookContact',
    'getPatronymic'
];