<?php

require_once 'custom/include/Nit/BeanHelper/Crypt.php';
require_once 'custom/include/Nit/Helper/Date.php';
require_once 'custom/include/Nit/Helper/Phone.php';

class ContactBeanHelperContact extends CryptHelper
{

    public $passwordKey = 'passport_';

    public function getData()
    {
        if (empty($this->bean->id)) {
            return;
        }

        $fields = $this->bean->field_defs;

        $data = [];

        foreach ($fields as $field) {
            $data[$field['name']] = $this->bean->{$field['name']};
        }

        return $data;
    }

    public function setSecureData()
    {
        $fields = $this->bean->field_defs;

        $secureContent = [];

        $helperPhone = new HelperPhone();

        foreach ($fields as $field) {
            if (empty($field['secure'])) {
                continue;
            }

            $secureContent[$field['name']] = !empty($this->bean->{$field['name']}) ? $this->bean->{$field['name']} : "";

            if ($field['name'] == 'phone_work' || $field['name'] == 'phone_mobile') {
                $secureContent[$field['name']] = $helperPhone->formatPhone($secureContent[$field['name']]);
                $smallPhoneField = "{$field['name']}_small";
                $this->bean->$smallPhoneField = $helperPhone->getSmallPhone($secureContent[$field['name']]);
            } else if ($field['name'] == 'email') {
                $this->bean->email_small = $this->getSmallEmail($secureContent[$field['name']]);
            }
        }

        $this->bean->secure_content = $this->encrypt($secureContent);

        return $this->bean;
    }

    public function getSmallEmail($email)
    {
        $smallEmail = substr($email, 0, strpos($email, "@"));
        return $smallEmail;
    }

    protected function getOnlyNumbers($str)
    {
        $numbers = preg_replace("/[^0-9]/", '', $str);
        return $numbers;
    }

    public function getSecureData()
    {
        global $timedate;

        $secureContent = $this->decrypt("secure_content");

        if (empty($secureContent)) {
            return $this->bean;
        }

        foreach ($secureContent as $field => $value) {
            $this->bean->{$field} = $value;
        }

        if (!empty($this->bean->passport_date_of_issue)) {
            $this->bean->passport_date_of_issue = date($timedate->get_date_format(), strtotime($this->bean->passport_date_of_issue));
        }

        return $this->bean;
    }

    public function setPatronymicAndFirstNameOnly()
    {
        if (empty($this->bean->first_name)) {
            return $this->bean;
        }

        $nameArray = explode(" ", $this->bean->first_name);

        $this->bean->first_name_only = $nameArray[0];

        if (!empty($nameArray[1])) {
            $this->bean->patronymic = $nameArray[1];
        }

        return $this->bean;
    }

    public function setPatronymicToFirstName()
    {
        if (empty($this->bean->patronymic) || empty($this->bean->first_name_only)) {
            return $this->bean;
        }

        $this->bean->first_name = $this->bean->first_name_only . " " . $this->bean->patronymic;

        return $this->bean;
    }

    public function getWherePhone($phone)
    {
        if (empty($phone)) {
            return;
        }

        $helperPhone = new HelperPhone();

        $query = " 
            AND (
                contacts.phone_work_small = '<phone>'
                OR
                contacts.phone_mobile_small = '<phone>'
            )";

        $sql = strtr($query, ["<phone>" => $helperPhone->getSmallPhone($helperPhone->getOnlyNumbers($phone))]);

        return $sql;
    }

    public function getWhereEmail($email)
    {
        if (empty($email)) {
            return;
        }

        $query = " 
            AND contacts.email_small = '<email>'";

        $sql = strtr($query, ["<email>" => $this->getSmallEmail($email)]);

        return $sql;
    }

    public function isExistsContact($firstName, $lastName, $contactId, $birthDate, $phoneWork, $phoneMobile, $email)
    {
        $result = $this->getDuplicateContact($firstName, $lastName, $contactId, $birthDate, $phoneWork, $phoneMobile, $email);
        
        $helperDate = new HelperDate();
        $helperPhone = new HelperPhone();
        $phoneWorkSmall = $helperPhone->getSmallPhone($helperPhone->getOnlyNumbers($phoneWork));
        $phoneMobileSmall = $helperPhone->getSmallPhone($helperPhone->getOnlyNumbers($phoneMobile));
        $dbBirthDate = $helperDate->toDbDate($birthDate);
        $emailSmall = $this->getSmallEmail($email);

        if (!empty($result)) {
            if (!empty($dbBirthDate) && $result['birth_date'] == $dbBirthDate) {
                return 'birth_date';
            } elseif (!empty($phoneWorkSmall) && $result['phone_work_small'] == $phoneWorkSmall) {
                return 'phone_work';
            } elseif (!empty($phoneMobileSmall) && $result['phone_mobile_small'] == $phoneMobileSmall) {
                return 'phone_mobile';
            } elseif (!empty($emailSmall) && $result['email_small'] == $emailSmall) {
                return 'email';
            }
        }

        return false;
    }

    public function getDuplicateContact($firstName, $lastName, $contactId, $birthDate, $phoneWork, $phoneMobile, $email)
    {
        $query = $this->getDuplicateQuery($firstName, $lastName, $contactId, $birthDate, $phoneWork, $phoneMobile, $email);

        $result = dbGetRow($query);
        
        return $result;
    }

    public function getDuplicateQuery($firstName, $lastName, $contactId, $birthDate, $phoneWork, $phoneMobile, $email)
    {
        $helperDate = new HelperDate();
        $helperPhone = new HelperPhone();
        $phoneWorkSmall = $helperPhone->getSmallPhone($helperPhone->getOnlyNumbers($phoneWork));
        $phoneMobileSmall = $helperPhone->getSmallPhone($helperPhone->getOnlyNumbers($phoneMobile));
        $dbBirthDate = $helperDate->toDbDate($birthDate);
        $emailSmall = $this->getSmallEmail($email);

        $sql = "
        SELECT
            c.id,
            c.first_name,
            c.last_name,
            c.birth_date,
            c.phone_work_small,
            c.phone_mobile_small,
            c.email_small
        FROM
            contacts AS c
        WHERE
            c.deleted = '0' AND
            (
                (
                    c.first_name = '<firstName>' AND
                    c.last_name = '<lastName>' AND
                    <birthDateWhere>
                    
                )
                <phoneWorkWhere>
                <phoneMobileWhere>
                <emailWhere>
            )
            <contactWhere>
        LIMIT 1
            ";

        $query = strtr($sql, [
            '<firstName>' => $firstName,
            '<lastName>' => $lastName,
            '<birthDateWhere>' => empty($dbBirthDate) ? "" : " c.birth_date = '$dbBirthDate'",
            '<phoneWorkWhere>' => empty($phoneWorkSmall) ? "" : "OR c.phone_work_small = '$phoneWorkSmall'",
            '<phoneMobileWhere>' => empty($phoneMobileSmall) ? "" : "OR c.phone_mobile_small = '$phoneMobileSmall'",
            '<emailWhere>' => empty($emailSmall) ? "" : "OR c.email_small = '$emailSmall'",
            '<contactWhere>' => empty($contactId) ? "" : "AND c.id <> '$contactId'"
        ]);

        return $query;
    }

}
