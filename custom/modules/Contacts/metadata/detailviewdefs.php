<?php

$viewdefs ['Contacts'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    'EDIT',
                    'DELETE',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'includes' =>
            array(
                0 =>
                array(
                    'file' => 'modules/Leads/Lead.js',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'LBL_CONTACT_INFORMATION' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ADVANCED' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'lbl_contact_information' =>
            array(
                array(
                    array(
                        'name' => 'last_name',
                    ),
                ),
                array(
                    array(
                        'name' => 'first_name_only',
                    ),
                ),
                array(
                    array(
                        'name' => 'patronymic',
                    ),
                ),
                array(
                    array(
                        'name' => 'gender',
                    ),
                ),
                array(
                    array(
                        'name' => 'birth_date',
                    ),
                ),
                array(
                    array(
                        'name' => 'phone_mobile',
                        'label' => 'LBL_MOBILE_PHONE',
                    ),
                ),
                array(
                    array(
                        'name' => 'phone_work',
                        'label' => 'LBL_OFFICE_PHONE',
                    ),
                ),
                array(
                    array(
                        'name' => 'email',
                        'studio' => 'false',
                        'label' => 'LBL_EMAIL_ADDRESS',
                    ),
                ),
                /* array(
                  array(
                  'name' => 'primary_address_street',
                  'label' => 'LBL_PRIMARY_ADDRESS',
                  'type' => 'address',
                  'displayParams' =>
                  array(
                  'key' => 'primary',
                  ),
                  ),
                  ), */
                array(
                    array(
                        'name' => 'description',
                        'comment' => 'Full text of the note',
                        'label' => 'LBL_DESCRIPTION',
                    ),
                ),
                array(
                    array(
                        'name' => 'assigned_user_name',
                        'label' => 'LBL_ASSIGNED_TO_NAME',
                    ),
                ),
                array(
                    array(
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),
                array(
                    array(
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                )
            ),
            'LBL_PASSPORT_PANEL' => array(
                array(
                    array(
                        'name' => 'passport_series',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_number',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_date_of_issue',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_division_code',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_issued_by',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_address',
                    ),
                ),
            ),
            'LBL_DISPATCH_PANEL' => array(
                array(
                    array(
                        'name' => 'last_email_dispatch_datetime',
                    ),
                ),
                array(
                    array(
                        'name' => 'last_sms_dispatch_datetime',
                    ),
                ),
                array(
                    array(
                        'name' => 'is_email_dispatch_blocked',
                    ),
                ),
                array(
                    array(
                        'name' => 'is_sms_dispatch_blocked',
                    ),
                ),
            ),
            'LBL_LAST_AGREEMENT_PANEL' => array (
                array(
                    array(
                        'name' => 'last_agreement_datetime',
                    ),
                ),
                array(
                    array(
                        'name' => 'opportunity_name',
                    ),
                ),
                array(
                    array(
                        'name' => 'is_imported',
                    ),
                ),
                array(
                    array(
                        'name' => 'contact_from',
                    ),
                ),
            )
        ),
    ),
);
