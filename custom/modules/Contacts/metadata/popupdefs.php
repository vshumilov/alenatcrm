<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings;

$popupMeta = array(
    'moduleMain' => 'Contact',
    'varName' => 'CONTACT',
    'orderBy' => 'contacts.first_name, contacts.last_name',
    'whereClauses' =>
    array('first_name' => 'contacts.first_name',
        'last_name' => 'contacts.last_name'),
    'searchInputs' =>
    array('first_name', 'last_name'),
    'create' =>
    array('formBase' => 'ContactFormBase.php',
        'formBaseClass' => 'ContactFormBase',
        'getFormBodyParams' => array('', '', 'ContactSave'),
        'createButton' => 'LNK_NEW_CONTACT'
    ),
    'listviewdefs' => array(
        'NAME' => array(
            'width' => '20%',
            'label' => 'LBL_LIST_NAME',
            'link' => true,
            'default' => true,
            'related_fields' => array('first_name', 'last_name', 'salutation')),
        'BIRTH_DATE' => array(
            'width' => '15%',
            'label' => 'LBL_BIRTH_DATE',
            'default' => true
        ),
    ),
    'searchdefs' => array(
        'first_name',
        'last_name',
    )
);
?>
