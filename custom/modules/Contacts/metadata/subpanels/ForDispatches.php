<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'Contacts'),
    ),
    'where' => '',
    'list_fields' => array(
        'name' => array(
            'widget_class' => 'SubPanelDetailViewLink',
        ),
        'first_name' => array(
            'name' => 'first_name',
        ),
        'last_name' => array(
            'name' => 'last_name',
        ),
        'birth_date' => array(
            'name' => 'birth_date',
        ),
        'remove_button' => array(
            'vname' => 'LBL_REMOVE',
            'widget_class' => 'SubPanelRemoveButton',
            'module' => 'Contacts',
            'width' => '5%',
        ),
    ),
);
?>
