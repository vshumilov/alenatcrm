<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Contacts'] = array(
    'NAME' => array(
        'width' => '20%',
        'label' => 'LBL_LIST_NAME',
        'link' => true,
        'contextMenu' => array('objectType' => 'sugarPerson',
            'metaData' => array('contact_id' => '{$ID}',
                'module' => 'Contacts',
                'return_action' => 'ListView',
                'contact_name' => '{$FULL_NAME}',
                'parent_id' => '{$ACCOUNT_ID}',
                'parent_name' => '{$ACCOUNT_NAME}',
                'return_module' => 'Contacts',
                'return_action' => 'ListView',
                'parent_type' => 'Account',
                'notes_parent_type' => 'Account')
        ),
        'orderBy' => 'name',
        'default' => true,
        'related_fields' => array('first_name', 'last_name', 'salutation', 'account_name', 'account_id'),
    ),
    'BIRTH_DATE' => array(
        'width' => '15%',
        'label' => 'LBL_BIRTH_DATE',
        'default' => true
    ),
    'CREATED_BY_NAME' => array(
        'width' => '10',
        'label' => 'LBL_CREATED'),
    'ASSIGNED_USER_NAME' => array(
        'width' => '10',
        'label' => 'LBL_LIST_ASSIGNED_USER',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => false),
    'MODIFIED_BY_NAME' => array(
        'width' => '10',
        'label' => 'LBL_MODIFIED'),
    'SYNC_CONTACT' => array(
        'type' => 'bool',
        'label' => 'LBL_SYNC_CONTACT',
        'width' => '10%',
        'default' => false,
        'sortable' => false,
    ),
    'DATE_ENTERED' => array(
        'width' => '10',
        'label' => 'LBL_DATE_ENTERED',
        'default' => false)
);
