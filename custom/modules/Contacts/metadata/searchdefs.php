<?php

$searchdefs['Contacts'] = array(
    'templateMeta' => array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' => array('label' => '10', 'field' => '30'),
    ),
    'layout' => array(
        'basic_search' =>
        array(
            array('name' => 'search_name', 'label' => 'LBL_NAME', 'type' => 'name'),
            array('name' => 'is_imported', 'label' => 'LBL_IS_IMPORTED', 'default' => true),
            array('name' => 'contact_from', 'label' => 'LBL_CONTACT_FROM', 'default' => true),
        ),
        'advanced_search' =>
        array(
            'first_name' =>
            array(
                'name' => 'first_name',
                'default' => true,
                'width' => '10%',
            ),
            'email' =>
            array(
                'name' => 'email',
                'label' => 'LBL_ANY_EMAIL',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'phone' =>
            array(
                'name' => 'phone',
                'label' => 'LBL_ANY_PHONE',
                'type' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'last_name' =>
            array(
                'name' => 'last_name',
                'default' => true,
                'width' => '10%',
            ),
            'assigned_user_id' =>
            array(
                'name' => 'assigned_user_id',
                'label' => 'LBL_ASSIGNED_TO',
                'default' => true,
                'width' => '10%',
            ),
            'is_imported'=> 
            array(
                'name' => 'is_imported',
                'label' => 'LBL_IS_IMPORTED',
                'default' => true
            ),
            'contact_from' =>
            array(
                'name' => 'contact_from', 
                'label' => 'LBL_CONTACT_FROM', 
                'default' => true
            ),
        ),
    )
);
