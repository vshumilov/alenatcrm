<?php

$viewdefs ['Contacts'] = array(
    'EditView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'hidden' =>
                array(
                    0 => '<input type="hidden" name="opportunity_id" value="{$smarty.request.opportunity_id}">',
                    1 => '<input type="hidden" name="case_id" value="{$smarty.request.case_id}">',
                    2 => '<input type="hidden" name="bug_id" value="{$smarty.request.bug_id}">',
                    3 => '<input type="hidden" name="email_id" value="{$smarty.request.email_id}">',
                    4 => '<input type="hidden" name="inbound_email_id" value="{$smarty.request.inbound_email_id}">',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'includes' =>
            array(
                array(
                    'file' => 'custom/modules/Contacts/js/contactExist.js',
                ),
                array(
                    'file' => 'custom/modules/Contacts/js/editview.js',
                ),
                array(
                    'file' => 'custom/include/Nit/js/notValidateAllFields.js',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array(
                'LBL_CONTACT_INFORMATION' =>
                array(
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ADVANCED' =>
                array(
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'lbl_contact_information' =>
            array(
                array(
                    array(
                        'name' => 'not_validate',
                    ),
                ),
                array(
                    array(
                        'name' => 'last_name',
                    ),
                ),
                array(
                    array(
                        'name' => 'first_name_only',
                    ),
                ),
                array(
                    array(
                        'name' => 'patronymic',
                    ),
                ),
                array(
                    array(
                        'name' => 'gender',
                    ),
                ),
                array(
                    array(
                        'name' => 'birth_date',
                    ),
                ),
                array(
                    array(
                        'name' => 'phone_mobile',
                        'comment' => 'Mobile phone number of the contact',
                        'label' => 'LBL_MOBILE_PHONE',
                    ),
                ),
                array(
                    array(
                        'name' => 'phone_work',
                        'comment' => 'Work phone number of the contact',
                        'label' => 'LBL_OFFICE_PHONE',
                    ),
                ),
                array(
                    array(
                        'name' => 'email',
                        'studio' => 'false',
                        'label' => 'LBL_EMAIL_ADDRESS',
                    ),
                ),
                /*
                  array(
                  array(
                  'name' => 'primary_address_street',
                  'hideLabel' => true,
                  'type' => 'address',
                  'displayParams' =>
                  array(
                  'key' => 'primary',
                  'rows' => 2,
                  'cols' => 30,
                  'maxlength' => 150,
                  ),
                  ),
                  ), */
                array(
                    array(
                        'name' => 'description',
                        'label' => 'LBL_DESCRIPTION',
                    ),
                ),
                array(
                    array(
                        'name' => 'assigned_user_name',
                        'label' => 'LBL_ASSIGNED_TO_NAME',
                    ),
                ),
            ),
            'LBL_PASSPORT_PANEL' => array(
                array(
                    array(
                        'name' => 'passport_series',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_number',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_date_of_issue',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_division_code',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_issued_by',
                    ),
                ),
                array(
                    array(
                        'name' => 'passport_address',
                    ),
                ),
            ),
            'LBL_DISPATCH_PANEL' => array(
                array(
                    array(
                        'name' => 'is_email_dispatch_blocked',
                    ),
                ),
                array(
                    array(
                        'name' => 'is_sms_dispatch_blocked',
                    ),
                ),
            )
        ),
    ),
);
