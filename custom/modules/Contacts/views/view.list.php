<?php

require_once('custom/include/MVC/View/views/view.list.php');
require_once('custom/modules/Contacts/ContactsListViewSmarty2.php');
require_once 'custom/modules/Contacts/BeanHelper/Contact.php';

class ContactsViewList extends ViewList
{
    /**
     * @see ViewList::preDisplay()
     */
    public function preDisplay(){
        require_once('modules/AOS_PDF_Templates/formLetter.php');
        formLetter::LVPopupHtml('Contacts');
        parent::preDisplay();

        $this->lv = new ContactsListViewSmarty2();
        $this->lv->targetList = false;
        $this->lv->email = false;
        $this->lv->mergeduplicates = false;
        $this->lv->mailMerge = false;
        $this->lv->showMassupdateFields = false;
    }
    
    public function listViewPrepare()
    {
        parent::listViewPrepare();
        
        $this->params['custom_where'] = "";
        
        if (!empty($_REQUEST['phone_advanced'])) {
            $contactHelper = new ContactBeanHelperContact($this->bean);
            $this->params['custom_where'] .= $contactHelper->getWherePhone($_REQUEST['phone_advanced']);
        }
        
        if (!empty($_REQUEST['email_advanced'])) {
            $contactHelper = new ContactBeanHelperContact($this->bean);
            $this->params['custom_where'] .= $contactHelper->getWhereEmail($_REQUEST['email_advanced']);
        }
    }
}
