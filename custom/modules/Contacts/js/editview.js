$(document).ready(function () {
    var $firstName = $('#first_name_only');
    var $lastName = $('#last_name');
    var $patronymic = $('#patronymic');
    var contactId = $('input[name="record"]').val();
    var $birthDate = $('#birth_date');
    var $phoneWork = $('#phone_work');
    var $phoneMobile = $('#phone_mobile');
    var $email = $('#email');
    var contactExist = new ContactExist();

    $firstName.on("change", function () {
        checkIsExistsContact();
    });

    $lastName.on("change", function () {
        checkIsExistsContact();
    });

    $birthDate.on("change", function () {
        checkIsExistsContact();
    });

    $phoneWork.on("change", function () {
        checkIsExistsContact();
    });

    $phoneMobile.on("change", function () {
        checkIsExistsContact();
    });

    $email.on("change", function () {
        checkIsExistsContact();
    });

    function checkIsExistsContact() {
        setTimeout(function () {
            var firstName = $firstName.val();

            if ($patronymic.val()) {
                firstName += " " + $patronymic.val();
            }
            
            contactExist.checkIsExists(
                    firstName,
                    $lastName.val(),
                    contactId,
                    $birthDate.val(),
                    $phoneWork.val(),
                    $phoneMobile.val(),
                    $email.val());
        }, 500);

    }
});

