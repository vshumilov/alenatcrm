var ContactExist = function (fieldKey) {
    var self = this;

    self.fieldKey = fieldKey ? fieldKey : "";
    
    this.checkIsExists = function (firstName, lastName, contactId, birthDate, phoneWork, phoneMobile, email) {
        var deferred = $.Deferred();
        var self = this;
        
        self.clearErrorMessages();

        LightAjax({
            module: 'Contacts',
            record: contactId,
            call: 'checkIsExistsContact',
            params: {
                first_name: firstName,
                last_name: lastName,
                contact_id: contactId,
                birth_date: birthDate,
                phone_work: phoneWork,
                phone_mobile: phoneMobile,
                email: email
            }
        }, function (field) {
            self.clearErrorMessages();
            
            var $saveButton = $('input[type="submit"]');
            
            if (field) {
                $('[field="' + self.fieldKey + field + '"]').append(
                        $('<div>')
                        .addClass('required validation-message client-exists')
                        .html(self.getErrorMessage(field))
                        );
                $saveButton.attr('disabled', 'disabled');
            } else {
                $saveButton.removeAttr('disabled');
            }

            deferred.resolve(field);
        }, function (data) {
            deferred.reject(data);
        });

        return deferred.promise();
    };

    self.clearErrorMessages = function () {
        var self = this;
        var fields = self.getFields();
        
        $.each(fields, function (field, str) {
            $('[field="' + self.fieldKey + field + '"]').find(".client-exists").remove();
        });
    };

    self.getErrorMessage = function (currentField) {
        var self = this;
        var errorMessage = 'Клиент с таким ФИО и <fieldStr> уже существует';
        var fieldStr = "";

        var fields = self.getFields();

        $.each(fields, function (field, str) {
            if (currentField == field) {
                fieldStr = str;
            }
        });

        errorMessage = errorMessage.replace('<fieldStr>', fieldStr);

        return errorMessage;
    };

    self.getFields = function () {
        var fields = {
            birth_date: "датой рождения",
            phone_work: "рабочим телефоном",
            phone_mobile: "мобильным телефоном",
            email: "e-mail"
        };

        return fields;
    };
};


