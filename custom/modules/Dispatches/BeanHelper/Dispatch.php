<?php

require_once 'custom/include/Nit/BeanHelper/Sms.php';
require_once 'custom/include/Nit/Helper/Phone.php';

class DispatchBeanHelperDispatch extends BeanHelperSMS
{

    public function sendCreateTourManagerEmail($agreement)
    {
        if (empty($agreement->id)) {
            return;
        }

        $sugarConfig = new SugarConfig();
        $createTourEmailManagerTemplateId = $sugarConfig->get('create_tour_email_manager_template_id');

        if (empty($createTourEmailManagerTemplateId)) {
            $GLOBALS['log']->fatal("create_tour_email_manager_template_id not exist in config.php");
            return;
        }

        $template = BeanFactory::getBean('EmailTemplates', $createTourEmailManagerTemplateId);

        $beans = [];
        $beans['Opportunities'] = $agreement->id;

        $tourManageRoleName = $sugarConfig->get('tour_manager_role_name');

        if (empty($tourManageRoleName)) {
            $GLOBALS['log']->fatal("tour_manager_role_name not exist in config.php");
            return;
        }

        $emails = $this->getEmailsByRoles([$tourManageRoleName]);

        $result = $this->sendEmail($emails, $template, [], true, $beans);

        if (!$result) {
            $GLOBALS['log']->fatal("create tour email to managers faild");
            return;
        }

        return $result;
    }

    public function sendCreateTourEmailAndSms($agreement, $priceId, $apartmentId)
    {
        if (empty($agreement->id) || empty($priceId) || empty($apartmentId)) {
            return;
        }

        $contact = BeanFactory::getBean('Contacts', $agreement->contact_id);

        if (empty($contact->id)) {
            return;
        }

        $sugarConfig = new SugarConfig();
        $createTourEmailTemplateId = $sugarConfig->get('create_tour_email_template_id');

        if (empty($createTourEmailTemplateId)) {
            $GLOBALS['log']->fatal("create_tour_email_template_id not exist in config.php");
            return;
        }

        $template = BeanFactory::getBean('EmailTemplates', $createTourEmailTemplateId);

        $price = BeanFactory::getBean('Prices', $priceId);

        if (empty($price->id)) {
            return;
        }

        $beans = [];
        $beans['Opportunities'] = $agreement->id;
        $beans['Contacts'] = $contact->id;
        $beans['Prices'] = $priceId;
        $beans['Apartments'] = $apartmentId;
        $beans['Sanatoriums'] = $price->parent_id;
        
        $text = $this->populateTemplate($template, false, $contact, $beans);

        $resultEmail = $this->sendEmail([$contact->email], $template, [], true, $beans);

        if ($resultEmail) {
            $this->createDispatcheslog($contact);
        }

        $helperPhone = new HelperPhone();

        if (!$helperPhone->isValidMobilePhone($contact->phone_mobile)) {
            return;
        }
        
        if (empty($text['body_alt'])) {
            $GLOBALS['log']->fatal("in EmailTemplate with create_tour_email_template_id body_alt is empty");
            return;
        }

        $resultSms = $this->sendSms([$contact->phone_mobile], $text['body_alt']);
        
        if (!$this->hasSmsErrors($resultSms)) {
            $this->createDispatcheslog($contact, 'Sms', $text['body_alt']);
        }
    }

    public function sendSmsCode($phone)
    {
        $helperPhone = new HelperPhone();

        if (!$helperPhone->isValidMobilePhone($phone)) {
            throw new Exception("phone is not correct");
        }

        $code = $this->generateCode();
        
        $sugarConfig = new SugarConfig();
        $smsCodeText = $sugarConfig->get('sms_code_text');
        
        if (empty($smsCodeText)) {
            $GLOBALS['log']->fatal("sms_code_text not exists in config.php");
            return;
        }

        $result = $this->sendSms([$phone], $smsCodeText . ' ' . $code, true);

        if (isset(json_decode($result, true)['error'])) {
            throw new Exception("send sms code error");
        }
    }

    public function checkSmsCode($code, $phone)
    {
        $helperPhone = new HelperPhone();

        if (!$helperPhone->isValidCode($code)) {
            throw new Exception("code is not correct");
        }

        $lastCode = $this->getLastCodeForPhone($phone);

        if (empty($lastCode) || $lastCode != $code) {
            throw new Exception("check sms code error");
        }
    }

    protected function getLastCodeForPhone($phone)
    {
        $helperPhone = new HelperPhone();

        if (!$helperPhone->isValidMobilePhone($phone)) {
            throw new Exception("phone is not correct");
        }

        $sql = "
        SELECT
            name
        FROM
            smslogs
        WHERE
            phone LIKE {phone} AND
            deleted = '0' AND
            TIMESTAMPDIFF(MINUTE, date_entered, NOW()) < 3600
        ORDER BY
            date_entered DESC
        LIMIT
            1
        ";

        $query = strtr($sql, [
            '{phone}' => dbQuote($phone)
        ]);

        $code = dbGetScalar($query);

        return $code;
    }

    protected function generateCode($digits = 4)
    {
        $i = 0;
        $code = "";

        while ($i < $digits) {
            $code .= mt_rand(0, 9);
            $i++;
        }

        return $code;
    }

    public function sendAllDispatches()
    {
        $GLOBALS['log']->fatal("Send Dispatches start datetime: " . date('d.m.Y H:i:s', time()));

        try {
            $db = getDatabaseInstance();

            $dbResult = $db->query($this->getAllActiveDispatchesQuery());

            if (empty($dbResult)) {
                return;
            }

            while (($row = $db->fetchByAssoc($dbResult)) != null) {
                $this->bean = null;
                $this->bean = BeanFactory::getBean('Dispatches', $row['id']);

                if (!$this->mustBeSend()) {
                    continue;
                }

                if ($this->bean->send_by_email) {
                    $this->sendEmailToContacts();
                }

                if ($this->bean->send_by_sms) {
                    $this->sendSmsToContacts();
                }
            }
        } catch (Exception $exc) {
            $GLOBALS['log']->fatal("Send Dispatches error: " . $exc->getTraceAsString());
        } finally {
            $GLOBALS['log']->fatal("Send Dispatches end datetime: " . date('d.m.Y H:i:s', time()));
        }
    }

    protected function getAllActiveDispatchesQuery()
    {
        $sql = "
        SELECT
            d.id
        FROM
            dispatches AS d
        WHERE
            d.deleted = '0' AND
            d.is_active = '1'
        ";

        return $sql;
    }

    protected function mustBeSend()
    {
        if (empty($this->bean->id)) {
            return false;
        }

        if ($this->isEqualWithCurrentDate()) {
            return true;
        }

        if ($this->isDaily()) {
            return true;
        }

        if ($this->isWeekDay()) {
            return true;
        }

        if ($this->isWeekEnd()) {
            return true;
        }

        if ($this->isWeekly()) {
            return true;
        }

        if ($this->isMonthly()) {
            return true;
        }

        if ($this->isQuarterly()) {
            return true;
        }

        if ($this->isEveryYear()) {
            return true;
        }

        return false;
    }

    public function sendEmailToContacts()
    {
        if (empty($this->bean->emailtemplate_id)) {
            return;
        }

        $db = getDatabaseInstance();

        $dbResult = null;

        if ($this->bean->send_to_all_contacts) {
            $dbResult = $db->query($this->getAllContactsQueryForDispatch());
        } else {
            $dbResult = $db->query($this->getDispatchContactsQuery());
        }

        if (empty($dbResult)) {
            return;
        }

        $template = BeanFactory::getBean('EmailTemplates', $this->bean->emailtemplate_id);

        $contact = null;

        while (($row = $db->fetchByAssoc($dbResult)) != null) {
            set_time_limit(30);
            $contact = null;
            $contact = BeanFactory::getBean('Contacts', $row['id']);

            if (!$this->isValidEmail($contact->email)) {
                $GLOBALS['log']->fatal("Send Dispatches email is not valid: " . $contact->email . " " . $contact->name);
                continue;
            }

            if (!empty($contact->is_email_dispatch_blocked)) {
                $GLOBALS['log']->fatal("Send Dispatches client is email dispatch blocked: " . $contact->email . " " . $contact->name);
                continue;
            }

            $result = $this->sendEmail([$contact->email], $template);

            if ($result) {
                $this->createDispatcheslog($contact);
            }
        }

        if (!$this->bean->is_repeated && $this->bean->send_type == 'date') {
            $this->bean->is_active = false;
            $this->bean->save();
        }
    }

    public function sendSmsToContacts()
    {
        if (empty($this->bean->emailtemplate_id)) {
            return;
        }

        $db = getDatabaseInstance();

        $dbResult = null;

        if ($this->bean->send_to_all_contacts) {
            $dbResult = $db->query($this->getAllContactsQueryForDispatch());
        } else {
            $dbResult = $db->query($this->getDispatchContactsQuery());
        }

        if (empty($dbResult)) {
            return;
        }

        $template = BeanFactory::getBean('EmailTemplates', $this->bean->emailtemplate_id);

        $contact = null;

        $helperPhone = new HelperPhone();

        while (($row = $db->fetchByAssoc($dbResult)) != null) {
            set_time_limit(30);
            $contact = null;
            $contact = BeanFactory::getBean('Contacts', $row['id']);

            if (!empty($contact->is_sms_dispatch_blocked)) {
                $GLOBALS['log']->fatal("Send Dispatches client is sms dispatch blocked: " . $contact->email . " " . $contact->name);
                continue;
            }

            $text = $this->populateTemplate($template, false, $contact);

            $result = false;

            if ($helperPhone->isValidMobilePhone($contact->phone_mobile)) {
                $result = $this->sendSms([$contact->phone_mobile], $text['body_alt']);
            }

            if ($helperPhone->isValidMobilePhone($contact->phone_work)) {
                $result = $this->sendSms([$contact->phone_work], $text['body_alt']);
            }

            if ($result) {
                $this->createDispatcheslog($contact, 'Sms');
            }
        }

        $this->closeDispatch();
    }

    protected function closeDispatch()
    {
        if (!$this->bean->is_repeated && $this->bean->send_type == 'date') {
            $this->bean->is_active = false;
            $this->bean->save();
        }
    }

    public function createDispatcheslog($contact, $dispatchType = 'Email', $text = '')
    {
        global $timedate;

        $currentDbDateTime = date('Y-m-d H:i:s', time());
        $humenCurrentDbDateTime = $timedate->to_display_date_time($currentDbDateTime);

        $dispatcheslog = new Dispatcheslog();
        $dispatcheslog->dispatch_id = $this->bean->id;
        $dispatcheslog->contact_id = $contact->id;
        $dispatcheslog->name = "$dispatchType уведомление " . date('d.m.Y H:i:s', time());
        
        if (!empty($text)) {
            $dispatcheslog->description = $text;
        }
        
        $dispatcheslog->save();

        if (empty($contact->is_email_dispatch_blocked)) {
            $contact->last_email_dispatch_datetime = $humenCurrentDbDateTime;
        }

        if (empty($contact->is_sms_dispatch_blocked)) {
            $contact->last_sms_dispatch_datetime = $humenCurrentDbDateTime;
        }

        $contact->save();
    }

    protected function getDispatchContactsQuery()
    {
        $sql = "
        SELECT
            c.id
        FROM
            contacts AS c
        JOIN
            dispatches_contacts AS dc
        ON
            c.id = dc.contact_id AND 
            dc.deleted = '0' AND
            dc.dispatch_id = <dispatchId>
        WHERE
            c.deleted = '0'
        GROUP BY
            c.id
        ";

        $query = strtr($sql, ['<dispatchId>' => dbQuote($this->bean->id)]);

        return $query;
    }

    protected function getAllContactsQueryForDispatch()
    {
        $sql = "
        SELECT
            c.id
        FROM
            contacts AS c
        WHERE
            c.deleted = '0' AND
            <whereContactFrom>
            (
                c.is_email_dispatch_blocked = '0' OR
                c.is_sms_dispatch_blocked = '0'
            )
        ";

        $query = strtr($sql, [
            '<whereContactFrom>' => ($this->bean->contact_from != 'all' ? "c.contact_from = " . dbQuote($this->bean->contact_from) . " AND " : "")
        ]);

        return $query;
    }

    protected function isEqualWithCurrentDate($sendDate, $currentDate)
    {
        global $timedate;
        $currentDate = date_parse(date($timedate->get_date_time_format(), time()));
        $sendDate = date_parse($this->bean->send_date);

        if (
                $this->bean->send_type == 'date' &&
                $sendDate['year'] == $currentDate['year'] &&
                $sendDate['month'] == $currentDate['month'] &&
                $sendDate['day'] == $currentDate['day'] &&
                $sendDate['hour'] == $currentDate['hour'] &&
                $sendDate['minute'] == $currentDate['minute']
        ) {
            return true;
        }

        return false;
    }

    protected function isDaily()
    {
        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'daily'
        ) {
            return true;
        }

        return false;
    }

    protected function isWeekDay()
    {
        $dayOfWeek = date('N');

        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'weekday' &&
                !in_array($dayOfWeek, $this->getWeekendDays())
        ) {
            return true;
        }

        return false;
    }

    protected function isWeekEnd()
    {
        $dayOfWeek = date('N');

        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'weekend' &&
                in_array($dayOfWeek, $this->getWeekendDays())
        ) {
            return true;
        }

        return false;
    }

    protected function isWeekly()
    {
        $dayOfWeek = date('N');

        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'weekly' &&
                $dayOfWeek == '6'
        ) {
            return true;
        }

        return false;
    }

    protected function isMonthly()
    {
        $day = date('d');

        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'monthly' &&
                in_array($day, $this->getSalaryDays())
        ) {
            return true;
        }

        return false;
    }

    protected function isQuarterly()
    {
        $day = date('d');
        $month = date('m') + 1;

        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'quarterly' &&
                $month % 4 == 0 &&
                in_array($day, $this->getSalaryDays())
        ) {
            return true;
        }

        return false;
    }

    protected function isHalfYear()
    {
        $day = date('d');
        $month = date('m') + 1;

        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'half_year' &&
                $month % 6 == 0 &&
                in_array($day, $this->getSalaryDays())
        ) {
            return true;
        }

        return false;
    }

    protected function isEveryYear()
    {
        $day = date('d');
        $month = date('m') + 1;

        if (
                $this->bean->send_type == 'period' &&
                $this->bean->send_period == 'every_year' &&
                $month == 12 &&
                in_array($day, $this->getSalaryDays())
        ) {
            return true;
        }

        return false;
    }

    protected function getSalaryDays()
    {
        return ['10', '15'];
    }

    protected function getWeekendDays()
    {
        return ['6', '7'];
    }

}
