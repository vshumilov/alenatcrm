<?php

require_once 'custom/modules/Sanatoriums/BeanHelper/City.php';
require_once 'custom/modules/Sanatoriums/BeanHelper/Sanatorium.php';

class SanatoriumHookSanatorium
{

    public function prepareTinymceFields(Sanatorium $bean)
    {
        $bean->prepareTinymceFields();

        return $bean;
    }

    public function getTinymceTexts(Sanatorium $bean)
    {
        $bean->getTinymceTexts();

        return $bean;
    }

    public function setCity(Sanatorium $bean)
    {
        if (!empty($bean->city_name) && empty($bean->city_id)) {
            $cityHelper = new SanatoriumBeanHelperCity($bean);
            $city = $cityHelper->getCityByName($bean->city_name);

            if (empty($city)) {
                return;
            }

            $bean->city_id = $city->id;
        }
    }

    public function setAvatar(Sanatorium $bean)
    {
        $sanatoriumHelper = new SanatoriumBeanHelperSanatorium($bean);
        $bean = $sanatoriumHelper->setAvatar();
    }

    public function prepareName(Sanatorium $bean)
    {
        $bean->name = $bean->prepareName($bean->name);
    }
    
    public function prepareNameAfterRetrieve(Sanatorium $bean)
    {
        $bean->name = $bean->prepareNameAfterRetrieve($bean->name);
    }

    public function setId(Sanatorium $bean)
    {
        $id = $bean->getIdByName($bean->name);
        
        if (empty($id)) {
            return;
        }

        $bean->id = $id;
    }

    public function getHrefThumbnailNameS(Sanatorium $bean)
    {
        $sugarConfig = new SugarConfig();
        $thumbnailsUrl = $sugarConfig->get('web_site_images_thumbnails_url');
        
        if (empty($thumbnailsUrl)) {
            $GLOBALS['log']->fatal("web_site_images_thumbnails_url not exists in config.php");
            return;
        }
        
        $bean->href_thumbnail_name_s = $thumbnailsUrl . $bean->thumbnail_name_s;
    }
}
