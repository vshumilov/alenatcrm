<?php

require_once 'custom/include/Nit/BeanHelper.php';
require_once 'custom/include/Nit/ParseSites/CurortExpertParse.php';
require_once 'modules/Images/Image.php';
require_once 'modules/SugarNitfile/SugarNitfile.php';

class SanatoriumBeanHelperSanatorium extends BeanHelper
{

    protected $curortExpertParse;
    
    public function setAvatar()
    {
        SugarNitfile::saveBeanNitfiles($this->bean, 'avatar', false);
        
        if (empty($_FILES)) {
            return $this->bean;
        }

        $image = BeanFactory::getBean('Images');

        $imageId = $this->getAvatarImageId();

        if (!empty($imageId)) {
            $image = BeanFactory::getBean('Images', $imageId);
        }

        if (empty($image->id)) {
            $image->name = 'Главное изображение';
            $image->parent_id = $this->bean->id;
            $image->parent_type = $this->bean->module_dir;
            $image->is_avatar = 1;
            $image->image_type = 'nature';
            $image->order_number = 1;
        } else {
            SugarNitfile::clearBeanNitfiles($image, 'file');
        }

        $image->file = 1;
        
        $_FILES['nitfile_images_file'] = $_FILES['nitfile_sanatoriums_avatar'];
        unset($_FILES['nitfile_sanatoriums_avatar']);
        
        $image->save();
        
        $image = BeanFactory::getBean('Images', $image->id);
        
        $this->bean->thumbnail_name_s = $image->thumbnail_name_s;
        
        return $this->bean;
    }

    protected function getAvatarImageId()
    {
        $sql = "
        SELECT
            id
        FROM
            images
        WHERE
            deleted = '0' AND
            is_avatar = '1' AND
            parent_type like <parentType> AND
            parent_id = <parentId>
        LIMIT
            1
        ";

        $query = strtr($sql, [
            '<parentType>' => dbQuote($this->bean->module_dir),
            '<parentId>' => dbQuote($this->bean->id)
        ]);

        return dbGetScalar($query);
    }

    public function setAttributesKeys()
    {

        $GLOBALS['log']->fatal("Start set sanatoriums attrs datetime: " . date('d.m.Y H:i:s', time()));

        try {
            $this->curortExpertParse = new CurortExpertParse();
            
            $db = getDatabaseInstance();
            
            $sanatoriumBean = BeanFactory::getBean('Sanatoriums');
            
            $dbResult = $db->query($sanatoriumBean->getListQuery());

            if (empty($dbResult)) {
                return;
            }

            while (($row = $db->fetchByAssoc($dbResult)) != null) {
                set_time_limit(60);
                $this->bean = null;
                $this->bean = BeanFactory::getBean('Sanatoriums', $row['id']);
                
                $this->setBeanAttributes($this->bean);
            }
        } catch (Exception $exc) {
            $GLOBALS['log']->fatal("Start set sanatoriums attrs error: " . $exc->getTraceAsString());
        } finally {
            $GLOBALS['log']->fatal("Start set sanatoriums attrs end datetime: " . date('d.m.Y H:i:s', time()));
        }
    }
    
    protected function setBeanAttributes($bean)
    {
        if (empty($bean)) {
            return;
        }
        
        $fields = $bean->field_defs;
        
        foreach ($fields as $field) {
            set_time_limit(60);
            if ($field['type'] != 'multienum') {
                continue;
            }
            
            $options = translate("sanatoriums_{$field['name']}_options");
            
            $values = explode($this->curortExpertParse->multienumDelimiter, $bean->{$field['name']});
            
            if (empty($values) || empty($options)) {
                continue;
            }
            
            $options = array_flip($options);
            $newValues = [];
            
            foreach ($values as $key => $value) {
                set_time_limit(60);
                
                $moduleName = $this->getOptionModuleName($field['name']);
                
                $value = trim(strtr($value, ['Как правильно выбрать?' => '']));
                
                if (empty($value)) {
                    continue;
                }
                
                if (empty($options[$value])) {
                    $optionId = $this->getOptionIdByName($moduleName, $value);
                    
                    if (empty($optionId)) {
                        $optionBean = BeanFactory::getBean($moduleName);
                        $optionBean->name = $value;
                        $optionBean->save();
                        $optionId = $optionBean->id;
                    }
                    
                    $options[$value] = $optionId;
                }
                
                $newValues[] = $options[$value];
               
                $optionBean = BeanFactory::getBean($moduleName, $options[$value]);
                
                if (!empty($optionBean->id)) {
                    continue;
                }
                
                $optionBean = BeanFactory::getBean($moduleName);
                $optionBean->id = $options[$value];
                $optionBean->name = $value;
                $optionBean->new_with_id = true;
                
                $optionBean->save();
            }
            
            $bean->{$field['name']} = implode($this->curortExpertParse->multienumDelimiter, $newValues);
        }
        
        if (!empty($newValues)) {
            $bean->save();
        }
        
        $GLOBALS['log']->fatal("Start set sanatoriums: " . $bean->name);
    }
    
    protected function getOptionModuleName($optionName)
    {
        return ucfirst($optionName) . 's';
    }
    
    protected function getOptionIdByName($moduleName, $name)
    {
        $sql = "
        SELECT
            id
        FROM 
            <table>
        WHERE
            name LIKE '<name>'
        LIMIT
            1
        ";
        
        $query = strtr($sql, ['<table>' => strtolower($moduleName), '<name>' => $name]);
        
        return dbGetScalar($query);
    }

}

