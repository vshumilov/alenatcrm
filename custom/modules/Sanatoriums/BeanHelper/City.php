<?php

require_once 'custom/include/Nit/BeanHelper.php';

class SanatoriumBeanHelperCity extends BeanHelper
{
    public function getCityByName($cityName) {
        if (empty($cityName)) {
            return;
        }
        
        $city = BeanFactory::getBean('Cities');
        
        $cityId = $city->getIdByName($cityName);
        
        if (!empty($cityId)) {
            $city = BeanFactory::getBean('Cities', $cityId);
            
            return $city;
        }
        
        $city->name = $cityName;
        
        $city->save();
        
        return $city;
    }
}

