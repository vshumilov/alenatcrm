<?php

$hook_version = 1;
$hook_array = Array();

$hook_array['before_save'] = Array();
$hook_array['after_retrieve'] = [];
$hook_array['after_save'] = [];

$hook_array['before_save'][] = [
    1,
    'prepare Name',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'prepareName'
];

$hook_array['before_save'][] = [
    2,
    'set Id',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'setId'
];

$hook_array['after_save'][] = [
    4,
    'prepare Tinymce Fields',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'prepareTinymceFields'
];

$hook_array['before_save'][] = [
    5,
    'set City',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'setCity'
];

$hook_array['before_save'][] = [
    6,
    'set Avatar',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'setAvatar'
];

$hook_array['after_retrieve'][] = [
    1,
    'prepare Name After Retrieve',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'prepareNameAfterRetrieve'
];

$hook_array['after_retrieve'][] = [
    2,
    'get Tinymce Texts',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'getTinymceTexts'
];

$hook_array['after_retrieve'][] = [
    3,
    'get href_thumbnail_name_s',
    'custom/modules/Sanatoriums/Hook/Sanatorium.php',
    'SanatoriumHookSanatorium',
    'getHrefThumbnailNameS'
];
