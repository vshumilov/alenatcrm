<?php

$app_list_strings['moduleList']['DocumentTemplates'] = 'Шаблоны документов';
$app_list_strings['document_template_parent_type'] = array(
    'Opportunities' => 'Договоры',
);
$app_list_strings['document_template_type_list'] = array(
    'undefined' => 'Неизвестный',
    'odf' => 'ODF',
    'pdf' => 'PDF',
);
$app_list_strings['date_month_list'] = array(
    1 => 'Январь',
    2 => 'Февраль',
    3 => 'Март',
    4 => 'Апрель',
    5 => 'Май',
    6 => 'Июнь',
    7 => 'Июль',
    8 => 'Август',
    9 => 'Сентябрь',
    10 => 'Октябрь',
    11 => 'Ноябрь',
    12 => 'Декабрь',
);
$app_list_strings['date_month_genitive_list'] = array(
    1 => 'Января',
    2 => 'Февраля',
    3 => 'Марта',
    4 => 'Апреля',
    5 => 'Мая',
    6 => 'Июня',
    7 => 'Июля',
    8 => 'Августа',
    9 => 'Сентября',
    10 => 'Октября',
    11 => 'Ноября',
    12 => 'Декабря',
);
