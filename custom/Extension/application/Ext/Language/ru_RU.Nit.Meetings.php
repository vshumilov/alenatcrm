<?php

$app_list_strings['moduleList']['Meetings'] = 'Уведомления абонентов';

$app_list_strings['meeting_status_dom'] = [
    'Planned' => 'Новое',
    'Sent' => 'Отправлено',
    'Held' => 'Завизировано',
];

$app_list_strings['meeting_parent_type_display'] = [
    'Accounts' => 'Абоненты',
];

$app_strings['LBL_CLOSE_ACTIVITY_CONFIRM'] = 'Вы действительно хотите завизировать это уведомление?';

