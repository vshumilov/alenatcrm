<?php

$app_list_strings['moduleList']['Sanatoriums'] = 'Санатории';

$app_list_strings['sanatoriums_treatment_options'] = [
    'general_recovery' => 'Общее оздоровление',
    'allergy' => 'Аллергия',
    'gynecology' => 'Гинекология',
    'dermatology' => 'Дерматология',
    'organs_of_sight' => 'Органы зрения',
    'neurology' => 'Неврология',
    'metabolism' => 'Обмен веществ',
    'oncology' => 'Онкология',
    'musculoskeletal_system' => 'Опорно-двигательный аппарат',
    'respiratory_system' => 'Органы дыхания',
    'organs_of_digestion' => 'Органы пищеварения',
    'cardiovascular_system' => 'Сердечно-сосудистая система',
    'urology' => 'Урология',
];

$app_list_strings['sanatoriums_suitable_options'] = [
    'kids' => 'Для малышей',
    'teenagers' => 'Для детей и подростков',
    'youngers' => 'Для молодежи',
    'grownups' => 'Для взрослых',
    'elderly' => 'Для престарелых',
];

$app_list_strings['sanatoriums_arrival_options'] = [
    'morning' => 'Утром',
    'day' => 'Днем',
    'evening' => 'Вечером',
];

$app_list_strings['sanatoriums_healthprogram_options'] = [
    'pregnants' => 'Оздоровление для будущих мам',
    'disabled_people' => 'Оздоровление для людей ограниченных в движении',
    'mom_and_kid' => 'Программа Мать и Дитя',
    'lose_weight' => 'Программа очищения организма и похудения'
];

$app_list_strings['sanatoriums_beauty_options'] = [
    'bathing_pond' => 'Водоем для купания',
    'beach' => 'Пляж',
    'playground' => 'Спортивная площадка',
    'gym' => 'Спортивный зал',
    'indoor_pool' => 'Крытый бассейн',
    'outdoor_pool' => 'Открытый бассейн'
];

$app_list_strings['sanatoriums_foodoption_options'] = [
    'custom_menu' => 'Заказное меню',
    'cafe_on_site' => 'Кафе на территории',
    'complex_table' => 'Комплексный стол',
    'buffet' => 'Шведский стол',
];

$app_list_strings['sanatoriums_roomsinfostructure_options'] = [
    'bathroom' => 'Есть санузел',
    'air_conditioning' => 'Есть кондиционер',
    'refrigerator' => 'Есть холодильник',
    'buffet' => 'Шведский стол',
];

$app_list_strings['sanatoriums_territory_options'] = [
    'large_fenced_area' => 'Большая огороженная территория',
    'no_own_territory' => 'Нет своей территории',
    'fenced_area' => 'Огороженная территория',
];

$app_list_strings['sanatoriums_internet_options'] = [
    'lobby' => 'В лобби',
    'rooms' => 'В номерах',
    'territory' => 'На территории',
];

$app_list_strings['sanatoriums_rent_options'] = [
    'bicycles' => 'Прокат велосипедов',
    'water_equipment' => 'Прокат водной техники',
    'sports_equipment' => 'Прокат спортивного инвентаря',
];

$app_list_strings['sanatoriums_sport_options'] = [
    'aerobics' => 'Занятия по аэробике',
    'shaping' => 'Занятия шейпингом',
    'gym' => 'Тренажерный зал',
];

$app_list_strings['sanatoriums_sales_status_options'] = [
    'no_contact' => 'Не было контакта',
    'is_interested' => 'Заинтересован',
    'agreement' => 'Заключен договор',
    'reject' => 'Отказался сотрудничать',
];


