<?php

$app_list_strings['moduleList']['DocumentTemplates'] = 'Document Templates';
$app_list_strings['document_template_parent_type'] = array(
    'Accounts' => 'Accounts',
    'Opportunities' => 'Opportunities',
    'Meetings' => 'Meetings',
    'Receivables' => 'Receivables',
    'Acceptmeteringdevices' => 'Acceptmeteringdevices',
    'Checkwatersupplysystems' => 'Checkwatersupplysystems',
);
$app_list_strings['document_template_type_list'] = array(
    'undefined' => 'Undefined',
    'odf' => 'ODF',
    'pdf' => 'PDF',
);
$app_list_strings['date_month_list'] = array(
    1 => 'January',
    2 => 'February',
    3 => 'March',
    4 => 'April',
    5 => 'May',
    6 => 'June',
    7 => 'July',
    8 => 'August',
    9 => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December',
);
$app_list_strings['date_month_genitive_list'] = $app_list_strings['date_month_list'];
