<?php

$app_list_strings['moduleList']['Dispatches'] = 'Рассылки';

$app_list_strings['dispatches_send_period_options'] = [
    'daily' => 'Ежедневно',
    'weekday' => 'Будний день',
    'weekend' => 'Выходной день',
    'weekly' => 'Еженедельно',
    'monthly' => 'Ежемесячно',
    'quarterly' => 'Ежеквартально',
    'half_year' => 'Каждые полгода',
    'every_year' => 'Каждый год'
];

$app_list_strings['dispatches_send_type_options'] = [
    'date' => 'Дата',
    'period' => 'Период'
];
