<?php

$app_list_strings['moduleList']['Contacts'] = 'Клиенты';

$app_list_strings['contacts_gender_options'] = [
    '1' => 'мужчина',
    '0' => 'женщина'
];

$app_list_strings['contacts_from_options'] = [
    'agreement' => 'С договоров',
    'alex' => 'Контакты Алексея'
];

$app_list_strings['contacts_from_with_all_options'] = [
    'all' => 'Все',
];

$app_list_strings['contacts_from_with_all_options'] = array_merge($app_list_strings['contacts_from_with_all_options'], $app_list_strings['contacts_from_options']);