<?php

$app_list_strings['moduleList']['Opportunities'] = 'Договоры';

$app_list_strings['opportunity_type_dom'] = [
    'application' => 'заявка на бронь',
    'booked' => 'забронирован',
    'agreement_sent' => 'договор отправлен клиенту',
    'prepayment' => 'предоплата',
    'paid' => 'оплачен',
    'closed' => 'закрыт',
    'canceled' => 'отменен',
];

$app_strings['LBL_TRACK_EMAIL_BUTTON_TITLE'] = 'Отправить email';
$app_strings['LBL_TRACK_EMAIL_BUTTON_LABEL'] = $app_strings['LBL_TRACK_EMAIL_BUTTON_TITLE'];

$app_list_strings['opportunities_client_from_source_options'] = [
    'stage' => 'Реклама на остановке',
    'site' => 'Сайт',
    'sms' => 'Sms рассылка',
    'email' => 'Email рассылка',
    'internet' => 'Реклама в интернете',
    'phone' => 'По телефонному звонку',
    'friends' => 'От знакомых',
    'press' => 'СМИ',
    'old' => 'Постоянный клиент'
];
