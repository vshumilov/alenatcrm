<?php

$app_list_strings['moduleList']['Images'] = 'Изображения';

$app_list_strings['images_parent_type_dom'] = [
    'Sanatoriums' => 'Санатории'
];

$app_list_strings['images_options_image_type'] = [
    '' => '',
    'health' => 'Лечебные процедуры',
    'rooms' => 'Размещение и интерьеры',
    'nature' => 'Природа',
    'food' => 'Питание',
    'entertainment' => 'Развлечения'
];

