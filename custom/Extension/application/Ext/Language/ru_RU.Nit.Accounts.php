<?php

$app_list_strings['moduleList']['Accounts'] = 'Абоненты';

$app_strings['LBL_QUICK_CONTACT'] = 'Создать клиента';
$app_strings['LBL_QUICK_OPPORTUNITY'] = 'Создать договор';
$app_strings['LBL_QUICK_TOUR'] = 'Создать тур';
$app_strings['LBL_QUICK_COUNTRY'] = 'Создать страну';
$app_strings['LBL_QUICK_CITY'] = 'Создать город';
$app_strings['LBL_QUICK_OPERATOR'] = 'Создать тур оператора';

$app_strings['LBL_LINK_ACTIONS'] = 'Действия';
$app_strings['LBL_ACTION_LINES'] = 'Действия';
$app_strings['LNK_BACKTOTOP'] = 'Вверх';
$app_strings['LBL_SUITE_TOP'] = 'Вверх';


$app_list_strings['account_type_dom'] = [
    'ИП' => 'ИП - индивидуальный предприниматель',
    'ООО' => 'ООО - общество с ограниченной ответственностью',
    'ОДО' => 'ОДО - общество с дополнительной ответственностью',
    'ОАО' => 'ОАО - открытое акционерное общество',
    'ЗАО' => 'ЗАО - закрытое акционерное общество',
    'ПК' => 'ПК - производственный кооператив',
    'КФХ' => 'КФХ - крестьянское (фермерское) хозяйство',
    'ГУП' => 'ГУП - государственное унитарное предприятие'
];

$app_strings['LBL_ERROR_LENGTH'] = ' длина должна быть не больше ';
$app_strings['LBL_ERROR_EQUAL_LENGTH'] = ' длина должна быть равна ';
