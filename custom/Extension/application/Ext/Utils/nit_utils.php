<?php

require_once 'custom/include/Nit/BeanHelper.php';
require_once 'custom/include/Nit/Helper.php';

function getLink($module, $id, $name, $action = 'DetailView')
{
    if (empty($module) || empty($id)) {
        return $name;
    } else {
        return '<a target="_blank" href="' .
        SugarConfig::get('site_url') . '/index.php?' . http_build_query(array(
            'module' => $module,
            'action' => $action,
            'record' => $id,
        )) . '">' . $name . '</a>';
    }
}

function getLinkByBean(SugarBean $bean, $action = 'DetailView')
{
    return getLink($bean->module_dir, $bean->id, $bean->get_summary_text(), $action);
}

/**
 * returns link to database object
 */
function & getDatabaseInstance()
{
    static $db = null;
    if (!isset($db)) {
        $db = & DBManagerFactory::getInstance();
    }
    return $db;
}

/**
 * Returns first element in first row of result of SQL query execution
 * @param	string	$sql
 * @param	object	$db
 */
function dbGetScalar($sql, & $db = null)
{
    if (empty($sql)) {
        return null;
    }
    if (empty($db)) {
        $db = getDatabaseInstance();
    }

    $db_result = $db->query($sql);
    $result = $db->fetchByAssoc($db_result);
    $result = empty($result) || !is_array($result) ? null : array_shift($result);
    return $result;
}

/**
 * Returns first row of result returned by database
 * @param	string	$sql
 * @param	object	$db
 */
function dbGetRow($sql, & $db = null)
{
    if (empty($sql)) {
        return null;
    }
    if (empty($db)) {
        $db = getDatabaseInstance();
    }
    $db_result = $db->query($sql/* , false, '', false, true */);
    $result = $db->fetchByAssoc($db_result);

    if (empty($result) || !is_array($result)) {
        $result = null;
    }
    return $result;
}

/**
 * Returns array by SQL query
 * @param	string	$sql
 * @param	object	$db
 */
function dbGetArray($sql, & $db = null)
{
    if (empty($sql)) {
        return null;
    }
    if (empty($db)) {
        $db = getDatabaseInstance();
    }
    $db_result = $db->query($sql/* , false, '', false, true */);
    if (empty($db_result)) {
        return null;
    }
    $result = array();
    while (($row = $db->fetchByAssoc($db_result)) != null) {
        array_push($result, $row);
    }
    return $result;
}

/**
 * Returns associative array by SQL query
 * @param	string	$sql
 * @param	object	$db
 */
function dbGetAssocArray($sql, & $db = null)
{
    if (empty($sql)) {
        return null;
    }
    if (empty($db)) {
        $db = getDatabaseInstance();
    }
    $db_result = $db->query($sql/* , false, '', false, true */);
    if (empty($db_result)) {
        return null;
    }
    $result = array();
    $row = $db->fetchByAssoc($db_result);

    if (!is_array($row) || count($row) == 0) {
        return $result;
    } elseif (count($row) == 1) {
        do {
            $key = array_shift($row);
            $result[$key] = $key;
        } while (($row = $db->fetchByAssoc($db_result)) != null);
    } elseif (count($row) == 2) {
        do {
            $key = array_shift($row);
            $result[$key] = array_shift($row);
        } while (($row = $db->fetchByAssoc($db_result)) != null);
    } elseif (count($row) > 2) {
        do {
            $key = array_shift($row);
            $result[$key] = $row;
        } while (($row = $db->fetchByAssoc($db_result)) != null);
    }

    return $result;
}

/**
 * Returns associative array by SQL query
 * @param	string	$sql
 * @param	boolean	$affected	# return affected rows count or not
 * @param	object	$db
 */
function dbExecute($sql, $affected = false, DBManager & $db = null)
{
    if (empty($sql)) {
        return null;
    }
    if (empty($db)) {
        $db = getDatabaseInstance();
    }
    $db_result = $db->query($sql);
    if ($affected) {
        return $db->getAffectedRowCount($db_result);
    } else {
        return $db_result;
    }
}

/**
 * Returns escaped string
 * 
 * @param $str
 */
function dbQuote($str = '', $add_quotes = true)
{
    $str = getDatabaseInstance()->quote($str);
    if ($add_quotes) {
        $str = "'" . $str . "'";
    }
    return $str;
}

/**
 * Returns escaped with ` string
 * 
 * @param $str
 */
function dbQuoteName($str = '')
{
    return '`' . getDatabaseInstance()->quote($str) . '`';
}

/**
 * Retunrs auto increment value by passed key
 * @IMPORTANT: for use this function you should add table to database
 * 
 * CREATE TABLE IF NOT EXISTS `auto_increments` (
 * 		`category` varchar(255) NOT NULL,
 * 		`ai` int(11) NOT NULL,
 * 		PRIMARY KEY (`category`)
 * ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
 * 
 * @param	string	$key
 */
function auto_increment($category = 'default', $increment = true)
{
    $table = 'vedisoft_autoinc';

    if ($increment)
        dbExecute('LOCK TABLES `' . $table . '` WRITE;');

    $category = dbQuote($category);

    $sql = 'SELECT `ai` FROM `' . $table . '` WHERE `category`=' . $category . ';';

    $existing_value = dbGetScalar($sql);

    if (!$increment) {
        return ($existing_value ? $existing_value : 0);
    }
    if ($existing_value) {
        $new_value = $existing_value + 1;
        $sql = 'UPDATE `' . $table . '` SET `ai`=' . dbQuote($new_value) . ' WHERE `category`=' . $category . ';';
    } else {
        $new_value = 1;
        $sql = 'INSERT INTO `' . $table . '` SET `category`=' . $category . ', `ai`=' . dbQuote($new_value) . ';';
    }

    $success = dbExecute($sql, true);

    if ($increment)
        dbExecute('UNLOCK TABLES;');

    return $new_value;
}

/**
 * @param SugarBean $bean
 * @param string $fieldValue
 * @param string $fieldName
 * @param bool $disableRowLevelSecurity
 * @return \SugarBean|false
 */
function getBeanByStringField(\SugarBean $bean, $fieldValue, $fieldName = 'code_mdm', $disableRowLevelSecurity = true)
{
    global $db;
    $seed = false;
    $sql = "SELECT id FROM {$bean->table_name} WHERE {$fieldName} = '" . $db->quote($fieldValue) . "'";
    if ($id = $db->getOne($sql)) {
        $bean->disable_row_level_security = $disableRowLevelSecurity;
        $seed = $bean->retrieve($id);
    }
    return $seed;
}

/**
 * Return xml object attributes as array
 * @param SimpleXMLElement $xmlObj
 * @return mixed
 */
function getXmlAttributesAsArray(\SimpleXMLElement $xmlObj)
{
    $attributes = (array) $xmlObj->attributes();
    return current($attributes);
}

function getFromDatabaseOptions($focus, $field = null, $value = '', $view = 'DetailView')
{
    $def = $focus->field_defs[$field];
    $options = array();
    $key = 'id';
    $value = 'name';

    if (!empty($def['function']['options'])) {
        $options = $def['function']['options'];
    }

    if (!empty($def['table'])) {
        $table = $def['table'];
    }

    if (!empty($options['table'])) {
        $table = $options['table'];
    }

    if (!empty($options['key'])) {
        $key = $options['key'];
    }

    if (empty($table)) {
        throw new Exception('table is required');
    }

    $sql = sprintf("SELECT %s, %s FROM %s", $key, $value, $table);

    return dbGetAssocArray($sql);
}

function getCustomEmailAddressWidget($focus, $field, $value, $view, $tabindex='0') {
    $sea = new SugarEmailAddress();
    $sea->setView($view);

        if($view == 'EditView' || $view == 'QuickCreate' || $view == 'ConvertLead') {
            $module = $focus->module_dir;
            if ($view == 'ConvertLead' && $module == "Contacts")  $module = "Leads";

            return $sea->getEmailAddressWidgetEditView($focus->id, $module, false,'custom/include/SugarEmailAddress/templates/forEditView.tpl',$tabindex);
        }

    return $sea->getEmailAddressWidgetDetailView($focus);
}
