<?php

$dictionary['Contact']['fields']['not_validate'] = [
    'name' => 'not_validate',
    'type' => 'bool',
    'default' => '0',
    'vname' => 'LBL_NOT_VALIDATE',
    'source' => 'non-db'
];

$dictionary['Contact']['fields']['gender'] = [
    'name' => 'gender',
    'type' => 'enum',
    'dbType' => 'tinyint(1)',
    'len' => '1',
    'default' => '1',
    'options' => 'contacts_gender_options',
    'vname' => 'LBL_GENDER',
    'isnull' => 'false',
    'required' => true
];

$dictionary['Contact']['fields']['email1']['importable'] = 'false';

$dictionary['Contact']['fields']['email'] = [
    'name' => 'email',
    'type' => 'nitemail',
    'dbType' => 'varchar',
    'vname' => 'LBL_EMAIL',
    'source' => 'non-db',
    'secure' => true,
    'required' => true,
    'importable' => 'true',
    'merge_filter' => 'selected',
];

$dictionary['Contact']['fields']['patronymic'] = [
    'name' => 'patronymic',
    'type' => 'varchar',
    'vname' => 'LBL_PATRONYMIC',
    'source' => 'non-db',
    'required' => true
];

$dictionary['Contact']['fields']['first_name']['required'] = true;

$dictionary['Contact']['fields']['passport_series'] = [
    'name' => 'passport_series',
    'type' => 'int',
    'vname' => 'LBL_PASSPORT_SERIES',
    'source' => 'non-db',
    'secure' => true
];

$dictionary['Contact']['fields']['passport_number'] = [
    'name' => 'passport_number',
    'type' => 'int',
    'vname' => 'LBL_PASSPORT_NUMBER',
    'source' => 'non-db',
    'secure' => true
];

$dictionary['Contact']['fields']['passport_date_of_issue'] = [
    'name' => 'passport_date_of_issue',
    'type' => 'date',
    'vname' => 'LBL_PASSPORT_DATE_OF_ISSUE',
    'source' => 'non-db',
    'secure' => true
];

$dictionary['Contact']['fields']['passport_issued_by'] = [
    'name' => 'passport_issued_by',
    'type' => 'text',
    'vname' => 'LBL_PASSPORT_ISSUED_BY',
    'source' => 'non-db',
    'secure' => true
];

$dictionary['Contact']['fields']['passport_division_code'] = [
    'name' => 'passport_division_code',
    'type' => 'int',
    'vname' => 'LBL_PASSPORT_DIVISION_CODE',
    'source' => 'non-db',
    'secure' => true
];

$dictionary['Contact']['fields']['passport_address'] = [
    'name' => 'passport_address',
    'type' => 'text',
    'vname' => 'LBL_PASSPORT_ADDRESS',
    'source' => 'non-db',
    'secure' => true
];

$dictionary['Contact']['fields']['secure_content'] = [
    'name' => 'secure_content',
    'type' => 'text',
    'vname' => 'LBL_SECURE_CONTENT'
];

$dictionary['Contact']['fields']['first_name_only'] = [
    'name' => 'first_name_only',
    'type' => 'varchar',
    'vname' => 'LBL_FIRST_NAME_ONLY',
    'source' => 'non-db'
];

$dictionary['Contact']['fields']['birth_date'] = [
    'name' => 'birth_date',
    'vname' => 'LBL_BIRTH_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => true,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    'merge_filter' => 'selected',
];

$dictionary['Contact']['fields']['phone_work']['source'] = 'non-db';
$dictionary['Contact']['fields']['phone_mobile']['source'] = 'non-db';
$dictionary['Contact']['fields']['phone_mobile']['required'] = true;

$dictionary['Contact']['fields']['phone_work']['merge_filter'] = 'selected';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter'] = 'selected';

$dictionary['Contact']['fields']['phone_work']['secure'] = true;
$dictionary['Contact']['fields']['phone_mobile']['secure'] = true;

$dictionary['Contact']['fields']['phone_work_small'] = [
    'name' => 'phone_work_small',
    'type' => 'int',
    'len' => '4',
    'vname' => 'LBL_PHONE_WORK_SMALL',
];

$dictionary['Contact']['fields']['phone_mobile_small'] = [
    'name' => 'phone_mobile_small',
    'type' => 'int',
    'len' => '4',
    'vname' => 'LBL_PHONE_MOBILE_SMALL',
];

$dictionary['Contact']['fields']['email_small'] = [
    'name' => 'email_small',
    'type' => 'varchar',
    'vname' => 'LBL_EMAIL_SMALL',
    'importable' => 'false'
];

$dictionary['Contact']['fields']['dispatches'] = array(
    'name' => 'dispatches',
    'type' => 'link',
    'relationship' => 'dispatches_contacts',
    'source' => 'non-db',
    'vname' => 'LBL_DISPATCH',
);

$dictionary['Contact']['fields']['last_email_dispatch_datetime'] = [
    'name' => 'last_email_dispatch_datetime',
    'type' => 'datetimecombo',
    'dbType' => 'datetime',
    'vname' => 'LBL_LAST_EMAIL_DISPATCH_DATETIME',
];

$dictionary['Contact']['fields']['last_sms_dispatch_datetime'] = [
    'name' => 'last_sms_dispatch_datetime',
    'type' => 'datetimecombo',
    'dbType' => 'datetime',
    'vname' => 'LBL_LAST_SMS_DISPATCH_DATETIME',
];

$dictionary['Contact']['fields']['is_email_dispatch_blocked'] = [
    'name' => 'is_email_dispatch_blocked',
    'type' => 'bool',
    'dbType' => 'tinyint(1)',
    'len' => '1',
    'default' => '0',
    'isnull' => 'false',
    'vname' => 'LBL_IS_EMAIL_DISPATCH_BLOCKED',
];

$dictionary['Contact']['fields']['is_sms_dispatch_blocked'] = [
    'name' => 'is_sms_dispatch_blocked',
    'type' => 'bool',
    'dbType' => 'tinyint(1)',
    'len' => '1',
    'default' => '0',
    'isnull' => 'false',
    'vname' => 'LBL_IS_SMS_DISPATCH_BLOCKED',
];

$dictionary['Contact']['fields']['last_agreement_datetime'] = [
    'name' => 'last_agreement_datetime',
    'type' => 'datetimecombo',
    'dbType' => 'datetime',
    'vname' => 'LBL_LAST_AGREEMENT_DATETIME',
];

$dictionary['Contact']['fields']['opportunity_name'] = [
    'name' => 'opportunity_name',
    'rname' => 'name',
    'id_name' => 'opportunity_id',
    'vname' => 'LBL_OPPORTUNITY_NAME',
    'type' => 'relate',
    'table' => 'opportunities',
    'join_name' => 'opportunities',
    'isnull' => 'true',
    'module' => 'Opportunities',
    'dbType' => 'varchar',
    'link' => 'opportunities',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
];

$dictionary['Contact']['fields']['opportunity_id'] = [
    'name' => 'opportunity_id',
    'vname' => 'LBL_OPPORTUNITY_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Contact']['fields']['opportunities'] = [
    'name' => 'opportunities',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Opportunities',
    'bean_name' => 'Opportunity',
    'vname' => 'LBL_CITIES',
    'relationship' => 'contacts_opportunities_one',
];

$dictionary['Contact']['relationships']['contacts_opportunities_one'] = array(
    'lhs_module' => 'Opportunities',
    'lhs_table' => 'opportunities',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'opportunity_id',
    'relationship_type' => 'one-to-many'
);

$dictionary['Contact']['fields']['agreements'] = [
    'name' => 'agreements',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Opportunities',
    'bean_name' => 'Opportunity',
    'vname' => 'LBL_CITIES',
    'relationship' => 'contacts_opportunities',
];

$dictionary['Contact']['fields']['is_imported'] = [
    'name' => 'is_imported',
    'type' => 'bool',
    'default' => '0',
    'isnull' => 'false',
    'vname' => 'LBL_IS_IMPORTED',
];

$dictionary['Contact']['fields']['contact_from'] = [
    'name' => 'contact_from',
    'type' => 'enum',
    'options' => 'contacts_from_options',
    'default' => 'agreement',
    'vname' => 'LBL_CONTACT_FROM',
];

$dictionary['Contact']['indices']['idx_last_first_birthdate'] = [
    'name' => 'idx_last_first_birthdate',
    'type' => 'index',
    'fields' => array('last_name', 'first_name', 'deleted', 'birth_date')
];

$dictionary['Contact']['indices']['idx_phone_mobile_small'] = [
    'name' => 'idx_phone_mobile_small',
    'type' => 'index',
    'fields' => array('phone_mobile_small', 'deleted')
];

$dictionary['Contact']['indices']['idx_phone_work_small'] = [
    'name' => 'idx_phone_work_small',
    'type' => 'index',
    'fields' => array('phone_work_small', 'deleted')
];

$dictionary['Contact']['indices']['idx_email_small'] = [
    'name' => 'idx_email_small',
    'type' => 'index',
    'fields' => array('email_small', 'deleted')
];

