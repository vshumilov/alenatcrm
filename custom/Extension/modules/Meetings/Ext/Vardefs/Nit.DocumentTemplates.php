<?php

$dictionary['Meeting']['fields']['document_template'] = array(
    'name' => 'document_template',
    'vname' => 'LBL_DOCUMENT_TEMPLATE',
    'type' => 'function',
    'custom_type' => 'documenttemplate',
    'source' => 'non-db',
    'audited' => false,
    'required' => false,
    'massupdate' => false,
    'importable' => 'false',
    'function_class' => 'DocumentTemplate',
    'function_name' => 'getDocumentTemplateMetadata',
    'function_params_source' => 'this',
    'function_params' => array(
        'module_dir'
    ),
);

$dictionary['Meeting']['fields']['nitfiles'] = array(
    'name' => 'nitfiles',
    'vname' => 'LBL_NITFILES',
    'type' => 'nitfile',
    'source' => 'non-db',
    'audited' => false,
    'required' => false,
    'importable' => 'true',
    'massupdate' => false,
);