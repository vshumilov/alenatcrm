<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Meeting']['fields']['date_start']['required'] = false;
$dictionary['Meeting']['fields']['date_end']['required'] = false;

$dictionary['Meeting']['fields']['parent_type']['options'] = 'meeting_parent_type_display';
$dictionary['Meeting']['fields']['parent_name']['options'] = 'meeting_parent_type_display';

require_once 'custom/modules/Meetings/BeanHelper/Meeting.php';
$dictionary['Meeting']['fields']['status']['function'] = 'MeetingBeanHelperMeeting::getStatusList';

$dictionary['Meeting']['fields']['incoming_number'] = [
    'name' => 'incoming_number',
    'vname' => 'LBL_INCOMING_NUMBER',
    'type' => 'varchar',
    'required' => false,
    'reportable' => false,
    'audited' => true,
    'comment' => 'incoming number',
];

$dictionary['Meeting']['fields']['system_number'] = [
    'name' => 'system_number',
    'vname' => 'LBL_SYSTEM_NUMBER',
    'type' => 'int',
    'default' => '0',
    'required' => false,
    'reportable' => false,
    'audited' => true,
    'comment' => 'system number',
];

$dictionary['Meeting']['fields']['documents'] = [
    'name' => 'documents',
    'type' => 'link',
    'relationship' => 'documents_meetings',
    'source' => 'non-db',
    'vname' => 'LBL_DOCUMENTS_SUBPANEL_TITLE',
];

$dictionary['Meeting']['fields']['date_notifing'] = [
    'name' => 'date_notifing',
    'vname' => 'LBL_DATE_NOTIFING',
    'type' => 'date',
    'audited' => true,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
];

$dictionary['Meeting']['fields']['notifing_mail_scan'] = [
    'name' => 'notifing_mail_scan',
    'vname' => 'LBL_NOTIFING_MAIL_SCAN',
    'type' => 'nitfile',
    'source' => 'non-db',
    'audited' => false,
    'required' => false,
    'importable' => 'true',
    'massupdate' => false,
];
$dictionary['Meeting']['fields']['date_abonent_registration'] = [
    'name' => 'date_abonent_registration',
    'vname' => 'LBL_DATE_ABONENT_REGISTRATION',
    'type' => 'date',
    'audited' => true,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
];
$dictionary['Meeting']['fields']['notifing_mail_user_id'] = [
    'name' => 'notifing_mail_user_id',
    'rname' => 'name',
    'id_name' => 'notifing_mail_user_id',
    'vname' => 'LBL_NOTIFING_MAIL_TO_ID',
    'group' => 'notifing_mail_user_name',
    'type' => 'relate',
    'table' => 'users',
    'module' => 'Users',
    'reportable' => true,
    'isnull' => 'false',
    'dbType' => 'id',
    'audited' => true,
    'comment' => 'User ID notifing_mail to record',
    'duplicate_merge' => 'disabled'
];
$dictionary['Meeting']['fields']['notifing_mail_user_name'] = [
    'name' => 'notifing_mail_user_name',
    'link' => 'notifing_mail_user_link',
    'vname' => 'LBL_NOTIFING_MAIL_TO_NAME',
    'rname' => 'name',
    'type' => 'relate',
    'reportable' => false,
    'source' => 'non-db',
    'table' => 'users',
    'id_name' => 'notifing_mail_user_id',
    'module' => 'Users',
    'duplicate_merge' => 'disabled'
];
$dictionary['Meeting']['fields']['notifing_mail_user_link'] = [
    'name' => 'notifing_mail_user_link',
    'type' => 'link',
    'relationship' => 'opportunities_notifing_mail_user',
    'vname' => 'LBL_NOTIFING_MAIL_TO_USER',
    'link_type' => 'one',
    'module' => 'Users',
    'bean_name' => 'User',
    'source' => 'non-db',
    'duplicate_merge' => 'enabled',
    'rname' => 'name',
    'id_name' => 'notifing_mail_user_id',
    'table' => 'users',
];

$dictionary['Meeting']['fields']['emails'] = [
    'name' => 'emails',
    'type' => 'link',
    'relationship' => 'emails_meetings_rel', /* reldef in emails */
    'source' => 'non-db',
    'vname' => 'LBL_EMAILS',
];
$dictionary['Meeting']['relationships']['emails_meetings_rel'] = [
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Emails',
    'rhs_table' => 'emails',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Meetings',
];

$dictionary['Meeting']['fields']['account_name'] = [
    'name' => 'account_name',
    'rname' => 'name',
    'id_name' => 'account_id',
    'vname' => 'LBL_ACCOUNT_NAME',
    'type' => 'relate',
    'table' => 'accounts',
    'join_name' => 'accounts',
    'isnull' => 'true',
    'module' => 'Accounts',
    'dbType' => 'varchar',
    'link' => 'accounts',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];
$dictionary['Meeting']['fields']['account_id'] = [
    'name' => 'account_id',
    'vname' => 'LBL_ACCOUNT_ID',
    'type' => 'id',
    'audited' => true,
];
$dictionary['Meeting']['fields']['accounts'] = [
    'name' => 'accounts',
    'type' => 'link',
    'relationship' => 'accounts_meetings',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Accounts',
    'bean_name' => 'Account',
    'vname' => 'LBL_ACCOUNTS',
];

$dictionary['Meeting']['relationships']['accounts_meetings'] = [
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'account_id',
    'relationship_type' => 'one-to-many',
];

$dictionary['Meeting']['fields']['city'] = [
    'name' => 'city',
    'rname' => 'name',
    'id_name' => 'city_id',
    'vname' => 'LBL_CITY',
    'type' => 'relate',
    'table' => 'cities',
    'join_name' => 'cities',
    'isnull' => 'true',
    'module' => 'Cities',
    'dbType' => 'varchar',
    'link' => 'cities',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['Meeting']['fields']['city_id'] = [
    'name' => 'city_id',
    'vname' => 'LBL_CITY_ID',
    'type' => 'id',
    'audited' => true,
    'source' => 'non-db',
];

$dictionary['Meeting']['fields']['cities'] = [
    'name' => 'cities',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Cities',
    'bean_name' => 'City',
    'vname' => 'LBL_CITIES',
];

