<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Account']['fields']['bean_type'] = [
    'name' => 'bean_type',
    'vname' => 'LBL_BEAN_TYPE',
    'type' => 'enum',
    'options' => 'account_type_dom',
    'default' => 'ИП',
    'required' => false,
    'reportable' => false,
    'audited' => true,
];

$dictionary['Account']['fields']['full_name'] = [
    'name' => 'full_name',
    'vname' => 'LBL_FULL_NAME',
    'type' => 'varchar',
    'required' => false,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Full name',
];

$dictionary['Account']['fields']['inn'] = [
    'name' => 'inn',
    'vname' => 'LBL_INN',
    'type' => 'basemax',
    'dbType' => 'varchar',
    'len' => '12',
    'required' => true,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Inn',
    'is_equal_mode' => true
];

$dictionary['Account']['fields']['kpp'] = [
    'name' => 'kpp',
    'vname' => 'LBL_KPP',
    'type' => 'basemax',
    'dbType' => 'varchar',
    'len' => '9',
    'required' => true,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Kpp',
    'is_equal_mode' => true
];

$dictionary['Account']['fields']['ogrn'] = [
    'name' => 'ogrn',
    'vname' => 'LBL_OGRN',
    'type' => 'basemax',
    'dbType' => 'varchar',
    'len' => '15',
    'required' => true,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Ogrn',
    'is_equal_mode' => true
];

$dictionary['Account']['fields']['date_ogrn'] = [
    'name' => 'date_ogrn',
    'vname' => 'LBL_DATE_OGRN',
    'type' => 'date',
    'audited' => true,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
];

$dictionary['Account']['fields']['email1']['function'] = 'getCustomEmailAddressWidget';

$dictionary['Account']['fields']['bank_name'] = [
    'name' => 'bank_name',
    'vname' => 'LBL_BANK_NAME',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['Account']['fields']['payment_account'] = [
    'name' => 'payment_account',
    'vname' => 'LBL_PAYMENT_ACCOUNT',
    'type' => 'integer',
    'dbType' => 'varchar',
    'len' => 20,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
    'is_equal_mode' => true
];

$dictionary['Account']['fields']['correspondent_account'] = [
    'name' => 'correspondent_account',
    'vname' => 'LBL_CORRESPONDENT_ACCOUNT',
    'type' => 'integer',
    'dbType' => 'varchar',
    'len' => 20,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
    'is_equal_mode' => true
];

$dictionary['Account']['fields']['bic'] = [
    'name' => 'bic',
    'vname' => 'LBL_BIC',
    'type' => 'integer',
    'dbType' => 'varchar',
    'len' => 9,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
    'is_equal_mode' => true
];

$dictionary['Account']['fields']['genitive_fio'] = [
    'name' => 'genitive_fio',
    'vname' => 'LBL_GENITIVE_FIO',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['Account']['fields']['genitive_position'] = [
    'name' => 'genitive_position',
    'vname' => 'LBL_GENITIVE_POSITION',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['Account']['fields']['nominative_fio'] = [
    'name' => 'nominative_fio',
    'vname' => 'LBL_NOMINATIVE_FIO',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['Account']['fields']['nominative_position'] = [
    'name' => 'nominative_position',
    'vname' => 'LBL_NOMINATIVE_POSITION',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['Account']['fields']['base_acts_signatory'] = [
    'name' => 'base_acts_signatory',
    'vname' => 'LBL_BASE_ACTS_SIGNATORY',
    'type' => 'text',
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['Account']['fields']['receivable_debt'] = [
    'name' => 'receivable_debt',
    'vname' => 'LBL_RECEIVABLE_DEBT',
    'type' => 'currency',
    'dbType' => 'double',
    'importable' => 'required',
    'duplicate_merge' => '1',
    'required' => true,
    'options' => 'numeric_range_search_dom',
    'enable_range_search' => true,
    'symbol_space' => true,
];

$dictionary['Account']['fields']['receivables'] = [
    'name' => 'receivables',
    'type' => 'link',
    'relationship' => 'accounts_receivables',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Receivables',
    'bean_name' => 'Receivable',
    'vname' => 'LBL_RECEIVABLES',
];

$dictionary['Account']['fields']['city'] = [
    'name' => 'city',
    'rname' => 'name',
    'id_name' => 'city_id',
    'vname' => 'LBL_CITY',
    'type' => 'relate',
    'table' => 'cities',
    'join_name' => 'cities',
    'isnull' => 'true',
    'module' => 'Cities',
    'dbType' => 'varchar',
    'link' => 'cities',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['Account']['fields']['city_id'] = [
    'name' => 'city_id',
    'vname' => 'LBL_CITY_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Account']['fields']['cities'] = [
    'name' => 'cities',
    'type' => 'link',
    'relationship' => 'cities_accounts',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Cities',
    'bean_name' => 'City',
    'vname' => 'LBL_CITIES',
];

$dictionary['Account']['fields']['acceptmeteringdevices'] = [
    'name' => 'acceptmeteringdevices',
    'type' => 'link',
    'relationship' => 'accounts_acceptmeteringdevices',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Acceptmeteringdevices',
    'bean_name' => 'Acceptmeteringdevice',
    'vname' => 'LBL_ACCEPTMETERINGDEVICES',
];

$dictionary['Account']['fields']['checkwatersupplysystems'] = [
    'name' => 'checkwatersupplysystems',
    'type' => 'link',
    'relationship' => 'accounts_checkwatersupplysystems',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Checkwatersupplysystems',
    'bean_name' => 'Checkwatersupplysystem',
    'vname' => 'LBL_CHECKWATERSUPPLYSYSTEMS',
];

$dictionary['Account']['fields']['infometeringdevices'] = [
    'name' => 'infometeringdevices',
    'type' => 'link',
    'relationship' => 'accounts_infometeringdevices',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Infometeringdevices',
    'bean_name' => 'Infometeringdevice',
    'vname' => 'LBL_INFOMETERINGDEVICES',
];

$dictionary['Account']['fields']['city'] = [
    'name' => 'city',
    'rname' => 'name',
    'id_name' => 'city_id',
    'vname' => 'LBL_CITY',
    'type' => 'relate',
    'table' => 'cities',
    'join_name' => 'cities',
    'isnull' => 'true',
    'module' => 'Cities',
    'dbType' => 'varchar',
    'link' => 'cities',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['Account']['fields']['city_id'] = [
    'name' => 'city_id',
    'vname' => 'LBL_CITY_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Account']['fields']['cities'] = [
    'name' => 'cities',
    'type' => 'link',
    'relationship' => 'cities_accounts',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Cities',
    'bean_name' => 'City',
    'vname' => 'LBL_CITIES',
];

$dictionary['Account']['fields']['last_agreement_status'] = [
    'name' => 'last_agreement_status',
    'rname' => 'name',
    'id_name' => 'last_agreement_id',
    'vname' => 'LBL_LAST_AGREEMENT_STATUS',
    'type' => 'relate',
    'table' => 'opportunities',
    'join_name' => 'last_agreement',
    'isnull' => 'true',
    'module' => 'Opportunities',
    'dbType' => 'varchar',
    'link' => 'last_agreement_status_link',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
];

$dictionary['Account']['fields']['last_agreement_id'] = [
    'name' => 'last_agreement_id',
    'vname' => 'LBL_LAST_AGREEMENT_ID',
    'type' => 'id',
    'audited' => true,
    'source' => 'non-db',
];

$dictionary['Account']['fields']['last_agreement_status_link'] = [
    'name' => 'last_agreement_status_link',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Opportunities',
    'bean_name' => 'Opportunity',
    'vname' => 'LBL_LAST_AGREEMENT_STATUS_LINK',
];

$dictionary['Account']['fields']['last_acceptmeteringdevice_status'] = [
    'name' => 'last_acceptmeteringdevice_status',
    'rname' => 'name',
    'id_name' => 'last_acceptmeteringdevice_id',
    'vname' => 'LBL_LAST_ACCEPTMETERINGDEVICE_STATUS',
    'type' => 'relate',
    'table' => 'acceptmeteringdevices',
    'join_name' => 'last_acceptmeteringdevice',
    'isnull' => 'true',
    'module' => 'Acceptmeteringdevices',
    'dbType' => 'varchar',
    'link' => 'last_acceptmeteringdevice_status_link',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
];

$dictionary['Account']['fields']['last_acceptmeteringdevice_id'] = [
    'name' => 'last_acceptmeteringdevice_id',
    'vname' => 'LBL_LAST_ACCEPTMETERINGDEVICE_ID',
    'type' => 'id',
    'audited' => true,
    'source' => 'non-db',
];

$dictionary['Account']['fields']['last_acceptmeteringdevice_status_link'] = [
    'name' => 'last_acceptmeteringdevice_status_link',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Acceptmeteringdevices',
    'bean_name' => 'Acceptmeteringdevice',
    'vname' => 'LBL_LAST_ACCEPTMETERINGDEVICE_STATUS_LINK',
];

$dictionary['Account']['fields']['meetings_link'] = [
    'name' => 'meetings_link',
    'type' => 'link',
    'relationship' => 'accounts_meetings',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Meetings',
    'bean_name' => 'Meeting',
    'vname' => 'LBL_MEETINGS',
];