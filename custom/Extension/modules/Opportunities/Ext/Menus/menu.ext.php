<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;

$module_menu = [];

if (ACLController::checkAccess('Opportunities', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Opportunities&action=EditView&return_module=Opportunities&return_action=DetailView", $mod_strings['LNK_NEW_OPPORTUNITY'], "Create");
}
if (ACLController::checkAccess('Opportunities', 'list', true)) {
    $module_menu[] = Array("index.php?module=Opportunities&action=index&return_module=Opportunities&return_action=DetailView", $mod_strings['LNK_OPPORTUNITY_LIST_SHORT'], "List");
} 