<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Opportunity']['fields']['not_validate'] = [
    'name' => 'not_validate',
    'type' => 'bool',
    'default' => '0',
    'vname' => 'LBL_NOT_VALIDATE',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['system_number'] = [
    'name' => 'system_number',
    'vname' => 'LBL_SYSTEM_NUMBER',
    'type' => 'int',
    'required' => false,
    'reportable' => false,
    'audited' => true,
    'comment' => 'system number'
];

$dictionary['Opportunity']['fields']['tour_name'] = [
    'name' => 'tour_name',
    'rname' => 'name',
    'id_name' => 'tour_id',
    'vname' => 'LBL_TOUR_NAME',
    'type' => 'relate',
    'table' => 'tours',
    'join_name' => 'tours',
    'isnull' => 'true',
    'module' => 'Tours',
    'dbType' => 'varchar',
    'link' => 'tours',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['Opportunity']['fields']['tour_id'] = [
    'name' => 'tour_id',
    'vname' => 'LBL_TOUR_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Opportunity']['fields']['tours'] = [
    'name' => 'tours',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Tours',
    'bean_name' => 'Tour',
    'vname' => 'LBL_CITIES',
    'relationship' => 'tours_opportunities',
];

$dictionary['Opportunity']['relationships']['tours_opportunities'] = array(
    'lhs_module' => 'Tours',
    'lhs_table' => 'tours',
    'lhs_key' => 'id',
    'rhs_module' => 'Opportunities',
    'rhs_table' => 'opportunities',
    'rhs_key' => 'tour_id',
    'relationship_type' => 'one-to-many'
);

$dictionary['Opportunity']['fields']['contact_name'] = [
    'name' => 'contact_name',
    'rname' => 'name',
    'id_name' => 'contact_id',
    'vname' => 'LBL_CONTACT',
    'type' => 'relate',
    'table' => 'contacts',
    'join_name' => 'contacts',
    'isnull' => 'true',
    'module' => 'Contacts',
    'dbType' => 'varchar',
    'link' => 'contacts',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['Opportunity']['fields']['contact_id'] = [
    'name' => 'contact_id',
    'vname' => 'LBL_CONTACT_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Opportunity']['fields']['contacts'] = [
    'name' => 'contacts',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Contacts',
    'bean_name' => 'Contact',
    'vname' => 'LBL_CITIES',
    'relationship' => 'contacts_opportunities',
];

$dictionary['Opportunity']['relationships']['contacts_opportunities'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Opportunities',
    'rhs_table' => 'opportunities',
    'rhs_key' => 'contact_id',
    'relationship_type' => 'one-to-many'
);

$dictionary['Opportunity']['fields']['contacts_gender'] = [
    'name' => 'contacts_gender',
    'type' => 'enum',
    'dbType' => 'tinyint(1)',
    'len' => '1',
    'default' => '1',
    'options' => 'contacts_gender_options',
    'vname' => 'LBL_CONTACTS_GENDER',
    'isnull' => 'false',
    'source' => 'non-db',
    'required' => true
];

$dictionary['Opportunity']['fields']['contacts_phone_work'] = [
    'name' => 'contacts_phone_work',
    'type' => 'varchar',
    'source' => 'non-db',
    'vname' => 'LBL_CONTACTS_PHONE_WORK',
];

$dictionary['Opportunity']['fields']['contacts_phone_mobile'] = [
    'name' => 'contacts_phone_mobile',
    'type' => 'varchar',
    'source' => 'non-db',
    'vname' => 'LBL_CONTACTS_PHONE_MOBILE',
];

$dictionary['Opportunity']['fields']['contacts_email'] = [
    'name' => 'contacts_email',
    'type' => 'nitemail',
    'dbType' => 'varchar',
    'source' => 'non-db',
    'vname' => 'LBL_CONTACTS_EMAIL',
];

$dictionary['Opportunity']['fields']['contacts_passport_series'] = [
    'name' => 'contacts_passport_series',
    'type' => 'int',
    'vname' => 'LBL_CONTACTS_PASSPORT_SERIES',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['contacts_passport_number'] = [
    'name' => 'contacts_passport_number',
    'type' => 'int',
    'vname' => 'LBL_CONTACTS_PASSPORT_NUMBER',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['contacts_passport_date_of_issue'] = [
    'name' => 'contacts_passport_date_of_issue',
    'type' => 'date',
    'vname' => 'LBL_CONTACTS_PASSPORT_DATE_OF_ISSUE',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['contacts_passport_issued_by'] = [
    'name' => 'contacts_passport_issued_by',
    'type' => 'text',
    'vname' => 'LBL_CONTACTS_PASSPORT_ISSUED_BY',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['contacts_passport_division_code'] = [
    'name' => 'contacts_passport_division_code',
    'type' => 'int',
    'vname' => 'LBL_CONTACTS_PASSPORT_DIVISION_CODE',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['contacts_passport_address'] = [
    'name' => 'contacts_passport_address',
    'type' => 'text',
    'vname' => 'LBL_CONTACTS_PASSPORT_ADDRESS',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['contacts_birth_date'] = [
    'name' => 'contacts_birth_date',
    'vname' => 'LBL_CONTACTS_BIRTH_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    'source' => 'non-db'
];

$dictionary['Opportunity']['fields']['tours_country_name'] = array(
    'name' => 'tours_country_name',
    'rname' => 'name',
    'id_name' => 'tours_country_id',
    'vname' => 'LBL_COUNTRY_NAME',
    'type' => 'relate',
    'table' => 'countries',
    'join_name' => 'countries',
    'isnull' => 'true',
    'module' => 'Countries',
    'dbType' => 'varchar',
    'link' => 'countries',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
);

$dictionary['Opportunity']['fields']['tours_country_id'] = array(
    'name' => 'tours_country_id',
    'vname' => 'LBL_COUNTRY_ID',
    'type' => 'id',
    'source' => 'non-db',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_operator_name'] = array(
    'name' => 'tours_operator_name',
    'rname' => 'name',
    'id_name' => 'tours_operator_id',
    'vname' => 'LBL_OPERATOR_NAME',
    'type' => 'relate',
    'table' => 'operators',
    'join_name' => 'operators',
    'isnull' => 'true',
    'module' => 'Operators',
    'dbType' => 'varchar',
    'link' => 'operators',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'importable' => 'required',
);

$dictionary['Opportunity']['fields']['tours_operator_id'] = array(
    'name' => 'tours_operator_id',
    'vname' => 'LBL_OPERATOR_ID',
    'type' => 'id',
    'source' => 'non-db',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_city_name'] = array(
    'name' => 'tours_city_name',
    'rname' => 'name',
    'id_name' => 'tours_city_id',
    'vname' => 'LBL_CITY_NAME',
    'type' => 'relate',
    'table' => 'cities',
    'join_name' => 'cities',
    'isnull' => 'true',
    'module' => 'Cities',
    'dbType' => 'varchar',
    'link' => 'cities',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
);

$dictionary['Opportunity']['fields']['tours_city_id'] = array(
    'name' => 'tours_city_id',
    'vname' => 'LBL_CITY_ID',
    'type' => 'id',
    'source' => 'non-db',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_route'] = array(
    'name' => 'tours_route',
    'vname' => 'LBL_ROUTE',
    'type' => 'varchar',
    'source' => 'non-db',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_hotel'] = array(
    'name' => 'tours_hotel',
    'vname' => 'LBL_HOTEL',
    'type' => 'varchar',
    'source' => 'non-db',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_foodoption_name'] = [
    'name' => 'tours_foodoption_name',
    'rname' => 'name',
    'id_name' => 'tours_foodoption_id',
    'vname' => 'LBL_TOURS_FOODOPTION_NAME',
    'type' => 'relate',
    'table' => 'foodoptions',
    'join_name' => 'foodoptions',
    'isnull' => 'true',
    'module' => 'Foodoptions',
    'dbType' => 'varchar',
    'link' => 'foodoptions',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
];

$dictionary['Opportunity']['fields']['tours_foodoption_id'] = [
    'name' => 'tours_foodoption_id',
    'vname' => 'LBL_TOURS_FOODOPTION_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Opportunity']['fields']['tours_foodoptions'] = [
    'name' => 'tours_foodoptions',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Foodoptions',
    'bean_name' => 'Foodoption',
    'vname' => 'LBL_TOURS_FOODOPTIONS',
    'relationship' => 'foodoptions_opportunities',
];

$dictionary['Opportunity']['fields']['tours_apartment_name'] = [
    'name' => 'tours_apartment_name',
    'rname' => 'name',
    'id_name' => 'tours_apartment_id',
    'vname' => 'LBL_TOURS_APARTMENT_NAME',
    'type' => 'relate',
    'table' => 'apartments',
    'join_name' => 'apartments',
    'isnull' => 'true',
    'module' => 'Apartments',
    'dbType' => 'varchar',
    'link' => 'apartments',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
];

$dictionary['Opportunity']['fields']['tours_apartment_id'] = [
    'name' => 'tours_apartment_id',
    'vname' => 'LBL_TOURS_APARTMENT_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Opportunity']['fields']['tours_apartments'] = [
    'name' => 'tours_apartments',
    'type' => 'link',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Apartments',
    'bean_name' => 'Apartment',
    'vname' => 'LBL_TOURS_APARTMENTS',
    'relationship' => 'apartments_opportunities',
];

$dictionary['Opportunity']['fields']['tours_days'] = array(
    'name' => 'tours_days',
    'vname' => 'LBL_DAYS',
    'type' => 'int',
    'dbType' => 'int',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_transfer'] = array(
    'name' => 'tours_transfer',
    'vname' => 'LBL_TRANSFER',
    'type' => 'bool',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_excursion'] = array(
    'name' => 'tours_excursion',
    'vname' => 'LBL_EXCURSION',
    'type' => 'bool',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_meeting'] = array(
    'name' => 'tours_meeting',
    'vname' => 'LBL_MEETING',
    'type' => 'bool',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_visa_support'] = array(
    'name' => 'tours_visa_support',
    'vname' => 'LBL_VISA_SUPPORT',
    'type' => 'bool',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_medical_insurance'] = array(
    'name' => 'tours_medical_insurance',
    'vname' => 'LBL_MEDICAL_INSURANCE',
    'type' => 'bool',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_insurance_on_his_own_recognizance'] = array(
    'name' => 'tours_insurance_on_his_own_recognizance',
    'vname' => 'LBL_INSURANCE_ON_HIS_OWN_RECOGNIZANCE',
    'type' => 'bool',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['tours_additional_offers'] = array(
    'name' => 'tours_additional_offers',
    'vname' => 'LBL_ADDITIONAL_OFFERS',
    'type' => 'text',
    'audited' => true,
);

$dictionary['Opportunity']['fields']['files'] = [
    'name' => 'files',
    'vname' => 'LBL_FILES',
    'type' => 'nitfile',
    'source' => 'non-db',
    'audited' => false,
    'required' => false,
    'importable' => 'true',
    'massupdate' => false,
];

$dictionary['Opportunity']['fields']['tourists'] = [
    'name' => 'tourists',
    'vname' => 'LBL_TOURISTS',
    'type' => 'text',
    'comment' => 'Например: Болотову Н.В., Гуровских Ю.Л.',
    'audited' => false,
    'required' => true,
    'importable' => 'true',
    'massupdate' => false,
];

$dictionary['Opportunity']['fields']['start_date_tour'] = [
    'name' => 'start_date_tour',
    'vname' => 'LBL_START_DATE_TOUR',
    'type' => 'date',
    'audited' => true,
    'required' => true,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
];

$dictionary['Opportunity']['fields']['end_date_tour'] = [
    'name' => 'end_date_tour',
    'vname' => 'LBL_END_DATE_TOUR',
    'type' => 'date',
    'audited' => true,
    'required' => true,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
];

$dictionary['Opportunity']['fields']['prepayment'] = [
    'name' => 'prepayment',
    'vname' => 'LBL_PREPAYMENT',
    'type' => 'currency',
    'dbType' => 'double',
    'importable' => 'required',
    'duplicate_merge' => '1',
    'required' => true,
    'options' => 'numeric_range_search_dom',
    'enable_range_search' => true,
];

$dictionary['Opportunity']['fields']['amount_with_discount'] = [
    'name' => 'amount_with_discount',
    'vname' => 'LBL_AMOUNT_WITH_DISCOUNT',
    'type' => 'currency',
    'dbType' => 'double',
    'importable' => 'required',
    'duplicate_merge' => '1',
    'options' => 'numeric_range_search_dom',
    'enable_range_search' => true,
];

$dictionary['Opportunity']['fields']['client_from_source'] = [
    'name' => 'client_from_source',
    'vname' => 'LBL_CLIENT_FROM_SOURCE',
    'type' => 'enum',
    'options' => 'opportunities_client_from_source_options',
];

$dictionary['Opportunity']['fields']['count_places'] = [
    'name' => 'count_places',
    'vname' => 'LBL_COUNT_PLACES',
    'type' => 'int',
    'default' => '0',
    'isnull' => 'false',
];

$dictionary['Opportunity']['fields']['count_places_with_treatment'] = [
    'name' => 'count_places_with_treatment',
    'vname' => 'LBL_COUNT_PLACES_WITH_TREATMENT',
    'type' => 'int',
    'default' => '0',
    'isnull' => 'false',
];

$dictionary['Opportunity']['fields']['href_bean'] = [
    'name' => 'href_bean',
    'vname' => 'LBL_HREF_BEAN',
    'type' => 'text',
    'source' => 'non-db',
];

$dictionary['Opportunity']['fields']['opportunity_type']['default'] = 'application';

$dictionary['Opportunity']['fields']['amount']['audited'] = true;

$dictionary['Opportunity']['fields']['booked_datetime'] = [
    'name' => 'booked_datetime',
    'vname' => 'LBL_BOOKED_DATETIME',
    'type' => 'datetime',
];

$dictionary['Opportunity']['fields']['agreement_sent_datetime'] = [
    'name' => 'agreement_sent_datetime',
    'vname' => 'LBL_AGREEMENT_SENT_DATETIME',
    'type' => 'datetime',
    'options' => 'date_range_search_dom',
];

$dictionary['Opportunity']['fields']['prepayment_datetime'] = [
    'name' => 'prepayment_datetime',
    'vname' => 'LBL_PREPAYMENT_DATETIME',
    'type' => 'datetime',
    'options' => 'date_range_search_dom',
];

$dictionary['Opportunity']['fields']['paid_datetime'] = [
    'name' => 'paid_datetime',
    'vname' => 'LBL_PAID_DATETIME',
    'type' => 'datetime',
    'options' => 'date_range_search_dom',
];

$dictionary['Opportunity']['fields']['closed_datetime'] = [
    'name' => 'closed_datetime',
    'vname' => 'LBL_CLOSED_DATETIME',
    'type' => 'datetime',
    'options' => 'date_range_search_dom',
];

$dictionary['Opportunity']['fields']['canceled_datetime'] = [
    'name' => 'canceled_datetime',
    'vname' => 'LBL_CANCELED_DATETIME',
    'type' => 'datetime',
    'options' => 'date_range_search_dom',
];

$dictionary['Opportunity']['fields']['nitfiles'] = [
    'name' => 'nitfiles',
    'vname' => 'LBL_NITFILES',
    'type' => 'nitfile',
    'audited' => true,
    'source' => 'non-db',
];

$dictionary['Opportunity']['fields']['agreement_pdf'] = [
    'name' => 'agreement_pdf',
    'vname' => 'LBL_AGREEMENT_PDF',
    'type' => 'nitfile',
    'audited' => true,
    'source' => 'non-db',
];

$dictionary['Opportunity']['fields']['href_agreement_confirm'] = [
    'name' => 'href_agreement_confirm',
    'vname' => 'LBL_HREF_AGREEMENT_CONFIRM',
    'type' => 'nitfile',
    'audited' => true,
    'source' => 'non-db',
];

$dictionary['Opportunity']['fields']['price_name'] = [
    'name' => 'price_name',
    'rname' => 'name',
    'id_name' => 'price_id',
    'vname' => 'LBL_PRICE_NAME',
    'type' => 'relate',
    'table' => 'prices',
    'join_name' => 'prices',
    'isnull' => 'true',
    'module' => 'Prices',
    'dbType' => 'varchar',
    'link' => 'prices',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['Opportunity']['fields']['price_id'] = [
    'name' => 'price_id',
    'vname' => 'LBL_PRICE_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['Opportunity']['fields']['prices'] = [
    'name' => 'prices',
    'type' => 'link',
    'relationship' => 'prices_sanatoriums',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Prices',
    'bean_name' => 'Price',
    'vname' => 'LBL_PRICES',
];

unset($dictionary['Opportunity']['fields']['uploadfile']['source']);
require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';
$dictionary['Opportunity']['fields']['opportunity_type']['function'] = 'OpportunityBeanHelperOpportunity::getTypeList';
