<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$job_strings[] = 'sendDispatches';

/**
 * send dispatches to contacts
 */
if (!function_exists('sendDispatches')) {
    require_once 'custom/modules/Dispatches/BeanHelper/Dispatch.php';

    function sendDispatches()
    {
        $dispatchBeanHelper = new DispatchBeanHelperDispatch(null);
        $dispatchBeanHelper->sendAllDispatches();

        return true;
    }

}

