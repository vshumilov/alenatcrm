<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$job_strings[] = 'updatePlacementFood';

/**
 * update placement
 */
if (!function_exists('updatePlacementFood')) {
    require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';
    require_once 'custom/modules/Tours/BeanHelper/Tour.php';
    require_once 'custom/application/Ext/Utils/custom_utils.ext.php';

    function updatePlacementFood()
    {
        $db = getDatabaseInstance();
        $foods = [];
        $placements = [];
        
        $opportunityBeanHelper = new OpportunityBeanHelperOpportunity(null);
        $dbOpportunitiesResult = $db->query($opportunityBeanHelper->getOpportunitiesQuery());
        
        $tourBeanHelper = new TourBeanHelperTour(null);
        $dbToursResult = $db->query($tourBeanHelper->getToursQuery());

        updatePlacementFoodBean($dbOpportunitiesResult, 'Opportunities', $foods, $placements);
        updatePlacementFoodBean($dbToursResult, 'Tours', $foods, $placements);

        return true;
    }
    
    function updatePlacementFoodBean($result, $module, &$foods, &$placements)
    {
        if (empty($result)) {
            return;
        }
        
        $bean = null;
        $foodBean = null;
        $placementBean = null;
        $foodOptions = translate("tours_food_options");
        $placementOptions = translate("tours_placement_options");
        
        $db = getDatabaseInstance();
        $alias = "";
        
        if ($module == 'Opportunities') {
            $alias = 'tours_';
        }
        
        $placementIdFieldName = $alias . 'placement_id';
        $foodIdFieldName = $alias . 'food_id';
        
        $placementFieldName = $alias . 'placement';
        $foodFieldName = $alias . 'food';
        
        while (($row = $db->fetchByAssoc($result)) != null) {
            $bean = null;
            $bean = BeanFactory::getBean($module, $row['id']);

            if (!empty($bean->{$foodFieldName})) {
                if (!in_array($bean->{$foodFieldName}, array_keys($foods))) {
                    $foodBean = BeanFactory::getBean('Foods');
                    $foodBean->name = $foodOptions[$bean->{$foodFieldName}];
                    $foodBean->save();
                    $foods[$bean->{$foodFieldName}] = $foodBean->id;
                }

                $bean->{$foodIdFieldName} = $foods[$bean->{$foodFieldName}];
            }

            if (!empty($bean->{$placementFieldName})) {
                if (!in_array($bean->{$placementFieldName}, array_keys($placements))) {
                    $placementBean = BeanFactory::getBean('Placements');
                    $placementBean->name = $placementOptions[$bean->{$placementFieldName}];
                    $placementBean->save();
                    $placements[$bean->{$placementFieldName}] = $placementBean->id;
                }

                $bean->{$placementIdFieldName} = $placements[$bean->{$placementFieldName}];
            }

            $bean->save();
        }
    }

}

