<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$job_strings[] = 'setSanatoriumsAttrs';

function setSanatoriumsAttrs()
{
    try {
        require_once 'custom/modules/Sanatoriums/BeanHelper/Sanatorium.php';

        $sanatoriumBeanHelper = new SanatoriumBeanHelperSanatorium(null);

        $sanatoriumBeanHelper->setAttributesKeys();
    } catch (Exception $exc) {
        $GLOBALS['log']->fatal("Start set sanatoriums attrs error: " . $exc->getTraceAsString());
    } finally {
        return true;
    }
}
