<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$job_strings[] = 'parseSites';

function parseSites()
{
    require_once 'vendor/autoload.php';
    require_once('vendor/facebook/webdriver/lib/__init__.php');
    require_once 'custom/include/Nit/ParseSites/CurortExpertParse.php';

    $curortExpertParse = new CurortExpertParse();
    $curortExpertParse->start();
}
