<?php

$layout_defs['Documnets']['subpanel_setup']['meetings'] = [
    'order' => 40,
    'module' => 'Meetings',
    'subpanel_name' => 'default',
    'sort_order' => 'asc',
    'sort_by' => 'id',
    'title_key' => 'LBL_MEETINGS_SUBPANEL_TITLE',
    'get_subpanel_data' => 'meetings',
    'top_buttons' =>
    array(
        0 =>
        array(
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),
        1 =>
        array(
            'widget_class' => 'SubPanelTopSelectButton',
            'mode' => 'MultiSelect',
        ),
    ),
];

