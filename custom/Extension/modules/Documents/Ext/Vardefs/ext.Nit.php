<?php

$dictionary['Document']['fields']['meetings'] = [
    'name' => 'meetings',
    'type' => 'link',
    'relationship' => 'documents_meetings',
    'source' => 'non-db',
    'vname' => 'LBL_MEETINGS_SUBPANEL_TITLE',
];

$dictionary['Document']['fields']['acceptmeteringdevices'] = [
    'name' => 'acceptmeteringdevices',
    'type' => 'link',
    'relationship' => 'documents_acceptmeteringdevices',
    'source' => 'non-db',
    'vname' => 'LBL_ACCEPTMETERINGDEVICES_SUBPANEL_TITLE',
];

$dictionary['Document']['fields']['checkwatersupplysystems'] = [
    'name' => 'checkwatersupplysystems',
    'type' => 'link',
    'relationship' => 'documents_checkwatersupplysystems',
    'source' => 'non-db',
    'vname' => 'LBL_CHECKWATERSUPPLYSYSTEMS_SUBPANEL_TITLE',
];

unset($dictionary['Document']['fields']['revision']['required']);
