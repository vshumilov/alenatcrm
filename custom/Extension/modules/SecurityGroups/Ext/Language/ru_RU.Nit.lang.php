<?php

$mod_strings['LBL_NAME'] = 'Сокращенное наименование';
$mod_strings['LBL_FULL_NAME'] = 'Полное наименование';
$mod_strings['LBL_INN'] = 'ИНН';
$mod_strings['LBL_KPP'] = 'КПП';
$mod_strings['LBL_OGRN'] = 'ОГРН';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Юридический адрес';
$mod_strings['LBL_SHIPPING_ADDRESS'] = 'Фактический адрес';
$mod_strings['LBL_ACCOUNT_DETAILS_PANEL'] = 'Банковские реквизиты';
$mod_strings['LBL_BANK_NAME'] = 'Банк';
$mod_strings['LBL_PAYMENT_ACCOUNT'] = 'Расчетный счет';
$mod_strings['LBL_CORRESPONDENT_ACCOUNT'] = 'Корсчет';
$mod_strings['LBL_BIC'] = 'БИК';
$mod_strings['LBL_DESCRIPTION'] = 'Основание работы на территории';
$mod_strings['LBL_SIGNED_PANEL'] = 'Подписан';
$mod_strings['LBL_GENITIVE_FIO'] = 'ФИО в родительном падеже';
$mod_strings['LBL_NOMINATIVE_FIO'] = 'ФИО в именительном падеже';
$mod_strings['LBL_GENITIVE_POSITION'] = 'Должность в родительном падеже';
$mod_strings['LBL_NOMINATIVE_POSITION'] = 'Должность в именительном падеже';
$mod_strings['LBL_BASE_ACTS_SIGNATORY'] = 'Основание действия подписанта';
$mod_strings['LBL_ADDRESS_PANEL'] = 'Адреса';
$mod_strings['LBL_PHONE'] = 'Телефон';
$mod_strings['LBL_EMAIL'] = 'Email';
$mod_strings['LBL_SECURITYGROUP_INFORMATION'] = 'Основаная информация';

$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Юридический адрес - город:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'Юридический адрес - страна:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'Юридический адрес - индекс:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Юридический адрес - область:';
$mod_strings['LBL_BILLING_ADDRESS_STREET_2'] = 'Юридический адрес - улица 2';
$mod_strings['LBL_BILLING_ADDRESS_STREET_3'] = 'Юридический адрес - улица 3';
$mod_strings['LBL_BILLING_ADDRESS_STREET_4'] = 'Юридический адрес - улица 4';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Юридический адрес - улица:';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Юридический адрес';

$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Фактический адрес - город:';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'Фактический адрес - страна:';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Фактический адрес - индекс:';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Фактический адрес - область:';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_2'] = 'Фактический адрес - улица 2';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_3'] = 'Фактический адрес - улица 3';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET_4'] = 'Фактический адрес - улица 4';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Фактический адрес - улица:';

$mod_strings['LBL_LIST_FORM_TITLE'] = 'Организации АЛЕНАТА';
$mod_strings['LBL_MODULE_NAME'] = 'Организации АЛЕНАТА';
$mod_strings['LBL_MODULE_TITLE'] = 'Организации АЛЕНАТА';
$mod_strings['LNK_NEW_RECORD'] = 'Создать организацию';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Поиск организаций';
$mod_strings['LBL_SECURITYGROUPS_SUBPANEL_TITLE'] = 'Организации АЛЕНАТА';

$mod_strings['LBL_USERS'] = 'Сотрудники';
$mod_strings['LBL_USERS_SUBPANEL_TITLE'] = 'Сотрудники';
$mod_strings['LBL_ADDITIVE_DESC'] = "Сотрудник имеет наивысшие права из всех ролей, назначенных сотруднику или организации";
$mod_strings['LBL_GROUP'] = "Организация";
$mod_strings['LBL_MASS_ASSIGN'] = "Организации: Массовое назначение";
$mod_strings['LBL_ASSIGN_CONFIRM'] = 'Вы уверены, что хотите добавить эту организацию к ';
$mod_strings['LBL_REMOVE_CONFIRM'] = 'Вы уверены, что хотите удалить эту организацию из ';
$mod_strings['LBL_SECURITYGROUP_USER_FORM_TITLE'] = 'Организации/Сотрудник';
$mod_strings['LBL_USER_NAME'] = 'Имя сотрудника';
$mod_strings['LBL_SECURITYGROUP_NAME'] = 'Организация';
$mod_strings['LBL_SELECT_GROUP'] = 'Выберите организацию';
$mod_strings['LBL_SELECT_GROUP_ERROR'] = 'Не выбрано ни одной организации';
$mod_strings['LBL_GROUP_SELECT'] = 'Выберите, какие организации должны иметь доступ к этой записи';
$mod_strings['LBL_HOOKUP_CONFIRM_PART1'] = "Будет добавлена связь между организацией и ";
$mod_strings['LBL_ERROR_DUPLICATE'] = 'Из-за возможных дубликатов вам необходимо вручную добавить организацию к создаваемой записи.';
$mod_strings['LBL_INBOUND_EMAIL_DESC'] = 'Разрешить доступ к учётной записи электронной почты только в том случае, если сотрудник принадлежит к организации, назначенной данной учётной записи.';
$mod_strings['LBL_PRIMARY_GROUP'] = 'Основная организация';
$mod_strings['LBL_WEBSITE'] = 'Сайт';
$mod_strings['LBL_STATE'] = 'Область';
$mod_strings['LBL_ADDRESS_MAP'] = 'Адрес на карте';
$mod_strings['LBL_PAYMENT_DESCRIPTION'] = 'Способы оплаты';




