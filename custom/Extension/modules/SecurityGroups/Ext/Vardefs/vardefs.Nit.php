<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['SecurityGroup']['fields']['full_name'] = [
    'name' => 'full_name',
    'vname' => 'LBL_FULL_NAME',
    'type' => 'varchar',
    'required' => false,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Full name',
];

$dictionary['SecurityGroup']['fields']['inn'] = [
    'name' => 'inn',
    'vname' => 'LBL_INN',
    'type' => 'basemax',
    'dbType' => 'varchar',
    'len' => '10',
    'required' => true,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Inn',
    'is_equal_mode' => true
];

$dictionary['SecurityGroup']['fields']['kpp'] = [
    'name' => 'kpp',
    'vname' => 'LBL_KPP',
    'type' => 'basemax',
    'dbType' => 'varchar',
    'len' => '9',
    'required' => true,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Kpp',
    'is_equal_mode' => true
];

$dictionary['SecurityGroup']['fields']['ogrn'] = [
    'name' => 'ogrn',
    'vname' => 'LBL_OGRN',
    'type' => 'basemax',
    'dbType' => 'varchar',
    'len' => '13',
    'required' => true,
    'reportable' => false,
    'audited' => true,
    'comment' => 'Ogrn',
    'is_equal_mode' => true
];

$dictionary['SecurityGroup']['fields']['bank_name'] = [
    'name' => 'bank_name',
    'vname' => 'LBL_BANK_NAME',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['SecurityGroup']['fields']['payment_account'] = [
    'name' => 'payment_account',
    'vname' => 'LBL_PAYMENT_ACCOUNT',
    'type' => 'integer',
    'dbType' => 'varchar',
    'len' => 20,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
    'is_equal_mode' => true
];

$dictionary['SecurityGroup']['fields']['correspondent_account'] = [
    'name' => 'correspondent_account',
    'vname' => 'LBL_CORRESPONDENT_ACCOUNT',
    'type' => 'integer',
    'dbType' => 'varchar',
    'len' => 20,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
    'is_equal_mode' => true
];

$dictionary['SecurityGroup']['fields']['bic'] = [
    'name' => 'bic',
    'vname' => 'LBL_BIC',
    'type' => 'integer',
    'dbType' => 'varchar',
    'len' => 9,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
    'is_equal_mode' => true
];

$dictionary['SecurityGroup']['fields']['genitive_fio'] = [
    'name' => 'genitive_fio',
    'vname' => 'LBL_GENITIVE_FIO',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['SecurityGroup']['fields']['genitive_position'] = [
    'name' => 'genitive_position',
    'vname' => 'LBL_GENITIVE_POSITION',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['SecurityGroup']['fields']['nominative_fio'] = [
    'name' => 'nominative_fio',
    'vname' => 'LBL_NOMINATIVE_FIO',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['SecurityGroup']['fields']['nominative_position'] = [
    'name' => 'nominative_position',
    'vname' => 'LBL_NOMINATIVE_POSITION',
    'type' => 'varchar',
    'len' => 255,
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['SecurityGroup']['fields']['base_acts_signatory'] = [
    'name' => 'base_acts_signatory',
    'vname' => 'LBL_BASE_ACTS_SIGNATORY',
    'type' => 'text',
    'audited' => false,
    'required' => false,
    'importadle' => 'true',
    'massupdate' => false,
];

$dictionary['SecurityGroup']['fields']['email'] = [
    'name' => 'email',
    'type' => 'varchar',
    'vname' => 'LBL_ANY_EMAIL',
    'importable' => false,
];

$dictionary['SecurityGroup']['fields']['website'] = [
    'name' => 'website',
    'type' => 'varchar',
    'vname' => 'LBL_WEBSITE',
    'importable' => false,
];

$dictionary['SecurityGroup']['fields']['city'] = [
    'name' => 'city',
    'rname' => 'name',
    'id_name' => 'city_id',
    'vname' => 'LBL_CITY',
    'type' => 'relate',
    'table' => 'cities',
    'join_name' => 'cities',
    'isnull' => 'true',
    'module' => 'Cities',
    'dbType' => 'varchar',
    'link' => 'cities',
    'len' => '255',
    'source' => 'non-db',
    'unified_search' => true,
    'required' => true,
    'importable' => 'required',
    'required' => true,
];

$dictionary['SecurityGroup']['fields']['city_id'] = [
    'name' => 'city_id',
    'vname' => 'LBL_CITY_ID',
    'type' => 'id',
    'audited' => true,
];

$dictionary['SecurityGroup']['fields']['cities'] = [
    'name' => 'cities',
    'type' => 'link',
    'relationship' => 'cities_securitygroups',
    'source' => 'non-db',
    'link_type' => 'one',
    'module' => 'Cities',
    'bean_name' => 'City',
    'vname' => 'LBL_CITIES',
];

$dictionary['SecurityGroup']['fields']['address_map'] = [
    'name' => 'address_map',
    'vname' => 'LBL_ADDRESS_MAP',
    'type' => 'text',
    'audited' => true,
];

$dictionary['SecurityGroup']['fields']['payment_description'] = [
    'name' => 'payment_description',
    'vname' => 'LBL_PAYMENT_DESCRIPTION',
    'type' => 'tinymce',
    'dbType' => 'html',
    'audited' => true,
];

VardefManager::createVardef('SecurityGroups','SecurityGroup', array('basic', 'assignable', 'company'));