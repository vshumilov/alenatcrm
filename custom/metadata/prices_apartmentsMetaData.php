<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['prices_apartments'] = array('table' => 'prices_apartments'
    , 'fields' => array(
        array('name' => 'id', 'type' => 'varchar', 'len' => '36')
        , array('name' => 'apartment_id', 'type' => 'varchar', 'len' => '36',)
        , array('name' => 'price_id', 'type' => 'varchar', 'len' => '36',)
        , array('name' => 'date_modified', 'type' => 'datetime')
        , array('name' => 'deleted', 'type' => 'bool', 'len' => '1', 'default' => '0', 'required' => false)
    )
    , 'indices' => array(
        array('name' => 'prices_apartments_pk', 'type' => 'primary', 'fields' => array('id'))
        , array('name' => 'idx_price_apartment', 'type' => 'alternate_key', 'fields' => array('price_id', 'apartment_id'))
        , array('name' => 'idx_apartmentid_del_priceid', 'type' => 'index', 'fields' => array('apartment_id', 'deleted', 'price_id'))
    )
    , 'relationships' => array(
        'prices_apartments' => array(
            'lhs_module' => 'Prices',
            'lhs_table' => 'prices',
            'lhs_key' => 'id',
            'rhs_module' => 'Apartments',
            'rhs_table' => 'apartments',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'prices_apartments',
            'join_key_lhs' => 'price_id',
            'join_key_rhs' => 'apartment_id'
        )
    )
);
