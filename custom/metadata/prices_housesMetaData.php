<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['prices_houses'] = array('table' => 'prices_houses'
    , 'fields' => array(
        array('name' => 'id', 'type' => 'varchar', 'len' => '36')
        , array('name' => 'house_id', 'type' => 'varchar', 'len' => '36',)
        , array('name' => 'price_id', 'type' => 'varchar', 'len' => '36',)
        , array('name' => 'date_modified', 'type' => 'datetime')
        , array('name' => 'deleted', 'type' => 'bool', 'len' => '1', 'default' => '0', 'required' => false)
    )
    , 'indices' => array(
        array('name' => 'prices_houses_pk', 'type' => 'primary', 'fields' => array('id'))
        , array('name' => 'idx_price_house', 'type' => 'alternate_key', 'fields' => array('price_id', 'house_id'))
        , array('name' => 'idx_houseid_del_priceid', 'type' => 'index', 'fields' => array('house_id', 'deleted', 'price_id'))
    )
    , 'relationships' => array(
        'prices_houses' => array(
            'lhs_module' => 'Prices',
            'lhs_table' => 'prices',
            'lhs_key' => 'id',
            'rhs_module' => 'Houses',
            'rhs_table' => 'houses',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'prices_houses',
            'join_key_lhs' => 'price_id',
            'join_key_rhs' => 'house_id'
        )
    )
);
