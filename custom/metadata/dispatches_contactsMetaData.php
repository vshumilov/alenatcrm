<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['dispatches_contacts'] = array('table' => 'dispatches_contacts'
    , 'fields' => array(
        array('name' => 'id', 'type' => 'varchar', 'len' => '36')
        , array('name' => 'contact_id', 'type' => 'varchar', 'len' => '36',)
        , array('name' => 'dispatch_id', 'type' => 'varchar', 'len' => '36',)
        , array('name' => 'date_modified', 'type' => 'datetime')
        , array('name' => 'deleted', 'type' => 'bool', 'len' => '1', 'default' => '0', 'required' => false)
    )
    , 'indices' => array(
        array('name' => 'dispatches_contactspk', 'type' => 'primary', 'fields' => array('id'))
        , array('name' => 'idx_dispatch_contact', 'type' => 'alternate_key', 'fields' => array('dispatch_id', 'contact_id'))
        , array('name' => 'idx_contactid_del_dispatchid', 'type' => 'index', 'fields' => array('contact_id', 'deleted', 'dispatch_id'))
    )
    , 'relationships' => array(
        'dispatches_contacts' => array(
            'lhs_module' => 'Dispatches',
            'lhs_table' => 'dispatches',
            'lhs_key' => 'id',
            'rhs_module' => 'Contacts',
            'rhs_table' => 'contacts',
            'rhs_key' => 'id',
            'relationship_type' => 'many-to-many',
            'join_table' => 'dispatches_contacts',
            'join_key_lhs' => 'dispatch_id',
            'join_key_rhs' => 'contact_id'
        )
    )
);
