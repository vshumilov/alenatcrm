<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Sanatoriumstexts', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Sanatoriumstexts&action=EditView&return_module=Sanatoriumstexts&return_action=DetailView", $mod_strings['LNK_NEW_SANATORIUMSTEXT'], "Create");
}
if (ACLController::checkAccess('Sanatoriumstexts', 'list', true)) {
    $module_menu[] = Array("index.php?module=Sanatoriumstexts&action=index&return_module=Sanatoriumstexts&return_action=DetailView", $mod_strings['LNK_SANATORIUMSTEXT_LIST'], "List");
}
if (ACLController::checkAccess('Sanatoriumstexts', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Sanatoriumstexts&return_module=Sanatoriumstexts&return_action=index", $mod_strings['LNK_IMPORT_SANATORIUMSTEXTS'], "Import");
}