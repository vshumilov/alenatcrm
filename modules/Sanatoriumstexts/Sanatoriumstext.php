<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Sanatoriumstext extends Basic
{

    public $table_name = "sanatoriumstexts";
    public $module_dir = "Sanatoriumstexts";
    public $object_name = "Sanatoriumstext";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }
    
    public function getSantoriumsTextIdBySanatoriumId($sanatoriumId) 
    {
        if (empty($sanatoriumId)) {
            return;
        }
        
        $query = "
        SELECT
            id
        FROM
            sanatoriumstexts
        WHERE
            sanatorium_id = '<sanatoriumId>' AND
            deleted = '0'
        LIMIT
            1
        ";
        
        $sql = strtr($query, ['<sanatoriumId>' => $sanatoriumId]);
        
        return dbGetScalar($sql);
    }
}
