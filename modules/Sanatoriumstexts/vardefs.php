<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Sanatoriumstext'] = array('table' => 'sanatoriumstexts', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Sanatoriumstexts',
    'fields' => array(
        'sanatorium_name' =>
        array(
            'name' => 'sanatorium_name',
            'rname' => 'name',
            'id_name' => 'sanatorium_id',
            'vname' => 'LBL_SANATORIUM_NAME',
            'type' => 'relate',
            'table' => 'sanatoriums',
            'join_name' => 'sanatoriums',
            'isnull' => 'true',
            'module' => 'Sanatoriums',
            'dbType' => 'varchar',
            'link' => 'sanatoriums',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'sanatorium_id' =>
        array(
            'name' => 'sanatorium_id',
            'vname' => 'LBL_SANATORIUM_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'sanatoriums' =>
        array(
            'name' => 'sanatoriums',
            'type' => 'link',
            'relationship' => 'sanatoriums_sanatoriumstexts',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Sanatoriums',
            'bean_name' => 'Sanatorium',
            'vname' => 'LBL_SANATORIUMS',
        ),
        'spa_text' =>
        array(
            'name' => 'spa_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_SPA_TEXT',
            'audited' => true,
        ),
        'accommodation_text' =>
        array(
            'name' => 'accommodation_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_ACCOMMODATION_TEXT',
            'audited' => true,
        ),
        'food_text' =>
        array(
            'name' => 'food_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_FOOD_TEXT',
            'audited' => true,
        ),
        'rest_text' =>
        array(
            'name' => 'rest_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_REST_TEXT',
            'audited' => true,
        ),
        'children_text' =>
        array(
            'name' => 'children_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_CHILDREN_TEXT',
            'audited' => true,
        ),
        'note_text' =>
        array(
            'name' => 'note_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_NOTE_TEXT',
            'audited' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_sanatoriumstext_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_sanatoriumstext_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_sanatoriumstext_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        'sanatoriums_sanatoriumstexts' =>
        array(
            'lhs_module' => 'Sanatoriums',
            'lhs_table' => 'sanatoriums',
            'lhs_key' => 'id',
            'rhs_module' => 'Sanatoriumstexts',
            'rhs_table' => 'sanatoriumstexts',
            'rhs_key' => 'sanatorium_id',
            'relationship_type' => 'one-to-many'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Sanatoriumstexts', 'Sanatoriumstext', array('default', 'assignable',
));

$dictionary['Sanatoriumstext']['fields']['description']['type'] = 'tinymce';
$dictionary['Sanatoriumstext']['fields']['description']['dbType'] = 'html';
