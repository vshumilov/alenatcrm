<?php

$viewdefs['Sanatoriumstexts']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
        'includes' =>
        array(
            array(
                'file' => 'custom/include/Nit/TinyMce/tinymce.min.js',
            ),
            array(
                'file' => 'custom/include/Nit/TinyMce/themes/mobile/theme.min.js',
            ),
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'name'
                ),
            ),
            array(
                array(
                    'name' => 'sanatorium_name'
                ),
            ),
            array(
                array(
                    'name' => 'spa_text'
                ),
            ),
            array(
                array(
                    'name' => 'description'
                ),
            ),
            array(
                array(
                    'name' => 'accommodation_text'
                ),
            ),
            array(
                array(
                    'name' => 'food_text'
                ),
            ),
            array(
                array(
                    'name' => 'rest_text'
                ),
            ),
            array(
                array(
                    'name' => 'children_text'
                ),
            ),
            array(
                array(
                    'name' => 'note_text'
                ),
            ),
        ),
    )
);
