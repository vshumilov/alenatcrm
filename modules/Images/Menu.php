<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Images', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Images&action=EditView&return_module=Images&return_action=DetailView", $mod_strings['LNK_NEW_IMAGE'], "Create");
}
if (ACLController::checkAccess('Images', 'list', true)) {
    $module_menu[] = Array("index.php?module=Images&action=index&return_module=Images&return_action=DetailView", $mod_strings['LNK_IMAGE_LIST'], "List");
}
if (ACLController::checkAccess('Images', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Images&return_module=Images&return_action=index", $mod_strings['LNK_IMPORT_IMAGES'], "Import");
}