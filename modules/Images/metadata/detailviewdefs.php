<?php

$viewdefs ['Images'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'DEFAULT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'default' =>
            array(
                array(
                    array(
                        'name' => 'name'
                    ),
                ),
                array(
                    array(
                        'name' => 'parent_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'file',
                        'displayParams' => array(
                            'thumbnail' => 's'
                        )
                    ),
                ),
                array(
                    array(
                        'name' => 'file_id'
                    ),
                ),
                array(
                    array(
                        'name' => 'file_ext'
                    ),
                ),
                array(
                    array(
                        'name' => 'thumbnail_name_s'
                    ),
                ),
                array(
                    array(
                        'name' => 'thumbnail_name_m'
                    ),
                ),
                array(
                    array(
                        'name' => 'is_avatar'
                    ),
                ),
                array(
                    array(
                        'name' => 'image_type'
                    ),
                ),
                array(
                    array(
                        'name' => 'order_number'
                    ),
                ),
            ),
            'LBL_PANEL_ASSIGNMENT' =>
            array(
                array(
                    array(
                        'name' => 'date_modified',
                        'label' => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),
                    array(
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                ),
            ),
        ),
    ),
);
