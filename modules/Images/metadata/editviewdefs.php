<?php

$viewdefs['Images']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'name'
                ),
            ),
            array(
                array(
                    'name' => 'parent_name'
                ),
            ),
            array(
                array(
                    'name' => 'file',
                    'displayParams' => array(
                        'thumbnail' => 's'
                    )
                ),
            ),
            array(
                array(
                    'name' => 'image_type'
                ),
            ),
            array(
                array(
                    'name' => 'order_number'
                ),
            ),
        ),
    )
);
