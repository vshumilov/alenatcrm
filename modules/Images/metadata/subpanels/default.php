<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopButtonQuickCreate'),
    ),
    'where' => '',
    'list_fields' => array(
        'order_number' => array(
            'name' => 'order_number',
            'vname' => 'LBL_ORDER_NUMBER',
            'width' => '15%',
        ),
        'name' => array(
            'name' => 'name',
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '40%',
        ),
        'file' => array(
            'name' => 'file',
            'vname' => 'LBL_FILE',
            'width' => '15%',
        ),
        'image_type' => array(
            'name' => 'image_type',
            'vname' => 'LBL_IMAGE_TYPE',
            'width' => '15%',
        ),
        'is_avatar' => array(
            'name' => 'is_avatar',
            'vname' => 'LBL_IS_AVATAR',
            'width' => '15%',
        ),
        'edit_button' => array(
            'vname' => 'LBL_EDIT_BUTTON',
            'widget_class' => 'SubPanelEditButton',
            'module' => 'Images',
            'width' => '4%',
        ),
        'remove_button' => array(
            'vname' => 'LBL_REMOVE',
            'widget_class' => 'SubPanelRemoveButton',
            'module' => 'Images',
            'width' => '4%',
        ),
    ),
);
