<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Image'] = array('table' => 'images', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Images',
    'fields' => array(
        'file' =>
        array(
            'name' => 'file',
            'vname' => 'LBL_FILE',
            'type' => 'nitfile',
            'audited' => true,
            'source' => 'non-db',
            'is_public' => true,
            'thumbnail_formats' => array(
                's' => array('width' => '240', 'height' => '150'),
                'm' => array('width' => '830', 'height' => '500'),
            )
        ),
        'parent_name' =>
        array(
            'required' => false,
            'source' => 'non-db',
            'name' => 'parent_name',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'parent',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 1,
            'reportable' => 0,
            'len' => 25,
            'options' => 'images_parent_type_dom',
            'studio' => 'visible',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'parent_type' => 'record_type_display',
        ),
        'parent_type' =>
        array(
            'required' => true,
            'name' => 'parent_type',
            'vname' => 'LBL_PARENT_TYPE',
            'type' => 'parent_type',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 100,
            'dbType' => 'varchar',
            'studio' => 'hidden',
            'isnull' => 'true',
        ),
        'parent_id' =>
        array(
            'required' => true,
            'name' => 'parent_id',
            'vname' => 'LBL_PARENT_ID',
            'type' => 'id',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 36,
            'isnull' => 'true',
        ),
        'sanatoriums' =>
        array(
            'name' => 'sanatoriums',
            'type' => 'link',
            'relationship' => 'sanatoriums_images',
            'module' => 'Sanatoriums',
            'bean_name' => 'Sanatorium',
            'source' => 'non-db',
            'side' => 'right',
        ),
        'is_avatar' =>
        array(
            'name' => 'is_avatar',
            'type' => 'bool',
            'len' => 255,
            'vname' => 'LBL_IS_AVATAR',
            'audited' => true,
            'default' => '0',
            'isnull' => 'false',
        ),
        'file_id' =>
        array(
            'name' => 'file_id',
            'vname' => 'LBL_FILE_ID',
            'type' => 'id',
            'audited' => true,
            'isnull' => 'false',
        ),
        'file_ext' => array(
            'name' => 'file_ext',
            'vname' => 'LBL_FILE_EXT',
            'type' => 'varchar',
            'len' => '50',
            'required' => false,
            'audited' => false,
            'massupdate' => false,
            'isnull' => false,
        ),
        'thumbnail_name_s' =>
        array(
            'name' => 'thumbnail_name_s',
            'vname' => 'LBL_THUMBNAIL_NAME_S',
            'type' => 'varchar',
            'audited' => true,
            'isnull' => 'false',
        ),
        'thumbnail_name_m' =>
        array(
            'name' => 'thumbnail_name_m',
            'vname' => 'LBL_THUMBNAIL_NAME_M',
            'type' => 'varchar',
            'audited' => true,
            'isnull' => 'false',
        ),
        'image_type' =>
        array(
            'name' => 'image_type',
            'vname' => 'LBL_IMAGE_TYPE',
            'type' => 'enum',
            'options' => 'images_options_image_type',
            'default' => '',
            'audited' => true,
            'isnull' => 'false',
        ),
        'order_number' =>
        array(
            'name' => 'order_number',
            'vname' => 'LBL_ORDER_NUMBER',
            'type' => 'int',
            'default' => '1',
            'audited' => true,
            'isnull' => 'false',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_image_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_image_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_image_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
        array(
            'name' => 'idx_image_parent_avatar_deleted',
            'type' => 'index',
            'fields' => array('parent_type', 'parent_id', 'is_avatar', 'deleted'),
        ),
        array(
            'name' => 'idx_image_parent_id_avatar_deleted',
            'type' => 'index',
            'fields' => array('parent_id', 'is_avatar', 'deleted'),
        ),
        array(
            'name' => 'idx_image_parent_deleted',
            'type' => 'index',
            'fields' => array('parent_id', 'parent_type', 'deleted')
        )
    ),
    'relationships' => array(
        'sanatoriums_images' => array(
            'lhs_module' => 'Sanatoriums',
            'lhs_table' => 'sanatoriums',
            'lhs_key' => 'id',
            'rhs_module' => 'Images',
            'rhs_table' => 'images',
            'rhs_key' => 'parent_id',
            'relationship_type' => 'one-to-many',
            'relationship_role_column' => 'parent_type',
            'relationship_role_column_value' => 'Sanatoriums'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Images', 'Image', array('default', 'assignable',
));
