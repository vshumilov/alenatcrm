<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Image extends Basic
{
    public $table_name = "images";
    public $module_dir = "Images";
    public $object_name = "Image";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }
}

