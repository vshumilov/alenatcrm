<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class House extends Basic
{

    public $table_name = "houses";
    public $module_dir = "Houses";
    public $object_name = "House";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList()
    {
        $sql = "
        SELECT
            id,
            name
        FROM
            houses
        WHERE
            deleted = '0'
            <andWhere>
        ORDER BY
            name
        ";

        $andWhere = "";

        if ($_REQUEST['module'] == 'Prices' && !empty($_REQUEST['record'])) {
            $price = BeanFactory::getBean('Prices', $_REQUEST['record']);
            $andWhere = "AND parent_id = " . dbQuote($price->parent_id);
        } else if ($_REQUEST['module'] == 'Home' && !empty($_REQUEST['parent_id'])) {
            $andWhere = "AND parent_id = " . dbQuote($_REQUEST['parent_id']);
        }

        $query = strtr($sql, ['<andWhere>' => $andWhere]);

        return dbGetAssocArray($query);
    }

}
