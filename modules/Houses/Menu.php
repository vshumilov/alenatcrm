<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Houses', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Houses&action=EditView&return_module=Houses&return_action=DetailView", $mod_strings['LNK_NEW_HOUSE'], "Create");
}
if (ACLController::checkAccess('Houses', 'list', true)) {
    $module_menu[] = Array("index.php?module=Houses&action=index&return_module=Houses&return_action=DetailView", $mod_strings['LNK_HOUSE_LIST'], "List");
}
if (ACLController::checkAccess('Houses', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Houses&return_module=Houses&return_action=index", $mod_strings['LNK_IMPORT_HOUSES'], "Import");
}