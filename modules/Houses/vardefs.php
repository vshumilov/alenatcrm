<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['House'] = array('table' => 'houses', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Houses',
    'fields' => array(
        'number' =>
        array(
            'name' => 'number',
            'vname' => 'LBL_NUMBER',
            'type' => 'varchar',
            'audited' => true,
            'required' => 'true'
        ),
        'parent_name' =>
        array(
            'required' => false,
            'source' => 'non-db',
            'name' => 'parent_name',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'parent',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 1,
            'reportable' => 0,
            'len' => 25,
            'options' => 'images_parent_type_dom',
            'studio' => 'visible',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'parent_type' => 'record_type_display',
        ),
        'parent_type' =>
        array(
            'required' => true,
            'name' => 'parent_type',
            'vname' => 'LBL_PARENT_TYPE',
            'type' => 'parent_type',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 100,
            'dbType' => 'varchar',
            'studio' => 'hidden',
            'isnull' => 'true',
        ),
        'parent_id' =>
        array(
            'required' => true,
            'name' => 'parent_id',
            'vname' => 'LBL_PARENT_ID',
            'type' => 'id',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 36,
            'isnull' => 'true',
        ),
        'sanatoriums' =>
        array(
            'name' => 'sanatoriums',
            'type' => 'link',
            'relationship' => 'sanatoriums_houses',
            'module' => 'Sanatoriums',
            'bean_name' => 'Sanatorium',
            'source' => 'non-db',
            'side' => 'right',
        ),
        'prices' =>
        array(
            'name' => 'prices',
            'type' => 'link',
            'relationship' => 'prices_houses',
            'module' => 'Prices',
            'bean_name' => 'Price',
            'source' => 'non-db',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_house_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_house_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_house_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
        array(
            'name' => 'idx_house_parent_deleted',
            'type' => 'index',
            'fields' => array('parent_id', 'parent_type', 'deleted')
        )
    ),
    'relationships' => array(
        'sanatoriums_houses' => array(
            'lhs_module' => 'Sanatoriums',
            'lhs_table' => 'sanatoriums',
            'lhs_key' => 'id',
            'rhs_module' => 'Houses',
            'rhs_table' => 'houses',
            'rhs_key' => 'parent_id',
            'relationship_type' => 'one-to-many',
            'relationship_role_column' => 'parent_type',
            'relationship_role_column_value' => 'Sanatoriums'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Houses', 'House', array('default', 'assignable',
));
