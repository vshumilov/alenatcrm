<?php

$viewdefs ['Prices'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'DEFAULT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'default' =>
            array(
                array(
                    array(
                        'name' => 'name'
                    ),
                ),
                array(
                    array(
                        'name' => 'parent_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'date_from'
                    ),
                ),
                array(
                    array(
                        'name' => 'date_to'
                    ),
                ),
                array(
                    array(
                        'name' => 'pricelevel_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'foodoption_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'houses_multienum'
                    ),
                ),
                array(
                    array(
                        'name' => 'apartments_multienum'
                    ),
                ),
                array(
                    array(
                        'name' => 'cost',
                        'label' => '{$MOD.LBL_COST} ({$CURRENCY})',
                    ),
                ),
                array(
                    array(
                        'name' => 'cost_with_treatment',
                        'label' => '{$MOD.LBL_COST_WITH_TREATMENT} ({$CURRENCY})',
                    ),
                ),
                array(
                    array(
                        'name' => 'treatment_cost',
                        'label' => '{$MOD.LBL_TREATMENT_COST} ({$CURRENCY})',
                    ),
                ),
                array(
                    array(
                        'name' => 'food_cost',
                        'label' => '{$MOD.LBL_FOOD_COST} ({$CURRENCY})',
                    ),
                ),
                array(
                    array(
                        'name' => 'accommodation_cost',
                        'label' => '{$MOD.LBL_ACCOMMODATION_COST} ({$CURRENCY})',
                    ),
                ),
            ),
            'LBL_PANEL_ASSIGNMENT' =>
            array(
                array(
                    array(
                        'name' => 'date_modified',
                        'label' => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),
                    array(
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                ),
            ),
        ),
    ),
);
