<?php

$viewdefs['Prices']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'parent_name'
                ),
            ),
            array(
                array(
                    'name' => 'date_from'
                ),
            ),
            array(
                array(
                    'name' => 'date_to'
                ),
            ),
            array(
                array(
                    'name' => 'pricelevel_name'
                ),
            ),
            array(
                array(
                    'name' => 'foodoption_name'
                ),
            ),
            array(
                array(
                    'name' => 'houses_multienum'
                ),
            ),
            array(
                array(
                    'name' => 'apartments_multienum'
                ),
            ),
            array(
                array(
                    'name' => 'currency_id',
                    'label'=>'LBL_CURRENCY'
                ),
            ),
            array(
                array(
                    'name' => 'cost'
                ),
            ),
            array(
                array(
                    'name' => 'cost_with_treatment'
                ),
            ),
            array(
                array(
                    'name' => 'treatment_cost'
                ),
            ),
            array(
                array(
                    'name' => 'food_cost'
                ),
            ),
            array(
                array(
                    'name' => 'accommodation_cost'
                ),
            ),
        ),
    )
);
