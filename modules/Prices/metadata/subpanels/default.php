<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopButtonQuickCreate'),
    ),
    'where' => '',
    'list_fields' => array(
        'name' => array(
            'name' => 'name',
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '40%',
        ),
        'date_from' => array(
            'name' => 'date_from',
            'vname' => 'LBL_DATE_FROM',
            'width' => '15%',
        ),
        'date_to' => array(
            'name' => 'date_to',
            'vname' => 'LBL_DATE_TO',
            'width' => '15%',
        ),
        'pricelevel_name' => array(
            'name' => 'pricelevel_name',
            'vname' => 'LBL_PRICELEVEL_NAME',
            'width' => '15%',
        ),
        'foodoption_name' => array(
            'name' => 'foodoption_name',
            'vname' => 'LBL_FOODOPTION_NAME',
            'width' => '15%',
        ),
        'cost' => array(
            'name' => 'cost',
            'vname' => 'LBL_COST',
            'width' => '15%',
        ),
        'cost_with_treatment' => array(
            'name' => 'cost_with_treatment',
            'vname' => 'LBL_COST_WITH_TREATMENT',
            'width' => '15%',
        ),
        'treatment_cost' => array(
            'name' => 'treatment_cost',
            'vname' => 'LBL_TREATMENT_COST',
            'width' => '15%',
        ),
        'food_cost' => array(
            'name' => 'food_cost',
            'vname' => 'LBL_FOOD_COST',
            'width' => '15%',
        ),
        'accommodation_cost' => array(
            'name' => 'accommodation_cost',
            'vname' => 'LBL_ACCOMMODATION_COST',
            'width' => '15%',
        ),
        'edit_button' => array(
            'vname' => 'LBL_EDIT_BUTTON',
            'widget_class' => 'SubPanelEditButton',
            'module' => 'Prices',
            'width' => '4%',
        ),
        'remove_button' => array(
            'vname' => 'LBL_REMOVE',
            'widget_class' => 'SubPanelRemoveButton',
            'module' => 'Prices',
            'width' => '4%',
        ),
    ),
);
