<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Prices'] = array(
    'NAME' => array(
        'width' => '30',
        'label' => 'LBL_NAME',
        'link' => true,
        'default' => true
    ),
    'PARENT_NAME' => array(
        'width' => '30',
        'label' => 'LBL_PARENT_NAME',
        'default' => true
    ),
    'PRICELEVEL_NAME' => array(
        'width' => '10',
        'label' => 'LBL_PRICELEVEL_NAME',
        'default' => true
    ),
    'FOODOPTION_NAME' => array(
        'width' => '10',
        'label' => 'LBL_FOODOPTION_NAME',
        'default' => true
    ),
    'DATE_FROM' => array(
        'width' => '10',
        'label' => 'LBL_DATE_FROM',
        'default' => true
    ),
    'DATE_TO' => array(
        'width' => '10',
        'label' => 'LBL_DATE_TO',
        'default' => true
    ),
    'CREATED_BY_NAME' => array(
        'width' => '10',
        'label' => 'LBL_CREATED'),
    'ASSIGNED_USER_NAME' => array(
        'width' => '5',
        'label' => 'LBL_LIST_ASSIGNED_USER',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => true),
    'MODIFIED_BY_NAME' => array(
        'width' => '5',
        'label' => 'LBL_MODIFIED'),
    'DATE_ENTERED' => array(
        'width' => '10',
        'label' => 'LBL_DATE_ENTERED')
);
