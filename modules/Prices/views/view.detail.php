<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('custom/include/MVC/View/views/view.detail.php');

class PricesViewDetail extends ViewDetail
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    function PricesViewDetail()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }

    function display()
    {

        $currency = new Currency();
        if (isset($this->bean->currency_id) && !empty($this->bean->currency_id)) {
            $currency->retrieve($this->bean->currency_id);
            if ($currency->deleted != 1) {
                $this->ss->assign('CURRENCY', $currency->iso4217 . ' ' . $currency->symbol);
            } else {
                $this->ss->assign('CURRENCY', $currency->getDefaultISO4217() . ' ' . $currency->getDefaultCurrencySymbol());
            }
        } else {
            $this->ss->assign('CURRENCY', $currency->getDefaultISO4217() . ' ' . $currency->getDefaultCurrencySymbol());
        }

        parent::display();
    }

}
