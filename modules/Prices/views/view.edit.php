<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('custom/include/MVC/View/views/view.edit.php');
require_once('modules/Prices/Price.php');

class PricesViewEdit extends ViewEdit
{

    function __construct()
    {
        parent::__construct();
        $this->useForSubpanel = true;
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    function PricesViewEdit()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }

    public function display()
    {
        if (empty($this->bean->id)) {
            $priceId = Price::getLastPriceId();
            if (!empty($priceId)) {
                $bean = BeanFactory::getBean($this->bean->module_dir, $priceId);
                $this->ev->focus = $bean;
                $this->ev->focus->id = null;
            }
        }
        
        parent::display();
    }

}