<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Price extends Basic
{

    public $table_name = "prices";
    public $module_dir = "Prices";
    public $object_name = "Price";
    public $is_after_save = false;

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public function get_list_view_data()
    {
        $data = parent::get_list_view_data();

        if (!empty($data['ID'])) {
            $apartment = BeanFactory::getBean($this->module_dir, $data['ID']);

            $data['PRICELEVEL_ID'] = $apartment->pricelevel_id;
            $data['PRICELEVEL_NAME'] = $apartment->pricelevel_name;

            $data['CURRENCY_NAME'] = $apartment->currency_name;
        }

        return $data;
    }
    
    public static function getLastPriceId()
    {
        $sql = "
        SELECT
            id
        FROM
            prices
        WHERE
            deleted = '0'
            <andWhere>
        ORDER BY
            date_modified DESC
        LIMIT
            1
        ";

        $andWhere = "";

        if ($_REQUEST['module'] == 'Home' && !empty($_REQUEST['parent_id'])) {
            $andWhere = "AND parent_id = " . dbQuote($_REQUEST['parent_id']);
        }

        $query = strtr($sql, ['<andWhere>' => $andWhere]);

        return dbGetScalar($query);
    }

}
