<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Prices', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Prices&action=EditView&return_module=Prices&return_action=DetailView", $mod_strings['LNK_NEW_PRICE'], "Create");
}
if (ACLController::checkAccess('Prices', 'list', true)) {
    $module_menu[] = Array("index.php?module=Prices&action=index&return_module=Prices&return_action=DetailView", $mod_strings['LNK_PRICE_LIST'], "List");
}
if (ACLController::checkAccess('Prices', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Prices&return_module=Prices&return_action=index", $mod_strings['LNK_IMPORT_PRICES'], "Import");
}