<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Price'] = array('table' => 'prices', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Prices',
    'fields' => array(
        'parent_name' =>
        array(
            'required' => false,
            'source' => 'non-db',
            'name' => 'parent_name',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'parent',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 1,
            'reportable' => 0,
            'len' => 25,
            'options' => 'images_parent_type_dom',
            'studio' => 'visible',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'parent_type' => 'record_type_display',
        ),
        'parent_type' =>
        array(
            'required' => true,
            'name' => 'parent_type',
            'vname' => 'LBL_PARENT_TYPE',
            'type' => 'parent_type',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 100,
            'dbType' => 'varchar',
            'studio' => 'hidden',
            'isnull' => 'true',
        ),
        'parent_id' =>
        array(
            'required' => true,
            'name' => 'parent_id',
            'vname' => 'LBL_PARENT_ID',
            'type' => 'id',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 36,
            'isnull' => 'true',
        ),
        'sanatoriums' =>
        array(
            'name' => 'sanatoriums',
            'type' => 'link',
            'relationship' => 'sanatoriums_prices',
            'module' => 'Sanatoriums',
            'bean_name' => 'Sanatorium',
            'source' => 'non-db',
            'side' => 'right',
        ),
        'pricelevel_name' =>
        array(
            'name' => 'pricelevel_name',
            'rname' => 'name',
            'id_name' => 'pricelevel_id',
            'vname' => 'LBL_PRICELEVEL_NAME',
            'type' => 'relate',
            'table' => 'pricelevels',
            'join_name' => 'pricelevels',
            'isnull' => 'true',
            'module' => 'Pricelevels',
            'dbType' => 'varchar',
            'link' => 'pricelevels',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'pricelevel_id' =>
        array(
            'name' => 'pricelevel_id',
            'vname' => 'LBL_PRICELEVEL_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'pricelevels' =>
        array(
            'name' => 'pricelevels',
            'type' => 'link',
            'relationship' => 'pricelevels_prices',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Pricelevels',
            'bean_name' => 'Pricelevel',
            'vname' => 'LBL_PRICELEVELS',
        ),
        'foodoption_name' =>
        array(
            'name' => 'foodoption_name',
            'rname' => 'name',
            'id_name' => 'foodoption_id',
            'vname' => 'LBL_FOODOPTION_NAME',
            'type' => 'relate',
            'table' => 'foodoptions',
            'join_name' => 'foodoptions',
            'isnull' => 'true',
            'module' => 'Foodoptions',
            'dbType' => 'varchar',
            'link' => 'foodoptions',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'foodoption_id' =>
        array(
            'name' => 'foodoption_id',
            'vname' => 'LBL_FOODOPTION_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'foodoptions' =>
        array(
            'name' => 'foodoptions',
            'type' => 'link',
            'relationship' => 'foodoptions_prices',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Foodoptions',
            'bean_name' => 'Foodoption',
            'vname' => 'LBL_FOODOPTIONS',
        ),
        'date_from' =>
        array(
            'name' => 'date_from',
            'vname' => 'LBL_DATE_FROM',
            'type' => 'date',
            'audited' => true,
            'importable' => 'required',
            'required' => true,
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
        ),
        'date_to' =>
        array(
            'name' => 'date_to',
            'vname' => 'LBL_DATE_TO',
            'type' => 'date',
            'audited' => true,
            'importable' => 'required',
            'required' => true,
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
        ),
        'cost' =>
        array(
            'name' => 'cost',
            'vname' => 'LBL_COST',
            'type' => 'currency',
            'dbType' => 'double',
            'importable' => 'required',
            'duplicate_merge' => '1',
            'required' => true,
            'options' => 'numeric_range_search_dom',
            'enable_range_search' => true,
        ),
        'cost_with_treatment' =>
        array(
            'name' => 'cost_with_treatment',
            'vname' => 'LBL_COST_WITH_TREATMENT',
            'type' => 'currency',
            'dbType' => 'double',
            'importable' => 'required',
            'duplicate_merge' => '1',
            'options' => 'numeric_range_search_dom',
            'enable_range_search' => true,
        ),
        'treatment_cost' =>
        array(
            'name' => 'treatment_cost',
            'vname' => 'LBL_TREATMENT_COST',
            'type' => 'currency',
            'dbType' => 'double',
            'importable' => 'required',
            'duplicate_merge' => '1',
            'options' => 'numeric_range_search_dom',
            'enable_range_search' => true,
        ),
        'food_cost' =>
        array(
            'name' => 'food_cost',
            'vname' => 'LBL_FOOD_COST',
            'type' => 'currency',
            'dbType' => 'double',
            'importable' => 'required',
            'duplicate_merge' => '1',
            'options' => 'numeric_range_search_dom',
            'enable_range_search' => true,
        ),
        'accommodation_cost' =>
        array(
            'name' => 'accommodation_cost',
            'vname' => 'LBL_ACCOMMODATION_COST',
            'type' => 'currency',
            'dbType' => 'double',
            'importable' => 'required',
            'duplicate_merge' => '1',
            'options' => 'numeric_range_search_dom',
            'enable_range_search' => true,
        ),
        'currency_id' =>
        array(
            'name' => 'currency_id',
            'type' => 'id',
            'group' => 'currency_id',
            'vname' => 'LBL_CURRENCY',
            'function' => array('name' => 'getCurrencyDropDown', 'returns' => 'html'),
            'reportable' => false,
            'comment' => 'Currency used for display purposes'
        ),
        'currency_name' =>
        array(
            'name' => 'currency_name',
            'rname' => 'name',
            'id_name' => 'currency_id',
            'vname' => 'LBL_CURRENCY_NAME',
            'type' => 'relate',
            'isnull' => 'true',
            'table' => 'currencies',
            'module' => 'Currencies',
            'source' => 'non-db',
            'function' => array('name' => 'getCurrencyNameDropDown', 'returns' => 'html'),
            'studio' => 'false',
            'duplicate_merge' => 'disabled',
        ),
        'currency_symbol' =>
        array(
            'name' => 'currency_symbol',
            'rname' => 'symbol',
            'id_name' => 'currency_id',
            'vname' => 'LBL_CURRENCY_SYMBOL',
            'type' => 'relate',
            'isnull' => 'true',
            'table' => 'currencies',
            'module' => 'Currencies',
            'source' => 'non-db',
            'function' => array('name' => 'getCurrencySymbolDropDown', 'returns' => 'html'),
            'studio' => 'false',
            'duplicate_merge' => 'disabled',
        ),
        'currencies' =>
        array(
            'name' => 'currencies',
            'type' => 'link',
            'relationship' => 'prices_currencies',
            'source' => 'non-db',
            'vname' => 'LBL_CURRENCIES',
        ),
        'houses_multienum' =>
        array(
            'name' => 'houses_multienum',
            'type' => 'multienum',
            'vname' => 'LBL_HOUSES_MULTIENUM',
            'function' => 'House::getList',
            'reportable' => false,
            'required' => true,
            'source' => 'non-db',
        ),
        'apartments_multienum' =>
        array(
            'name' => 'apartments_multienum',
            'type' => 'multienum',
            'vname' => 'LBL_APARTMENTS_MULTIENUM',
            'function' => 'Apartment::getList',
            'reportable' => false,
            'required' => true,
            'source' => 'non-db',
        ),
        'houses' =>
        array(
            'name' => 'houses',
            'type' => 'link',
            'relationship' => 'prices_houses',
            'module' => 'Houses',
            'bean_name' => 'House',
            'true_relationship_type' => 'many-to-many',
            'source' => 'non-db',
        ),
        'apartments' =>
        array(
            'name' => 'apartments',
            'type' => 'link',
            'relationship' => 'prices_apartments',
            'module' => 'Apartments',
            'bean_name' => 'Apartment',
            'true_relationship_type' => 'many-to-many',
            'source' => 'non-db',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_price_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_price_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_price_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
        array(
            'name' => 'idx_parent_id_deleted',
            'type' => 'index',
            'fields' => array('parent_id', 'deleted'),
        ),
        array(
            'name' => 'idx_price_dates',
            'type' => 'index',
            'fields' => array('date_from', 'date_to'),
        ),
        array(
            'name' => 'idx_price_cost',
            'type' => 'index',
            'fields' => array('cost'),
        ),
    ),
    'relationships' => array(
        'sanatoriums_prices' => array(
            'lhs_module' => 'Sanatoriums',
            'lhs_table' => 'sanatoriums',
            'lhs_key' => 'id',
            'rhs_module' => 'Prices',
            'rhs_table' => 'prices',
            'rhs_key' => 'parent_id',
            'relationship_type' => 'one-to-many',
            'relationship_role_column' => 'parent_type',
            'relationship_role_column_value' => 'Sanatoriums'
        ),
        'pricelevels_prices' =>
        array(
            'lhs_module' => 'Pricelevels',
            'lhs_table' => 'pricelevels',
            'lhs_key' => 'id',
            'rhs_module' => 'Prices',
            'rhs_table' => 'prices',
            'rhs_key' => 'pricelevel_id',
            'relationship_type' => 'one-to-many'
        ),
        'foodoptions_prices' =>
        array(
            'lhs_module' => 'Foodoptions',
            'lhs_table' => 'foodoptions',
            'lhs_key' => 'id',
            'rhs_module' => 'Prices',
            'rhs_table' => 'prices',
            'rhs_key' => 'foodoption_id',
            'relationship_type' => 'one-to-many'
        ),
        'prices_currencies' => array(
            'lhs_module' => 'Prices',
            'lhs_table' => 'prices',
            'lhs_key' => 'currency_id',
            'rhs_module' => 'Currencies',
            'rhs_table' => 'currencies',
            'rhs_key' => 'id',
            'relationship_type' => 'one-to-many'
        )
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Prices', 'Price', array('default', 'assignable',
));
