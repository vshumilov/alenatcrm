<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Dispatch'] = array('table' => 'dispatches', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Dispatches',
    'fields' => array(
        'contacts' =>
        array(
            'name' => 'contacts',
            'type' => 'link',
            'relationship' => 'dispatches_contacts',
            'source' => 'non-db',
            'vname' => 'LBL_CONTACT',
        ),
        'emailtemplate_name' =>
        array(
            'name' => 'emailtemplate_name',
            'rname' => 'name',
            'id_name' => 'emailtemplate_id',
            'vname' => 'LBL_EMAILTEMPLATE_NAME',
            'type' => 'relate',
            'table' => 'emailtemplates',
            'join_name' => 'emailtemplates',
            'isnull' => 'true',
            'module' => 'EmailTemplates',
            'dbType' => 'varchar',
            'link' => 'emailtemplates',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'emailtemplate_id' =>
        array(
            'name' => 'emailtemplate_id',
            'vname' => 'LBL_EMAILTEMPLATE_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'emailtemplates' =>
        array(
            'name' => 'emailtemplates',
            'type' => 'link',
            'relationship' => 'emailtemplates_dispatches',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'EmailTemplates',
            'bean_name' => 'EmailTemplate',
            'vname' => 'LBL_EMAILTEMPLATES',
        ),
        'send_date' =>
        array(
            'name' => 'send_date',
            'type' => 'datetimecombo',
            'dbtType' => 'datetime',
            'vname' => 'LBL_SEND_DATETIME',
        ),
        'send_period' =>
        array(
            'name' => 'send_period',
            'type' => 'enum',
            'options' => 'dispatches_send_period_options',
            'vname' => 'LBL_SEND_PERIOD',
        ),
        'send_type' =>
        array(
            'name' => 'send_type',
            'type' => 'radioenum',
            'dbType' => 'enum',
            'options' => 'dispatches_send_type_options',
            'vname' => 'LBL_SEND_TYPE',
        ),
        'send_to_all_contacts' =>
        array(
            'name' => 'send_to_all_contacts',
            'type' => 'bool',
            'len' => '1',
            'default' => '1',
            'vname' => 'LBL_SEND_TO_ALL_CONTACTS',
        ),
        'send_by_email' =>
        array(
            'name' => 'send_by_email',
            'type' => 'bool',
            'len' => '1',
            'default' => '1',
            'vname' => 'LBL_SEND_BY_EMAIL',
        ),
        'send_by_sms' =>
        array(
            'name' => 'send_by_sms',
            'type' => 'bool',
            'len' => '1',
            'default' => '1',
            'vname' => 'LBL_SEND_BY_SMS',
        ),
        'is_active' =>
        array(
            'name' => 'is_active',
            'type' => 'bool',
            'len' => '1',
            'default' => '1',
            'vname' => 'LBL_IS_ACTIVE',
        ),
        'is_repeated' =>
        array(
            'name' => 'is_repeated',
            'type' => 'bool',
            'len' => '1',
            'default' => '0',
            'vname' => 'LBL_IS_REPEATED',
        ),
        'contact_from' =>
        array(
            'name' => 'contact_from',
            'type' => 'enum',
            'options' => 'contacts_from_with_all_options',
            'default' => 'all',
            'vname' => 'LBL_CONTACT_FROM',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_dispatch_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_dispatch_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_dispatch_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        'emailtemplates_dispatches' =>
        array(
            'lhs_module' => 'EmailTemplates',
            'lhs_table' => 'emailtemplates',
            'lhs_key' => 'id',
            'rhs_module' => 'Dispatches',
            'rhs_table' => 'dispatches',
            'rhs_key' => 'emailtemplate_id',
            'relationship_type' => 'one-to-many'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Dispatches', 'Dispatch', array('default', 'assignable',
));
