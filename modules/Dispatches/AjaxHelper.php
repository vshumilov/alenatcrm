<?php
require_once 'custom/modules/Dispatches/BeanHelper/Dispatch.php';
require_once 'custom/include/Nit/Helper/Request.php';

class DispatchAjaxHelper extends AjaxHelper
{
    public function sendSmsCode()
    {
        $phone = HelperRequest::processReqStr($_REQUEST['params']['phone']);
        
        if (empty($phone)) {
            throw new Exception("phone is empty");
        }
        
        $beanHelper = new DispatchBeanHelperDispatch($this->_bean);
        $result = $beanHelper->sendSmsCode($phone);
        
        if (!empty($result['error'])) {
            throw new Exception("send sms code error");
        }
        
        return;
    }
    
    public function checkSmsCode()
    {
        $code = HelperRequest::processReqStr($_REQUEST['params']['code']);
        $phone = HelperRequest::processReqStr($_REQUEST['params']['phone']);
        
        if (empty($code) || empty($phone)) {
            throw new Exception("code or phone is empty");
        }
        
        $beanHelper = new DispatchBeanHelperDispatch($this->_bean);
        $result = $beanHelper->checkSmsCode($code, $phone);
        
        if (!empty($result['error'])) {
            throw new Exception("check sms code error");
        }
        
        return;
    }
}

