<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Dispatch extends Basic
{

    public $table_name = "dispatches";
    public $module_dir = "Dispatches";
    public $object_name = "Dispatch";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

}
