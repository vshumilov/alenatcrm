<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$layout_defs['Dispatches'] = array(
    'subpanel_setup' => array(
        'contacts' => array(
            'order' => 1,
            'module' => 'Contacts',
            'sort_order' => 'asc',
            'sort_by' => 'last_name',
            'subpanel_name' => 'ForDispatches',
            'get_subpanel_data' => 'contacts',
            'title_key' => 'LBL_CONTACTS_SUBPANEL_TITLE',
            'top_buttons' => array(
                array(
                    'widget_class' => 'SubPanelTopSelectButton',
                    'popup_module' => 'Contacts',
                    'mode' => 'MultiSelect',
                ),
            ),
        ),
    ),
);
