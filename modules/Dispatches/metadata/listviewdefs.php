<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Dispatches'] = array(
    'NAME' => array(
        'width' => '30',
        'label' => 'LBL_NAME',
        'link' => true,
        'default' => true),
    'SEND_DATETIME' => array(
        'width' => '10',
        'label' => 'LBL_SEND_DATETIME'),
    'SEND_PERIOD' => array(
        'width' => '10',
        'label' => 'LBL_SEND_PERIOD'),
    'SEND_TO_ALL_CONTACTS' => array(
        'width' => '10',
        'label' => 'LBL_SEND_TO_ALL_CONTACTS'),
    'SEND_BY_EMAIL' => array(
        'width' => '10',
        'label' => 'LBL_SEND_BY_EMAIL'),
    'SEND_BY_SMS' => array(
        'width' => '10',
        'label' => 'LBL_SEND_BY_SMS'),
    'IS_REPEATED' => array(
        'width' => '10',
        'label' => 'LBL_IS_REPEATED'),
    'CREATED_BY_NAME' => array(
        'width' => '10',
        'label' => 'LBL_CREATED'),
    'ASSIGNED_USER_NAME' => array(
        'width' => '5',
        'label' => 'LBL_LIST_ASSIGNED_USER',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => true),
    'MODIFIED_BY_NAME' => array(
        'width' => '5',
        'label' => 'LBL_MODIFIED'),
    'DATE_ENTERED' => array(
        'width' => '10',
        'label' => 'LBL_DATE_ENTERED',
        'default' => true)
);
