<?php

$viewdefs ['Dispatches'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'DEFAULT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'default' =>
            array(
                array(
                    'name',
                ),
                array(
                    'emailtemplate_name',
                ),
                array(
                    array(
                        'name' => 'send_type'
                    ),
                ),
                array(
                    array(
                        'name' => 'send_date'
                    ),
                ),
                array(
                    array(
                        'name' => 'is_repeated'
                    ),
                ),
                array(
                    array(
                        'name' => 'send_period'
                    ),
                ),
                array(
                    array(
                        'name' => 'send_to_all_contacts'
                    ),
                ),
                array(
                    array(
                        'name' => 'contact_from'
                    ),
                ),
                array(
                    array(
                        'name' => 'send_by_email'
                    ),
                ),
                array(
                    array(
                        'name' => 'send_by_sms'
                    ),
                ),
                array(
                    array(
                        'name' => 'is_active'
                    ),
                ),
            ),
            'LBL_PANEL_ASSIGNMENT' =>
            array(
                array(
                    array(
                        'name' => 'date_modified',
                        'label' => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),
                    array(
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                ),
            ),
        ),
    ),
);
