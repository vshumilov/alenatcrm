<?php

$viewdefs['Dispatches']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'name'
                ),
            ),
            array(
                array(
                    'name' => 'emailtemplate_name'
                ),
            ),
            array(
                array(
                    'name' => 'send_type'
                ),
            ),
            array(
                array(
                    'name' => 'send_date'
                ),
            ),
            array(
                array(
                    'name' => 'is_repeated'
                ),
            ),
            array(
                array(
                    'name' => 'send_period'
                ),
            ),
            array(
                array(
                    'name' => 'send_to_all_contacts'
                ),
            ),
            array(
                array(
                    'name' => 'contact_from'
                ),
            ),
            array(
                array(
                    'name' => 'send_by_email'
                ),
            ),
            array(
                array(
                    'name' => 'send_by_sms'
                ),
            ),
            array(
                array(
                    'name' => 'is_active'
                ),
            ),
        ),
    )
);
