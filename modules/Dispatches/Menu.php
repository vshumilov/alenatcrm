<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Dispatches', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Dispatches&action=EditView&return_module=Dispatches&return_action=DetailView", $mod_strings['LNK_NEW_DISPATCH'], "Create");
}
if (ACLController::checkAccess('Dispatches', 'list', true)) {
    $module_menu[] = Array("index.php?module=Dispatches&action=index&return_module=Dispatches&return_action=DetailView", $mod_strings['LNK_DISPATCH_LIST'], "List");
}
if (ACLController::checkAccess('Dispatches', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Dispatches&return_module=Dispatches&return_action=index", $mod_strings['LNK_IMPORT_DISPATCHES'], "Import");
}