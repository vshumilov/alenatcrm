<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Варианты питания',
    'LBL_MODULE_TITLE' => 'Варианты питания - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск вариантов питания',
    'LBL_VIEW_FORM_TITLE' => 'Обзор вариантов питания',
    'LBL_LIST_FORM_TITLE' => 'Список вариантов питания',
    'LBL_FOODOPTION_NAME' => 'Название варианта питания:',
    'LBL_FOODOPTION' => 'Варианты питания:',
    'LBL_NAME' => 'Название варианта питания',
    'UPDATE' => 'Варианты питания - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся варианта питания',
    'MSG_DUPLICATE' => 'Создаваемая вами варианта питания возможно дублирует уже имеющийся варианта питания. Варианты питания, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового варианта питания или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать вариант питания',
    'LNK_NEW_FOODOPTION' => 'Создать вариант питания',
    'LNK_FOODOPTION_LIST' => 'Варианты питания',
    'ERR_DELETE_RECORD' => 'Перед удалением варианта питания должен быть определён номер записи.',
    'FOODOPTION_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный варианта питания из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Варианты питания',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_FOODOPTIONS' => 'Импорт вариантов питания',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
    'LBL_IS_FOR_CHILDREN' => 'Для детей'
);

$mod_strings['LBL_DESCRIPTION'] = 'Название на английском';
