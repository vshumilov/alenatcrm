<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Foodoption'] = array('table' => 'foodoptions', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Foodoptions',
    'fields' => array(
        'is_for_children' =>
        array(
            'name' => 'is_for_children',
            'vname' => 'LBL_IS_FOR_CHILDREN',
            'type' => 'bool',
            'default' => '0',
            'audited' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_foodoption_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_foodoption_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_foodoption_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Foodoptions', 'Foodoption', array('default', 'assignable',
));

$dictionary['Foodoption']['fields']['description']['type'] = 'varchar';
