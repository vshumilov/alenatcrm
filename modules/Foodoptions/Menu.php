<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Foodoptions', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Foodoptions&action=EditView&return_module=Foodoptions&return_action=DetailView", $mod_strings['LNK_NEW_FOODOPTION'], "Create");
}
if (ACLController::checkAccess('Foodoptions', 'list', true)) {
    $module_menu[] = Array("index.php?module=Foodoptions&action=index&return_module=Foodoptions&return_action=DetailView", $mod_strings['LNK_FOODOPTION_LIST'], "List");
}
if (ACLController::checkAccess('Foodoptions', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Foodoptions&return_module=Foodoptions&return_action=index", $mod_strings['LNK_IMPORT_FOODOPTIONS'], "Import");
}