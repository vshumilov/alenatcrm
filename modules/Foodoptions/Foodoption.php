<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Foodoption extends Basic
{
    public $table_name = "foodoptions";
    public $module_dir = "Foodoptions";
    public $object_name = "Foodoption";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            foodoptions
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
