<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Belong extends Basic
{

    public $table_name = "belongs";
    public $module_dir = "Belongs";
    public $object_name = "Belong";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            belongs
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
