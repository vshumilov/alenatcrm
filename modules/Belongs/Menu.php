<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Belongs', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Belongs&action=EditView&return_module=Belongs&return_action=DetailView", $mod_strings['LNK_NEW_BELONG'], "Create");
}
if (ACLController::checkAccess('Belongs', 'list', true)) {
    $module_menu[] = Array("index.php?module=Belongs&action=index&return_module=Belongs&return_action=DetailView", $mod_strings['LNK_BELONG_LIST'], "List");
}
if (ACLController::checkAccess('Belongs', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Belongs&return_module=Belongs&return_action=index", $mod_strings['LNK_IMPORT_BELONGS'], "Import");
}