<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Entertainmenttype extends Basic
{

    public $table_name = "entertainmenttypes";
    public $module_dir = "Entertainmenttypes";
    public $object_name = "Entertainmenttype";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            entertainmenttypes
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
