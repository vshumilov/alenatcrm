<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Entertainmenttypes', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Entertainmenttypes&action=EditView&return_module=Entertainmenttypes&return_action=DetailView", $mod_strings['LNK_NEW_ENTERTAINMENTTYPE'], "Create");
}
if (ACLController::checkAccess('Entertainmenttypes', 'list', true)) {
    $module_menu[] = Array("index.php?module=Entertainmenttypes&action=index&return_module=Entertainmenttypes&return_action=DetailView", $mod_strings['LNK_ENTERTAINMENTTYPE_LIST'], "List");
}
if (ACLController::checkAccess('Entertainmenttypes', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Entertainmenttypes&return_module=Entertainmenttypes&return_action=index", $mod_strings['LNK_IMPORT_ENTERTAINMENTTYPES'], "Import");
}