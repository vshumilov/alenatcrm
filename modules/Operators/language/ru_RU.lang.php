<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Операторы',
    'LBL_MODULE_TITLE' => 'Операторы - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск операторов',
    'LBL_VIEW_FORM_TITLE' => 'Обзор операторов',
    'LBL_LIST_FORM_TITLE' => 'Список операторов',
    'LBL_OPERATOR_NAME' => 'Название оператора:',
    'LBL_OPERATOR' => 'Оператор:',
    'LBL_NAME' => 'Название оператора',
    'UPDATE' => 'Операторы - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся операторы',
    'MSG_DUPLICATE' => 'Создаваемая вами операторы возможно дублирует уже имеющийся оператора. Операторы, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового операторы или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать оператора',
    'LNK_NEW_OPERATOR' => 'Создать оператора',
    'LNK_OPERATOR_LIST' => 'Операторы',
    'ERR_DELETE_RECORD' => 'Перед удалением операторы должен быть определён номер записи.',
    'OPERATOR_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный оператора из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Операторы',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_OPERATORS' => 'Импорт оператор',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
    'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Абоненты'
);


