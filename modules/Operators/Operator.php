<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Operator extends Basic
{

    public $table_name = "operators";
    public $module_dir = "Operators";
    public $object_name = "Operator";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

}
