<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Operators', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Operators&action=EditView&return_module=Operators&return_action=DetailView", $mod_strings['LNK_NEW_OPERATOR'], "Create");
}
if (ACLController::checkAccess('Operators', 'list', true)) {
    $module_menu[] = Array("index.php?module=Operators&action=index&return_module=Operators&return_action=DetailView", $mod_strings['LNK_OPERATOR_LIST'], "List");
}
if (ACLController::checkAccess('Operators', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Operators&return_module=Operators&return_action=index", $mod_strings['LNK_IMPORT_OPERATORS'], "Import");
}