<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Operator'] = array('table' => 'operators', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Operators',
    'fields' => array(
        'tours' =>
        array(
            'name' => 'tours',
            'type' => 'link',
            'relationship' => 'operators_tours',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Tours',
            'bean_name' => 'Tour',
            'vname' => 'LBL_TOURS',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_operator_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_operator_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_operator_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Operators', 'Operator', array('default', 'assignable',
));
