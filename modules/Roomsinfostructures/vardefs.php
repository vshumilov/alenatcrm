<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Roomsinfostructure'] = array('table' => 'roomsinfostructures', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Roomsinfostructures',
    'fields' => array(
        'is_for_children' =>
        array(
            'name' => 'is_for_children',
            'vname' => 'LBL_IS_FOR_CHILDREN',
            'type' => 'bool',
            'default' => '0',
            'audited' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_roomsinfostructure_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_roomsinfostructure_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_roomsinfostructure_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Roomsinfostructures', 'Roomsinfostructure', array('default', 'assignable',
));

$dictionary['Roomsinfostructure']['fields']['description']['type'] = 'varchar';
