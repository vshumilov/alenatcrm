<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Инфроструктура номеров',
    'LBL_MODULE_TITLE' => 'Инфроструктура номеров - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск инфроструктуры номеров',
    'LBL_VIEW_FORM_TITLE' => 'Обзор инфроструктуры номеров',
    'LBL_LIST_FORM_TITLE' => 'Список инфроструктуры номеров',
    'LBL_ROOMSINFOSTRUCTURE_NAME' => 'Название инфроструктуры номеров:',
    'LBL_ROOMSINFOSTRUCTURE' => 'Инфроструктура номеров:',
    'LBL_NAME' => 'Название инфроструктуры номеров',
    'UPDATE' => 'Инфроструктура номеров - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся инфроструктуры номеров',
    'MSG_DUPLICATE' => 'Создаваемая вами инфроструктуры номеров возможно дублирует уже имеющийся инфроструктуры номеров. Инфроструктура номеров, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового инфроструктуры номеров или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать инфроструктуру номеров',
    'LNK_NEW_ROOMSINFOSTRUCTURE' => 'Создать инфроструктуру номеров',
    'LNK_ROOMSINFOSTRUCTURE_LIST' => 'Инфроструктура номеров',
    'ERR_DELETE_RECORD' => 'Перед удалением инфроструктуры номеров должен быть определён номер записи.',
    'ROOMSINFOSTRUCTURE_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный инфроструктуры номеров из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Инфроструктура номеров',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_ROOMSINFOSTRUCTURES' => 'Импорт инфроструктуры номеров',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
    'LBL_IS_FOR_CHILDREN' => 'Для детей'
);

$mod_strings['LBL_DESCRIPTION'] = 'Название на английском';


