<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Roomsinfostructure extends Basic
{
    public $table_name = "roomsinfostructures";
    public $module_dir = "Roomsinfostructures";
    public $object_name = "Roomsinfostructure";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            roomsinfostructures
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
    
    public static function getListForChildren() {
        $sql = "
        SELECT
            id,
            name
        FROM
            roomsinfostructures
        WHERE
            deleted = '0' AND
            is_for_children = '1'
        ";
        
        return dbGetAssocArray($sql);
    }
}
