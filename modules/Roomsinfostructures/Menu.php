<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Roomsinfostructures', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Roomsinfostructures&action=EditView&return_module=Roomsinfostructures&return_action=DetailView", $mod_strings['LNK_NEW_ROOMSINFOSTRUCTURE'], "Create");
}
if (ACLController::checkAccess('Roomsinfostructures', 'list', true)) {
    $module_menu[] = Array("index.php?module=Roomsinfostructures&action=index&return_module=Roomsinfostructures&return_action=DetailView", $mod_strings['LNK_ROOMSINFOSTRUCTURE_LIST'], "List");
}
if (ACLController::checkAccess('Roomsinfostructures', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Roomsinfostructures&return_module=Roomsinfostructures&return_action=index", $mod_strings['LNK_IMPORT_ROOMSINFOSTRUCTURES'], "Import");
}