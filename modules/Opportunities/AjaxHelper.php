<?php

require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';
require_once 'custom/modules/Dispatches/BeanHelper/Dispatch.php';
require_once 'custom/include/Ajax/AjaxHelper.php';
require_once 'custom/modules/Dispatches/BeanHelper/Dispatch.php';
require_once 'custom/include/Nit/Helper/Request.php';
require_once 'custom/include/Nit/Helper/Guid.php';
require_once 'custom/include/Ajax/AjaxHelper.php';
require_once 'modules/Opportunities/Opportunity.php';
require_once 'modules/Dispatches/Dispatch.php';

class OpportunityAjaxHelper extends AjaxHelper
{

    public function getById()
    {

        $opportunityId = HelperRequest::processReqStr($_REQUEST['params']['opportunity_id']);
        $onlyShortData = HelperRequest::processReqStr($_REQUEST['params']['only_short_data']);
        $code = HelperRequest::processReqStr($_REQUEST['params']['code']);

        $beanHelper = new OpportunityBeanHelperOpportunity($this->_bean);
        $data = $beanHelper->getByIdWithCode($opportunityId, $code, $onlyShortData);

        return $data;
    }

    public function checkCode()
    {
        $opportunityId = HelperRequest::processReqStr($_REQUEST['params']['opportunity_id']);
        $phone = HelperRequest::processReqStr($_REQUEST['params']['phone']);
        $code = HelperRequest::processReqStr($_REQUEST['params']['code']);

        $guidHelper = new HelperGuid();

        if (!$guidHelper->validate($opportunityId)) {
            throw new Exception('opportunityId is not valid');
        }

        $opportunity = new Opportunity();
        $opportunity->retrieve($opportunityId);

        if ($opportunity->contacts_phone_mobile != $phone) {
            throw new Exception("check agreement phone error");
        }

        $beanHelper = new DispatchBeanHelperDispatch(new Dispatch());
        $result = $beanHelper->checkSmsCode($code, $opportunity->contacts_phone_mobile);

        if (!empty($result['error'])) {
            throw new Exception("check sms code error");
        }

        return;
    }

    public function getPdfAgreement()
    {
        $opportunityId = HelperRequest::processReqStr($_REQUEST['params']['opportunity_id']);
        $code = HelperRequest::processReqStr($_REQUEST['params']['code']);

        $beanHelper = new OpportunityBeanHelperOpportunity($this->_bean);
        $sugarNitFilePdf = $beanHelper->getPdfAgreementNitFileWithCode($opportunityId, $code);
        
        $pdfFilePath = $sugarNitFilePdf->getFilePath();

        if (empty($pdfFilePath)) {
            throw new Exception("pdf file path is empty");
        }
        
        $data = [];
        $data['file_name'] = $sugarNitFilePdf->name;
        $data['file_base64'] = chunk_split(base64_encode(file_get_contents($pdfFilePath)));

        return $data;
    }

    /* public function create()
      {
      $priceToApartmentId = HelperRequest::processReqStr($_REQUEST['params']['price_to_apartment_id']);
      $dateFrom = HelperRequest::processReqStr($_REQUEST['params']['date_from']);
      $dateTo = HelperRequest::processReqStr($_REQUEST['params']['date_to']);
      $countPlaces = HelperRequest::processReqStr($_REQUEST['params']['count_places']);
      $countPlacesWithTreatment = HelperRequest::processReqStr($_REQUEST['params']['count_places_with_treatment']);
      $lastName = HelperRequest::processReqStr($_REQUEST['params']['last_name']);
      $firstName = HelperRequest::processReqStr($_REQUEST['params']['first_name']);
      $middleName = HelperRequest::processReqStr($_REQUEST['params']['middle_name']);
      $birthDate = HelperRequest::processReqStr($_REQUEST['params']['birth_date']);
      $email = HelperRequest::processReqStr($_REQUEST['params']['email']);
      $phoneMobile = HelperRequest::processReqStr($_REQUEST['params']['phone_mobile']);

      $beanHelper = new OpportunityBeanHelperOpportunity($this->_bean);
      $agreement = $beanHelper->createFromSanatorium(
      $priceToApartmentId, $dateFrom, $dateTo, $countPlaces, $countPlacesWithTreatment, $lastName, $firstName, $middleName, $birthDate, $email, $phoneMobile
      );

      $row = $beanHelper->getPriceAndApartmentIds($priceToApartmentId);

      if (!empty($agreement->system_number)) {
      $dispatchHelper = new DispatchBeanHelperDispatch();
      $dispatchHelper->sendCreateOpportunityEmailAndSms($agreement, $row['price_id'], $row['apartment_id']);
      $dispatchHelper->sendCreateOpportunityManagerEmail($agreement);

      return $agreement->system_number;
      }
      } */
}
