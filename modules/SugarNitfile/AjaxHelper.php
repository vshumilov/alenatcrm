<?php

class SugarNitfileAjaxHelper extends AjaxHelper
{

    protected $_access_types_tist = array(
        'download' => 'view',
        'delete' => 'edit',
        'restore' => 'edit',
    );

    /**
     * Constructor
     * 
     * @param	object	$ajaxManager
     * @param	string	$module
     */
    public function __construct(& $ajaxManager, $module = 'SugarNitfile', $record = null)
    {
        parent::__construct($ajaxManager, $module, $record);
    }

    /**
     * Perfom nitfile file downloading
     * URL: index.php?entryPoint=ajax&module=SugarNitfile&call=download&options[format]=file&options[download]=1&params[id]=xxx-xxx-xxx-xxx
     * 
     * @access	public
     * @param	string	$id
     * @return	boolean
     */
    public function download($id)
    {
        /* @var $nitfile SugarNitfile */
        $nitfile = $this->_bean->retrieve($id);

        if (!$nitfile) {
            $this->setError(translate('ERR_NITFILE_NOFILE'), true);
            return false;
        }
        $file = array(
            'file' => $nitfile->getFilePath(),
            'mime' => $nitfile->file_mime,
            'size' => $nitfile->file_size,
            'name' => $nitfile->file_name,
        );

        return $file;
    }

    /**
     * Perfom nitfile deleting (only mark db record and destroy nitfiles)
     * URL: index.php?entryPoint=ajax&module=SugarNitfile&call=delete&options[format]=json&params[id]=xxx-xxx-xxx-xxx
     * 
     * @access	public
     * @param	string	$id
     * @return	boolean
     */
    public function delete($id)
    {
        $this->_bean->retrieve($id);
        $this->_bean->mark_deleted($id);
        return true;
    }

    /**
     * Perfom nitfile file downloading
     * URL: index.php?entryPoint=ajax&module=SugarNitfile&call=delete&options[format]=json&params[id]=xxx-xxx-xxx-xxx
     * 
     * @access	public
     * @param	string	$id
     * @return	boolean
     */
    public function restore($id)
    {
        $this->_bean->mark_undeleted($id);
        if (!$this->_bean->retrieve($id)) {
            $this->setError(translate('ERR_NITFILE_RESTORE_FAILED'), true);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Return thumbnail image of nitfile file.
     * @param	mixed	$format, @see SugarNitfile::createThumbnail
     * @return	array
     */
    public function getThumbnail($format)
    {
        try {
            $file_path = $this->_bean->createThumbnail($format);
            if (!$file_path) {
                $this->setError(translate('ERR_FAILED_TO_CREATE_THUMBNAIL', $this->_module), true);
                return null;
            } else {
                $filemtime = filemtime($file_path);
                $modified_since = $this->ajaxManagerCall('getHeader', 'If-Modified-Since');
                if ($_SERVER['HTTP_IF_MODIFIED_SINCE'] !== null) {
                    $modified_since = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
                    if ($modified_since <= $filemtime) {
                        header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0') . ' 304 Not Modified');
                        die();
                    }
                }
                $this->ajaxManagerCall('setHeader', 'Cache-Control', 'public');
                $this->ajaxManagerCall('setHeader', 'Expires', date(DATE_RFC1123, time() + 86400 * 30));
                $this->ajaxManagerCall('setHeader', 'Last-Modified', date(DATE_RFC1123, $filemtime));
                return array(
                    'file' => $file_path,
                    'mime' => $this->_bean->file_mime,
                    'name' => $this->_bean->file_name,
                );
            }
        } catch (Exception $ex) {
            LoggerManager::getLogger()->fatal($ex->getMessage());
        }
    }

}
