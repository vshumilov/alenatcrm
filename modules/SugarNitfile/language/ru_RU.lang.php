<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Nitfiles',
    'LBL_MODULE_TITLE' => 'Nitfiles: Home',
    'LBL_SEARCH_FORM_TITLE' => 'Nitfiles Search',
    'LBL_LIST_FORM_TITLE' => 'Nitfiles List',
    'LBL_SHARED' => 'Открытое',
    'LBL_BEAN_TYPE' => 'Тип связанного объекта',
    'LBL_BEAN_ID' => 'ID Связанного объекта',
    'LBL_BEAN_NAME' => 'Связанный объект',
    'LBL_BEAN_FIELD' => 'Поле связанного объекта',
    'LBL_FILE_NAME' => 'Имя файла',
    'LBL_FILE_EXT' => 'Расширение файла',
    'LBL_FILE_MIME' => 'MIME-тип файла',
    'LBL_FILE_SIZE' => 'Размер файла',
    'LBL_FILE_THUMBNAIL' => 'Предпросмотр доступен',
    'LBL_FILE_URL' => 'Ссылка для загрузки',
    'LBL_FILE_THUMBNAIL_URL' => 'Предпросомтр',
    'LBL_DOCUMENTTEMPLATE_LINK' => 'Шаблон документа (link)',
    'LBL_DOCUMENTTEMPLATE_ID' => 'Шаблон документа (ID)',
    'LBL_DOCUMENTTEMPLATE_NAME' => 'Шаблон документа',
    'ERR_FAILED_TO_CREATE_THUMBNAIL' => 'Ошибка создания миниатюры',
);
