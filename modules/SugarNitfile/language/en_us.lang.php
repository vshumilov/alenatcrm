<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Nitfiles',
    'LBL_MODULE_TITLE' => 'Nitfiles: Home',
    'LBL_SEARCH_FORM_TITLE' => 'Nitfiles Search',
    'LBL_LIST_FORM_TITLE' => 'Nitfiles List',
    'LBL_SHARED' => 'Shared',
    'LBL_BEAN_TYPE' => 'Related bean type',
    'LBL_BEAN_ID' => 'Related bean ID',
    'LBL_BEAN_NAME' => 'Related bean',
    'LBL_BEAN_FIELD' => 'Field of related bean',
    'LBL_FILE_NAME' => 'File name',
    'LBL_FILE_EXT' => 'File extenxtion',
    'LBL_FILE_MIME' => 'File MIME',
    'LBL_FILE_SIZE' => 'File size',
    'LBL_FILE_THUMBNAIL' => 'Thumbnails available',
    'LBL_FILE_URL' => 'Download url',
    'LBL_FILE_THUMBNAIL_URL' => 'Thumbnail',
    'LBL_DOCUMENTTEMPLATE_LINK' => 'Document template (link)',
    'LBL_DOCUMENTTEMPLATE_ID' => 'Document template (ID)',
    'LBL_DOCUMENTTEMPLATE_NAME' => 'Document template',
    'ERR_FAILED_TO_CREATE_THUMBNAIL' => 'Thumbnail creation error',
);
