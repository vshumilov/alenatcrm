<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
if (!defined('VEDISOFT_CUSTOM_INCLUDE_LOADED') || !VEDISOFT_CUSTOM_INCLUDE_LOADED)
    die('Please check file custom/include/custom_utils.php');

require_once('data/SugarBean.php');

class SugarNitfile extends SugarBean
{

    const THUMBNAIL_QUALITY_DEFAULT = 70;
    const THUMBNAIL_FORMAT_DEFAULT = 'jpeg';
    const THUMBNAIL_CROP_DEFAULT = true;

    // SugarNitfile service fields
    public $table_name = 'sugar_nitfiles';
    public $object_name = 'SugarNitfile';
    public $module_dir = 'SugarNitfile';
    public $importable = false;
    public $bean_field_def = null;
    // SugarNitfile bean fields
    public $bean_type;
    public $bean_id;
    public $bean_field;
    public $bean_name;
    public $file_name;
    public $file_ext;
    public $file_mime;
    public $file_size;
    public $file_thumbnail;
    // the same as at parent bean
    public $assigned_user_id;
    // always current user
    public $created_by;
    protected $related_bean;
    // filesystem variables
    static $FS_MOD = 0744;
    static $FS_ROOT = null;
    static $FS_PDF_ROOT = null;
    static $FS_THUMB_ROOT = null;
    // 
    static $EXT_TO_MIME = null;
    static $THUMBNAIL_FORMATS = null;
    static $THUMBNAIL_SUPPORTED_TYPES = null;

    /**
     * Returns bean, nitfile relates with.
     * On fail return FALSE
     * 
     * @access	public
     * @param	boolean	$refresh
     * @param	boolean	$bean
     * @return	mixed
     */
    function & getRelatedBean($refresh = false, & $bean = null)
    {
        if (!empty($bean)) {
            $this->related_bean = $bean;
        } elseif ((!isset($this->related_bean) || $refresh) &&
                (!empty($this->bean_id) && !empty($this->bean_type))) {
            $this->related_bean = loadBean($this->bean_type);
            if ($this->related_bean) {
                $this->related_bean->retrieve($this->bean_id, true, false);
            } else {
                $this->related_bean = null;
            }
        }
        return $this->related_bean;
    }

    /**
     * Access rules defined by object nitfile relate with
     * 
     * (non-PHPdoc)
     * @see SugarBean::UserACLAccess()
     */
    function UserACLAccess($user, $view, $permission = self::PERMISSION_NOT_SET)
    {
        switch (strtolower($view)) {
            case 'list':
            case 'index':
            case 'listview':
            case 'edit':
            case 'save':
            case 'popupeditview':
            case 'editview':
            case 'view':
            case 'detail':
            case 'detailview':
                $related_bean = $this->getRelatedBean();
                $return = (empty($related_bean) ? parent::UserACLAccess($user, $view, $permission) : $related_bean->UserACLAccess($user, $view, self::PERMISSION_NOT_SET));
                break;
            case 'delete':
                $related_bean = $this->getRelatedBean();
                $return = (empty($related_bean) ? parent::UserACLAccess($user, $view, $permission) : $related_bean->UserACLAccess($user, 'edit', self::PERMISSION_NOT_SET));
                break;
            case 'export':
            case 'import':
                $return = false;
                break;
        }
        return $return;
    }

    public function mark_deleted($id)
    {
        $this->deleteFile();

        return parent::mark_deleted($id);
    }

    public function fill_in_additional_parent_fields()
    {
        parent::fill_in_additional_parent_fields();
        if (!empty($this->bean_type) &&
                !empty($this->bean_id) &&
                empty($this->bean_name)) {
            if (($bean = loadBean($this->bean_type)) &&
                    ($bean = $bean->retrieve($this->bean_id))) {
                $this->bean_name = $bean->name;
            }
        }
    }

    /**
     * Set errors.
     * Write errors to sugar log (fatal level),
     * also add errors to custom object GLOBAL_ERROR
     * 
     * @access	public
     * @param	string	$error
     * @param	boolean	$fatal
     * @return	void
     */
    public function setError($error, $fatal = false)
    {
        if (class_exists('GLOBAL_ERRORS'))
            GLOBAL_ERRORS::set($error, 'SugarNitfile');
        $GLOBALS['log']->fatal($error);
        if ($fatal)
            die($error);
    }

    /**
     * Returns errors
     * 
     * @access	public
     * @return	array
     */
    public static function getErrors()
    {
        if (class_exists('GLOBAL_ERRORS'))
            return GLOBAL_ERRORS::get('SugarNitfile', true);
        return false;
    }

    /**
     * Save files to sugar nitfile folder
     * 
     * @access	public
     * @param	string	$source source file name
     * @param	boolean	$is_file_upload
     * @return	void
     */
    public function moveFile($source, $is_file_upload = false)
    {
        if (!file_exists($source)) {
            throw new Vedisoft_Exception('ERR_NITFILE_SOURCE_FILE_NOT_EXIST', array(
        'source' => $source
            ));
        }

        $destination = $this->getFilePath();
        $destination_dir = dirname($destination);
        // check directory exists and try to create if not
        if (!is_dir($destination_dir) &&
                !mkdir(dirname($destination), self::$FS_MOD, true)) {
            throw new Exception('ERROR. Can\'t create folder to store nitfiles.' . $destination);
        }
        if ($is_file_upload) {
            $success = move_uploaded_file($source, $destination) &&
                    chmod($destination, self::$FS_MOD);
        } else {
            $success = copy($source, $destination) &&
                    chmod($destination, self::$FS_MOD);
        }

        return $success;
    }

    /**
     * Tell us whether nitfile supports thumbnail or not
     * @return boolean
     */
    public function supportsThumbnail()
    {
        return (
                isset(self::$THUMBNAIL_SUPPORTED_TYPES[$this->file_mime]) ||
                in_array($this->file_ext, self::$THUMBNAIL_SUPPORTED_TYPES)
                );
    }

    /**
     * Creates thumbnail of nitfile file
     * Format of thumbnail defined by $format param, which can be presented as 
     *  - string - name of thumbnail format
     *  - array - full thumbnail format description with fields:
     *    - integer	width, width of thumbnail image
     *    - integer	height, width of thumbnail image
     *    - string	format, width of thumbnail image (jpeg, png etc.)
     *    - integer	quality, quality of compression 1 to 100, greater quality results finest image
     *    - boolean	crop, use crip or not?
     * 
     * @access	public
     * @param	mixed	$format
     * @param	boolean	$force
     * @return	string file name or null on fail
     */
    public function createThumbnail($format, $force = false)
    {
        if (!$this->supportsThumbnail()) {
            throw new Vedisoft_Exception(
            'ERR_NITFILE_UNSUPPORTED_NITFILE_TYPE', array('file_mime' => $this->file_mime)
            );
        }
        if (is_array($format)) {
            $formats = self::composeThumbnailFormats(array('dynamic' => $format), strtolower($this->file_ext));
            $format_key = 'dynamic';
        } else {
            $formats = $this->getThumbnailFormats();
            $format_key = $format;
        }
        if (!isset($formats[$format_key])) {
            throw new Vedisoft_Exception(
            'ERR_NITFILE_INVALID_THUMBNAIL_FORMAT', array('format' => var_export($format, true))
            );
        }
        $format = $formats[$format_key];
        $thumbnail_file = $this->getThumbnailPath($format);
        if ($force || !file_exists($thumbnail_file)) {
            if (!class_exists('Imagick')) {
                throw new Vedisoft_Exception('ERR_NITFILE_IMAGE_EXTENTION_NOT_INSTALLED');
            }
            $source_file = $this->getFilePath();
            if (!file_exists($source_file) || !is_readable($source_file)) {
                throw new Vedisoft_Exception('ERR_NITFILE_IMAGE_EXTENTION_NOT_INSTALLED');
            }
            $image = new Imagick(str_replace('\\', '/', $this->getFilePath()));
            if (!empty($format['crop'])) {
                $image->cropThumbnailImage($format['width'], $format['height']);
            } else {
                if (!empty($format['fill']))
                    $image->setImageBackgroundColor($format['fill']);
                $image->scaleImage($format['width'], $format['height'], true);

                $offset = array(
                    'x' => floor(($format['width'] - $image->getImageWidth()) / 2),
                    'y' => floor(($format['height'] - $image->getImageHeight()) / 2),
                );

                // @HACK in some cases offset for extent function may be negated
                // i.e. on Centos 6.4 with imagick module version 2.2.2 and ImageMagick version	6.5.4-7
                if (SugarConfig::get('negate_imagick_extent_offset'))
                    $offset = array_map(function($el) {
                        return -1 * $el;
                    }, $offset);

                $image->extentImage($format['width'], $format['height'], $offset['x'], $offset['y']);
            }
            $image->setImageFormat($format['format']);
            $image->setImageCompressionQuality($format['quality']);
            if (!$image->writeImage($thumbnail_file)) {
                throw new Vedisoft_Exception('ERR_NITFILE_IMAGE_WRITE_ERROR');
            }
        }
        return $thumbnail_file;
    }

    /**
     * Return thumbnail formats for bean
     * @return	array
     */
    public function getThumbnailFormats()
    {
        $thumbnail_formats = null;
        if (!empty($this->bean_type) && !empty($this->bean_field)) {
            $related_bean = isset($this->related_bean) && ($this->related_bean instanceof SugarBean) ? $this->related_bean : loadBean($this->bean_type);
            $field_def = $related_bean->getFieldDefinition($this->bean_field);
            if (isset($field_def['thumbnail_formats'])) {
                $thumbnail_formats = $field_def['thumbnail_formats'];
            }
        }
        return self::composeThumbnailFormats(isset($thumbnail_formats) ? $thumbnail_formats : self::$THUMBNAIL_FORMATS, strtolower($this->file_ext));
    }

    /**
     * Compose thumbnail format lis from $formats
     * @param	array	$formats
     * @return	array
     */
    public static function composeThumbnailFormats($formats, $file_format = null)
    {
        $composed_formats = array();
        foreach ($formats as $name => $description) {
            // try to identify format description from default list of prevew formats
            if (is_string($description) && isset(self::$THUMBNAIL_FORMATS[$description]))
                $description = self::$THUMBNAIL_FORMATS[$description];
            elseif (is_string($description) && isset(self::$THUMBNAIL_FORMATS[$name]))
                $description = self::$THUMBNAIL_FORMATS[$name];

            // skip bad formats
            if (!is_array($description) || !isset($description['width']) || !isset($description['height']))
                continue;

            // set default values
            if (!isset($description['quality']))
                $description['quality'] = self::THUMBNAIL_QUALITY_DEFAULT;

            if (!isset($description['format']))
                $description['format'] = !isset($file_format) ? self::THUMBNAIL_FORMAT_DEFAULT : $file_format;

            if (!isset($description['crop']))
                $description['crop'] = $description['width'] != 0 && $description['height'] != 0 ? self::THUMBNAIL_CROP_DEFAULT : false;

            $composed_formats[$name] = $description;
        }
        return $composed_formats;
    }

    /**
     * Set bean related bean information
     * 
     * @access	public
     * @param	object	$bean
     * @return	boolean
     */
    public function setBean(SugarBean $bean, $field = null)
    {
        $beanField = $this->getBeanField($bean, $field);

        $this->assigned_user_id = $bean->created_by;
        $this->bean_id = $bean->id;
        $this->bean_type = $bean->module_dir;
        $this->bean_field = $beanField;

        if (!empty($bean->assigned_user_id)) {
            $this->assigned_user_id = $bean->assigned_user_id;
        }

        return true;
    }

    protected function getBeanField(SugarBean $bean, $field)
    {
        $beanField = self::getBeanFieldDeinition($bean, $field, 'name');

        if (empty($beanField)) {
            throw new Exception(sprintf('ERROR. Call %s with wrong $field param. $field = %s', array(__METHOD__, $beanField)));
        }

        return $beanField;
    }

    /**
     * Perfoms uploaded file processing
     * 
     * @access	public
     * @param	array	$file array with same structure as element of $_FILE array
     * @param	boolean	$is_file_upload, if true check upload file errors, if false do not
     * @return	boolean
     */
    public function setFile($file = null, $is_file_upload = false)
    {
        global $sugar_config;

        $error = false;

        if (empty($this->id)) {
            throw new Vedisoft_Exception('ERR_NITFILE_OBJECT_NOT_FOUND');
        } elseif (empty($file)) {
            throw new Vedisoft_Exception('ERR_NITFILE_FILE_NOT_GIVEN');
        } elseif (!is_writable($sugar_config[$this->getUploadDir()])) {
            throw new Vedisoft_Exception(
            'ERR_NITFILE_HAVE_NOT_RIGHTS_TO_WRITE_IN_DERICTORY', array('folder' => $sugar_config[$this->getUploadDir()])
            );
        } elseif ($is_file_upload && isset($file['error']) && $file['error'] != UPLOAD_ERR_OK) {
            throw new Vedisoft_Exception("ERR_NITFILE_" . $file['error']);
        }

        $this->moveFile($file['tmp_name'], $is_file_upload);

        // sets file attributes
        if (empty($this->name)) {
            $this->name = $file['name'];
        }

        $this->file_name = $file['name'];
        $this->file_mime = $file['type'];
        $this->file_size = $file['size'];

        if (empty($this->file_mime) || $this->file_mime == 'application/octet-stream') {
            $new_mime = $this->getMIMEByFile($this->getFilePath());
            if (empty($new_mime)) {
                $new_mime = $this->getMIMEByEXT($this->file_name);
            }
            $this->file_mime = $new_mime ? $new_mime : 'application/octet-stream';
        }

        $this->file_ext = $this->getEXT();

        if (!empty($this->file_ext)) {
            $this->file_name = preg_match('/\.[^.]*?$/', $this->file_name) ? preg_replace('/\.[^.]*?$/', '', $this->file_name) . '.' . $this->file_ext : $this->file_name . '.' . $this->file_ext;
        }

        // set thumbnail support
        $this->file_thumbnail = $this->supportsThumbnail();

        return true;
    }

    /**
     * FS hepler method. Returns root directory of SugarAttachemnts files storage
     * 
     * @return	string
     */
    public function getFilesDir()
    {
        if (!isset(self::$FS_ROOT)) {
            self::$FS_ROOT = SugarConfig::getInstance()->get($this->getUploadDir()) . 'nitfiles';
            if (!is_dir(self::$FS_ROOT)) {
                mkdir(self::$FS_ROOT, self::$FS_MOD, true);
            }
        }

        return self::$FS_ROOT;
    }
    
    public function getPdfFilesDir()
    {
        if (!isset(self::$FS_PDF_ROOT)) {
            self::$FS_PDF_ROOT = SugarConfig::getInstance()->get($this->getUploadDir()) . 'pdfs';
            if (!is_dir(self::$FS_PDF_ROOT)) {
                mkdir(self::$FS_PDF_ROOT, self::$FS_MOD, true);
            }
        }

        return self::$FS_PDF_ROOT;
    }
    
    protected function getUploadDir()
    {
        if ($this->isPublicFile()) {
            return 'public_upload_dir';
        }
        
        return 'upload_dir';
    }

    protected function isPublicFile()
    {
        $fieldDef = $this->getBeanFieldDef();

        if (!empty($fieldDef['is_public'])) {
            return true;
        }
        
        return false;
    }

    protected function getBeanFieldDef()
    {
        if (empty($this->bean_type)) {
            return;
        }

        $bean = BeanFactory::getBean($this->bean_type);

        $fieldDefs = $bean->field_defs;

        foreach ($fieldDefs as $fieldDef) {
            if ($fieldDef['name'] == $this->bean_field) {
                return $fieldDef;
            }
        }
    }

    /**
     * FS hepler method. Returns root directory of SugarAttachemnts thumbnails files storage
     * 
     * @return	string
     */
    public function getThumbnailDir()
    {
        if (!isset(self::$FS_THUMB_ROOT)) {
            self::$FS_THUMB_ROOT = SugarConfig::getInstance()->get($this->getUploadDir()) . 'thumbs';
            if (!is_dir(self::$FS_THUMB_ROOT)) {
                mkdir(self::$FS_THUMB_ROOT, self::$FS_MOD, true);
            }
        }

        return self::$FS_THUMB_ROOT;
    }

    /**
     * Retunrs nitfile file path
     *
     * @return	string
     */
    public function getFilePath()
    {
        return $this->getFilesDir() . DIRECTORY_SEPARATOR . $this->id;
    }
    
    public function getPdfFilePath()
    {
        return $this->getPdfFilesDir() . DIRECTORY_SEPARATOR . $this->id . '.pdf';
    }
    
    /**
     * Retunrs nitfile thumbnail file path
     * 
     * @param	array	$format thumbnail format description
     * @return	string
     */
    public function getThumbnailPath($format)
    {
        return $this->getThumbnailDir() . DIRECTORY_SEPARATOR . $this->getThumbnailFileName($format);
    }

    public function getThumbnailFileName($format)
    {
        return $this->id . '_' . md5(serialize($format));
    }

    /**
     * Delete files of nitfile
     */
    public function deleteFile()
    {
        $file = $this->getFilePath();
        if (file_exists($file)) {
            unlink($file);
        }
    }

    /**
     * Returns a name of wich would download by user.
     * Generate right name string.
     * Example:
     * self::getDownloadFileName( '/path/файл.ext' );
     * returns 'файл.ext'
     * 
     * @access	public
     * @param	string	$name file name
     * @return	string
     */
    public function getDownloadFileName($name)
    {
        $name = pathinfo($name, PATHINFO_BASENAME);
        // cut logn filenames if remote user works in Windows OS
        if (isset($_REQUEST) &&
                isset($_SERVER['HTTP_USER_AGENT']) &&
                preg_match('/windows/i', $_SERVER['HTTP_USER_AGENT']) &&
                strlen($name) > 227) {
            // max filename lengtn at Windows OS is 227 bytes
            $name = pathinfo($name);
            $ext = $name['extension'];
            $name = $name['basename'];
            $ext_length = strlen($ext);
            if ($ext_length > 0) {
                $name = substr($name, 0, -($ext_length + 1));
                $max_length = 227 - $ext_length - 1;
            } else {
                $max_length = 227;
            }
            while (strlen($name) > $max_length) {
                $name = mb_substr($name, 0, -1, 'UTF-8');
            }
            if ($ext_length > 0) {
                $name = $name . '.' . $ext;
            }
        }
        return $name;
    }

    /**
     * Returns fied definition of passed bean and field.
     * If $attr passed, then returns appropriate param of field definition
     * 
     * @access	public
     * @param	object	$bean
     * @param	string	$field
     * @param	string	$attr
     * @return	mixed
     */
    public static function getBeanFieldDeinition(& $bean, $field, $attr = false)
    {
        $definition = $bean->getFieldDefinition($field);
        $return = $attr ? (isset($definition) && isset($definition[$attr]) ? $definition[$attr] : null) : (isset($definition) ? $definition : null);
        return $return;
    }

    /**
     * Returns associated array of nitfiles related with defined bean.
     * Array structure:
     * array(
     * 		$nitfile->id => $nitfile,
     * 		...
     * );
     * For right nitfile selection we shoud define:
     *  - type of relate dbean
     *  - id of related bean
     *  - name of related bean field, wich nitfiles assign to
     * There are 2 ways of definition these params:
     * 1. Passing to method 2 params ((object)$bean and (string)$field),
     *    In this case we gets type of bean and bean id from $bean object.
     * 2. Passing to method all 3 params ((string)$bean, (string)$field and (string)$extra)
     *    In this case (string)$bean corresponds to bean type,
     *    (string)$field corresponds to bean id and (string)$extra to field name
     * 
     * @access	public
     * @param	SugarBean $bean
     * @param	string	$field
     * @return	array
     */
    public static function getBeanNitfiles(SugarBean $bean, $field)
    {
        if (empty($bean) || empty($field)) {
            return array();
        }

        $nitfile = new SugarNitfile();
        $bean_field = $nitfile->getBeanField($bean, $field);

        $sql = 'SELECT `id` FROM `sugar_nitfiles` ' .
                'WHERE ' .
                '`bean_type`=' . dbQuote($bean->module_dir) . ' AND ' .
                '`bean_id`=' . dbQuote($bean->id) . ' AND ' .
                '`bean_field`=' . dbQuote($bean_field) . ' AND ' .
                '`deleted`=0 ' .
                'ORDER BY `file_thumbnail` DESC, `date_entered`';

        $ids = dbGetAssocArray($sql);
        $nitfiles = array();

        if (empty($ids)) {
            return $nitfiles;
        }

        foreach ($ids as $id) {
            $nitfile = new self;
            $nitfile->retrieve($id);
            $nitfiles[$id] = $nitfile;
        }

        return $nitfiles;
    }

    /**
     * Call saveBeanNitfilesField function for each bean nitfile field
     * Adds this method execution after related bean saving
     * (at module controller or toSave.php action file)
     * 
     * @param	SugarBean	$bean
     * @param	array	$fields
     * @return	integer
     */
    public static function saveBeanNitfiles(SugarBean $bean, $fields = null, $is_file_upload = true)
    {
        global $sugar_config;
        global $current_user;

        if (empty($bean)) {
            return 0;
        }
        $saved_nitfiles = array();
        if (!isset($fields)) {
            $fields = array();
            foreach ($bean->getFieldDefinitions() as $field_def) {
                if (isset($field_def['type']) && $field_def['type'] == 'nitfile') {
                    $fields[] = $field_def['name'];
                }
            }
        }
        if (!is_array($fields)) {
            $fields = array($fields);
        }
        // handle saving
        foreach ($fields as $field) {
            // update old bean nitfiles
            $nitfiles = self::getBeanNitfiles($bean, $field);
            foreach ($nitfiles as $nitfile) {
                $nitfile->setBean($bean, $field);
                $nitfile->save(false);
            }
            unset($nitfiles);

            // save new bean nitfiles
            $post_field = self::generateFormFieldName($bean, $field);
            if (isset($_FILES[$post_field]) && count($_FILES[$post_field]['name']) > 0) {
                $files_keys = array_keys($_FILES[$post_field]['name']);
                $success_count = 0;
                foreach ($files_keys as $key) {
                    $file = array(
                        'name' => $_FILES[$post_field]['name'][$key],
                        'type' => $_FILES[$post_field]['type'][$key],
                        'tmp_name' => $_FILES[$post_field]['tmp_name'][$key],
                        'error' => $_FILES[$post_field]['error'][$key],
                        'size' => $_FILES[$post_field]['size'][$key],
                    );
                    if (empty($file['name']) && empty($file['type']) &&
                            empty($file['tmp_name']) &&
                            $file['error'] == 4 && $file['size'] == 0) {
                        // file just was't selected at edit form
                        continue;
                    }
                    
                    $nitfile = self::createBeanNitfile($bean, $field, $file, $is_file_upload);

                    if ($nitfile !== false) {
                        $saved_nitfiles[] = $nitfile;
                    }
                }
            }
        }
        return $saved_nitfiles;
    }

    /**
     * Clear bean nitfiles associated with field
     * @param	SugarBean	$bean
     * @param	string	$field
     * @return	void
     */
    public static function clearBeanNitfiles(SugarBean $bean, $field)
    {
        $sql = 'SELECT id ' .
                'FROM sugar_nitfiles ' .
                'WHERE ' .
                'deleted = 0 AND ' .
                'bean_type = ' . dbQuote($bean->module_dir) . ' AND ' .
                'bean_id = ' . dbQuote($bean->id) . ' AND ' .
                'bean_field = ' . dbQuote($field);
        $nitfile_ids = dbGetAssocArray($sql);
        foreach ($nitfile_ids as $id) {
            $nitfile = new self();
            $nitfile->retrieve($id);
            $nitfile->mark_deleted($id);
        }
    }

    /**
     * Creates bean nitfile for $bean->$field, nitfile file description - $file
     * 
     * @param	SugarBean	$bean,	bean nitfile will relate to
     * @param	string	$field,	field of bean, nitfile will associated with
     * @param	array	$file,	file description array
     * @param	boolean	$is_file_upload
     * @return	object	created attachnent, false on fail
     */
    public static function createBeanNitfile(SugarBean $bean, $field, $file, $is_file_upload = false)
    {
        if (!isset($file['tmp_name']) || !isset($file['name'])) {
            return false;
        }

        $nitfile = new self;
        $nitfile->id = create_guid();
        $nitfile->new_with_id = true;

        try {
            $nitfile->setBean($bean, $field);
            $nitfile->setFile($file, $is_file_upload);
            $nitfile->save(false);
            return $nitfile;
        } catch (Exception $ex) {
            $nitfile->deleteFile();
            $nitfile->mark_deleted($nitfile->id);
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * Returns name of form field for nitfile object
     * 
     * @access	public
     * @param	object	$bean
     * @param	string	$field
     * @return	string
     */
    public static function generateFormFieldName(& $bean, $field)
    {
        $name = self::getBeanFieldDeinition($bean, $field, 'name');
        $name = 'nitfile_' . strtolower($bean->module_dir) . '_' . strtolower($name);
        return $name;
    }

    /**
     * Tries to define mime type by file extension string.
     * Returns mime in case of success or false in other case
     * 
     * @access	public
     * @param	string	$ext
     * @return	mixed
     */
    public function getMIMEByEXT($ext)
    {
        $ext = trim($ext);
        return (!empty($ext) && isset(self::$EXT_TO_MIME[$ext]) ? self::$EXT_TO_MIME[$ext] : false);
    }

    /**
     * Tries to define mime type by file name string.
     * Returns mime in case of success or false in other case
     *  
     * @access	public
     * @param	string	$name
     * @return	mixed
     */
    public function getMIMEByFile($file)
    {
        if (file_exists($file)) {
            $file_path = dirname($_SERVER['SCRIPT_FILENAME']);
            $file_path = preg_replace('/[\\\\\/]/', '\\', $file_path);
            $file_path = $file_path . DIRECTORY_SEPARATOR . $file;
            if (version_compare(phpversion(), '5.3.0', '>=')) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $file_path);
            } elseif (function_exists('mime_content_type')) {
                $mime = mime_content_type($file_path);
            } else {
                $mime = false;
            }
        }
        if (empty($mime)) {
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $mime = $this->getMIMEByEXT($ext);
        }
        return $mime;
    }

    /**
     * Tries to define file extension by mime type.
     * Returns extension on success or false
     * 
     * @access	public
     * @param	string	$mime
     * @return	mixed
     */
    public function getEXTByMIME()
    {
        $key = array_search(trim($this->file_mime), self::$EXT_TO_MIME);
        return (!empty($key) ? $key : false);
    }

    /**
     * Tries to define file extension by file name and its mime type.
     * Returns extension on success or false
     * 
     * @access	public
     * @param	string	$name
     * @param	string	$mime
     * @return	mixed
     */
    public function getEXT()
    {
        $ext = pathinfo($this->file_name, PATHINFO_EXTENSION);
        if (empty($ext))
            $ext = $this->getEXTByMIME();
        return $ext;
    }

    /**
     * Loads meta array from external file.
     * Meta variables should be stored at file
     * with same name as variable named, but with '_' prefix.
     * Files must be located at same dir as this file.
     * this methods uses only when SugarNitfile class initialization.
     * 
     * @access	public
     * @param	string	$variable
     * @return	mixed
     */
    public static function getMetaVar($variable = null)
    {
        if (empty($variable))
            return null;

        if (!isset(self::$$variable) && file_exists('modules/SugarNitfile/_' . $variable . '.php')) {
            include('modules/SugarNitfile/_' . $variable . '.php');
            if (isset($$variable))
                self::$$variable = $$variable;
        }
        return isset(self::$$variable) ? self::$$variable : null;
    }

    public static function getInstance()
    {
        static $instance = null;
        if (is_null($instance)) {
            $instance = new SugarNitfile();
        }
        return $instance;
    }

}

SugarNitfile::getMetaVar('EXT_TO_MIME');
SugarNitfile::getMetaVar('THUMBNAIL_FORMATS');
SugarNitfile::getMetaVar('THUMBNAIL_SUPPORTED_TYPES');
