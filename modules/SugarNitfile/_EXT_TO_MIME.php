<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$EXT_TO_MIME = array(
    // A
    'aif' => 'audio/aiff',
    'aiff' => 'audio/aiff',
    'avi' => 'video/msvideo',
    // B
    'bmp' => 'image/bmp',
    // C
    'css' => 'text/css',
    // D
    'doc' => 'application/msword',
    'docx' => 'application/msword',
    //'docx'	=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'docm' => 'application/vnd.ms-word.document.macroEnabled.12',
    'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    'dotm' => 'application/vnd.ms-word.template.macroEnabled.12',
    // E
    // F
    'flv' => 'video/x-flv',
    // G
    'gif' => 'image/gif',
    // H
    'html' => 'text/html',
    'htm' => 'text/html',
    // I
    // J
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'jpe' => 'image/jpeg',
    'js' => 'application/x-javascript',
    'json' => 'application/json',
    // K
    // L
    // M
    'mpeg' => 'video/mpeg',
    'mpg' => 'video/mpeg',
    'mpe' => 'video/mpeg',
    'mp3' => 'audio/mpeg3',
    'mov' => 'video/quicktime',
    // N
    // O
    'odt' => 'application/vnd.oasis.opendocument.text',
    'ott' => 'application/vnd.oasis.opendocument.text-template',
    'oth' => 'application/vnd.oasis.opendocument.text-web',
    'odm' => 'application/vnd.oasis.opendocument.text-master',
    'odg' => 'application/vnd.oasis.opendocument.graphics',
    'otg' => 'application/vnd.oasis.opendocument.graphics-template',
    'odp' => 'application/vnd.oasis.opendocument.presentation',
    'otp' => 'application/vnd.oasis.opendocument.presentation-template',
    'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    'ots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
    'odc' => 'application/vnd.oasis.opendocument.chart',
    'odf' => 'application/vnd.oasis.opendocument.formula',
    'odb' => 'application/vnd.oasis.opendocument.database',
    'odi' => 'application/vnd.oasis.opendocument.image',
    'oxt' => 'application/vnd.openofficeorg.extension',
    'onetoc' => 'application/onenote',
    'onetoc2' => 'application/onenote',
    'onetmp' => 'application/onenote',
    'onepkg' => 'application/onenote',
    // P
    'php' => 'text/html',
    'pdf' => 'application/pdf',
    'png' => 'image/png',
    'ppt' => 'application/vnd.ms-powerpoint',
    'pps' => 'application/vnd.ms-powerpoint',
    'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'pptm' => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
    'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
    'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
    'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
    'potm' => 'application/vnd.ms-powerpoint.template.macroEnabled.12',
    'ppam' => 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
    // Q
    // R
    'rtf' => 'application/rtf',
    // S
    'swf' => 'application/x-shockwave-flash',
    'sldx' => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
    'sldm' => 'application/vnd.ms-powerpoint.slide.macroEnabled.12',
    // T
    'tar' => 'application/x-tar',
    'tiff' => 'image/tiff',
    'txt' => 'text/plain',
    'thmx' => 'application/vnd.ms-officetheme',
    // U
    // V
    // W
    'wav' => 'audio/wav',
    'wmv' => 'video/x-ms-wmv',
    // X
    'xml' => 'application/xml',
    'xls' => 'application/vnd.ms-excel',
    'xlt' => 'application/vnd.ms-excel',
    'xlm' => 'application/vnd.ms-excel',
    'xld' => 'application/vnd.ms-excel',
    'xla' => 'application/vnd.ms-excel',
    'xlc' => 'application/vnd.ms-excel',
    'xlw' => 'application/vnd.ms-excel',
    'xll' => 'application/vnd.ms-excel',
    'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'xlsm' => 'application/vnd.ms-excel.sheet.macroEnabled.12',
    'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
    'xltm' => 'application/vnd.ms-excel.template.macroEnabled.12',
    'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
    'xlam' => 'application/vnd.ms-excel.addin.macroEnabled.12',
    // Y
    // Z
    'zip' => 'application/zip',
);
