<?php

class SugarNitfileAjaxHelperShared extends AjaxHelperShared
{

    /**
     * Constructor
     * 
     * @param	object	$ajaxManager
     * @param	string	$module
     */
    function __construct(& $ajaxManager, $module = 'SugarNitfile', $record = null)
    {
        parent::__construct($ajaxManager, $module, $record);
    }

    /**
     * Perfom nitfile file downloading
     * URL: index.php?entryPoint=ajax&module=SugarNitfile&call=download&options[format]=file&options[force_download]=1&params[id]=xxx-xxx-xxx-xxx
     * 
     * @access	public
     * @param	string	$id
     * @return	boolean
     */
    function download($id)
    {
        $this->_bean->retrieve($id);
        if (!$this->_bean->shared) {
            $this->setError(translate('ERR_NITFILE_ACCESS_DENIED'), true);
            return false;
        }
        $file = array(
            'file' => SugarNitfile::getFilePath($id),
            'mime' => $this->_bean->file_mime,
            'size' => $this->_bean->file_size,
            'name' => $this->_bean->file_name,
        );
        return $file;
    }

}
