<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$THUMBNAIL_SUPPORTED_TYPES = array();
if (class_exists('Imagick')) {
    $THUMBNAIL_SUPPORTED_TYPES['image/gif'] = 'gif';
    $THUMBNAIL_SUPPORTED_TYPES['image/jpg'] = 'jpg';
    $THUMBNAIL_SUPPORTED_TYPES['image/jpeg'] = 'jpg';
    $THUMBNAIL_SUPPORTED_TYPES['image/pjpeg'] = 'jpg';
    $THUMBNAIL_SUPPORTED_TYPES['image/png'] = 'png';
    $THUMBNAIL_SUPPORTED_TYPES['image/x-png'] = 'png';
    $THUMBNAIL_SUPPORTED_TYPES['image/bmp'] = 'bmp';
    $THUMBNAIL_SUPPORTED_TYPES['image/x-ms-bmp'] = 'bmp';
    $THUMBNAIL_SUPPORTED_TYPES['image/x-windows-bmp'] = 'bmp';
}