<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$THUMBNAIL_FORMATS = array(
    'xs' => array('width' => '45', 'height' => '30'),
    's' => array('width' => '60', 'height' => '40'),
    'm' => array('width' => '75', 'height' => '50'),
    'l' => array('width' => '99', 'height' => '66'),
    'xl' => array('width' => '150', 'height' => '100'),
);
