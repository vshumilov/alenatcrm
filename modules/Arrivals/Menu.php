<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Arrivals', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Arrivals&action=EditView&return_module=Arrivals&return_action=DetailView", $mod_strings['LNK_NEW_ARRIVAL'], "Create");
}
if (ACLController::checkAccess('Arrivals', 'list', true)) {
    $module_menu[] = Array("index.php?module=Arrivals&action=index&return_module=Arrivals&return_action=DetailView", $mod_strings['LNK_ARRIVAL_LIST'], "List");
}
if (ACLController::checkAccess('Arrivals', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Arrivals&return_module=Arrivals&return_action=index", $mod_strings['LNK_IMPORT_ARRIVALS'], "Import");
}