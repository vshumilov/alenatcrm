<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Arrival extends Basic
{
    public $table_name = "arrivals";
    public $module_dir = "Arrivals";
    public $object_name = "Arrival";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            arrivals
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}

