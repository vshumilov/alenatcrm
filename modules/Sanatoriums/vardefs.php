<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Sanatorium'] = array('table' => 'sanatoriums', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Sanatoriums',
    'fields' => array(
        'full_name' =>
        array(
            'name' => 'full_name',
            'vname' => 'LBL_FULL_NAME',
            'type' => 'varchar',
            'len' => '255',
            'audited' => true,
            'required' => true,
        ),
        'sales_status' =>
        array(
            'name' => 'sales_status',
            'vname' => 'LBL_SALES_STATUS',
            'type' => 'enum',
            'options' => 'sanatoriums_sales_status_options',
            'default' => 'no_contact',
            'audited' => true,
            'required' => true,
        ),
        'country_name' =>
        array(
            'name' => 'country_name',
            'rname' => 'name',
            'id_name' => 'country_id',
            'vname' => 'LBL_COUNTRY_NAME',
            'type' => 'relate',
            'table' => 'countries',
            'join_name' => 'countries',
            'isnull' => 'true',
            'module' => 'Countries',
            'dbType' => 'varchar',
            'link' => 'countries',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'country_id' =>
        array(
            'name' => 'country_id',
            'vname' => 'LBL_COUNTRY_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'countries' =>
        array(
            'name' => 'countries',
            'type' => 'link',
            'relationship' => 'countries_sanatoriums',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Countries',
            'bean_name' => 'Country',
            'vname' => 'LBL_COUNTRIES',
        ),
        'city_name' =>
        array(
            'name' => 'city_name',
            'rname' => 'name',
            'id_name' => 'city_id',
            'vname' => 'LBL_CITY_NAME',
            'type' => 'relate',
            'table' => 'cities',
            'join_name' => 'cities',
            'isnull' => 'true',
            'module' => 'Cities',
            'dbType' => 'varchar',
            'link' => 'cities',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'city_id' =>
        array(
            'name' => 'city_id',
            'vname' => 'LBL_CITY_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'cities' =>
        array(
            'name' => 'cities',
            'type' => 'link',
            'relationship' => 'cities_sanatoriums',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Cities',
            'bean_name' => 'City',
            'vname' => 'LBL_CITIES',
        ),
        'avatar' =>
        array(
            'name' => 'avatar',
            'vname' => 'LBL_AVATAR',
            'type' => 'nitfile',
            'audited' => true,
            'source' => 'non-db',
            'is_public' => true,
            'thumbnail_formats' => array(
                's' => array('width' => '240', 'height' => '150'),
                'm' => array('width' => '830', 'height' => '500'),
            )
        ),
        'thumbnail_name_s' =>
        array(
            'name' => 'thumbnail_name_s',
            'vname' => 'LBL_THUMBNAIL_NAME_S',
            'type' => 'varchar',
            'audited' => true,
        ),
        'treatment' =>
        array(
            'name' => 'treatment',
            'vname' => 'LBL_TREATMENT',
            'type' => 'multienum',
            'function' => 'Treatment::getList',
            'audited' => true,
            'required' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'suitable' =>
        array(
            'name' => 'suitable',
            'vname' => 'LBL_SUITABLE',
            'type' => 'multienum',
            'function' => 'Suitable::getList',
            'audited' => true,
            'required' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'pricelevel' =>
        array(
            'name' => 'pricelevel',
            'vname' => 'LBL_PRICELEVEL',
            'type' => 'multienum',
            'function' => 'Pricelevel::getList',
            'audited' => true,
            'required' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'arrival' =>
        array(
            'name' => 'arrival',
            'vname' => 'LBL_ARRIVAL',
            'type' => 'multienum',
            'function' => 'Arrival::getList',
            'audited' => true,
            'required' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'child_age' =>
        array(
            'name' => 'child_age',
            'vname' => 'LBL_CHILD_AGE',
            'type' => 'Int',
            'default' => '0',
            'audited' => true,
        ),
        'belongs' =>
        array(
            'name' => 'belongs',
            'vname' => 'LBL_BELONGS',
            'type' => 'enum',
            'function' => 'Belong::getList',
            'audited' => true,
        ),
        'healthprogram' =>
        array(
            'name' => 'healthprogram',
            'vname' => 'LBL_HEALTHPROGRAM',
            'type' => 'multienum',
            'function' => 'Healthprogram::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'beauty' =>
        array(
            'name' => 'beauty',
            'vname' => 'LBL_BEAUTY',
            'type' => 'multienum',
            'function' => 'Beauty::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'foodoption' =>
        array(
            'name' => 'foodoption',
            'vname' => 'LBL_FOODOPTION',
            'type' => 'multienum',
            'function' => 'Foodoption::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'health_child_age' =>
        array(
            'name' => 'health_child_age',
            'vname' => 'LBL_HEALTH_CHILD_AGE',
            'type' => 'Int',
            'default' => '0',
            'audited' => true,
        ),
        'entertainment' =>
        array(
            'name' => 'entertainment',
            'vname' => 'LBL_ENTERTAINMENT',
            'type' => 'multienum',
            'function' => 'Entertainment::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'territory' =>
        array(
            'name' => 'territory',
            'vname' => 'LBL_TERRITORY',
            'type' => 'multienum',
            'function' => 'Territory::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'internet' =>
        array(
            'name' => 'internet',
            'vname' => 'LBL_INTERNET',
            'type' => 'multienum',
            'function' => 'Internet::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'arrival_days' =>
        array(
            'name' => 'arrival_days',
            'vname' => 'LBL_ARRIVAL_DAYS',
            'type' => 'varchar',
            'audited' => true,
        ),
        'rent' =>
        array(
            'name' => 'rent',
            'vname' => 'LBL_RENT',
            'type' => 'multienum',
            'function' => 'Rent::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'sport' =>
        array(
            'name' => 'sport',
            'vname' => 'LBL_SPORT',
            'type' => 'multienum',
            'function' => 'Sport::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
        'price_from' =>
        array(
            'name' => 'price_from',
            'vname' => 'LBL_PRICE_FROM',
            'type' => 'currency',
            'dbType' => 'double',
            'importable' => 'required',
            'duplicate_merge' => '1',
            'options' => 'numeric_range_search_dom',
            'enable_range_search' => true,
        ),
        'currency_id' =>
        array(
            'name' => 'currency_id',
            'type' => 'id',
            'group' => 'currency_id',
            'vname' => 'LBL_CURRENCY',
            'function' => array('name' => 'getCurrencyDropDown', 'returns' => 'html'),
            'reportable' => false,
            'comment' => 'Currency used for display purposes'
        ),
        'currency_name' =>
        array(
            'name' => 'currency_name',
            'rname' => 'name',
            'id_name' => 'currency_id',
            'vname' => 'LBL_CURRENCY_NAME',
            'type' => 'relate',
            'isnull' => 'true',
            'table' => 'currencies',
            'module' => 'Currencies',
            'source' => 'non-db',
            'function' => array('name' => 'getCurrencyNameDropDown', 'returns' => 'html'),
            'studio' => 'false',
            'duplicate_merge' => 'disabled',
        ),
        'currency_symbol' =>
        array(
            'name' => 'currency_symbol',
            'rname' => 'symbol',
            'id_name' => 'currency_id',
            'vname' => 'LBL_CURRENCY_SYMBOL',
            'type' => 'relate',
            'isnull' => 'true',
            'table' => 'currencies',
            'module' => 'Currencies',
            'source' => 'non-db',
            'function' => array('name' => 'getCurrencySymbolDropDown', 'returns' => 'html'),
            'studio' => 'false',
            'duplicate_merge' => 'disabled',
        ),
        'currencies' =>
        array(
            'name' => 'currencies',
            'type' => 'link',
            'relationship' => 'sanatoriums_currencies',
            'source' => 'non-db',
            'vname' => 'LBL_CURRENCIES',
        ),
        'site' =>
        array(
            'name' => 'site',
            'type' => 'url',
            'dbType' => 'varchar',
            'len' => 255,
            'vname' => 'LBL_SITE',
            'audited' => true,
        ),
        'spa_text' =>
        array(
            'name' => 'spa_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_SPA_TEXT',
            'audited' => true,
            'source' => 'non-db',
        ),
        'accommodation_text' =>
        array(
            'name' => 'accommodation_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_ACCOMMODATION_TEXT',
            'audited' => true,
            'source' => 'non-db',
        ),
        'food_text' =>
        array(
            'name' => 'food_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_FOOD_TEXT',
            'audited' => true,
            'source' => 'non-db',
        ),
        'rest_text' =>
        array(
            'name' => 'rest_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_REST_TEXT',
            'audited' => true,
            'source' => 'non-db',
        ),
        'children_text' =>
        array(
            'name' => 'children_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_CHILDREN_TEXT',
            'audited' => true,
            'source' => 'non-db',
        ),
        'note_text' =>
        array(
            'name' => 'note_text',
            'type' => 'tinymce',
            'dbType' => 'html',
            'vname' => 'LBL_NOTE_TEXT',
            'audited' => true,
            'source' => 'non-db',
        ),
        'images' =>
        array(
            'name' => 'images',
            'type' => 'link',
            'relationship' => 'sanatoriums_images',
            'module' => 'Images',
            'bean_name' => 'Image',
            'source' => 'non-db',
        ),
        'prices' =>
        array(
            'name' => 'prices',
            'type' => 'link',
            'relationship' => 'sanatoriums_prices',
            'module' => 'Prices',
            'bean_name' => 'Price',
            'source' => 'non-db',
        ),
        'houses' =>
        array(
            'name' => 'houses',
            'type' => 'link',
            'relationship' => 'sanatoriums_houses',
            'module' => 'Houses',
            'bean_name' => 'House',
            'source' => 'non-db',
        ),
        'apartments' =>
        array(
            'name' => 'apartments',
            'type' => 'link',
            'relationship' => 'sanatoriums_apartments',
            'module' => 'Apartments',
            'bean_name' => 'Apartment',
            'source' => 'non-db',
        ),
        'href_thumbnail_name_s' =>
        array(
            'name' => 'href_thumbnail_name_s',
            'vname' => 'LBL_HREF_THUMBNAIL_NAME_S',
            'type' => 'text',
            'source' => 'non-db',
        ),
        'lat' => 
        array(
            'name' => 'lat',
            'vname' => 'LBL_LAT',
            'type' => 'varhar',
            'dbType' => 'DECIMAL(11, 8)',
            'audited' => true,
        ),
        'lon' => 
        array(
            'name' => 'lon',
            'vname' => 'LBL_LON',
            'type' => 'varhar',
            'dbType' => 'DECIMAL(11, 8)',
            'audited' => true,
        )
    ),
    'indices' => array(
        array(
            'name' => 'idx_sanatorium_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_sanatorium_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_sanatorium_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
        array(
            'name' => 'idx_country_id',
            'type' => 'index',
            'fields' => array('country_id'),
        ),
        array(
            'name' => 'idx_city_id',
            'type' => 'index',
            'fields' => array('city_id'),
        ),
    ),
    'relationships' => array(
        'countries_sanatoriums' =>
        array(
            'lhs_module' => 'Countries',
            'lhs_table' => 'countries',
            'lhs_key' => 'id',
            'rhs_module' => 'Sanatoriums',
            'rhs_table' => 'sanatoriums',
            'rhs_key' => 'country_id',
            'relationship_type' => 'one-to-many'
        ),
        'cities_sanatoriums' =>
        array(
            'lhs_module' => 'Cities',
            'lhs_table' => 'cities',
            'lhs_key' => 'id',
            'rhs_module' => 'Sanatoriums',
            'rhs_table' => 'sanatoriums',
            'rhs_key' => 'city_id',
            'relationship_type' => 'one-to-many'
        ),
        'sanatoriums_currencies' => array(
            'lhs_module' => 'Sanatoriums',
            'lhs_table' => 'sanatoriums',
            'lhs_key' => 'currency_id',
            'rhs_module' => 'Currencies',
            'rhs_table' => 'currencies',
            'rhs_key' => 'id',
            'relationship_type' => 'one-to-many'
        )
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Sanatoriums', 'Sanatorium', array('default', 'assignable',
));

$dictionary['Sanatorium']['fields']['description']['type'] = 'tinymce';
$dictionary['Sanatorium']['fields']['description']['dbType'] = 'html';
$dictionary['Sanatorium']['fields']['description']['source'] = 'non-db';
