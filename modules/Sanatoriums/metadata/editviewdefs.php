<?php

$viewdefs['Sanatoriums']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
        'includes' =>
        array(
            array(
                'file' => 'custom/include/Nit/TinyMce/tinymce.min.js',
            ),
            array(
                'file' => 'custom/include/Nit/TinyMce/themes/mobile/theme.min.js',
            ),
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'name'
                ),
            ),
            array(
                array(
                    'name' => 'full_name'
                ),
            ),
            array(
                array(
                    'name' => 'avatar',
                    'displayParams' => array(
                        'thumbnail' => 's'
                    )
                ),
            ),
            array(
                array(
                    'name' => 'sales_status'
                ),
            ),
            array(
                array(
                    'name' => 'country_name'
                ),
            ),
            array(
                array(
                    'name' => 'city_name'
                ),
            ),
            array(
                array(
                    'name' => 'lat'
                ),
            ),
            array(
                array(
                    'name' => 'lon'
                ),
            ),
            array(
                array(
                    'name' => 'currency_id',
                    'label' => 'LBL_CURRENCY'
                ),
            ),
            array(
                array(
                    'name' => 'price_from'
                ),
            ),
            array(
                array(
                    'name' => 'site'
                ),
            ),
            array(
                array(
                    'name' => 'treatment'
                ),
            ),
            array(
                array(
                    'name' => 'suitable'
                ),
            ),
            array(
                array(
                    'name' => 'pricelevel'
                ),
            ),
            array(
                array(
                    'name' => 'child_age'
                ),
            ),
            array(
                array(
                    'name' => 'belongs'
                ),
            ),
            array(
                array(
                    'name' => 'healthprogram'
                ),
            ),
            array(
                array(
                    'name' => 'beauty'
                ),
            ),
            array(
                array(
                    'name' => 'foodoption'
                ),
            ),
            array(
                array(
                    'name' => 'entertainment'
                ),
            ),
            array(
                array(
                    'name' => 'health_child_age'
                ),
            ),
            array(
                array(
                    'name' => 'territory'
                ),
            ),
            array(
                array(
                    'name' => 'internet'
                ),
            ),
            array(
                array(
                    'name' => 'arrival_days'
                ),
            ),
            array(
                array(
                    'name' => 'rent'
                ),
            ),
            array(
                array(
                    'name' => 'sport'
                ),
            ),
            array(
                array(
                    'name' => 'spa_text'
                ),
            ),
            array(
                array(
                    'name' => 'description'
                ),
            ),
            array(
                array(
                    'name' => 'accommodation_text'
                ),
            ),
            array(
                array(
                    'name' => 'food_text'
                ),
            ),
            array(
                array(
                    'name' => 'rest_text'
                ),
            ),
            array(
                array(
                    'name' => 'children_text'
                ),
            ),
            array(
                array(
                    'name' => 'note_text'
                ),
            ),
        ),
    )
);
