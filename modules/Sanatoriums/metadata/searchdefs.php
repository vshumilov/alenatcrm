<?php

$searchdefs['Sanatoriums'] = array(
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout' =>
    array(
        'basic_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'sales_status' =>
            array(
                'name' => 'sales_status',
                'default' => true,
                'width' => '10%',
            ),
            'current_user_only' =>
            array(
                'name' => 'current_user_only',
                'label' => 'LBL_CURRENT_USER_FILTER',
                'type' => 'bool',
                'default' => true,
                'width' => '10%',
            ),
        ),
        'advanced_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'country_name' =>
            array(
                'name' => 'country_name',
                'default' => true,
                'width' => '10%',
            ),
            'city_name' =>
            array(
                'name' => 'city_name',
                'default' => true,
                'width' => '10%',
            ),
            'sales_status' =>
            array(
                'name' => 'sales_status',
                'default' => true,
                'width' => '10%',
            ),
            'treatment' =>
            array(
                'name' => 'treatment',
                'default' => true,
                'width' => '10%',
            ),
            'suitable' =>
            array(
                'name' => 'suitable',
                'default' => true,
                'width' => '10%',
            ),
            'pricelevel' =>
            array(
                'name' => 'pricelevel',
                'default' => true,
                'width' => '10%',
            ),
            'arrival' =>
            array(
                'name' => 'arrival',
                'default' => true,
                'width' => '10%',
            ),
            'belongs' =>
            array(
                'name' => 'belongs',
                'default' => true,
                'width' => '10%',
            ),
            'healthprogram' =>
            array(
                'name' => 'healthprogram',
                'default' => true,
                'width' => '10%',
            ),
            'beauty' =>
            array(
                'name' => 'beauty',
                'default' => true,
                'width' => '10%',
            ),
            'foodoption' =>
            array(
                'name' => 'foodoption',
                'default' => true,
                'width' => '10%',
            ),
            'roomsinfostructure' =>
            array(
                'name' => 'roomsinfostructure',
                'default' => true,
                'width' => '10%',
            ),
            'entertainment' =>
            array(
                'name' => 'entertainment',
                'default' => true,
                'width' => '10%',
            ),
            'territory' =>
            array(
                'name' => 'territory',
                'default' => true,
                'width' => '10%',
            ),
            'internet' =>
            array(
                'name' => 'internet',
                'default' => true,
                'width' => '10%',
            ),
            'rent' =>
            array(
                'name' => 'rent',
                'default' => true,
                'width' => '10%',
            ),
            'sport' =>
            array(
                'name' => 'sport',
                'default' => true,
                'width' => '10%',
            ),
            'health_child_age' =>
            array(
                'name' => 'health_child_age',
                'default' => true,
                'width' => '10%',
            ),
            'arrival_days' =>
            array(
                'name' => 'arrival_days',
                'default' => true,
                'width' => '10%',
            ),
            'child_age' =>
            array(
                'name' => 'child_age',
                'default' => true,
                'width' => '10%',
            ),
        ),
    ),
);
