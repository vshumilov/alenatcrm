<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$layout_defs['Sanatoriums'] = array(
    'subpanel_setup' => array(
        'prices' => array(
            'order' => 1,
            'module' => 'Prices',
            'get_subpanel_data' => 'prices',
            'sort_order' => 'asc',
            'sort_by' => 'date_from, foodoption_id, pricelevel_id, cost',
            'subpanel_name' => 'default',
            'title_key' => 'LBL_PRICES_SUBPANEL_TITLE',
            'top_buttons' => array(
                array('widget_class' => 'SubPanelTopButtonQuickCreate'),
            ),
        ),
        'images' => array(
            'order' => 2,
            'module' => 'Images',
            'get_subpanel_data' => 'images',
            'sort_order' => 'asc',
            'sort_by' => 'image_type, order_number',
            'subpanel_name' => 'default',
            'title_key' => 'LBL_IMAGES_SUBPANEL_TITLE',
            'top_buttons' => array(
                array('widget_class' => 'SubPanelTopButtonQuickCreate'),
            ),
        ),
        'houses' => array(
            'order' => 3,
            'module' => 'Houses',
            'get_subpanel_data' => 'houses',
            'sort_order' => 'asc',
            'sort_by' => 'name',
            'subpanel_name' => 'default',
            'title_key' => 'LBL_HOUSES_SUBPANEL_TITLE',
            'top_buttons' => array(
                array('widget_class' => 'SubPanelTopButtonQuickCreate'),
            ),
        ),
        'apartments' => array(
            'order' => 4,
            'module' => 'Apartments',
            'get_subpanel_data' => 'apartments',
            'sort_order' => 'asc',
            'sort_by' => 'pricelevel_id, name',
            'subpanel_name' => 'default',
            'title_key' => 'LBL_ROOMS_SUBPANEL_TITLE',
            'top_buttons' => array(
                array('widget_class' => 'SubPanelTopButtonQuickCreate'),
            ),
        ),
    ),
);
