<?php

$viewdefs ['Sanatoriums'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'DEFAULT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'default' =>
            array(
                array(
                    array(
                        'name' => 'name'
                    ),
                ),
                array(
                    array(
                        'name' => 'full_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'sales_status'
                    ),
                ),
                array(
                    array(
                        'name' => 'country_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'city_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'lat'
                    ),
                ),
                array(
                    array(
                        'name' => 'lon'
                    ),
                ),
                array(
                    array(
                        'name' => 'price_from',
                        'label' => '{$MOD.LBL_PRICE_FROM} ({$CURRENCY})',
                    ),
                ),
                array(
                    array(
                        'name' => 'site'
                    ),
                ),
                array(
                    array(
                        'name' => 'treatment'
                    ),
                ),
                array(
                    array(
                        'name' => 'suitable'
                    ),
                ),
                array(
                    array(
                        'name' => 'pricelevel'
                    ),
                ),
                array(
                    array(
                        'name' => 'child_age'
                    ),
                ),
                array(
                    array(
                        'name' => 'belongs'
                    ),
                ),
                array(
                    array(
                        'name' => 'healthprogram'
                    ),
                ),
                array(
                    array(
                        'name' => 'beauty'
                    ),
                ),
                array(
                    array(
                        'name' => 'foodoption'
                    ),
                ),
                array(
                    array(
                        'name' => 'entertainment'
                    ),
                ),
                array(
                    array(
                        'name' => 'health_child_age'
                    ),
                ),
                array(
                    array(
                        'name' => 'territory'
                    ),
                ),
                array(
                    array(
                        'name' => 'internet'
                    ),
                ),
                array(
                    array(
                        'name' => 'arrival_days'
                    ),
                ),
                array(
                    array(
                        'name' => 'rent'
                    ),
                ),
                array(
                    array(
                        'name' => 'sport'
                    ),
                ),
                array(
                    array(
                        'name' => 'spa_text'
                    ),
                ),
                array(
                    array(
                        'name' => 'description'
                    ),
                ),
                array(
                    array(
                        'name' => 'accommodation_text'
                    ),
                ),
                array(
                    array(
                        'name' => 'food_text'
                    ),
                ),
                array(
                    array(
                        'name' => 'rest_text'
                    ),
                ),
                array(
                    array(
                        'name' => 'children_text'
                    ),
                ),
                array(
                    array(
                        'name' => 'note_text'
                    ),
                ),
            ),
            'LBL_PANEL_ASSIGNMENT' =>
            array(
                array(
                    array(
                        'name' => 'date_modified',
                        'label' => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),
                    array(
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                ),
            ),
        ),
    ),
);
