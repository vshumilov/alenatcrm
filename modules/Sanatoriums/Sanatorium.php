<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Sanatorium extends Basic
{

    public $table_name = "sanatoriums";
    public $module_dir = "Sanatoriums";
    public $object_name = "Sanatorium";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public function prepareName($name)
    {
        return addslashes($name);
    }
    
    public function prepareNameAfterRetrieve($name)
    {
        return stripcslashes($name);
    }
    
    public function get_list_view_data()
    {
        $row = parent::get_list_view_data();
        
        $row['NAME'] = $this->prepareNameAfterRetrieve($row['NAME']);
        
        return $row;
    }

    public function getIdByName($name)
    {
        if (empty($name)) {
            return;
        }

        $sql = "
        SELECT
            id
        FROM
            <table>
        WHERE
            name LIKE '<name>' AND
            deleted = '0'
        ";

        $query = strtr($sql, ['<name>' => $this->prepareName($name), '<table>' => $this->table_name]);

        return dbGetScalar($query);
    }
    
    public function getListQuery() {
        $sql = "
        SELECT
            id,
            name
        FROM
            <table>
        WHERE
            deleted = '0'
        ";
        
        $query = strtr($sql, ['<table>' => $this->table_name]);
        
        return $query;
    }

    public function prepareTinymceFields()
    {
        $sanatoriumstext = new Sanatoriumstext();
        $sanatoriumstextId = $sanatoriumstext->getSantoriumsTextIdBySanatoriumId($this->id);

        if (empty($sanatoriumstextId)) {
            $sanatoriumstext = BeanFactory::getBean('Sanatoriumstexts');
        } else {
            $sanatoriumstext = BeanFactory::getBean('Sanatoriumstexts', $sanatoriumstextId);
        }

        foreach ($this->field_defs as $field) {
            if ($field['type'] != 'tinymce') {
                continue;
            }

            $sanatoriumstext->{$field['name']} = $this->{$field['name']};
        }

        $sanatoriumstext->name = $this->name;
        
        $sanatoriumstext->sanatorium_id = $this->id;

        $sanatoriumstext->save();
    }

    public function getTinymceTexts()
    {
        $sanatoriumstext = new Sanatoriumstext();
        $sanatoriumstextId = $sanatoriumstext->getSantoriumsTextIdBySanatoriumId($this->id);
       
        if (empty($sanatoriumstextId)) {
            return;
        }

        $sanatoriumstext = BeanFactory::getBean('Sanatoriumstexts', $sanatoriumstextId);

        foreach ($this->field_defs as $field) {
            if ($field['type'] != 'tinymce') {
                continue;
            }

            $this->{$field['name']} = $sanatoriumstext->{$field['name']};
        }
    }

}
