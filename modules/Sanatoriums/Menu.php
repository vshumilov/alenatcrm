<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Sanatoriums', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Sanatoriums&action=EditView&return_module=Sanatoriums&return_action=DetailView", $mod_strings['LNK_NEW_SANATORIUM'], "Create");
}
if (ACLController::checkAccess('Sanatoriums', 'list', true)) {
    $module_menu[] = Array("index.php?module=Sanatoriums&action=index&return_module=Sanatoriums&return_action=DetailView", $mod_strings['LNK_SANATORIUM_LIST'], "List");
}
if (ACLController::checkAccess('Sanatoriums', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Sanatoriums&return_module=Sanatoriums&return_action=index", $mod_strings['LNK_IMPORT_SANATORIUMS'], "Import");
}