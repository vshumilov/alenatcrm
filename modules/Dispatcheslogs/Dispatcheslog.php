<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Dispatcheslog extends Basic
{

    public $table_name = "dispatcheslogs";
    public $module_dir = "Dispatcheslogs";
    public $object_name = "Dispatcheslog";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

}
