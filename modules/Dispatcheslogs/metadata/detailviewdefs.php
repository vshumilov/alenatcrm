<?php

$viewdefs ['Dispatcheslogs'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'DEFAULT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'default' =>
            array(
                array(
                    array(
                        'name' => 'name'
                    ),
                ),
                array(
                    array(
                        'name' => 'dispatch_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'contact_name'
                    ),
                ),
                array(
                    array(
                        'name' => 'description'
                    ),
                ),
                array(
                    array(
                        'name' => 'date_entered'
                    ),
                ),
            ),
        ),
    ),
);
