<?php

$searchdefs['Dispatcheslogs'] = array(
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout' =>
    array(
        'basic_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'current_user_only' =>
            array(
                'name' => 'current_user_only',
                'label' => 'LBL_CURRENT_USER_FILTER',
                'type' => 'bool',
                'default' => true,
                'width' => '10%',
            ),
        ),
        'advanced_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'dispatch_name' =>
            array(
                'name' => 'dispatch_name',
                'default' => true,
                'width' => '10%',
            ),
            'contact_name' =>
            array(
                'name' => 'contact_name',
                'default' => true,
                'width' => '10%',
            ),
            'date_entered' =>
            array(
                'name' => 'date_entered',
                'default' => true,
                'width' => '10%',
            ),
        ),
    ),
);
