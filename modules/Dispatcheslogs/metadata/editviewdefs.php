<?php

$viewdefs['Dispatcheslogs']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'name'
                ),
            ),
            array(
                array(
                    'name' => 'dispatch_name'
                ),
            ),
            array(
                array(
                    'name' => 'contact_name'
                ),
            ),
            array(
                array(
                    'name' => 'date_entered'
                ),
            ),
        ),
    )
);
