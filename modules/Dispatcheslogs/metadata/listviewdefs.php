<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Dispatcheslogs'] = array(
    'NAME' => array(
        'width' => '30',
        'label' => 'LBL_NAME',
        'link' => true,
        'default' => true),
    'DISPATCH_NAME' => array(
        'width' => '20',
        'label' => 'LBL_DISPATCH_NAME',
        'id' => 'DISPATCH_ID',
        'module' => 'Dispatches',
        'link' => true,
        'default' => true,
        'sortable' => true,
        'ACLTag' => 'DISPATCH',
        'related_fields' => array('dispatch_id')),
    'CONTACT_NAME' => array(
        'width' => '20',
        'label' => 'LBL_CONTACT',
        'id' => 'CONTACT_ID',
        'module' => 'Contacts',
        'link' => true,
        'default' => true,
        'sortable' => true,
        'ACLTag' => 'CONTACT',
        'related_fields' => array('contact_id')),
    'DATE_ENTERED' => array(
        'width' => '10',
        'label' => 'LBL_DATE_ENTERED',
        'default' => true)
);
