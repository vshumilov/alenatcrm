<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();

if (ACLController::checkAccess('Dispatcheslogs', 'list', true)) {
    $module_menu[] = Array("index.php?module=Dispatcheslogs&action=index&return_module=Dispatcheslogs&return_action=DetailView", $mod_strings['LNK_DISPATCHESLOG_LIST'], "List");
}