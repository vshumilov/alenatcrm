<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Dispatcheslog'] = array('table' => 'dispatcheslogs', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Dispatcheslogs',
    'fields' => array(
        'dispatch_name' =>
        array(
            'name' => 'dispatch_name',
            'rname' => 'name',
            'id_name' => 'dispatch_id',
            'vname' => 'LBL_DISPATCH_NAME',
            'type' => 'relate',
            'table' => 'dispatches',
            'join_name' => 'dispatches',
            'isnull' => 'true',
            'module' => 'Dispatches',
            'dbType' => 'varchar',
            'link' => 'dispatches',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'dispatch_id' =>
        array(
            'name' => 'dispatch_id',
            'vname' => 'LBL_DISPATCH_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'dispatches' =>
        array(
            'name' => 'dispatches',
            'type' => 'link',
            'relationship' => 'dispatches_dispatcheslogs',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Dispatches',
            'bean_name' => 'Dispatches',
            'vname' => 'LBL_DISPATCHES',
        ),
        'contact_name' =>
        array(
            'name' => 'contact_name',
            'rname' => 'name',
            'id_name' => 'contact_id',
            'vname' => 'LBL_CONTACT_NAME',
            'type' => 'relate',
            'table' => 'contacts',
            'join_name' => 'contacts',
            'isnull' => 'true',
            'module' => 'Contacts',
            'dbType' => 'varchar',
            'link' => 'contacts',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'contact_id' =>
        array(
            'name' => 'contact_id',
            'vname' => 'LBL_CONTACT_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'contacts' =>
        array(
            'name' => 'contacts',
            'type' => 'link',
            'relationship' => 'contacts_dispatcheslogs',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Contacts',
            'bean_name' => 'Contact',
            'vname' => 'LBL_CONTACTS',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_dispatcheslog_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_dispatcheslog_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_dispatcheslog_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        'dispatches_dispatcheslogs' =>
        array(
            'lhs_module' => 'Dispatches',
            'lhs_table' => 'dispatches',
            'lhs_key' => 'id',
            'rhs_module' => 'Dispatcheslogs',
            'rhs_table' => 'dispatcheslogs',
            'rhs_key' => 'dispatch_id',
            'relationship_type' => 'one-to-many'
        ),
        'contacts_dispatcheslogs' =>
        array(
            'lhs_module' => 'Contacts',
            'lhs_table' => 'contacts',
            'lhs_key' => 'id',
            'rhs_module' => 'Dispatcheslogs',
            'rhs_table' => 'dispatcheslogs',
            'rhs_key' => 'contact_id',
            'relationship_type' => 'one-to-many'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Dispatcheslogs', 'Dispatcheslog', array('default', 'assignable',
));
