<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Профили лечения',
    'LBL_MODULE_TITLE' => 'Профили лечения - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск профилей лечения',
    'LBL_VIEW_FORM_TITLE' => 'Обзор профилей лечения',
    'LBL_LIST_FORM_TITLE' => 'Список профилей лечения',
    'LBL_TREATMENT_NAME' => 'Название профиля лечения:',
    'LBL_TREATMENT' => 'Профиль лечения:',
    'LBL_NAME' => 'Название профиля лечения',
    'UPDATE' => 'Профили лечения - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся профили лечения',
    'MSG_DUPLICATE' => 'Создаваемая вами профили лечения возможно дублирует уже имеющийся профиля лечения. Профили лечения, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового профили лечения или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать профиль лечения',
    'LNK_NEW_TREATMENT' => 'Создать профиль лечения',
    'LNK_TREATMENT_LIST' => 'Профили лечения',
    'ERR_DELETE_RECORD' => 'Перед удалением профили лечения должен быть определён номер записи.',
    'TREATMENT_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный профиля лечения из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Профили лечения',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_TREATMENTS' => 'Импорт профилей лечения',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
);

$mod_strings['LBL_DESCRIPTION'] = 'Название на английском';


