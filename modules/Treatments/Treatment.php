<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Treatment extends Basic
{

    public $table_name = "treatments";
    public $module_dir = "Treatments";
    public $object_name = "Treatment";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            treatments
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
