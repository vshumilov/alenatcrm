<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Treatments', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Treatments&action=EditView&return_module=Treatments&return_action=DetailView", $mod_strings['LNK_NEW_TREATMENT'], "Create");
}
if (ACLController::checkAccess('Treatments', 'list', true)) {
    $module_menu[] = Array("index.php?module=Treatments&action=index&return_module=Treatments&return_action=DetailView", $mod_strings['LNK_TREATMENT_LIST'], "List");
}
if (ACLController::checkAccess('Treatments', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Treatments&return_module=Treatments&return_action=index", $mod_strings['LNK_IMPORT_TREATMENTS'], "Import");
}