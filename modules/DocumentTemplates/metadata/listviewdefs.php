<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['DocumentTemplates'] = array(
    'NAME' => array(
        'width' => '30',
        'label' => 'LBL_NAME',
        'default' => true,
        'link' => true,
    ),
    'PARENT_TYPE' => array(
        'width' => '15',
        'label' => 'LBL_PARENT_TYPE',
        'default' => true,
    ),
    'ENABLED' => array(
        'width' => '10',
        'label' => 'LBL_ENABLED',
        'default' => true,
    ),
    'FILE_LINK' => array(
        'width' => '30',
        'label' => 'LBL_FILE_LINK',
        'default' => true,
        'related_fields' => array(
            'type',
            'file_name',
            'file_ext',
            'file_mime',
        ),
    ),
    'ASSIGNED_USER_NAME' => array(
        'width' => '10',
        'label' => 'LBL_ASSIGNED_TO_NAME',
        'default' => false,
    ),
);
