<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$searchFields['DocumentTemplates'] = array(
    'name' => array(
        'query_type' => 'default'
    ),
    'owner_only' => array(
        'query_type' => 'default',
        'assignment' => 'owner',
        'vname' => 'LBL_OWNER_ONLY_FILTER',
    ),
    'assigned_user_id' => array(
        'query_type' => 'default'
    ),
    'team_only' => array(
        'query_type' => 'default',
        'assignment' => 'team',
        'vname' => 'LBL_TEAM_ONLY_FILTER',
    ),
);
