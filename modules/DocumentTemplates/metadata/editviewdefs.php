<?php

$viewdefs['DocumentTemplates']['EditView'] = array(
    'templateMeta' => array(
        'form' => array(
            'enctype' => 'multipart/form-data',
        ),
        'maxColumns' => '2',
        'widths' => array(
            array(
                'label' => '10',
                'field' => '30'
            ),
            array(
                'label' => '10',
                'field' => '30'
            ),
        ),
    ),
    'panels' => array(
        'default' => array(
            array(
                array(
                    'name' => 'name',
                    'label' => 'LBL_NAME',
                    'customCode' =>
                    '<input id="name" type="text" name="name" value="{$fields.name.value}" style="width: 326px;" />' .
                    '<script type="text/javascript">' .
                    '$(document).ready(function(){ldelim}' .
                    '$("#name").change(function(){ldelim}' .
                    'if ($("#name_format").val() == "") {ldelim}' .
                    '$("#name_format").val($(this).val())' .
                    '{rdelim};' .
                    '$("#name_format_notice").show();' .
                    'setTimeout(function(){ldelim}' .
                    '$("#name_format_notice").fadeOut(1000);' .
                    '{rdelim}, 5000);' .
                    '{rdelim});' .
                    '{rdelim});' .
                    '</script>',
                ),
            ),
            array(
                array(
                    'name' => 'name_format',
                    'label' => 'LBL_NAME_FORMAT',
                    'customCode' =>
                    '<input id="name_format" type="text" name="name_format" value="{$fields.name_format.value}" style="width: 326px;" />&nbsp;' .
                    '<span id="name_format_notice" style="display: none;" class="error">{sugar_translate label="MSG_CHANGE_NAME_FORMAT" module="DocumentTemplates"}</span>'
                ),
            ),
            array(
                array(
                    'name' => 'enabled',
                    'label' => 'LBL_ENABLED',
                ),
            ),
            array(
                array(
                    'name' => 'parent_type',
                    'label' => 'LBL_PARENT_TYPE',
                    'displayParams' => array(
                        'width' => '326',
                    ),
                ),
            ),
            array(
                array(
                    'name' => 'nitfile_save',
                    'label' => 'LBL_NITFILE_SAVE',
                    'customCode' =>
                    '<input type="hidden" name="nitfile_save" value="0" />' .
                    '<input id="nitfile_save" type="checkbox" name="nitfile_save" value="1" {if $fields.nitfile_save.value}checked="checked"{/if} />&nbsp;' .
                    '<select id="nitfile_field" name="nitfile_field" style="width: 300px;{if !$fields.nitfile_save.value}display:none;{/if}">' .
                    '{html_options options=$nitfile_fields_options[$fields.parent_type.value] selected=$fields.nitfile_field.value}' .
                    '</select>&nbsp;' .
                    '<span id="nitfile_replace_label"{if !$fields.nitfile_save.value} style="display:none;"{/if}>{sugar_translate label="LBL_NITFILE_REPLACE_HELP" module="DocumentTemplates"}</span>&nbsp;' .
                    '<input type="hidden" name="nitfile_replace" value="0" />' .
                    '<input id="nitfile_replace" type="checkbox" name="nitfile_replace" value="1" style="{if !$fields.nitfile_save.value}display:none;{/if}" {if $fields.nitfile_replace.value}checked="checked"{/if} />&nbsp;' .
                    '<script type="text/javascript">' .
                    '$(document).ready(function(){ldelim}' .
                    'var module = $("#parent_type");' .
                    'var save = $("#nitfile_save");' .
                    'var field = $("#nitfile_field");' .
                    'var replace = $("#nitfile_replace");' .
                    'var replace_label = $("#nitfile_replace_label");' .
                    'var options = {$nitfile_fields_options_json};' .
                    'module.change(function(){ldelim}' .
                    'var value_current = field.val();' .
                    'var options_current = options[$(this).val()];' .
                    'field.html("");' .
                    'for(var value in options_current) {ldelim}' .
                    'var option = $("<option></option>")' .
                    '.attr("label", options_current[value])' .
                    '.attr("value", value)' .
                    '.html(options_current[value]);' .
                    'if (value_current == value) {ldelim}' .
                    'option.attr("selected", "selected");' .
                    '{rdelim};' .
                    'field.append(option);' .
                    '{rdelim}' .
                    'field.val(value_current);' .
                    '{rdelim});' .
                    'save.click(function(){ldelim}' .
                    'if (this.checked) {ldelim}' .
                    'field.show();' .
                    'replace.show();' .
                    'replace_label.show();' .
                    '{rdelim} else {ldelim}' .
                    'field.hide();' .
                    'replace.hide();' .
                    'replace_label.hide();' .
                    '{rdelim}' .
                    '{rdelim});' .
                    '{rdelim});' .
                    '</script>'
                ),
            ),
            array(
                array(
                    'name' => 'file_link',
                    'label' => 'LBL_FILE_LINK',
                    'customCode' => '{$fields.file_link.value}',
                ),
            ),
            array(
                array(
                    'name' => 'file_upload',
                    'label' => 'LBL_FILE_UPLOAD',
                    'type' => 'filename',
                    'displayParams' => array(
                        'width' => '326',
                    ),
                ),
            ),
            array(
                array(
                    'name' => 'assigned_user_name',
                    'label' => 'LBL_ASSIGNED_TO_NAME',
                    'displayParams' => array(
                        'size' => '30',
                    ),
                ),
            ),
            array(
                array(
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                ),
            ),
            array(
                array(
                    'name' => 'parameters',
                    'label' => 'LBL_PARAMETERS',
                    'customLabel' => '{if $is_admin}{$MOD.LBL_PARAMETERS}{/if}',
                    'customCode' => '{if $is_admin}<textarea id="parameters" name="parameters" cols="60" rows="4">{$fields.parameters.value}</textarea>{/if}'
                ),
            ),
        ),
    ),
);
