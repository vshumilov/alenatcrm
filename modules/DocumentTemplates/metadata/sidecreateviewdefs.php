<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings;
$viewdefs['DocumentTemplates']['SideQuickCreate'] = array(
    'templateMeta' => array(
        'form' => array(
            'buttons' => array(
                'SAVE'
            ),
            'button_location' => 'bottom',
            'headerTpl' => 'include/EditView/header.tpl',
            'footerTpl' => 'include/EditView/footer.tpl',
            'enctype' => 'multipart/form-data',
        ),
        'maxColumns' => '1',
        'panelClass' => 'none',
        'labelsOnTop' => true,
        'widths' => array(
            array(
                'label' => '10',
                'field' => '30'
            ),
        ),
    ),
    'panels' => array(
        'DEFAULT' => array(
            array(
                array(
                    'name' => 'name',
                    'displayParams' => array(
                        'size' => 25,
                    ),
                ),
            ),
            array(
                array(
                    'name' => 'parent_type',
                    'label' => 'LBL_PARENT_TYPE',
                    'displayParams' => array(
                        'width' => '145',
                    ),
                ),
            ),
            array(
                array(
                    'name' => 'file_upload',
                    'label' => 'LBL_FILE_UPLOAD',
                    'displayParams' => array(
                        'size' => 12,
                    ),
                ),
            ),
            array(
                array(
                    'name' => 'description',
                    'displayParams' => array(
                        'rows' => 3,
                        'cols' => 20
                    ),
                ),
            ),
            array(
                array(
                    'name' => 'assigned_user_name',
                    'displayParams' => array(
                        'size' => 12,
                        'selectOnly' => true
                    ),
                ),
            ),
        ),
    ),
);
