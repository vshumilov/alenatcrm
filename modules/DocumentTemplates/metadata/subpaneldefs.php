<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$layout_defs['DocumentTemplates'] = array(
    // list of what Subpanels to show in the DetailView 
    'subpanel_setup' => array(
        'contacts' => array(
            'order' => 10,
            'module' => 'SugarNitfile',
            'sort_order' => 'asc',
            'sort_by' => 'date_entered',
            'subpanel_name' => 'default',
            'get_subpanel_data' => 'sugarnitfile_link',
            'title_key' => 'LBL_SUGARNITFILE_SUBPANEL_TITLE',
            'top_buttons' => array(),
        ),
    ),
);
