<?php

$viewdefs['DocumentTemplates']['DetailView'] = array(
    'templateMeta' => array(
        'form' => array(
            'buttons' => array(
                'EDIT',
                'DELETE',
            )
        ),
        'maxColumns' => '2',
        'widths' => array(
            array(
                'label' => '10',
                'field' => '30'
            ),
            array(
                'label' => '10',
                'field' => '30'
            ),
        ),
    ),
    'panels' => array(
        'default' => array(
            array(
                array(
                    'name' => 'name',
                    'label' => 'LBL_NAME',
                ),
            ),
            array(
                array(
                    'name' => 'name_format',
                    'label' => 'LBL_NAME_FORMAT',
                ),
            ),
            array(
                array(
                    'name' => 'enabled',
                    'label' => 'LBL_ENABLED',
                ),
            ),
            array(
                array(
                    'name' => 'parent_type',
                    'label' => 'LBL_PARENT_TYPE',
                ),
            ),
            array(
                array(
                    'name' => 'nitfile_save',
                    'label' => 'LBL_NITFILE_SAVE',
                    'customCode' =>
                    '<input type="checkbox" disabled="disabled" {if $fields.nitfile_save.value}checked="checked"{/if}>&nbsp;' .
                    '{if $nitfile_field}' .
                    '{$fields.parent_type.options[$fields.parent_type.value]}->{$nitfile_field}&nbsp;' .
                    '{if $fields.nitfile_replace.value}, {sugar_translate label="LBL_NITFILE_REPLACE_HELP" module="DocumentTemplates"}{/if}' .
                    '{/if}',
                ),
            ),
            array(
                array(
                    'name' => 'file_link',
                    'label' => 'LBL_FILE_LINK',
                ),
            ),
            array(
                array(
                    'name' => 'date_entered',
                    'label' => 'LBL_DATE_ENTERED',
                    'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                ),
            ),
            array(
                array(
                    'name' => 'date_modified',
                    'label' => 'LBL_DATE_MODIFIED',
                    'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                ),
            ),
            array(
                array(
                    'name' => 'assigned_user_name',
                    'label' => 'LBL_ASSIGNED_TO_NAME',
                ),
            ),
            array(
                array(
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                ),
            ),
            array(
                array(
                    'name' => 'parameters',
                    'label' => 'LBL_PARAMETERS',
                    'customLabel' => '{if $is_admin}{$MOD.LBL_PARAMETERS}{/if}',
                    'customCode' => '{if $is_admin}{$fields.parameters.value|nl2br}{/if}',
                ),
            ),
        ),
    ),
);
