<h1>{sugar_translate module="DocumentTemplates" label="LBL_FIELDS"}</h1>
<br/>
{foreach from=$data.fields key=name item=fields}
<table class="view list">
    <th colspan="2"><h2>{$name}</h2></th>
{foreach from=$fields key=field_name item=field_value}
    <tr>
        <td>[{$name}.{$field_name}]</td>
        <td>{$field_value}</td>
    </tr>
{/foreach}
</table>
<br/>
{/foreach}
<h1>{sugar_translate module="DocumentTemplates" label="LBL_BLOCKS"}</h1>
<br/>
{foreach from=$data.blocks key=blockname item=items}
{if $items|@current}
<table class="view list">
    <th colspan="2"><h2>{$blockname}</h2></th>
{foreach from=$items|@current key=field_name item=field_value}
    <tr>
        <td>[{$blockname}.{$field_name};block=w:tr]</td>
        <td>{$field_value}</td>
    </tr>
{/foreach}
</table>
{/if}
{/foreach}