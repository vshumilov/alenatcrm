<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once 'custom/modules/Accounts/BeanHelper/Account.php';
require_once 'custom/modules/Opportunities/BeanHelper/Opportunity.php';

class DocumentTemplateDataProvider {

    protected $_status;
    protected $_template;
    protected $_bean;
    protected static $_bean_fields_defs = array();

    function __construct(& $template, & $bean) {
        global $beanList;

        $this->status(true);
        if (empty($template) || !($template instanceof DocumentTemplate)) {
            $this->status(false);
        } else {
            $this->_template = $template;
        }
        if (empty($bean) || !($bean instanceof SugarBean)) {
            $this->status(false);
        } else {
            $this->_bean = $bean;
        }
    }

    /**
     * Returns object status
     * If $status passed, sets new status
     * @param	boolean	$status
     * @return	boolean
     */
    protected function status($status = null) {
        if (!is_null($status))
            $this->_status = $status && true;
        return $this->_status;
    }

    /**
     * Returns bean fields map
     * @param	object	$bean
     * @param	string	$fields_type [scalar, links]
     * @return	array
     */
    protected function getBeanFieldsDefs(& $bean, $fields_type) {
        global $beanList;

        $bean_class = get_class($bean);
        if (isset(self::$_bean_fields_defs[$bean_class]) &&
                isset(self::$_bean_fields_defs[$bean_class][$fields_type])) {
            return self::$_bean_fields_defs[$bean_class][$fields_type];
        }
        self::$_bean_fields_defs[$bean_class] = array(
            'scalar' => array(),
            'links' => array(),
        );
        foreach ($bean->getFieldDefinitions() as $field_name => $field_def) {
            if (!isset($field_def['name'])) {
                $field_def['name'] = $field_name;
            }
            if (!isset($field_def['type'])) {
                $field_def['type'] = isset($field_def['dbType']) ? $field_def['dbType'] : 'varchar';
            }
            switch ($field_def['type']) {
                case 'link':
                    if (!$bean->load_relationship($field_def['name'])) {
                        break;
                    }
                    if (!isset($field_def['document_template_is_block_link'])) {
                        $is_bean_lhs = $bean->{$field_def['name']}->_get_bean_position();
                        $relationship_type = $bean->{$field_def['name']}->_relationship->relationship_type;
                        $field_def['document_template_is_block_link'] = ($relationship_type == 'many-to-many') ||
                                ($relationship_type == 'one-to-many' && $is_bean_lhs) ||
                                ($relationship_type == 'many-to-one' && !$is_bean_lhs);
                    }
                    if (!isset($field_def['document_template_assign'])) {
                        $field_def['document_template_assign'] = !$field_def['document_template_is_block_link'];
                    }
                    if ($field_def['document_template_is_block_link'] && (
                            $field_def['document_template_assign'] === 'first' ||
                            $field_def['document_template_assign'] === 'last')) {
                        $field_def['document_template_is_block_link'] = false;
                    }
                    if (!isset($field_def['document_template_name'])) {
                        $linked_module = $bean->{$field_def['name']}->getRelatedModuleName();
                        $linked_class = $beanList[$linked_module];

                        if (strpos($field_def['name'], '_link') === (strlen($field_def['name']) - 5)) {
                            $field_def['document_template_name'] = substr($field_def['name'], 0, -5);
                            if ($field_def['document_template_is_block_link'] &&
                                    strtolower($linked_class) == $field_def['document_template_name']) {
                                $field_def['document_template_name'] = strtolower($linked_module);
                            }
                        } else
                        if (!$field_def['document_template_is_block_link'] &&
                                strtolower($linked_module) == $field_def['name']) {
                            $field_def['document_template_name'] = strtolower($linked_class);
                        } else {
                            $field_def['document_template_name'] = $field_def['name'];
                        }
                    }
                    self::$_bean_fields_defs[$bean_class]['links'][$field_def['name']] = $field_def;
                    break;
                default:
                    if (!isset($field_def['document_template_assign'])) {
                        $field_def['document_template_assign'] = true;
                    }
                    if (!isset($field_def['document_template_name'])) {
                        $field_def['document_template_name'] = $field_def['name'];
                    }
                    self::$_bean_fields_defs[$bean_class]['scalar'][$field_def['name']] = $field_def;
                    break;
            }
        }
        return self::$_bean_fields_defs[$bean_class][$fields_type];
    }

    /**
     * Check for conflicts in data fields
     * @param	array	$data template data array
     * @param	string	$field_name
     * @return	boolean
     */
    protected function dataFieldNoConflict(& $fields, $field_name) {
        if (isset($fields[$field_name])) {
            $GLOBALS['log']->fatal('DocumentTemplateDataProvider - conflict in data fields [field: ' . $field_name . ', bean: ' . get_class($this->_bean) . ']');
            $fields[$field_name . '_'] = $fields[$field_name];
            unset($fields[$field_name]);
            return false;
        }
        return true;
    }

    /**
     * Check for conflicts in data blocks
     * @param	array	$data template data array
     * @param	string	$field_name
     * @return	boolean
     */
    protected function dataBlockNoConflict(& $blocks, $block_name) {
        if (isset($blocks[$block_name])) {
            $GLOBALS['log']->fatal('DocumentTemplateDataProvider - conflict in data blocks [block: ' . $block_name . ', bean: ' . get_class($this->_bean) . ']');
            $blocks[$block_name . '_'] = $blocks[$block_name];
            unset($blocks[$block_name]);
            return false;
        }
        return true;
    }

    /**
     * Generate sugar bean data for TBS teplates
     * @return	array
     */
    public function getBeanData() {
        $data = array(
            'fields' => array(),
            'blocks' => array(),
        );
        if (!$this->status()) {
            return $data;
        }
        $this->preProcessBean();
        $bean_class = get_class($this->_bean);
        $bean_field_name = strtolower($bean_class);
        $this->processBeanLinksFields($this->_bean, $data);
        $this->dataFieldNoConflict($data['fields'], $bean_field_name);
        $data['fields'][$bean_field_name] = $this->processBeanScalarFields($this->_bean);
        $this->postProcessBean();

        return $data;
    }

    /**
     * Perfoms bean proeiing before generate template data
     * @return	void
     */
    protected function preProcessBean() {
        $params = $this->_template->decodeTemplateParameters();
        $tasks = array(
            //	task_name	=> method name
            'setField' => 'setBeanField',
        );
        foreach ($tasks as $task_name => $task_method) {
            if (!isset($params[$task_name]))
                continue;
            foreach ($params[$task_name] as $task_params) {
                call_user_func_array(array($this, $task_method), $task_params);
            }
        }
    }

    /**
     * Perfoms bean processing after generate bean data
     * @return	void
     */
    protected function postProcessBean() {
        $params = $this->_template->decodeTemplateParameters();
        $tasks = array(
            //	task_name	=> method name
            'saveBean' => 'saveBeanData',
        );
        foreach ($tasks as $task_name => $task_method) {
            if (!isset($params[$task_name]))
                continue;
            foreach ($params[$task_name] as $task_params) {
                call_user_func_array(array($this, $task_method), $task_params);
            }
        }
    }

    /**
     * Set special bean fields
     * @param	string	$field
     * @param	string	$value
     * @param	boolean	$force
     */
    protected function setBeanField($field, $value = '', $force = 'false') {
        global $timedate;

        if (!empty($this->_bean->$field) && $force !== 'true')
            return;
        $field_def = $this->_bean->getFieldDefinition($field);
        if (empty($field_def))
            return;

        if (isset($field_def['document_template_type']))
            $type = $field_def['document_template_type'];
        elseif (isset($field_def['type']))
            $type = $field_def['type'];
        elseif (isset($field_def['dbType']))
            $type = $field_def['dbType'];
        else
            $type = 'varchar';

        switch ($type) {
            case 'date':
                $value = empty($value) ? 'now' : $value;
                $value = date($timedate->get_date_format(), strtotime($value));
                break;
            case 'datetime':
                $value = empty($value) ? 'now' : $value;
                $value = date($timedate->get_date_time_format(), strtotime($value));
                break;
            case 'auto_increment':
                $category = isset($field_def['auto_increment_category']) ? $field_def['auto_increment_category'] : get_class($this->_bean) . '::$' . $field;
                $format = isset($field_def['auto_increment_format']) ? $field_def['auto_increment_format'] : '%u';
                $value = sprintf($format, auto_increment($category));
                break;
            default:
                break;
        }
        $this->_bean->$field = $value;
    }

    /**
     * Save bean data
     * @return	void
     */
    protected function saveBeanData() {
        $this->_bean->save(false);
    }

    /**
     * Process scalar fields of SugarBean object
     * @return	array
     */
    protected function processBeanScalarFields(& $bean) {
        global
        $timedate,
        $beanList;

        $data = array();
        $bean_field_defs = $this->getBeanFieldsDefs($bean, 'scalar');
        $bean_list_fields = $bean->get_list_view_data();
        foreach ($bean_field_defs as $field_def) {
            if ($field_def['document_template_assign'] == false) {
                continue;
            }
            $field_def['value'] = $bean->{$field_def['name']};
            switch ($field_def['type']) {
                case 'date':
                    $this->processDateField(
                            $field_def['document_template_name'], $field_def['value'], 'date', $data);

                    $field_def['value'] = strtotime($timedate->to_db_date($field_def['value'], false));
                    break;
                case 'datetime':
                    $this->processDateField(
                            $field_def['document_template_name'], $field_def['value'], 'datetime', $data);

                    $field_def['value'] = strtotime($timedate->to_db($field_def['value']));
                    break;
                case 'bool':
                    $field_def['value'] = empty($bean->{$field_def['name']}) ? 0 : 1;
                    break;
                case 'phone':
                    // nothing to do
                    break;
                case 'currency':
                    $params = array();
                    if (isset($fieldDef['convert']))
                        $params['convert'] = $fieldDef['convert'];
                    // define currency for this field
                    $currency_id_field = null;
                    if (isset($fieldDef['currency']))
                        $currency_id_field = $fieldDef['currency'];
                    elseif (isset($fieldDef['group']))
                        $currency_id_field = $fieldDef['group'];
                    else
                        $currency_id_field = 'currency_id';
                    if (!empty($currency_id_field) && isset($bean->$currency_id_field))
                        $params['currency_id'] = $bean->$currency_id_field;
                    $field_def['value'] = unformat_number($field_def['value'], $params);
                    $currency = Nit_Helper::getCurrency(isset($params['currency_id']) ? $params['currency_id'] : SugarConfig::getInstance()->get('default_currency_id'));
                    $currency->currency_penny_forms = 'коппек';
                    $currency->currency_unit_forms = 'рубль';
                    $this->processCurrencyField(
                            $field_def['document_template_name'], $field_def['value'], $currency, $data);

                    break;
                case 'multienum':
                    if (empty($field_def['function']) &&
                            !empty($field_def['options'])) {
                        $module = array_search(get_class($bean), $beanList);
                        $return_array[$list_field] = array();
                        $field_def['value'] = unencodeMultienum($field_def['value']);
                        foreach ($field_def['value'] as & $value_item) {
                            $value_item = translate($field_def['options'], $module, $value_item);
                        }
                        $field_def['value'] = implode(', ', $field_def['value']);
                    }
                    break;
                default:
                    $field_def['name_list'] = strtoupper($field_def['name']);
                    if (isset($bean_list_fields[$field_def['name_list']]))
                        $field_def['value'] = $bean_list_fields[$field_def['name_list']];
                    break;
            }
            $data[$field_def['document_template_name']] = htmlspecialchars_decode($field_def['value'], ENT_QUOTES);
        }
        $extended_method_name = 'process' . get_class($bean) . 'ScalarFields';
        if (method_exists($this, $extended_method_name)) {
            $bean_reference = & $bean;
            $data_reference = & $data;
            $data = call_user_func(array($this, $extended_method_name), $bean_reference, $data_reference);
        }
        return $data;
    }

    /**
     * Process link fields of bean
     * @param	object	$bean
     * @return	array
     */
    protected function processBeanLinksFields(& $bean, & $data = array()) {
        global
        $beanList,
        $beanFiles;

        $data = array_merge($data, array(
            'fields' => array(),
            'blocks' => array(),
        ));
        $bean_class = get_class($bean);
        $bean_field_defs = $this->getBeanFieldsDefs($bean, 'links');
        foreach ($bean_field_defs as $field_def) {
            if ($field_def['document_template_assign'] == false) {
                continue;
            }
            $loaded = $bean->load_relationship($field_def['name']);
            if (!$loaded) {
                continue;
            }
            $linked_module = $bean->{$field_def['name']}->getRelatedModuleName();
            if (!isset($beanList[$linked_module]) ||
                    !isset($beanFiles[$beanList[$linked_module]])) {
                continue;
            }
            $linked_class = $beanList[$linked_module];
            require_once($beanFiles[$linked_class]);
            $linked_ids = $bean->{$field_def['name']}->get();

            if ($field_def['document_template_is_block_link']) {
                $linked_data = array();
                $linked_count = 0;
                foreach ($linked_ids as $linked_id) {
                    $linked_count++;
                    $linked_bean = new $linked_class();
                    $linked_bean->retrieve($linked_id);
                    $linked_data[$linked_count] = $this->processBeanScalarFields($linked_bean);
                    $linked_data[$linked_count]['i'] = $linked_count;
                    unset($linked_bean);
                }
                $this->dataBlockNoConflict($data['blocks'], $field_def['document_template_name']);
                $data['blocks'][$field_def['document_template_name']] = $linked_data;
            } else {
                $linked_data = array();
                if (!empty($linked_ids)) {
                    $linked_id = $field_def['document_template_assign'] === 'first' ? array_shift($linked_ids) : array_pop($linked_ids);
                    $linked_bean = new $linked_class();
                    $linked_bean->retrieve($linked_id);
                    $linked_data = $this->processBeanScalarFields($linked_bean);
                }
                $this->dataFieldNoConflict($data['fields'], $field_def['document_template_name']);
                $data['fields'][$field_def['document_template_name']] = $linked_data;
            }
            unset($linked_data);
        }
        $extended_method_name = 'process' . get_class($bean) . 'LinksFields';
        if (method_exists($this, $extended_method_name)) {
            $bean_reference = & $bean;
            $data_reference = & $data;
            $data = call_user_func(array($this, $extended_method_name), $bean_reference, $data_reference);
        }
        return $data;
    }

    /**
     * Process of currency fields, add data array
     *  - integral part of number (suffix _i)
     *  - fractional part of number (suffix _f)
     *  - string representation of number (suffix _str)
     *  - string representation of integral part of number (suffix _i_str)
     *  - string representation of integral part of number without currency sign (suffix _i_nocurrency_str)
     * @param	string	$fieldName
     * @param	mixed	$fieldValue
     * @param	object	$currency
     * @param	array	$data
     * @return	void
     */
    protected function processCurrencyField($fieldName, $fieldValue, $currency, & $data) {
        $data[$fieldName . '_i'] = floor($fieldValue);
        $data[$fieldName . '_f'] = sprintf('%02s', round(100 * ($fieldValue - floor($fieldValue))));
        $data[$fieldName . '_str'] = Nit_Helper::getLocaleAmountString($fieldValue, $currency);
        $data[$fieldName . '_i_str'] = Nit_Helper::getLocaleAmountString($fieldValue, $currency, false);
        $data[$fieldName . '_i_nocurrency_str'] = Nit_Helper::getLocaleAmountString($fieldValue, $currency, false, false);

        $data[$fieldName . '_str_uc'] = mb_strtoupper(mb_substr($data[$fieldName . '_str'], 0, 1, 'utf-8'), 'utf-8') .
                mb_substr($data[$fieldName . '_str'], 1, mb_strlen($data[$fieldName . '_str'], 'utf-8'), 'utf-8');

        $data[$fieldName . '_i_str_uc'] = mb_strtoupper(mb_substr($data[$fieldName . '_i_str'], 0, 1, 'utf-8'), 'utf-8') .
                mb_substr($data[$fieldName . '_i_str'], 1, mb_strlen($data[$fieldName . '_i_str'], 'utf-8'), 'utf-8');

        $data[$fieldName . '_i_nocurrency_str_uc'] = mb_strtoupper(mb_substr($data[$fieldName . '_i_nocurrency_str'], 0, 1, 'utf-8'), 'utf-8') .
                mb_substr($data[$fieldName . '_i_nocurrency_str'], 1, mb_strlen($data[$fieldName . '_i_nocurrency_str'], 'utf-8'), 'utf-8');

        $data[$fieldName . '_rus'] = $this->num2str($fieldValue);

        $rusMoneyData = explode(' ', $data[$fieldName . '_rus']);

        $countRusMoneyData = count($rusMoneyData);

        $data[$fieldName . '_rus_kop'] = $rusMoneyData[$countRusMoneyData - 2] . ' ' . $rusMoneyData[$countRusMoneyData - 1];

        $dataRusRub = [];

        $endRubs = $countRusMoneyData - 3;

        for ($i = 0; $i < $endRubs; $i++) {
            $dataRusRub[] = $rusMoneyData[$i];
        }
        
        $data[$fieldName . '_rus_rubles_measure'] = $rusMoneyData[$endRubs];
        
        $data[$fieldName . '_rus_rubles_str'] = implode(' ', $dataRusRub);
        
        $data[$fieldName . '_rus_rubles'] = explode('.', $fieldValue)[0];
    }

    public function num2str($num) {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array(// Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v))
                    continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1)
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];# 20-99
                else
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];# 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1)
                    $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        } else
            $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . $this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    protected function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20)
            return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5)
            return $f2;
        if ($n == 1)
            return $f1;
        return $f5;
    }

    /**
     * Process date fields, add data array
     *  - month name (suffix _month)
     *  - month name genetive form (suffix _month_genitive)
     * @param	string	$fieldName
     * @param	mixed	$fieldValue
     * @param	string	$format
     * @param	array	$data
     * @return	void
     */
    protected function processDateField($fieldName, $fieldValue, $format, & $data) {
        global $timedate;

        switch ($format) {
            case 'datetime':
                $pattern = '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2}) ([0-9]{2})\:([0-9]{2})\:([0-9]{2})$/';
                $count = 7;
                $date = $timedate->to_db($fieldValue);
                break;
            case 'date':
            default:
                $pattern = '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/';
                $count = 4;
                $date = $timedate->to_db_date($fieldValue, false);
                break;
        }

        preg_match($pattern, $date, $date);

        if (!empty($date) && count($date) == $count) {
            $data[$fieldName . '_month'] = mb_strtolower(translate('date_month_list', null, (integer) $date[2]), 'utf8');
            $data[$fieldName . '_month_genitive'] = mb_strtolower(translate('date_month_genitive_list', null, (integer) $date[2]), 'utf8');
            $data[$fieldName . '_day'] = $date[3];
            $data[$fieldName . '_month_number'] = $date[2];
            $data[$fieldName . '_year'] = $date[1];
        } else {
            $data[$fieldName . '_month'] = '';
            $data[$fieldName . '_month_genitive'] = '';
            $data[$fieldName . '_day'] = '';
            $data[$fieldName . '_month_number'] = '';
            $data[$fieldName . '_year'] = '';
        }
    }

    // LOGIC HOOKS SECTION
    /**
     * Logic hook for accounts
     * @param	object	$bean
     * @param	array	$data
     * @return	array
     */
    protected function processAccountScalarFields(& $bean, & $data) {
        $address_types = array(
            'billing_address',
            'shipping_address',
        );
        $address_fields = array(
            'postalcode',
            'country',
            'state',
            'city',
            'street',
        );

        foreach ($address_types as $address_type) {
            $address = array();
            foreach ($address_fields as $address_field) {
                $address_field = $address_type . '_' . $address_field;
                if (!empty($bean->$address_field)) {
                    $address[] = $bean->$address_field;
                }
            }
            $data[$address_type] = implode(', ', $address);
        }

        $data = $this->setOrganizationByAccount($bean, $data);

        $data = $this->setCurrentDate($data);
        
        $data['ogrn_label'] = 'ОГРН';
        
        if (empty($data['kpp'])) {
             $data['ogrn_label'] = 'ОГРНИП';
        }

        return $data;
    }

    protected function processUserScalarFields(&$bean, &$data) {
        $name = $bean->last_name . ' ' . $bean->first_name;
        $accountBeanHelper = new AccountBeanHelperAccount();
        $data['full_name_inisials'] = $accountBeanHelper->getFioWithInisials($name);

        return $data;
    }
    
    protected function processContactScalarFields(&$bean, &$data) {
        $name = $bean->last_name . ' ' . $bean->first_name;
        
        $accountBeanHelper = new AccountBeanHelperAccount();
        $data['full_name_inisials'] = $accountBeanHelper->getFioWithInisials($name);

        return $data;
    }

    protected function processOpportunityScalarFields(&$bean, &$data) {
        $opportunity = BeanFactory::getBean('Opportunities', $bean->id);
        
        $fields = $opportunity->field_defs;
        
        foreach ($fields as $field) {
            if (!isset($data[$field['name']])) {
                continue;
            }
            
            $data[$field['name']] = strtr($data[$field['name']], ['"' => '']);
        }
        
        $data = $this->setBoolFields($opportunity, $data);

        return $data;
    }

    protected function setBean($bean, $data) {
        if (empty($bean->id)) {
            return $data;
        }

        $beanClass = strtolower(get_class($bean));

        foreach ($bean->field_defs as $field) {
            $data[$beanClass . '_' . $field['name']] = $bean->{$field['name']};

            if ($field['type'] == 'date') {
                $this->processDateField($beanClass . '_' . $field['name'], $bean->{$field['name']}, 'date', $data);
            }
        }

        return $data;
    }

    protected function setBoolFields($bean, $data) {
        $config = new SugarConfig();
        $docBoolSignYes = $config->get('doc_bool_sign_yes');
        $docBoolSignNo = $config->get('doc_bool_sign_no');

        if (empty($docBoolSignYes)) {
            $GLOBALS['log']->info("DocumentTemplateDataProvider::setBean: doc_bool_sign_yes doesn't exist in config.php");
            return;
        }

        foreach ($bean->field_defs as $field) {
            if ($field['type'] != 'bool') {
                continue;
            }
            
            $data[$field['name']] = $bean->{$field['name']};

            $data = $this->setBoolField($bean, $data, $field['name'], $docBoolSignYes, $docBoolSignNo);
        }

        return $data;
    }

    protected function setBoolField($bean, $data, $fieldName, $docBoolSign, $docBoolSignNo = null) {
        $data[$fieldName . '_yes'] = '';
        $data[$fieldName . '_no'] = '';

        if (empty($docBoolSignNo)) {
            $docBoolSignNo = $docBoolSign;
        }

        if (!empty($bean->{$fieldName})) {
            $data[$fieldName . '_yes'] = $docBoolSign;
            $data[$fieldName] = $docBoolSign;
        } else {
            $data[$fieldName . '_no'] = $docBoolSignNo;
            $data[$fieldName] = $docBoolSignNo;
        }

        return $data;
    }

    protected function setOrganizationByAccount($account, $data) {
        $accountBeanHelper = new AccountBeanHelperAccount($account);
        $organization = $accountBeanHelper->getOrganization();

        if (!empty($organization->id)) {
            foreach ($organization->field_defs as $field) {
                $data['organization_' . $field['name']] = $organization->{$field['name']};

                if (mb_strpos($field['name'], 'fio') !== false) {
                    $data['organization_' . $field['name'] . '_inisials'] = $accountBeanHelper->getFioWithInisials($organization->{$field['name']});
                }
            }
        }

        $data['genitive_fio_inisials'] = $accountBeanHelper->getFioWithInisials($account->genitive_fio);
        $data['nominative_fio_inisials'] = $accountBeanHelper->getFioWithInisials($account->nominative_fio);

        return $data;
    }

    protected function setCurrentDate($data) {
        global $timedate;
        $currentDate = date($timedate->get_date_format(), time());

        $this->processDateField('current_date', $currentDate, 'date', $data);

        return $data;
    }

}
