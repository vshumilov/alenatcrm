<?php

class DocumentTemplatesAjaxHelper extends AjaxHelper
{

    protected $_access_types_tist = array(
        'download' => 'view',
        'delete' => 'edit',
        'restore' => 'edit',
    );

    /**
     * Constructor
     * 
     * @param	object	$ajaxManager
     * @param	string	$module
     */
    function __construct(& $ajaxManager, $module = 'DocumentTemplates', $record = null)
    {
        parent::__construct($ajaxManager, $module, $record);
    }

    /**
     * Perfom nitfile file downloading
     * URL: index.php?entryPoint=ajax&module=SugarNitfile&call=download&options[format]=file&options[download]=1&params[id]=xxx-xxx-xxx-xxx
     * 
     * @access	public
     * @return	array
     */
    public function download()
    {
        if (empty($this->_bean->id)) {
            $this->setError(translate('ERR_UNDEFINED_DOCUMENT_TEMPLATE', $this->_module), true);
            return false;
        }
        $file = array(
            'file' => $this->_bean->file_path,
            'extension' => $this->_bean->file_ext,
            'mime' => $this->_bean->file_mime,
            'name' => $this->_bean->name . '.' . $this->_bean->file_ext,
        );

        return $file;
    }

    /**
     * Generate document from template
     * @param	string	$bean_module - target bean module
     * @param	string	$bean_id - target bean id
     * @return	array
     */
    public function generate($bean_module, $bean_id)
    {
        if (empty($this->_bean->id)) {
            $this->setError(translate('ERR_UNDEFINED_DOCUMENT_TEMPLATE', $this->_module), true);
            return false;
        }
        $bean = loadBean($bean_module);
        if (empty($bean)) {
            $this->setError(translate('ERR_UNDEFINED_OBJECT', $this->_module), true);
            return false;
        }
        $bean = $bean->retrieve($bean_id);
        if (empty($bean)) {
            $this->setError(translate('ERR_UNDEFINED_OBJECT', $this->_module), true);
            return false;
        }
        $document = $this->_bean->generateDocument($bean);
        if (empty($document)) {
            $this->setError(translate('ERR_DOCUMENT_TEMPLATE_PROCESSING_ERROR', $this->_module), true);
            return false;
        }
        return $document;
    }

    /**
     * Refreshe attachmnets list, linked with bean
     * @param	string	$bean_module - target bean module
     * @param	string	$bean_id - target bean id
     * @return	array
     */
    public function refreshNitfiles($bean_module, $bean_id)
    {
        if (empty($this->_bean->nitfile_save) ||
        empty($this->_bean->nitfile_field)) {
            return null;
        }
        require_once('include/SugarFields/Fields/Nitfile/SugarFieldNitfile.php');
        $bean = loadBean($bean_module);
        if (empty($bean)) {
            $this->setError(translate('ERR_UNDEFINED_OBJECT', $this->_module), true);
            return null;
        }
        $bean = $bean->retrieve($bean_id);
        if (empty($bean)) {
            $this->setError(translate('ERR_UNDEFINED_OBJECT', $this->_module), true);
            return null;
        }
        return array(
            $this->_bean->nitfile_field => SugarFieldNitfile::processNitfileField($bean, $this->_bean->nitfile_field),
        );
    }

}
