<?php

require_once 'include/upload_file.php';

class UploadFile2 extends UploadFile
{

    public static function get_url($stored_file_name, $bean_id)
    {
        $url = SugarConfig::getInstance()->get('upload_dir', self::$url);

        if (empty($bean_id) && empty($stored_file_name)) {
            return $url;
        }

        return $url . $bean_id;
    }

}
