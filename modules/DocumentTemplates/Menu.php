<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;

if (ACLController::checkAccess('DocumentTemplates', 'edit', true))
    $module_menu[] = array("index.php?module=DocumentTemplates&action=EditView&return_module=DocumentTemplates&return_action=DetailView", $mod_strings['LNK_NEW_RECORD'], 'CreateDocumentTemplates', 'DocumentTemplates');
if (ACLController::checkAccess('DocumentTemplates', 'list', true))
    $module_menu[] = array("index.php?module=DocumentTemplates&action=index&return_module=DocumentTemplates&return_action=DetailView", $mod_strings['LNK_LIST'], 'DocumentTemplates', 'DocumentTemplates');