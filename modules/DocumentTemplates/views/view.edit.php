<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
require_once('custom/include/MVC/View/views/view.edit.php');

class DocumentTemplatesViewEdit extends ViewEdit
{

    function DocumentsViewEdit()
    {
        parent::ViewEdit();
    }

    function display()
    {
        global $current_user;
        global $app_list_strings;

        $nitfile_fields_options = array();
        foreach ($app_list_strings['document_template_parent_type'] as $module => $module_label) {
            $nitfile_fields_options[$module] = array();
            $bean = loadBean($module);
            
            if (!is_object($bean)) {
                continue;
            }
            
            foreach ($bean->getFieldDefinitions() as $field_def) {
                if (isset($field_def['type']) && $field_def['type'] == 'nitfile') {
                    $nitfile_fields_options[$module][$field_def['name']] = translate($field_def['vname'], $module);
                }
            }
        }
        $this->ev->ss->assign('is_admin', is_admin($current_user));
        $this->ev->ss->assign('nitfile_fields_options', $nitfile_fields_options);
        $this->ev->ss->assign('nitfile_fields_options_json', getJSONobj()->encode($nitfile_fields_options));
        parent::display();
    }

}
