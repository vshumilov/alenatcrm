<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
require_once('custom/include/MVC/View/views/view.detail.php');

class DocumentTemplatesViewDetail extends ViewDetail
{

    function DocumentsViewDetail()
    {
        parent::ViewDetail();
    }

    function display()
    {
        global $current_user;

        if (!empty($this->bean->parent_type) &&
        !empty($this->bean->nitfile_save) &&
        !empty($this->bean->nitfile_field)) {
            $bean = loadBean($this->bean->parent_type);
            if (!empty($bean)) {
                $field_def = $bean->getFieldDefinition($this->bean->nitfile_field);
                if (!empty($field_def)) {
                    $this->dv->ss->assign('nitfile_field', translate($field_def['vname'], $this->bean->parent_type));
                }
            }
        }

        $this->dv->ss->assign('is_admin', is_admin($current_user));
        parent::display();
    }

}
