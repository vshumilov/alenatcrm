<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('modules/DocumentTemplates/DocumentTemplateDataProvider.php');

class DocumentTemplatesViewTags extends SugarView
{

    public function process()
    {
        /* @var $document_template DocumentTemplate */
        $document_template = BeanFactory::getBean('DocumentTemplates', $_REQUEST['record']);

        $bean = BeanFactory::getBean($document_template->parent_type, $_REQUEST['bean_id']);

        $data_provider = new DocumentTemplateDataProvider($document_template, $bean);
        $TBS_DATA = $data_provider->getBeanData();

        $this->ss->assign('data', $TBS_DATA);

        parent::process();
    }

    public function display()
    {
        echo $this->ss->fetch('modules/DocumentTemplates/tpls/tags.tpl');
    }

}
