<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once dirname(__FILE__) . '/UploadFile2.php';

class DocumentTemplate extends Basic
{

    const TEMP_FILE_PREFIX = 'Sugar.DocumentTemplates';

    public $name_format;
    public $parameters;
    public $parent_type;
    public $enabled;
    public $type;
    public $nitfile_save;
    public $nitfile_field;
    public $nitfile_replace;
    public $file_path;
    public $file_link;
    public $file_upload;
    public $file_name;
    public $file_ext;
    public $file_mime;
    private $_tmp_path = array();

    /**
     * Constructor
     */
    function __construct()
    {
        $this->module_dir = 'DocumentTemplates';
        $this->object_name = 'DocumentTemplate';
        $this->table_name = 'documenttemplates';
        $this->importable = false;
        parent::Basic();
    }

    /**
     * Destructor
     */
    function __destruct()
    {
        // clear all tmp path
        foreach ($this->_tmp_path as $tmp_path) {
            rmdir_recursive($tmp_path);
        }
    }

    /**
     * (non-PHPdoc)
     * @see SugarBean::save()
     */
    function save($check_notify = false)
    {
        if (empty($this->name_format)) {
            $this->name_format = $this->name;
        }
        $this->name_format = trim($this->name_format);
        $this->setTemplateType();
        return parent::save($check_notify);
    }

    /**
     * (non-PHPdoc)
     * @see SugarBean::retrieve()
     */
    function retrieve($id = -1, $encode = true, $deleted = true)
    {
        $return = parent::retrieve($id, $encode, $deleted);
        $this->setFileFields();
        return $return;
    }

    /**
     * (non-PHPdoc)
     * @see SugarBean::get_list_view_array()
     */
    function get_list_view_array()
    {
        $this->setFileFields();
        return parent::get_list_view_array();
    }

    /**
     * Detect and set template type by template files and mimy types
     * @return	string
     */
    public function setTemplateType()
    {
        switch (strtolower($this->file_ext)) {
            case 'odt':
                $this->type = 'odf';
                $this->file_mime = 'application/vnd.oasis.opendocument.text';
                break;
            case 'docx':
                $this->type = 'odf';
                $this->file_mime = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                break;
            case 'html':
                $this->type = 'pdf';
                $this->file_mime = 'text/html';
                break;
            case 'zip':
                $this->type = 'pdf';
                $this->file_mime = 'application/zip';
                break;
            default:
                $this->type = 'undefined';
                break;
        }
        return $this->type;
    }

    /**
     * Strip template parameters
     * @return	array
     */
    public function decodeTemplateParameters()
    {
        $tmp = explode("\n", $this->parameters);
        $parameters = array();
        foreach ($tmp as & $tmp_item) {
            $tmp_item = explode(':', $tmp_item);
            $tmp_item = array_map('trim', $tmp_item);
            $tmp_item_name = array_shift($tmp_item);
            if (empty($tmp_item_name)) {
                continue;
            }
            if (!isset($parameters[$tmp_item_name])) {
                $parameters[$tmp_item_name] = array();
            }
            $parameters[$tmp_item_name][] = $tmp_item;
        }
        return $parameters;
    }

    /**
     * Generate file link for this template
     * @return	void
     */
    protected function setFileFields()
    {
        global $theme;

        if ($this->isCreation())
            return;

        if (empty($this->file_mime) ||
        empty($this->file_ext) ||
        empty($this->file_name))
            return;

        switch ($this->type) {
            case 'odf':
                $img = 'DocumentTemplate_odf';
                break;
            case 'pdf':
                $img = 'DocumentTemplate_pdf';
                break;
            default:
                $img = 'DocumentTemplate_undefined';
                break;
        }
        $title = translate('LBL_DOWNLOAD_DOCUMENT_TEMPLATE', $this->module_dir);
        $img_alt = translate('document_template_type_list', $this->module_dir, $this->type);
        $img = get_image('themes/' . $theme . '/images/' . $img, 'title="' . htmlspecialchars($title) . '" alt="' . htmlspecialchars($img_alt) . '"  border="0"');
        $this->file_path = UploadFile2::get_url($this->file_name, $this->id);
        if ($this->ACLAccess('view')) {
            $json = getJSONobj();
            $light_ajax_call = array(
                'module' => $this->module_dir,
                'record' => $this->id,
                'call' => 'download',
                'options' => array(
                    'format' => 'file',
                    'download' => true,
                ),
            );
            $light_ajax_call = 'LightAjax(' . $json->encode($light_ajax_call) . ')';
            $id = 'DocumentTemplateLink-' . $this->id;
            $this->file_link = '<a ' .
            'title="' . htmlspecialchars($title) . '"' .
            'id="' . htmlspecialchars($id) . '" ' .
            'href="#' . htmlspecialchars($id) . '" ' .
            'onclick="' . htmlspecialchars($light_ajax_call) . ';return false;" ' .
            'style="cursor:pointer;">' . $img . '&nbsp;' . $this->file_name . '</a>';
        } else {
            $this->file_link = '<span>' . $img . '&nbsp;' . $this->file_name . '</span>';
        }
    }

    /**
     * Generate document by passed bean and return file resource
     * @param	object	$bean
     * @return	resource
     */
    public function generateDocument(& $bean)
    {
        switch ($this->type) {
            case 'odf':
                $file_handler = $this->generateDocumentODF($bean);
                $file_ext = $this->file_ext;
                $file_mime = $this->file_mime;
                break;
            case 'pdf':
                $file_handler = $this->generateDocumentPDF($bean);
                $file_ext = 'pdf';
                $file_mime = 'application/pdf';
                break;
            default:
                $GLOBALS['log']->fatal('DocumentTemplate::generateDocument - ERROR - invalid document type "' . $this->type . '" [id: ' . $this->id . ']');
                $document = null;
        }
        if (empty($file_handler)) {
            return null;
        }

        fseek($file_handler, null, SEEK_END);
        $file_size = ftell($file_handler);

        $result = array(
            'file' => $file_handler,
            'extension' => $file_ext,
            'mime' => $file_mime,
            'name' => $this->generateDocumentFileName($bean, $file_ext),
            'size' => $file_size,
        );

        $this->saveNitfile($bean, $result);

        return $result;
    }

    protected function saveNitfile(SugarBean $bean, $result)
    {
        if (empty($this->nitfile_save) || empty($this->nitfile_field)) {
            return;
        }

        $field_def = $bean->getFieldDefinition($this->nitfile_field);

        if (!isset($field_def['type'])) {
            return;
        }

        if ($field_def['type'] !== 'nitfile') {
            return;
        }

        if (!empty($this->nitfile_replace)) {
            $nitfiles = SugarNitfile::getBeanNitfiles($bean, $this->nitfile_field);
            foreach ($nitfiles as $nitfile) {
                $nitfile->mark_deleted($nitfile->id);
            }
        }

        $tmp_file_name = self::generateTempName('Sugar.SugarNitfile', $result['extension']);
        rewind($result['file']);
        file_put_contents($tmp_file_name, fread($result['file'], $result['size']));
        $nitfile_file = array(
            'tmp_name' => $tmp_file_name,
            'name' => $result['name'],
            'type' => $result['mime'],
            'size' => $result['size'],
        );
        $nitfile = SugarNitfile::createBeanNitfile($bean, $this->nitfile_field, $nitfile_file);

        $this->load_relationship('sugarnitfile_link');
        $this->sugarnitfile_link->add($nitfile->id);
    }

    /**
     * Generate ODF document, usinf OpenTBS template engine
     * @param	object	$bean
     * @return	file handler or null on fail
     */
    private function generateDocumentODF(& $bean)
    {
        require_once('modules/DocumentTemplates/DocumentTemplateDataProvider.php');
        require_once('custom/include/OpenTBS/tbs_class.php');
        require_once('custom/include/OpenTBS/tbs_plugin_opentbs.php');
        global $current_user;

        if (empty($this->file_path)) {
            $this->setFileFields();
        }
        if (empty($this->file_path)) {
            $GLOBALS['log']->fatal('DocumentTemplate - ERROR - can\'t process template, empty template file');
            return null;
        }
        // create temp file
        $tmp_file = self::generateTempName(self::TEMP_FILE_PREFIX, $this->file_ext);
        // copy template file to temp file
        if (!copy($this->file_path, $tmp_file)) {
            $GLOBALS['log']->fatal('DocumentTemplate - ERROR - can\'t copy template file to temp destination. (source: ' . $this->file_path . ', dest: ' . $tmp_file . ')');
            return null;
        }
        // register temp file path
        $this->_tmp_path[] = $tmp_file;

        // hack, OpenTBS template engine needs
        // for global variable $TBS_DATA exists
        global $TBS_DATA;
        $data_provider = new DocumentTemplateDataProvider($this, $bean);
        $TBS_DATA = $data_provider->getBeanData();

        $TBS = new clsTinyButStrong();
        $TBS->NoErr = true;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate($tmp_file, OPENTBS_ALREADY_UTF8);
        // TODO: find way to change chart's data sources in xlsx documents
        // look at clsOpenTBS::Ext_PrepareInfo, look at $i['load'] = $this->OpenXML_MapGetFiles(array('drawingml.chart+xml'));
        //die(var_dump($TBS_DATA));
        foreach ($TBS_DATA['fields'] as $field_name => & $field_data) {
            $TBS->MergeField($field_name, $field_data);
        }
        foreach ($TBS_DATA['blocks'] as $block_name => & $block_data) {
            $TBS->MergeBlock($block_name, 'array', $block_data);
        }
        if ($TBS->ErrCount > 0) {
            $GLOBALS['log']->fatal('DocumentTemplate::generateDocumentODF - ERROR - ' . $TBS->ErrMsg);
            return null;
        } else {
            $TBS->Show(TBSZIP_STRING);

            $handler = fopen('php://temp', 'w+');
            fwrite($handler, $TBS->Source);

            // clear $TBS_DATA var
            unset($TBS_DATA);
            unset($TBS);

            return $handler;
        }
    }

    /**
     * Generate ODF document, usinf OpenTBS template engine
     * @param	object	$bean
     * @return	string
     */
    private function generateDocumentPDF(& $bean)
    {
        require_once('modules/DocumentTemplates/DocumentTemplateDataProvider.php');
        require_once('include/WebKit/WebKitHTMLTOPDF.php');
        if (empty($this->file_path)) {
            $this->setFileFields();
        }
        if (empty($this->file_path)) {
            $GLOBALS['log']->fatal('DocumentTemplate - ERROR - can\'t process template, empty template file');
            return null;
        }

        $template_file = null;
        $tmp_file = self::generateTempName(self::TEMP_FILE_PREFIX, $this->file_ext);
        switch ($this->file_mime) {
            // simple html file, nothing else
            case 'text/html':
                if (!copy($this->file_path, $tmp_file)) {
                    $GLOBALS['log']->fatal('DocumentTemplate - ERROR - can\'t copy template file to temp destination');
                    return null;
                }
                $template_file = $tmp_file;
                // register temp file path
                $this->_tmp_path[] = $tmp_file;
                break;
            // html file with plugging external resources
            case 'application/zip':
                $tmp_dir = $tmp_file;
                if (!is_dir($tmp_dir)) {
                    mkdir($tmp_dir, 0770, true);
                }
                if (!$this->extractZipFile($this->file_path, $tmp_dir)) {
                    $GLOBALS['log']->fatal('DocumentTemplate - ERROR - can\'t extract template files to temp drectory');
                    return null;
                }
                $template_file = $tmp_dir . DIRECTORY_SEPARATOR . 'index.html';
                // register temp file path
                $this->_tmp_path[] = $tmp_dir;
                break;
        }

        $data_provider = new DocumentTemplateDataProvider($this, $bean);
        $data = $data_provider->getBeanData();

        // prepare smarty
        $ss = new Sugar_Smarty();
        foreach ($data['fields'] as $field_name => & $field_data) {
            $ss->assign($field_name, $field_data);
        }
        foreach ($data['blocks'] as $block_name => & $block_data) {
            $ss->assign($block_name, $block_data);
        }
        file_put_contents($template_file, $ss->fetch($template_file));

        $pdf_file_name = self::generateTempName('PDF.Document', 'pdf');
        $pdf_document_title = $this->generateDocumentName($bean);

        // hack only for windows systems without utf-8 support
        if (is_windows()) {
            $pdf_document_title = iconv('utf-8', 'cp1251', $this->generateDocumentName($bean));
        }

        $handler = fopen('php://temp', 'w+');
        $options = array('title' => $pdf_document_title);
        if (WebKitHTMLTOPDF::convert($template_file, $handler, $options) === false) {
            fclose($handler);
            return null;
        } else {
            return $handler;
        }
    }

    /**
     * Generates temp file name and clean file if exists
     * @return	string
     */
    private static function generateTempName($prefix, $ext)
    {
        global $current_user;
        $temp_name = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $prefix . '.' . substr($current_user->id, 0, 8) . '.' . $ext;
        if (is_file($temp_name)) {
            unlink($temp_name);
        }
        return $temp_name;
    }

    /**
     * Extract zip archive to target destination
     * @param	object	$zip
     * @param	string	$destination
     * @return	boolean
     */
    private function extractZipFile($zip_file, $destination)
    {
        $zip = zip_open($zip_file);
        while (($zip_entity = zip_read($zip)) !== false) {
            if (is_numeric($zip_entity)) {
                $GLOBALS['log']->fatal('DocumentTemplate - ERROR - extracting template files to temp drectory returned error: ' . $zip_entity);
                return false;
            }
            $zip_entity_name = zip_entry_name($zip_entity);
            $zip_entity_name = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $zip_entity_name);
            $zip_entiry_size = zip_entry_filesize($zip_entity);
            // this is a directory in zip archive
            if ($zip_entiry_size === 0 &&
            substr($zip_entity_name, -1) === DIRECTORY_SEPARATOR) {
                $destination_entity_name = $destination . DIRECTORY_SEPARATOR . substr($zip_entity_name, 0, -1);
                if (!is_dir($destination_entity_name)) {
                    mkdir($destination_entity_name, 0770, true);
                }
            }
            // zipped file
            else {
                $destination_entity_name = $destination . DIRECTORY_SEPARATOR . $zip_entity_name;
                $zip_entity_contents = zip_entry_read($zip_entity, $zip_entiry_size);
                file_put_contents($destination_entity_name, $zip_entity_contents);
            }
        }
        return true;
    }

    /**
     * Generate document name
     * @param	object	$bean
     * @return	string
     */
    public function generateDocumentName(& $bean)
    {
        global $timedate;

        $document_name = $this->name_format;
        preg_match_all('/\{[^{}]+\}/', $document_name, $matches);
        if (empty($matches) || empty($matches[0])) {
            return $document_name;
        }
        foreach ($matches[0] as $search) {
            $variable = substr($search, 1, -1);
            // we should check bean field
            if (strpos($variable, '$') === 0) {
                $replace = self::getBeanFieldVisibleValue($bean, substr($variable, 1));
            }
            // we should check env var
            else {
                switch ($variable) {
                    case 'now':
                    case 'time':
                        $replace = $timedate->get_gmt_db_datetime();
                        $replace = $timedate->to_display($replace);
                        break;
                    case 'today':
                    case 'date':
                        $replace = $timedate->get_gmt_db_datetime();
                        $replace = $timedate->to_display_date($replace);
                        break;
                    default:
                        $replace = '';
                        break;
                }
            }
            $document_name = str_replace($search, $replace, $document_name);
        }
        return $document_name;
    }

    /**
     * Generate file name for document
     * @param	object	$bean
     * @param	string	$extension_override
     * @return	string
     */
    public function generateDocumentFileName(& $bean, $extension_override = null)
    {
        $denied_sybmols = array(' ', '/', '|', '\\', ':', '*', '?', '"', '<', '>', "\n", "\r", "\t");
        $document_name = $this->generateDocumentName($bean);
        $document_name = str_replace($denied_sybmols, '-', $document_name);
        $document_name = preg_replace('/\-+/', '-', $document_name);
        $document_name = isset($extension_override) ? $document_name . '.' . $extension_override : $document_name . '.' . $this->file_ext;
        return $document_name;
    }

    public static function getDocumentTemplateMetadata($module)
    {
        return array(
            'options' => self::getDocumentTemplatesOptions($module),
            'template_id' => md5(microtime(true)),
            'value' => null,
            'show_document_templates_tags' => true,//PrettyConfig::get('documenttemplates_showtagsbutton'),
        );
    }

    /**
     * Generate teplates options for special module
     * @param	object	$bean
     * @param	array	$filters
     * @return	array
     */
    public static function getDocumentTemplatesOptions($module, $filters = array())
    {
        $db = DBManagerFactory::getInstance();
        $where = array();
        $where[] = 'deleted = 0';
        $where[] = 'enabled != 0';
        $where[] = 'parent_type = \'' . $db->quote($module) . '\'';
        // apply special filters
        if (!empty($filters) && is_array($filters)) {
            foreach ($filters as $field => $filter) {
                if (substr($filter, 0, 5) === 'bean.') {
                    $filter = self::getBeanFieldVisibleValue($bean, substr($filter, 5));
                }
                $where[] = $field . ' LIKE \'%' . $db->quote($filter) . '%\'';
            }
        }

        $sql = 'SELECT `id`, `name` ' .
        'FROM `documenttemplates` ' .
        'WHERE ' . implode(' AND ', $where) . ' ' .
        'ORDER BY `name`';

        $options = array();
        $result = $db->query($sql);

        while (($row = $db->fetchByAssoc($result)) != null) {
            $options[$row['id']] = $row['name'];
        }

        return $options;
    }

    /**
     * Returns default template name
     * @param	string	$bean
     * @return	boolean
     */
    public static function getDocumentTemplatesDefault($bean)
    {
        return null;
    }

    public static function getBeanFieldVisibleValue($bean, $field)
    {
        $visible_value = '';
        $field_def = $bean->getFieldDefinition($field);
        if (!empty($field_def)) {
            switch ($field_def['type']) {
                case 'enum':
                    $visible_value = translate($field_def['options'], $bean->module_dir, isset($bean->{$field}) ? $bean->{$field} : '');
                    break;
                default:
                    $visible_value = isset($bean->{$field}) ? $bean->{$field} : '';
                    break;
            }
        }
        return $visible_value;
    }

    public function isCreation($check_db = false)
    {
        $has_id = !empty($this->id);
        $new_with_id = !empty($this->new_with_id);
        if (!$check_db) {
            return (!$has_id || $new_with_id);
        }
        if ($has_id) {
            $sql = 'SELECT COUNT(`id`) `c` FROM `' . $this->table_name . '` WHERE `id`=\'' . $this->db->quote($this->id) . '\'';
            $count = $this->db->fetchByAssoc($this->db->query($sql));
            return (is_array($count) && isset($count['c']) ? $count['c'] == 0 : false);
        } else {
            return true;
        }
    }

}
