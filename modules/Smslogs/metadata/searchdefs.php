<?php

$searchdefs['Smslogs'] = array(
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout' =>
    array(
        'basic_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'phone' =>
            array(
                'name' => 'phone',
                'default' => true,
                'width' => '10%',
            ),
            'description' =>
            array(
                'name' => 'description',
                'type' => 'varchar',
                'default' => true,
                'width' => '10%',
            ),
        ),
        'advanced_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'phone' =>
            array(
                'name' => 'phone',
                'default' => true,
                'width' => '10%',
            ),
            'description' =>
            array(
                'name' => 'description',
                'type' => 'varchar',
                'default' => true,
                'width' => '10%',
            ),
        ),
    ),
);
