<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Smslogs'] = array(
    'NAME' => array(
        'width' => '30',
        'label' => 'LBL_NAME',
        'link' => true,
        'default' => true
    ),
    'PHONE' => array(
        'width' => '30',
        'label' => 'LBL_PHONE',
        'default' => true
    ),
    'DESCRIPTION' => array(
        'width' => '30',
        'label' => 'LBL_DESCRIPTION',
        'default' => true
    ),
    'CREATED_BY_NAME' => array(
        'width' => '10',
        'label' => 'LBL_CREATED'),
    'ASSIGNED_USER_NAME' => array(
        'width' => '5',
        'label' => 'LBL_LIST_ASSIGNED_USER',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => true),
    'MODIFIED_BY_NAME' => array(
        'width' => '5',
        'label' => 'LBL_MODIFIED'),
    'DATE_ENTERED' => array(
        'width' => '10',
        'label' => 'LBL_DATE_ENTERED')
);
