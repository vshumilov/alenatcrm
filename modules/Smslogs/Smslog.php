<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Smslog extends Basic
{

    public $table_name = "smslogs";
    public $module_dir = "Smslogs";
    public $object_name = "Smslog";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            smslogs
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
