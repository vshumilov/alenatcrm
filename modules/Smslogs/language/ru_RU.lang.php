<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Sms лог',
    'LBL_MODULE_TITLE' => 'Sms лог - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск sms',
    'LBL_VIEW_FORM_TITLE' => 'Обзор sms',
    'LBL_LIST_FORM_TITLE' => 'Список sms',
    'LBL_SMSLOG_NAME' => 'Название sms:',
    'LBL_SMSLOG' => 'Профиль лечения:',
    'LBL_NAME' => 'Название sms',
    'UPDATE' => 'Sms лог - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся sms',
    'MSG_DUPLICATE' => 'Создаваемая вами sms возможно дублирует уже имеющийся sms. Sms лог, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового sms или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать sms',
    'LNK_NEW_SMSLOG' => 'Создать sms',
    'LNK_SMSLOG_LIST' => 'Sms лог',
    'ERR_DELETE_RECORD' => 'Перед удалением sms должен быть определён номер записи.',
    'SMSLOG_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный sms из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Sms лог',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_SMSLOGS' => 'Импорт sms',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
    'LBL_PHONE' => 'Телефон'
);


