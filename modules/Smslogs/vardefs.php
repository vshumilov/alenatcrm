<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Smslog'] = array('table' => 'smslogs', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Smslogs',
    'fields' => array(
        'phone' =>
        array(
            'name' => 'phone',
            'vname' => 'LBL_PHONE',
            'type' => 'varchar',
            'len' => '20',
            'isnull' => 'false',
            'required' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_smslog_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_smslog_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_smslog_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Smslogs', 'Smslog', array('default', 'assignable',
));
