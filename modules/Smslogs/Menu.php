<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();

if (ACLController::checkAccess('Smslogs', 'list', true)) {
    $module_menu[] = Array("index.php?module=Smslogs&action=index&return_module=Smslogs&return_action=DetailView", $mod_strings['LNK_SMSLOG_LIST'], "List");
}