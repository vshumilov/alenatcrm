<?php

require_once 'custom/modules/Tours/BeanHelper/Tour.php';
require_once 'custom/modules/Dispatches/BeanHelper/Dispatch.php';
require_once 'custom/include/Nit/Helper/Request.php';

class TourAjaxHelper extends AjaxHelper
{

    public function getTour()
    {
        $beanHelper = new TourBeanHelperTour($this->_bean);

        $tourData = $beanHelper->getData();

        return $tourData;
    }

    public function create()
    {
        $priceToApartmentId = HelperRequest::processReqStr($_REQUEST['params']['price_to_apartment_id']);
        $dateFrom = HelperRequest::processReqStr($_REQUEST['params']['date_from']);
        $dateTo = HelperRequest::processReqStr($_REQUEST['params']['date_to']);
        $countPlaces = HelperRequest::processReqStr($_REQUEST['params']['count_places']);
        $countPlacesWithTreatment = HelperRequest::processReqStr($_REQUEST['params']['count_places_with_treatment']);
        $lastName = HelperRequest::processReqStr($_REQUEST['params']['last_name']);
        $firstName = HelperRequest::processReqStr($_REQUEST['params']['first_name']);
        $middleName = HelperRequest::processReqStr($_REQUEST['params']['middle_name']);
        $birthDate = HelperRequest::processReqStr($_REQUEST['params']['birth_date']);
        $email = HelperRequest::processReqStr($_REQUEST['params']['email']);
        $phoneMobile = HelperRequest::processReqStr($_REQUEST['params']['phone_mobile']);

        $beanHelper = new TourBeanHelperTour($this->_bean);
        $agreement = $beanHelper->createFromSanatorium(
                $priceToApartmentId, $dateFrom, $dateTo, $countPlaces, $countPlacesWithTreatment, $lastName, $firstName, $middleName, $birthDate, $email, $phoneMobile
        );
        
        $row = $beanHelper->getPriceAndApartmentIds($priceToApartmentId);
        
        if (!empty($agreement->system_number)) {
            $dispatchHelper = new DispatchBeanHelperDispatch();
            $dispatchHelper->sendCreateTourEmailAndSms($agreement, $row['price_id'], $row['apartment_id']);
            $dispatchHelper->sendCreateTourManagerEmail($agreement);
            
            return $agreement->system_number;
        }
    }

}
