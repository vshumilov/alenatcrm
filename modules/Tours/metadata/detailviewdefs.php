<?php

$viewdefs ['Tours'] = array(
    'DetailView' =>
    array(
        'templateMeta' =>
        array(
            'form' =>
            array(
                'buttons' =>
                array(
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => true,
            'tabDefs' =>
            array(
                'DEFAULT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array(
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array(
            'default' =>
            array(
                array(
                    'name',
                ),
                array(
                    'name' => 'country_name'
                ),
                array(
                    'name' => 'city_name'
                ),
                array(
                    'name' => 'operator_name'
                ),
                array(
                    'name' => 'route'
                ),
                array(
                    'name' => 'hotel'
                ),
                array(
                    'name' => 'foodoption_name'
                ),
                array(
                    'name' => 'apartment_name'
                ),
                array(
                    'name' => 'days'
                ),
                array(
                    'name' => 'transfer'
                ),
                array(
                    'name' => 'excursion'
                ),
                array(
                    'name' => 'meeting'
                ),
                array(
                    'name' => 'visa_support'
                ),
                array(
                    'name' => 'medical_insurance'
                ),
                array(
                    'name' => 'insurance_on_his_own_recognizance'
                ),
                array(
                    'name' => 'additional_offers'
                ),
            ),
            'LBL_PANEL_ASSIGNMENT' =>
            array(
                array(
                    array(
                        'name' => 'date_modified',
                        'label' => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),
                    array(
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                ),
            ),
        ),
    ),
);
