<?php

$searchdefs['Tours'] = array(
    'templateMeta' =>
    array(
        'maxColumns' => '3',
        'maxColumnsBasic' => '4',
        'widths' =>
        array(
            'label' => '10',
            'field' => '30',
        ),
    ),
    'layout' =>
    array(
        'basic_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'current_user_only' =>
            array(
                'name' => 'current_user_only',
                'label' => 'LBL_CURRENT_USER_FILTER',
                'type' => 'bool',
                'default' => true,
                'width' => '10%',
            ),
        ),
        'advanced_search' =>
        array(
            'name' =>
            array(
                'name' => 'name',
                'default' => true,
                'width' => '10%',
            ),
            'country_name' =>
            array(
                'name' => 'country_name',
                'default' => true,
                'width' => '10%',
            ),
            'city_name' =>
            array(
                'name' => 'city_name',
                'default' => true,
                'width' => '10%',
            ),
            'operator_name' =>
            array(
                'name' => 'operator_name',
                'default' => true,
                'width' => '10%',
            ),
            'route' =>
            array(
                'name' => 'route',
                'default' => true,
                'width' => '10%',
            ),
            'hotel' =>
            array(
                'name' => 'hotel',
                'default' => true,
                'width' => '10%',
            ),
            'foodoption_name' =>
            array(
                'name' => 'foodoption_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'apartment_name' =>
            array(
                'name' => 'apartment_name',
                'default' => true,
                'width' => '10%',
                'displayParams' => array(
                    'useIdSearch' => true,
                )
            ),
            'days' =>
            array(
                'name' => 'days',
                'default' => true,
                'width' => '10%',
            ),
            'transfer' =>
            array(
                'name' => 'transfer',
                'default' => true,
                'width' => '10%',
            ),
            'excursion' =>
            array(
                'name' => 'excursion',
                'default' => true,
                'width' => '10%',
            ),
            'meeting' =>
            array(
                'name' => 'meeting',
                'default' => true,
                'width' => '10%',
            ),
            'visa_support' =>
            array(
                'name' => 'visa_support',
                'default' => true,
                'width' => '10%',
            ),
            'medical_insurance' =>
            array(
                'name' => 'medical_insurance',
                'default' => true,
                'width' => '10%',
            ),
            'insurance_on_his_own_recognizance' =>
            array(
                'name' => 'insurance_on_his_own_recognizance',
                'default' => true,
                'width' => '10%',
            ),
            'additional_offers' =>
            array(
                'name' => 'additional_offers',
                'default' => true,
                'width' => '10%',
            ),
        ),
    ),
);
