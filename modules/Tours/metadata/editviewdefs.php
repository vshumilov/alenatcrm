<?php

$viewdefs['Tours']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
        'includes' =>
        array(
            array(
                'file' => 'modules/Tours/js/editview.js',
            ),
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'name'
                ),
            ),
            array(
                array(
                    'name' => 'country_name'
                ),
            ),
            array(
                array(
                    'name' => 'city_name'
                ),
            ),
            array(
                array(
                    'name' => 'operator_name'
                ),
            ),
            array(
                array(
                    'name' => 'route'
                ),
            ),
            array(
                array(
                    'name' => 'hotel'
                ),
            ),
            array(
                array(
                    'name' => 'foodoption_name'
                ),
            ),
            array(
                array(
                    'name' => 'apartment_name'
                ),
            ),
            array(
                array(
                    'name' => 'days'
                ),
            ),
            array(
                array(
                    'name' => 'transfer'
                ),
            ),
            array(
                array(
                    'name' => 'excursion'
                ),
            ),
            array(
                array(
                    'name' => 'meeting'
                ),
            ),
            array(
                array(
                    'name' => 'visa_support'
                ),
            ),
            array(
                array(
                    'name' => 'medical_insurance'
                ),
            ),
            array(
                array(
                    'name' => 'insurance_on_his_own_recognizance'
                ),
            ),
            array(
                array(
                    'name' => 'additional_offers'
                ),
            ),
        ),
    )
);
