<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$searchFields['Tours'] = array(
    'name' => array(
        'query_type' => 'default',
    ),
    'favorites_only' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'checked_only' => true,
        'subquery' => "SELECT favorites.parent_id FROM favorites
			                    WHERE favorites.deleted = 0
			                        and favorites.parent_type = 'Opportunities'
			                        and favorites.assigned_user_id = '{1}'",
        'db_field' => array('id')),
    'foodoption_name' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'foodoption_id' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.foodoption_id'
        ),
    ),
    'apartment_name' =>
    array(
        'query_type' => 'default',
        'db_field' => array(),
    ),
    'apartment_id' =>
    array(
        'query_type' => 'default',
        'db_field' => array(
            'tours.apartment_id'
        ),
    ),
);
