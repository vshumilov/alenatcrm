<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Tour'] = array('table' => 'tours', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Tours',
    'fields' => array(
        'country_name' =>
        array(
            'name' => 'country_name',
            'rname' => 'name',
            'id_name' => 'country_id',
            'vname' => 'LBL_COUNTRY_NAME',
            'type' => 'relate',
            'table' => 'countries',
            'join_name' => 'countries',
            'isnull' => 'true',
            'module' => 'Countries',
            'dbType' => 'varchar',
            'link' => 'countries',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'country_id' =>
        array(
            'name' => 'country_id',
            'vname' => 'LBL_COUNTRY_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'countries' =>
        array(
            'name' => 'countries',
            'type' => 'link',
            'relationship' => 'countries_tours',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Countries',
            'bean_name' => 'Country',
            'vname' => 'LBL_COUNTRIES',
        ),
        'operator_name' =>
        array(
            'name' => 'operator_name',
            'rname' => 'name',
            'id_name' => 'operator_id',
            'vname' => 'LBL_OPERATOR_NAME',
            'type' => 'relate',
            'table' => 'operators',
            'join_name' => 'operators',
            'isnull' => 'true',
            'module' => 'Operators',
            'dbType' => 'varchar',
            'link' => 'operators',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'importable' => 'required',
        ),
        'operator_id' =>
        array(
            'name' => 'operator_id',
            'vname' => 'LBL_OPERATOR_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'operators' =>
        array(
            'name' => 'operators',
            'type' => 'link',
            'relationship' => 'operators_tours',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Operators',
            'bean_name' => 'Operator',
            'vname' => 'LBL_OPERATORS',
        ),
        'city_name' =>
        array(
            'name' => 'city_name',
            'rname' => 'name',
            'id_name' => 'city_id',
            'vname' => 'LBL_CITY_NAME',
            'type' => 'relate',
            'table' => 'cities',
            'join_name' => 'cities',
            'isnull' => 'true',
            'module' => 'Cities',
            'dbType' => 'varchar',
            'link' => 'cities',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'city_id' =>
        array(
            'name' => 'city_id',
            'vname' => 'LBL_CITY_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'cities' =>
        array(
            'name' => 'cities',
            'type' => 'link',
            'relationship' => 'cities_tours',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Cities',
            'bean_name' => 'City',
            'vname' => 'LBL_CITIES',
        ),
        'route' =>
        array(
            'name' => 'route',
            'vname' => 'LBL_ROUTE',
            'type' => 'varchar',
            'audited' => true,
        ),
        'hotel' =>
        array(
            'name' => 'hotel',
            'vname' => 'LBL_HOTEL',
            'type' => 'varchar',
            'audited' => true,
        ),
        'foodoption_name' =>
        array(
            'name' => 'foodoption_name',
            'rname' => 'name',
            'id_name' => 'foodoption_id',
            'vname' => 'LBL_FOODOPTION_NAME',
            'type' => 'relate',
            'table' => 'foodoptions',
            'join_name' => 'foodoptions',
            'isnull' => 'true',
            'module' => 'Foodoptions',
            'dbType' => 'varchar',
            'link' => 'foodoptions',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'foodoption_id' =>
        array(
            'name' => 'foodoption_id',
            'vname' => 'LBL_FOODOPTION_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'foodoptions' =>
        array(
            'name' => 'foodoptions',
            'type' => 'link',
            'relationship' => 'foodoptions_prices',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Foodoptions',
            'bean_name' => 'Foodoption',
            'vname' => 'LBL_FOODOPTIONS',
        ),
        'apartment_name' => [
            'name' => 'apartment_name',
            'rname' => 'name',
            'id_name' => 'apartment_id',
            'vname' => 'LBL_APARTMENT_NAME',
            'type' => 'relate',
            'table' => 'apartments',
            'join_name' => 'apartments',
            'isnull' => 'true',
            'module' => 'Apartments',
            'dbType' => 'varchar',
            'link' => 'apartments',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
        ],
        'apartment_id' => [
            'name' => 'apartment_id',
            'vname' => 'LBL_APARTMENT_ID',
            'type' => 'id',
            'audited' => true,
        ],
        'apartments' => [
            'name' => 'apartments',
            'type' => 'link',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Apartments',
            'bean_name' => 'Apartment',
            'vname' => 'LBL_APARTMENTS',
            'relationship' => 'apartments_tours',
        ],
        'days' =>
        array(
            'name' => 'days',
            'vname' => 'LBL_DAYS',
            'type' => 'int',
            'dbType' => 'int',
            'audited' => true,
        ),
        'transfer' =>
        array(
            'name' => 'transfer',
            'vname' => 'LBL_TRANSFER',
            'type' => 'bool',
            'audited' => true,
        ),
        'excursion' =>
        array(
            'name' => 'excursion',
            'vname' => 'LBL_EXCURSION',
            'type' => 'bool',
            'audited' => true,
        ),
        'meeting' =>
        array(
            'name' => 'meeting',
            'vname' => 'LBL_MEETING',
            'type' => 'bool',
            'audited' => true,
        ),
        'visa_support' =>
        array(
            'name' => 'visa_support',
            'vname' => 'LBL_VISA_SUPPORT',
            'type' => 'bool',
            'audited' => true,
        ),
        'medical_insurance' =>
        array(
            'name' => 'medical_insurance',
            'vname' => 'LBL_MEDICAL_INSURANCE',
            'type' => 'bool',
            'audited' => true,
        ),
        'insurance_on_his_own_recognizance' =>
        array(
            'name' => 'insurance_on_his_own_recognizance',
            'vname' => 'LBL_INSURANCE_ON_HIS_OWN_RECOGNIZANCE',
            'type' => 'bool',
            'audited' => true,
        ),
        'additional_offers' =>
        array(
            'name' => 'additional_offers',
            'vname' => 'LBL_ADDITIONAL_OFFERS',
            'type' => 'text',
            'audited' => true,
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_tour_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_tour_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_tour_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        'countries_tours' =>
        array(
            'lhs_module' => 'Countries',
            'lhs_table' => 'countries',
            'lhs_key' => 'id',
            'rhs_module' => 'Tours',
            'rhs_table' => 'tours',
            'rhs_key' => 'country_id',
            'relationship_type' => 'one-to-many'
        ),
        'operators_tours' =>
        array(
            'lhs_module' => 'Operators',
            'lhs_table' => 'operators',
            'lhs_key' => 'id',
            'rhs_module' => 'Tours',
            'rhs_table' => 'tours',
            'rhs_key' => 'operator_id',
            'relationship_type' => 'one-to-many'
        ),
        'cities_tours' =>
        array(
            'lhs_module' => 'Cities',
            'lhs_table' => 'cities',
            'lhs_key' => 'id',
            'rhs_module' => 'Tours',
            'rhs_table' => 'tours',
            'rhs_key' => 'city_id',
            'relationship_type' => 'one-to-many'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Tours', 'Tour', array('default', 'assignable',
));
