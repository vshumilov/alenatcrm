<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Tour extends Basic
{

    public $table_name = "tours";
    public $module_dir = "Tours";
    public $object_name = "Tour";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

}
