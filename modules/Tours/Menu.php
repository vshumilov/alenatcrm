<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Tours', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Tours&action=EditView&return_module=Tours&return_action=DetailView", $mod_strings['LNK_NEW_TOUR'], "Create");
}
if (ACLController::checkAccess('Tours', 'list', true)) {
    $module_menu[] = Array("index.php?module=Tours&action=index&return_module=Tours&return_action=DetailView", $mod_strings['LNK_TOUR_LIST'], "List");
}
if (ACLController::checkAccess('Tours', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Tours&return_module=Tours&return_action=index", $mod_strings['LNK_IMPORT_TOURS'], "Import");
}