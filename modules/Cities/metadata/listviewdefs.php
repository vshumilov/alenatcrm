<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Cities'] = array(
    'NAME' => array(
        'width' => '10',
        'label' => 'LBL_NAME',
        'default' => true,
        'link' => true,
    ),
    'COUNTRY_NAME' => array(
        'width' => '10',
        'label' => 'LBL_COUNTRY_NAME',
        'default' => true,
        'link' => true,
    ),
);
