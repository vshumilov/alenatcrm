<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['City'] = array('table' => 'cities', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'An city is the target of selling activities',
    'fields' => array(
        'name' =>
        array(
            'name' => 'name',
            'vname' => 'LBL_NAME',
            'type' => 'name',
            'dbType' => 'varchar',
            'len' => '50',
            'unified_search' => true,
            'full_text_search' => array('boost' => 3),
            'merge_filter' => 'selected',
            'importable' => 'required',
            'required' => true,
        ),
        'country_name' =>
        array(
            'name' => 'country_name',
            'rname' => 'name',
            'id_name' => 'country_id',
            'vname' => 'LBL_COUNTRY_NAME',
            'type' => 'relate',
            'table' => 'countries',
            'join_name' => 'countries',
            'isnull' => 'true',
            'module' => 'Countries',
            'dbType' => 'varchar',
            'link' => 'countries',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'country_id' =>
        array(
            'name' => 'country_id',
            'vname' => 'LBL_COUNTRY_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'countries' =>
        array(
            'name' => 'countries',
            'type' => 'link',
            'relationship' => 'countries_cities',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Countries',
            'bean_name' => 'Country',
            'vname' => 'LBL_COUNTRIES',
        ),
        'accounts' =>
        array(
            'name' => 'accounts',
            'type' => 'link',
            'relationship' => 'cities_accounts',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Accounts',
            'bean_name' => 'Account',
            'vname' => 'LBL_ACCOUNTS',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_cities_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_cities_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_cities_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        'countries_cities' =>
        array(
            'lhs_module' => 'Countries',
            'lhs_table' => 'countries',
            'lhs_key' => 'id',
            'rhs_module' => 'Cities',
            'rhs_table' => 'cities',
            'rhs_key' => 'country_id',
            'relationship_type' => 'one-to-many'
        ),
        'cities_accounts' => array(
            'lhs_module' => 'Cities',
            'lhs_table' => 'cities',
            'lhs_key' => 'id',
            'rhs_module' => 'Accounts',
            'rhs_table' => 'accounts',
            'rhs_key' => 'city_id',
            'relationship_type' => 'one-to-many'
        ),
        'cities_securitygroups' => array(
            'lhs_module' => 'Cities',
            'lhs_table' => 'cities',
            'lhs_key' => 'id',
            'rhs_module' => 'SecurityGroups',
            'rhs_table' => 'securitygroups',
            'rhs_key' => 'city_id',
            'relationship_type' => 'one-to-many'
        ),
    )
    , 'optimistic_locking' => true,
);
VardefManager::createVardef('Cities', 'City', array('default', 'assignable',
));
