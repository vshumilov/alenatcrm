<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Cities', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Cities&action=EditView&return_module=Cities&return_action=DetailView", $mod_strings['LNK_NEW_CITY'], "Create");
}
if (ACLController::checkAccess('Cities', 'list', true)) {
    $module_menu[] = Array("index.php?module=Cities&action=index&return_module=Cities&return_action=DetailView", $mod_strings['LNK_CITY_LIST'], "List");
}