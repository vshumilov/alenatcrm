<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('include/SugarObjects/templates/basic/Basic.php');

class City extends Basic
{

    public $table_name = "cities";
    public $module_dir = "Cities";
    public $object_name = "City";
    public $importable = true;
    
    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    } 
    
    public function getIdByName($name) {
        if (empty($name)) {
            return;
        }
        
        $sql = "
        SELECT
            id
        FROM
            cities
        WHERE
            name LIKE '<name>' AND
            deleted = '0'
        ";
        
        $query = strtr($sql, ['<name>' => $name]);
        
        return dbGetScalar($query);
    }
}
