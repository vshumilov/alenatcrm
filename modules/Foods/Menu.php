<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Foods', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Foods&action=EditView&return_module=Foods&return_action=DetailView", $mod_strings['LNK_NEW_FOOD'], "Create");
}
if (ACLController::checkAccess('Foods', 'list', true)) {
    $module_menu[] = Array("index.php?module=Foods&action=index&return_module=Foods&return_action=DetailView", $mod_strings['LNK_FOOD_LIST'], "List");
}
if (ACLController::checkAccess('Foods', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Foods&return_module=Foods&return_action=index", $mod_strings['LNK_IMPORT_FOODS'], "Import");
}