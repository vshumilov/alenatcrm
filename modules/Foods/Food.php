<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Food extends Basic
{

    public $table_name = "foods";
    public $module_dir = "Foods";
    public $object_name = "Food";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

}
