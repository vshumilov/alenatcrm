<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Food'] = array('table' => 'foods', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Foods',
    'fields' => array(
        'opportunities' =>
        array(
            'name' => 'opportunities',
            'type' => 'link',
            'relationship' => 'foods_opportunities',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Opportunities',
            'bean_name' => 'Opportunity',
            'vname' => 'LBL_OPPORTUNITIES',
        ),
        'tours' =>
        array(
            'name' => 'tours',
            'type' => 'link',
            'relationship' => 'foods_tours',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Tours',
            'bean_name' => 'Tour',
            'vname' => 'LBL_TOURS',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_food_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_food_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_food_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        'foods_opportunities' =>
        array(
            'lhs_module' => 'Foods',
            'lhs_table' => 'foods',
            'lhs_key' => 'id',
            'rhs_module' => 'Opportunities',
            'rhs_table' => 'opportunities',
            'rhs_key' => 'tours_food_id',
            'relationship_type' => 'one-to-many'
        ),
        'foods_tours' =>
        array(
            'lhs_module' => 'Foods',
            'lhs_table' => 'foods',
            'lhs_key' => 'id',
            'rhs_module' => 'Tours',
            'rhs_table' => 'tours',
            'rhs_key' => 'food_id',
            'relationship_type' => 'one-to-many'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Foods', 'Food', array('default', 'assignable',
));
