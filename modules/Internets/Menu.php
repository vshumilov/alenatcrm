<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Internets', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Internets&action=EditView&return_module=Internets&return_action=DetailView", $mod_strings['LNK_NEW_INTERNET'], "Create");
}
if (ACLController::checkAccess('Internets', 'list', true)) {
    $module_menu[] = Array("index.php?module=Internets&action=index&return_module=Internets&return_action=DetailView", $mod_strings['LNK_INTERNET_LIST'], "List");
}
if (ACLController::checkAccess('Internets', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Internets&return_module=Internets&return_action=index", $mod_strings['LNK_IMPORT_INTERNETS'], "Import");
}