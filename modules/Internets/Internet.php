<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Internet extends Basic
{

    public $table_name = "internets";
    public $module_dir = "Internets";
    public $object_name = "Internet";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            internets
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
