<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Pricelevel extends Basic
{
    public $table_name = "pricelevels";
    public $module_dir = "Pricelevels";
    public $object_name = "Pricelevel";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            pricelevels
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}

