<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Pricelevels', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Pricelevels&action=EditView&return_module=Pricelevels&return_action=DetailView", $mod_strings['LNK_NEW_PRICELEVEL'], "Create");
}
if (ACLController::checkAccess('Pricelevels', 'list', true)) {
    $module_menu[] = Array("index.php?module=Pricelevels&action=index&return_module=Pricelevels&return_action=DetailView", $mod_strings['LNK_PRICELEVEL_LIST'], "List");
}
if (ACLController::checkAccess('Pricelevels', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Pricelevels&return_module=Pricelevels&return_action=index", $mod_strings['LNK_IMPORT_PRICELEVELS'], "Import");
}