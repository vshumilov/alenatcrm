<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Pricelevel'] = array('table' => 'pricelevels', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Pricelevels',
    'fields' => array(
        
    ),
    'indices' => array(
        array(
            'name' => 'idx_pricelevel_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_pricelevel_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_pricelevel_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
        
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Pricelevels', 'Pricelevel', array('default', 'assignable',
));
