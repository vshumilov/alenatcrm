<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Placements', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Placements&action=EditView&return_module=Placements&return_action=DetailView", $mod_strings['LNK_NEW_PLACEMENT'], "Create");
}
if (ACLController::checkAccess('Placements', 'list', true)) {
    $module_menu[] = Array("index.php?module=Placements&action=index&return_module=Placements&return_action=DetailView", $mod_strings['LNK_PLACEMENT_LIST'], "List");
}
if (ACLController::checkAccess('Placements', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Placements&return_module=Placements&return_action=index", $mod_strings['LNK_IMPORT_PLACEMENTS'], "Import");
}