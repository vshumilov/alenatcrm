<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Placement extends Basic
{

    public $table_name = "placements";
    public $module_dir = "Placements";
    public $object_name = "Placement";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

}
