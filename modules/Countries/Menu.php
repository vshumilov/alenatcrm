<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Countries', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Countries&action=EditView&return_module=Countries&return_action=DetailView", $mod_strings['LNK_NEW_COUNTRY'], "Create");
}
if (ACLController::checkAccess('Countries', 'list', true)) {
    $module_menu[] = Array("index.php?module=Countries&action=index&return_module=Countries&return_action=DetailView", $mod_strings['LNK_COUNTRY_LIST'], "List");
}
if (ACLController::checkAccess('Countries', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Countries&return_module=Countries&return_action=index", $mod_strings['LNK_IMPORT_COUNTRIES'], "Import");
}