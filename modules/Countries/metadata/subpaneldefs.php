<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$layout_defs['Countries'] = array(
    'subpanel_setup' => array(
        'accounts' => array(
            'order' => 1,
            'module' => 'Cities',
            'sort_order' => 'asc',
            'sort_by' => 'name',
            'subpanel_name' => 'default',
            'get_subpanel_data' => 'cities',
            'title_key' => 'LBL_CITIES_SUBPANEL_TITLE',
            'top_buttons' => array(
                array(
                    'widget_class' => 'SubPanelTopButtonQuickCreate'
                ),
                array(
                    'widget_class' => 'SubPanelTopSelectButton',
                    'popup_module' => 'Cities',
                    'mode' => 'MultiSelect',
                ),
            ),
        ),
    ),
);
