<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Country extends Basic
{

    public $table_name = "countries";
    public $module_dir = "Countries";
    public $object_name = "Country";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

}
