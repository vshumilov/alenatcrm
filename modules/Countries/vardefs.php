<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Country'] = array('table' => 'countries', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Countries',
    'fields' => array(
        'tours' =>
        array(
            'name' => 'tours',
            'type' => 'link',
            'relationship' => 'countries_tours',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Tours',
            'bean_name' => 'Tour',
            'vname' => 'LBL_TOURS',
        ),
        'cities' =>
        array(
            'name' => 'cities',
            'type' => 'link',
            'relationship' => 'countries_cities',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Cities',
            'bean_name' => 'City',
            'vname' => 'LBL_CITIES',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_country_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_country_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_country_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
    ),
    'relationships' => array(
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Countries', 'Country', array('default', 'assignable',
));
