<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Rents', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Rents&action=EditView&return_module=Rents&return_action=DetailView", $mod_strings['LNK_NEW_RENT'], "Create");
}
if (ACLController::checkAccess('Rents', 'list', true)) {
    $module_menu[] = Array("index.php?module=Rents&action=index&return_module=Rents&return_action=DetailView", $mod_strings['LNK_RENT_LIST'], "List");
}
if (ACLController::checkAccess('Rents', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Rents&return_module=Rents&return_action=index", $mod_strings['LNK_IMPORT_RENTS'], "Import");
}