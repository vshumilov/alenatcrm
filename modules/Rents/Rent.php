<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Rent extends Basic
{

    public $table_name = "rents";
    public $module_dir = "Rents";
    public $object_name = "Rent";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            rents
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
