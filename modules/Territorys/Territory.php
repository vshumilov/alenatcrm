<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Territory extends Basic
{

    public $table_name = "territorys";
    public $module_dir = "Territorys";
    public $object_name = "Territory";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            territorys
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
