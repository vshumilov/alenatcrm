<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Territorys', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Territorys&action=EditView&return_module=Territorys&return_action=DetailView", $mod_strings['LNK_NEW_TERRITORY'], "Create");
}
if (ACLController::checkAccess('Territorys', 'list', true)) {
    $module_menu[] = Array("index.php?module=Territorys&action=index&return_module=Territorys&return_action=DetailView", $mod_strings['LNK_TERRITORY_LIST'], "List");
}
if (ACLController::checkAccess('Territorys', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Territorys&return_module=Territorys&return_action=index", $mod_strings['LNK_IMPORT_TERRITORYS'], "Import");
}