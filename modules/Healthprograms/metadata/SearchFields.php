<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$searchFields['Healthprograms'] = array(
    'name' => array(
        'query_type' => 'default',
        'operator' => 'like',
        'full_text_search' => true,
        'db_field' => array('name'),
    ),
    'favorites_only' => array(
        'query_type' => 'format',
        'operator' => 'subquery',
        'checked_only' => true,
        'subquery' => "SELECT favorites.parent_id FROM favorites
			                    WHERE favorites.deleted = 0
			                        and favorites.parent_type = 'Opportunities'
			                        and favorites.assigned_user_id = '{1}'",
        'db_field' => array('id')),
);
