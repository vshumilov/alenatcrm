<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Лечебные программы',
    'LBL_MODULE_TITLE' => 'Лечебные программы - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск лечебных программ',
    'LBL_VIEW_FORM_TITLE' => 'Обзор лечебных программ',
    'LBL_LIST_FORM_TITLE' => 'Список лечебных программ',
    'LBL_HEALTHPROGRAM_NAME' => 'Название лечебной программы:',
    'LBL_HEALTHPROGRAM' => 'Профиль лечения:',
    'LBL_NAME' => 'Название лечебной программы',
    'UPDATE' => 'Лечебные программы - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся лечебные программы',
    'MSG_DUPLICATE' => 'Создаваемая вами лечебные программы возможно дублирует уже имеющийся лечебной программы. Лечебные программы, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового лечебные программы или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать лечебную программу',
    'LNK_NEW_HEALTHPROGRAM' => 'Создать лечебную программу',
    'LNK_HEALTHPROGRAM_LIST' => 'Лечебные программы',
    'ERR_DELETE_RECORD' => 'Перед удалением лечебные программы должен быть определён номер записи.',
    'HEALTHPROGRAM_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный лечебной программы из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Лечебные программы',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_HEALTHPROGRAMS' => 'Импорт лечебных программ',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
);

$mod_strings['LBL_DESCRIPTION'] = 'Название на английском';


