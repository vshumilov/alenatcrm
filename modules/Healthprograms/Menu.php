<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Healthprograms', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Healthprograms&action=EditView&return_module=Healthprograms&return_action=DetailView", $mod_strings['LNK_NEW_HEALTHPROGRAM'], "Create");
}
if (ACLController::checkAccess('Healthprograms', 'list', true)) {
    $module_menu[] = Array("index.php?module=Healthprograms&action=index&return_module=Healthprograms&return_action=DetailView", $mod_strings['LNK_HEALTHPROGRAM_LIST'], "List");
}
if (ACLController::checkAccess('Healthprograms', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Healthprograms&return_module=Healthprograms&return_action=index", $mod_strings['LNK_IMPORT_HEALTHPROGRAMS'], "Import");
}