<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Healthprogram extends Basic
{
    public $table_name = "healthprograms";
    public $module_dir = "Healthprograms";
    public $object_name = "Healthprogram";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            healthprograms
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}

