<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Entertainment extends Basic
{

    public $table_name = "entertainments";
    public $module_dir = "Entertainments";
    public $object_name = "Entertainment";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            entertainments
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
