<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Entertainments', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Entertainments&action=EditView&return_module=Entertainments&return_action=DetailView", $mod_strings['LNK_NEW_ENTERTAINMENT'], "Create");
}
if (ACLController::checkAccess('Entertainments', 'list', true)) {
    $module_menu[] = Array("index.php?module=Entertainments&action=index&return_module=Entertainments&return_action=DetailView", $mod_strings['LNK_ENTERTAINMENT_LIST'], "List");
}
if (ACLController::checkAccess('Entertainments', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Entertainments&return_module=Entertainments&return_action=index", $mod_strings['LNK_IMPORT_ENTERTAINMENTS'], "Import");
}