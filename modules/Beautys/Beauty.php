<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');


class Beauty extends Basic
{
    public $table_name = "beautys";
    public $module_dir = "Beautys";
    public $object_name = "Beauty";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            beautys
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
