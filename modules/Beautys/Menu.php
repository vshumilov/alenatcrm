<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Beautys', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Beautys&action=EditView&return_module=Beautys&return_action=DetailView", $mod_strings['LNK_NEW_BEAUTY'], "Create");
}
if (ACLController::checkAccess('Beautys', 'list', true)) {
    $module_menu[] = Array("index.php?module=Beautys&action=index&return_module=Beautys&return_action=DetailView", $mod_strings['LNK_BEAUTY_LIST'], "List");
}
if (ACLController::checkAccess('Beautys', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Beautys&return_module=Beautys&return_action=index", $mod_strings['LNK_IMPORT_BEAUTYS'], "Import");
}