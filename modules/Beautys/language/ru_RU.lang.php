<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Красота и здоровье',
    'LBL_MODULE_TITLE' => 'Красота и здоровье - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск красота и здоровье',
    'LBL_VIEW_FORM_TITLE' => 'Обзор красота и здоровье',
    'LBL_LIST_FORM_TITLE' => 'Список красота и здоровье',
    'LBL_BEAUTY_NAME' => 'Название красоты и здоровья:',
    'LBL_BEAUTY' => 'Красота и здоровье:',
    'LBL_NAME' => 'Название красоты и здоровья',
    'UPDATE' => 'Красота и здоровье - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся красоты и здоровья',
    'MSG_DUPLICATE' => 'Создаваемая вами красоты и здоровья возможно дублирует уже имеющийся красоты и здоровья. Красота и здоровье, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового красоты и здоровья или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать красоту и здоровье',
    'LNK_NEW_BEAUTY' => 'Создать красоту и здоровье',
    'LNK_BEAUTY_LIST' => 'Красота и здоровье',
    'ERR_DELETE_RECORD' => 'Перед удалением красоты и здоровья должен быть определён номер записи.',
    'BEAUTY_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный красоты и здоровья из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Красота и здоровье',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_BEAUTYS' => 'Импорт красота и здоровье',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
);

$mod_strings['LBL_DESCRIPTION'] = 'Название на английском';

