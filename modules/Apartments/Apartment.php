<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Apartment extends Basic
{

    public $table_name = "apartments";
    public $module_dir = "Apartments";
    public $object_name = "Apartment";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            apartments
        WHERE
            deleted = '0'
            <andWhere>
        ORDER BY
            name
        ";
        
        $andWhere = "";
        
        if ($_REQUEST['module'] == 'Prices' && !empty($_REQUEST['record'])) {
            $price = BeanFactory::getBean('Prices', $_REQUEST['record']);
            $andWhere = "AND parent_id = " . dbQuote($price->parent_id);
        } else if ($_REQUEST['module'] == 'Home' && !empty($_REQUEST['parent_id'])) {
            $andWhere = "AND parent_id = " . dbQuote($_REQUEST['parent_id']);
        }
        
        $query = strtr($sql, ['<andWhere>' => $andWhere]);
        
        return dbGetAssocArray($query);
    }
}
