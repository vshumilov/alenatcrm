<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$listViewDefs['Apartments'] = array(
    'NAME' => array(
        'width' => '30',
        'label' => 'LBL_NAME',
        'link' => true,
        'default' => true
    ),
    'PARENT_NAME' => array(
        'width' => '30',
        'label' => 'LBL_PARENT_NAME',
        'default' => true
    ),
    'ROOMS_COUNT' => array(
        'width' => '10',
        'label' => 'LBL_ROOMS_COUNT',
        'default' => true
    ),
    'PLACES_COUNT' => array(
        'width' => '10',
        'label' => 'LBL_PLACES_COUNT',
        'default' => true
    ),
    'CREATED_BY_NAME' => array(
        'width' => '10',
        'label' => 'LBL_CREATED'),
    'ASSIGNED_USER_NAME' => array(
        'width' => '5',
        'label' => 'LBL_LIST_ASSIGNED_USER',
        'module' => 'Employees',
        'id' => 'ASSIGNED_USER_ID',
        'default' => true),
    'MODIFIED_BY_NAME' => array(
        'width' => '5',
        'label' => 'LBL_MODIFIED'),
    'DATE_ENTERED' => array(
        'width' => '10',
        'label' => 'LBL_DATE_ENTERED')
);
