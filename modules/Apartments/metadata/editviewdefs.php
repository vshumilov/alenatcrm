<?php

$viewdefs['Apartments']['EditView'] = array(
    'templateMeta' => array('maxColumns' => '2',
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30')
        ),
    ),
    'panels' => array(
        'default' =>
        array(
            array(
                array(
                    'name' => 'name'
                ),
            ),
            array(
                array(
                    'name' => 'parent_name'
                ),
            ),
            array(
                array(
                    'name' => 'pricelevel_name'
                ),
            ),
            array(
                array(
                    'name' => 'rooms_count'
                ),
            ),
            array(
                array(
                    'name' => 'places_count'
                ),
            ),
            array(
                array(
                    'name' => 'roomsinfostructure'
                ),
            ),
            array(
                array(
                    'name' => 'description'
                ),
            ),
        ),
    )
);
