<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

$dictionary['Apartment'] = array('table' => 'apartments', 'audited' => true, 'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true, 'duplicate_merge' => true,
    'comment' => 'Apartments',
    'fields' => array(
        'rooms_count' =>
        array(
            'name' => 'rooms_count',
            'vname' => 'LBL_ROOMS_COUNT',
            'type' => 'int',
            'audited' => true,
            'default' => '1',
            'required' => 'true'
        ),
        'places_count' =>
        array(
            'name' => 'places_count',
            'vname' => 'LBL_PLACES_COUNT',
            'type' => 'int',
            'audited' => true,
            'default' => '1',
            'required' => 'true'
        ),
        'parent_name' =>
        array(
            'required' => false,
            'source' => 'non-db',
            'name' => 'parent_name',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'parent',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 1,
            'reportable' => 0,
            'len' => 25,
            'options' => 'images_parent_type_dom',
            'studio' => 'visible',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'parent_type' => 'record_type_display',
        ),
        'parent_type' =>
        array(
            'required' => true,
            'name' => 'parent_type',
            'vname' => 'LBL_PARENT_TYPE',
            'type' => 'parent_type',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 100,
            'dbType' => 'varchar',
            'studio' => 'hidden',
            'isnull' => 'true',
        ),
        'parent_id' =>
        array(
            'required' => true,
            'name' => 'parent_id',
            'vname' => 'LBL_PARENT_ID',
            'type' => 'id',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 36,
            'isnull' => 'true',
        ),
        'sanatoriums' =>
        array(
            'name' => 'sanatoriums',
            'type' => 'link',
            'relationship' => 'sanatoriums_apartments',
            'module' => 'Sanatoriums',
            'bean_name' => 'Sanatorium',
            'source' => 'non-db',
            'side' => 'right',
        ),
        'prices' =>
        array(
            'name' => 'prices',
            'type' => 'link',
            'relationship' => 'prices_apartments',
            'module' => 'Prices',
            'bean_name' => 'Price',
            'source' => 'non-db',
        ),
        'pricelevel_name' =>
        array(
            'name' => 'pricelevel_name',
            'rname' => 'name',
            'id_name' => 'pricelevel_id',
            'vname' => 'LBL_PRICELEVEL_NAME',
            'type' => 'relate',
            'table' => 'pricelevels',
            'join_name' => 'pricelevels',
            'isnull' => 'true',
            'module' => 'Pricelevels',
            'dbType' => 'varchar',
            'link' => 'pricelevels',
            'len' => '255',
            'source' => 'non-db',
            'unified_search' => true,
            'required' => true,
            'importable' => 'required',
            'required' => true,
        ),
        'pricelevel_id' =>
        array(
            'name' => 'pricelevel_id',
            'vname' => 'LBL_PRICELEVEL_ID',
            'type' => 'id',
            'audited' => true,
        ),
        'pricelevels' =>
        array(
            'name' => 'pricelevels',
            'type' => 'link',
            'relationship' => 'pricelevels_prices',
            'source' => 'non-db',
            'link_type' => 'one',
            'module' => 'Pricelevels',
            'bean_name' => 'Pricelevel',
            'vname' => 'LBL_PRICELEVELS',
        ),
        'roomsinfostructure' =>
        array(
            'name' => 'roomsinfostructure',
            'vname' => 'LBL_ROOMSINFOSTRUCTURE',
            'type' => 'multienum',
            'function' => 'Roomsinfostructure::getList',
            'audited' => true,
            'isMultiSelect' => true,
            'glue' => 'and'
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_apartment_name',
            'type' => 'index',
            'fields' => array('name'),
        ),
        array(
            'name' => 'idx_apartment_assigned',
            'type' => 'index',
            'fields' => array('assigned_user_id'),
        ),
        array(
            'name' => 'idx_apartment_id_deleted',
            'type' => 'index',
            'fields' => array('id', 'deleted'),
        ),
        array(
            'name' => 'idx_apartment_parent_deleted',
            'type' => 'index',
            'fields' => array('parent_id', 'parent_type', 'deleted')
        )
    ),
    'relationships' => array(
        'sanatoriums_apartments' => array(
            'lhs_module' => 'Sanatoriums',
            'lhs_table' => 'sanatoriums',
            'lhs_key' => 'id',
            'rhs_module' => 'Apartments',
            'rhs_table' => 'apartments',
            'rhs_key' => 'parent_id',
            'relationship_type' => 'one-to-many',
            'relationship_role_column' => 'parent_type',
            'relationship_role_column_value' => 'Sanatoriums'
        ),
    )
//This enables optimistic locking for Saves From EditView
    , 'optimistic_locking' => true,
);

VardefManager::createVardef('Apartments', 'Apartment', array('default', 'assignable',
));
