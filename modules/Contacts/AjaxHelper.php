<?php
require_once 'custom/modules/Contacts/BeanHelper/Contact.php';

class ContactAjaxHelper extends AjaxHelper
{
    public function getContact()
    {
        $beanHelper = new ContactBeanHelperContact($this->_bean);
        
        $contactData = $beanHelper->getData();
        
        return $contactData;
    }
    
    public function checkIsExistsContact()
    {
        $firstName = $_REQUEST['params']['first_name'];
        $lastName = $_REQUEST['params']['last_name'];
        $contactId = $_REQUEST['params']['contact_id'];
        $birthDate = $_REQUEST['params']['birth_date'];
        $phoneWork = $_REQUEST['params']['phone_work'];
        $phoneMobile = $_REQUEST['params']['phone_mobile'];
        $email = $_REQUEST['params']['email'];
        
        $contactHelper = new ContactBeanHelperContact($this->_bean);
        $result = $contactHelper->isExistsContact($firstName, $lastName, $contactId, $birthDate, $phoneWork, $phoneMobile, $email);
        
        return $result;
    }
}

