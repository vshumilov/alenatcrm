<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Suitables', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Suitables&action=EditView&return_module=Suitables&return_action=DetailView", $mod_strings['LNK_NEW_SUITABLE'], "Create");
}
if (ACLController::checkAccess('Suitables', 'list', true)) {
    $module_menu[] = Array("index.php?module=Suitables&action=index&return_module=Suitables&return_action=DetailView", $mod_strings['LNK_SUITABLE_LIST'], "List");
}
if (ACLController::checkAccess('Suitables', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Suitables&return_module=Suitables&return_action=index", $mod_strings['LNK_IMPORT_SUITABLES'], "Import");
}