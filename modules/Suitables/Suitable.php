<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Suitable extends Basic 
{
    public $table_name = "suitables";
    public $module_dir = "Suitables";
    public $object_name = "Suitable";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            suitables
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
