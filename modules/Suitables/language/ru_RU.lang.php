<?php

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Для кого подходит',
    'LBL_MODULE_TITLE' => 'Для кого подходит - ГЛАВНАЯ',
    'LBL_SEARCH_FORM_TITLE' => 'Поиск для кого подходит',
    'LBL_VIEW_FORM_TITLE' => 'Обзор для кого подходит',
    'LBL_LIST_FORM_TITLE' => 'Список для кого подходит',
    'LBL_SUITABLE_NAME' => 'Название для кого подходит:',
    'LBL_SUITABLE' => 'Санаторий:',
    'LBL_NAME' => 'Название для кого подходит',
    'UPDATE' => 'Для кого подходит - обновление',
    'LBL_DUPLICATE' => 'Возможно дублирующиеся для кого подходит',
    'MSG_DUPLICATE' => 'Создаваемая вами для кого подходит возможно дублирует уже имеющийся для кого подходит. Для кого подходит, имеющие схожие названия показаны ниже. Нажмите кнопку "Сохранить"  для продолжения создания нового для кого подходит или кнопку "Отмена" для возврата в модуль.',
    'LBL_NEW_FORM_TITLE' => 'Создать для кого подходит',
    'LNK_NEW_SUITABLE' => 'Создать для кого подходит',
    'LNK_SUITABLE_LIST' => 'Для кого подходит',
    'ERR_DELETE_RECORD' => 'Перед удалением для кого подходит должен быть определён номер записи.',
    'SUITABLE_REMOVE_PROJECT_CONFIRM' => 'Вы действительно хотите удалить данный для кого подходит из проекта?',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Для кого подходит',
    'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая): ',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
    'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
    'LBL_CREATED_ID' => 'Создано(ID)',
    'LBL_MODIFIED_ID' => 'Изменено(ID)',
    'LBL_MODIFIED_NAME' => 'Изменено',
    'LBL_CREATED_USER' => 'Создано',
    'LBL_MODIFIED_USER' => 'Изменено',
    'LABEL_PANEL_ASSIGNMENT' => 'Назначение ответственного',
    'LNK_IMPORT_SUITABLES' => 'Импорт для кого подходит',
    'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Ответственный(ая)',
    'LBL_EXPORT_ASSIGNED_USER_ID' => 'Ответственный(ая)',
    'LBL_EXPORT_MODIFIED_USER_ID' => 'Изменено(ID)',
    'LBL_EXPORT_CREATED_BY' => 'Создано (ID)',
    'LBL_EXPORT_NAME' => 'Название',
);

$mod_strings['LBL_DESCRIPTION'] = 'Название на английском';

