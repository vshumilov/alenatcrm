<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class Sport extends Basic
{

    public $table_name = "sports";
    public $module_dir = "Sports";
    public $object_name = "Sport";

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    public static function getList() {
        $sql = "
        SELECT
            id,
            name
        FROM
            sports
        WHERE
            deleted = '0'
        ";
        
        return dbGetAssocArray($sql);
    }
}
