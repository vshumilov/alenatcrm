<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config;
$module_menu = Array();
if (ACLController::checkAccess('Sports', 'edit', true)) {
    $module_menu[] = Array("index.php?module=Sports&action=EditView&return_module=Sports&return_action=DetailView", $mod_strings['LNK_NEW_SPORT'], "Create");
}
if (ACLController::checkAccess('Sports', 'list', true)) {
    $module_menu[] = Array("index.php?module=Sports&action=index&return_module=Sports&return_action=DetailView", $mod_strings['LNK_SPORT_LIST'], "List");
}
if (ACLController::checkAccess('Sports', 'import', true)) {
    $module_menu[] = Array("index.php?module=Import&action=Step1&import_module=Sports&return_module=Sports&return_action=index", $mod_strings['LNK_IMPORT_SPORTS'], "Import");
}